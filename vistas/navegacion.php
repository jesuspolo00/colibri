<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log           = $_SESSION['id'];
$id_perfil_sesion = $_SESSION['rol'];
$nom_log_session  = $_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido'];
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon">
      <img src="<?=PUBLIC_PATH?>img/icono.png" class="img-fluid" width="50">
    </div>
    <div class="sidebar-brand-text mx-3 text-secondary mt-3">
      codetec
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home text-secondary"></i>
      <span class="text-muted">Inicio</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider bg-gray">

    <?php
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>usuarios/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users text-secondary"></i>
          <span class="text-muted">Usuarios</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>areas/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-map-marker-alt text-secondary"></i>
          <span class="text-muted">Area/Oficina</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>inventario/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-barcode text-secondary"></i>
          <span class="text-muted">Inventario</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#reportes" aria-expanded="true" aria-controls="reportes">
          <i class="fas fa-tools text-secondary"></i>
          <span class="text-muted">Reportes</span>
        </a>
        <div id="reportes" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <?php
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 89);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/miReportes">Mis reportes</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 29);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/listado">Reportes de da&ntilde;o</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 30);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/areas">Inspeccion de areas (Individual)</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 31);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/listadoAreas">Listado de inspecciones (Individual)</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 67);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/areas_general/index">Inspeccion de areas (General)</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 68);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/areas_general/listado">Listado de inspecciones (General)</a>
              <?php
            }
            $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 32);
            if ($permiso) {
              ?>
              <a class="collapse-item" href="<?=BASE_URL?>reportes/historial">Historial de zonas</a>
              <?php
            }
            ?>
          </div>
        </div>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-truck-loading text-secondary"></i>
          <span class="text-muted">Proveedor</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>proveedor/index">Listado de proveedores</a>
            <a class="collapse-item" href="<?=BASE_URL?>proveedor/registro">Registrar proveedores</a>
          </div>
        </div>
      </li>
    <?php }

    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);
    if ($permiso && $_SESSION['rol'] > 2 && $_SESSION['rol'] != 10 && $_SESSION['rol'] != 11) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTree" aria-expanded="true" aria-controls="collapseTree">
          <i class="fas fa-clipboard-list text-secondary"></i>
          <span class="text-muted">Solicitudes</span>
        </a>
        <div id="collapseTree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/listadoUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/index">Registrar solicitud</a>
          </div>
        </div>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);
    if ($_SESSION['rol'] == 10 || $_SESSION['rol'] <= 2 || $_SESSION['rol'] == 11 && $permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTree" aria-expanded="true" aria-controls="collapseTree">
          <i class="fas fa-clipboard-list text-secondary"></i>
          <span class="text-muted">Solicitudes</span>
        </a>
        <div id="collapseTree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/listado">Listado de solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/listadoUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>solicitud/index">Registrar solicitud</a>
          </div>
        </div>
      </li>
      <?php
    }

    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 15);
    if ($_SESSION['rol'] == 10 || $_SESSION['rol'] == 1 && $permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
          <i class="fas fa-user-clock text-secondary"></i>
          <span class="text-muted">Permisos laborales</span>
        </a>
        <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>permisos/listado">Listado de solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>permisos/listadoUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>permisos/solicitud">Solicitar Permiso</a>
          </div>
        </div>
      </li>
      <?php
    }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 17);
    if ($_SESSION['rol'] == 10 || $_SESSION['rol'] == 1 && $permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
          <i class="fas fa-wallet text-secondary"></i>
          <span class="text-muted">Aprobacion Pagos</span>
        </a>
        <div id="collapseFive" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>aprobacion/index">Listado de solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>aprobacion/solicitudUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>aprobacion/solicitud">Solicitar Aprobacion</a>
          </div>
        </div>
      </li>
      <?php
    }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 14);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>ingresos/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-money-bill-alt text-secondary"></i>
          <span class="text-muted">Ingresos</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 14);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>egresos/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="far fa-money-bill-alt text-secondary"></i>
          <span class="text-muted">Egresos</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 16);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>carnet/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-id-card-alt text-secondary"></i>
          <span class="text-muted">Carnets</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 13);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          <i class="fas fa-file-invoice-dollar text-secondary"></i>
          <span class="text-muted">WS Egresos Proyectos</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>proyectos/index">Listado de proyectos</a>
            <a class="collapse-item" href="<?=BASE_URL?>recibos/index">Generar nuevo recibo</a>
          </div>
        </div>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 28);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>egresos_ws/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="far fa-money-bill-alt text-secondary"></i>
          <span class="text-muted">WS Egresos</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 15);
    if ($permiso && $_SESSION['rol'] >= 2) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
          <i class="fas fa-user-clock text-secondary"></i>
          <span class="text-muted">Permisos laborales</span>
        </a>
        <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>permisos/listadoUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>permisos/solicitud">Solicitar Permiso</a>
          </div>
        </div>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 17);
    if ($permiso && $_SESSION['rol'] >= 2) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
          <i class="fas fa-wallet text-secondary"></i>
          <span class="text-muted">Aprobacion Pagos</span>
        </a>
        <div id="collapseFive" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Acciones:</h6>
            <a class="collapse-item" href="<?=BASE_URL?>aprobacion/solicitudUsuario">Mis solicitudes</a>
            <a class="collapse-item" href="<?=BASE_URL?>aprobacion/solicitud">Solicitar Aprobacion</a>
          </div>
        </div>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>reportes/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-tools text-secondary"></i>
          <span class="text-muted">Listado de reportes</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>programa/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-layer-group text-secondary"></i>
          <span class="text-muted">Programa</span>
        </a>
      </li>
    <?php }
    $permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 44);
    if ($permiso) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>nomina/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-file-invoice-dollar text-secondary"></i>
          <span class="text-muted">Nomina</span>
        </a>
      </li>
    <?php }
    ?>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-gray">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars text-secondary"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

<!--           <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle text-dark" href="<?=BASE_URL?>lector/index" id="messagesDropdown" role="button" aria-haspopup="true" aria-expanded="false" data-tooltip="tooltip" title="Lector" data-placement="bottom">
              <img src="<?=PUBLIC_PATH?>img/barcode.png" class="img-fluid img" width="35">
            </a>
          </li> -->

          <div class="topbar-divider d-none d-sm-block"></div>

          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
              <img class="img-profile rounded-circle" src="<?=PUBLIC_PATH?>img/user.svg">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="<?=BASE_URL?>perfil/index">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Perfil
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?=BASE_URL?>salir">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Cerrar sesion
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">

        <?php
        include_once VISTA_PATH . 'script_and_final.php';
      ?>