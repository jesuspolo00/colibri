<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlPerfil::singleton_perfil();
$datos     = $instancia->mostrarDatosPerfilControl($id_log);

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-success">Perfil</h4>
				</div>
				<div class="card-body">
					<form action="" method="POST" id="form_enviar">
						<input type="hidden" value="<?=$datos['id_user']?>" name="id_user">
						<input type="hidden" value="<?=$datos['pass']?>" name="pass_old">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="font-weight-bold">Numero de Documento <span class="text-danger">*</span></label>
									<input type="text" class="form-control numeros" required value="<?=$datos['documento']?>" name="documento">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
									<input type="text" class="form-control" value="<?=$datos['nombre']?>" name="nombre" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
									<input type="text" class="form-control" required value="<?=$datos['apellido']?>" name="apellido">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Correo</label>
									<input type="email" class="form-control" value="<?=$datos['correo']?>" name="correo">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Telefono</label>
									<input type="text" class="form-control numeros" value="<?=$datos['telefono']?>" name="telefono">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Usuario</label>
									<input type="text" class="form-control" value="<?=$datos['user']?>" disabled>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-4">
								<h4 class="font-weight-bold text-success text-center">Cambiar Contraseña</h4>
								<hr>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Nueva Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Confirmar Nueva Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password">
								</div>
							</div>
						</div>
						<div class="form-group mt-4 float-right">
							<button type="submit" class="btn btn-success btn-sm" id="enviar_perfil">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar Cambios
							</button>
							<input type="hidden" name="perfil" value="<?=$datos['perfil']?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['documento'])) {
	$instancia->editarPerfilControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/validaciones.js"></script>