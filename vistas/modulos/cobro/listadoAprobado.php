<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

if (isset($_POST['buscar'])) {
	$buscar    = $_POST['buscar'];
	$anio      = $_POST['anio'];
	$mes       = $_POST['mes'];
	$ver_boton = '';

	$datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $buscar);

	$datos_cobro = $instancia->buscarCuentaCobroControl($datos);
} else {
	$ver_boton = 'd-none';

	$datos_cobro = $instancia->mostrarLimiteLibreCuentasCobroControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 43);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>cobro/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de Cuentas de cobro - Exportar
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="anio" class="form-control">
									<?php
									for ($i = date('Y') - 1; $i <= 2050; $i++) {
										$selected = (date('Y-m-d') == $i) ? 'selected' : '';
										?>
										<option value="<?=$i?>" <?=$selected?>><?=$i?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<select name="mes" class="form-control">
									<option value="" selected>Seleccione una opcion...</option>
									<option value="1">Enero</option>
									<option value="2">Febrero</option>
									<option value="3">Marzo</option>
									<option value="4">Abril</option>
									<option value="5">Mayo</option>
									<option value="6">Junio</option>
									<option value="7">Julio</option>
									<option value="8">Agosto</option>
									<option value="9">Septiembre</option>
									<option value="10">Octubre</option>
									<option value="11">Noviembre</option>
									<option value="12">Diciembre</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" value="<?=$buscar?>" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="row <?=$ver_boton?>">
						<div class="col-lg-12 form-group mt-2 text-right">
							<a href="<?=BASE_URL?>imprimir/cobro/reporteCuentas?anio=<?=$anio?>&mes=<?=$mes?>&buscar=<?=$buscar?>" class="btn btn-primary btn-sm" target="_blank">
								<i class="fa fa-file-pdf"></i>
								&nbsp;
								Descargar PDF
							</a>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Cuenta</th>
									<th scope="col">Documento / NIT</th>
									<th scope="col">Apellidos y Nombre</th>
									<th scope="col">Valor Hora</th>
									<th scope="col">Horas (MES)</th>
									<th scope="col">Subtotal a pagar</th>
									<th scope="col">Valor a pagar</th>
									<th scope="col">Fecha Creacion</th>
									<th scope="col">Tipo</th>
									<th scope="col">Coordinador</th>
									<th scope="col">Horas Aprobadas (Coordinador)</th>
									<th scope="col">Contabilidad</th>
									<th scope="col">Horas Aprobadas (Contabilidad)</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_cobro as $cobro) {
									$id_cobro              = $cobro['id'];
									$usuario               = ($cobro['razon'] == '') ? $cobro['usuario'] : $cobro['razon'];
									$documento             = $cobro['documento'];
									$lugar                 = $cobro['lugar'];
									$direccion             = $cobro['direccion'];
									$telefono              = $cobro['telefono'];
									$anio                  = (!empty($cobro['anio_cuenta_asistencia'])) ? $cobro['anio_cuenta_asistencia'] : 'N/A';
									$mes                   = $cobro['mes_cuenta_asistencia'];
									$valor                 = number_format($cobro['valor']);
									$cuenta_detalle        = $cobro['cuenta_detalle'];
									$cuenta_documentos     = $cobro['cuenta_documentos'];
									$estado                = $cobro['estado'];
									$fechareg              = $cobro['fechareg'];
									$tipo                  = ($cobro['tipo'] == 1) ? '<span class="badge badge-danger">Distrito</span>' : '<span class="badge badge-warning">Codetec</span>';
									$horas_asistencia      = $cobro['horas_aprobadas_asistencia'];
									$horas_aprobadas_coord = (empty($cobro['horas_coord'])) ? 'N/A' : $cobro['horas_coord'];
									$horas_aprobadas_cont  = (empty($cobro['horas_cont'])) ? 'N/A' : $cobro['horas_cont'];
									$hora_valor            = (empty($cobro['hora_valor'])) ? $cobro['valor'] : $cobro['hora_valor'];
									$horas_pagar           = (empty($cobro['horas_pagar'])) ? 'N/A' : $cobro['horas_pagar'];

									$mes_asistencia = (!empty($mes)) ? mesesEspanol($mes) : 'N/A';
									/*--------------------------------------*/
									$ver_info = ($cuenta_detalle == 1 && $cuenta_documentos == 1 && $estado == 2 && $cobro['estado_contabilidad'] == 1) ? '' : 'd-none';

									$ver_archivo = ($cuenta_detalle == 1 && $cuenta_documentos == 1 && $estado == 2 && $cobro['estado_contabilidad'] == 1) ? '' : 'd-none';

									$ver_documentos = ($cuenta_detalle == 1 && $cuenta_documentos == 0) ? '' : 'd-none';

									$ver_detalle = ($cuenta_detalle == 0) ? '' : 'd-none';
									/*--------------------------------------*/
									$span_estado = ($estado == 0) ? '<span class="badge badge-secondary">Aún faltan completar pasos</span>' : '';
									$span_estado = ($estado == 1) ? '<span class="badge badge-warning">Pendiente de aprobacion</span>' : $span_estado;
									$span_estado = ($estado == 2) ? '<span class="badge badge-success">Aprobado</span>' : $span_estado;
									$span_estado = ($estado == 3) ? '<span class="badge badge-danger">Denegado</span>' : $span_estado;
									/*--------------------------------------*/
									$span_estado_cont = ($cobro['estado_contabilidad'] == 0) ? '<span class="badge badge-warning">Pendiente de aprobacion</span>' : '';
									$span_estado_cont = ($cobro['estado_contabilidad'] == 1) ? '<span class="badge badge-success">Aprobado</span>' : $span_estado_cont;
									$span_estado_cont = ($cobro['estado_contabilidad'] == 2) ? '<span class="badge badge-danger">Denegado</span>' : $span_estado_cont;
									/*--------------------------------------*/

									if ($cobro['estado_contabilidad'] == 1) {
										?>
										<tr class="text-center">
											<td><?=$id_cobro?></td>
											<td><?=$documento?></td>
											<td><?=$usuario?></td>
											<td>$<?=number_format($hora_valor)?></td>
											<td><?=$horas_pagar?></td>
											<td>$<?=number_format($hora_valor * $horas_pagar)?></td>
											<td>$<?=number_format($hora_valor * $horas_pagar)?></td>
											<td><?=date('Y-m-d', strtotime($fechareg))?></td>
											<td><?=$tipo?></td>
											<td><?=$span_estado?></td>
											<td><?=$horas_aprobadas_coord?></td>
											<td><?=$span_estado_cont?></td>
											<td><?=$horas_aprobadas_cont?></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>