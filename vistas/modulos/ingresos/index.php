<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'ingresos' . DS . 'ControlIngresos.php';

$instancia = ControlIngresos::singleton_ingresos();

$datos_metodo   = $instancia->mostrarMetodosPagoControl();
$datos_concepto = $instancia->mostrarConceptoPagoControl();

if (isset($_POST['buscar'])) {
	$datos          = array('concepto' => $_POST['concepto'], 'metodo' => $_POST['metodo'], 'buscar' => $_POST['buscar']);
	$datos_ingresos = $instancia->buscarIngresosControl($datos);
} else {
	$datos_ingresos = $instancia->ultimosIngresosControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 14);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/contabilidad" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Ingresos
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/reporteIngresos" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a>
						<button class="btn btn-dark btn-sm" type="button" data-toggle="modal" data-target="#agregar_concepto">
							<i class="fas fa-plus-circle"></i>
							&nbsp;
							Agregar Concepto
						</button>
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_ingreso">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Ingreso
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="concepto" id="" class="form-control">
									<option value="" selected>Seleccione un concepto...</option>
									<?php
									foreach ($datos_concepto as $concepto) {
										$id_concepto  = $concepto['id'];
										$nom_concepto = $concepto['nombre'];
										?>
										<option value="<?=$id_concepto?>"><?=$nom_concepto?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<select name="metodo" id="" class="form-control">
									<option value="" selected>Seleccione un metodo...</option>
									<?php
									foreach ($datos_metodo as $metodo) {
										$id_metodo  = $metodo['id'];
										$nom_metodo = $metodo['nombre'];
										?>
										<option value="<?=$id_metodo?>"><?=$nom_metodo?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Ingreso</th>
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Concepto</th>
									<th scope="col">Fecha de ingreso</th>
									<th scope="col">Metodo de pago</th>
									<th scope="col">Realizado por</th>
									<th scope="col">Valor</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_ingresos as $ingreso) {
									$id_ingreso   = $ingreso['id'];
									$nom_user     = $ingreso['nom_user'];
									$nom_concepto = $ingreso['nom_concepto'];
									$nom_pago     = $ingreso['nom_pago'];
									$fecha        = $ingreso['fecha'];
									$valor        = number_format($ingreso['valor']);
									$doc_user     = $ingreso['doc_user'];
									$observacion  = $ingreso['observacion'];
									$activo       = $ingreso['activo'];

									if ($activo == 0) {
										$ver_recibo   = 'd-none';
										$ver_activo   = '';
										$ver_inactivo = 'd-none';
										$span_estado = '<span class="badge badge-danger">Anulado</span>';
									} else {
										$ver_recibo   = '';
										$ver_activo   = 'd-none';
										$ver_inactivo = '';
										$span_estado = '';
									}

									if ($ingreso['tipo_registro'] == 1) {
										?>
										<tr class="text-center">
											<td><?=$id_ingreso?></td>
											<td><?=$doc_user?></td>
											<td>
												<a href="<?=BASE_URL?>ingresos/historial?usuario=<?=base64_encode($ingreso['id_usuario'])?>"><?=$nom_user?></a>
											</td>
											<td><?=$nom_concepto?></td>
											<td><?=$fecha?></td>
											<td><?=$nom_pago?></td>
											<td><?=$ingreso['usuario_realiza']?></td>
											<td>$<?=$valor?></td>
											<td><?=$observacion?></td>
											<td class="span_<?=$id_ingreso?>"><?=$span_estado?></td>
											<td class="<?=$ver_recibo?> recibo_<?=$id_ingreso?>">
												<div class="btn-group">
													<a href="<?=BASE_URL?>ingresos/historial?usuario=<?=base64_encode($ingreso['id_usuario'])?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Historial de usuario" data-placement="bottom">
														<i class="fa fa-eye"></i>
													</a>
													<a href="<?=BASE_URL?>imprimir/ingresos?ingreso=<?=base64_encode($id_ingreso)?>" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Recibo" data-placement="bottom" target="_blank">
														<i class="fas fa-money-check"></i>
													</a>
												</div>
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-danger btn-sm anular <?=$ver_inactivo?>" type="button" data-tooltip="tooltip" title="Anular" data-placement="bottom" id="anular_<?=$id_ingreso?>" data-id="<?=$id_ingreso?>" data-log="<?=$id_log?>">
														<i class="fa fa-times"></i>
													</button>
													<button class="btn btn-success btn-sm autorizar <?=$ver_activo?>" type="submit" data-tooltip="tooltip" title="Autorizar" data-placement="bottom" id="autorizar_<?=$id_ingreso?>" data-id="<?=$id_ingreso?>" data-log="<?=$id_log?>">
														<i class="fa fa-check"></i>
													</button>
												</div>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'ingresos' . DS . 'agregarIngreso.php';
include_once VISTA_PATH . 'modulos' . DS . 'ingresos' . DS . 'agregarConcepto.php';

if (isset($_POST['documento'])) {
	$instancia->registrarIngresoControl();
}

if (isset($_POST['nom_concepto'])) {
	$instancia->agregarConceptoControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/ingresos/funcionesIngreso.js"></script>