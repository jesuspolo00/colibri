<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlHoja::singleton_hoja();
$instancia_perfil  = ControlPerfil::singleton_perfil();
$instancia_usuario = ControlUsuario::singleton_usuario();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 61);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_usuario = base64_decode($_GET['id']);
	$id_url     = base64_decode($_GET['enlace']);

	$url = ($id_url == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
	$url = ($id_url == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

	$datos            = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
	$datos_hoja       = $instancia->informacionHojaVidaControl($id_usuario);
	$datos_documentos = ($id_url == 1) ? $instancia->mostrarDocumentosHojaControl($id_usuario, $id_perfil_sesion) : $instancia->mostrarDocumentosHojaCidaProyectosControl($id_usuario);
	$validar_aut_user = $instancia_usuario->validarAutorizacionUsuarioControl($id_usuario, $id_log, date('Y'));

	$foto_perfil = (!empty($datos_hoja['foto'])) ? $datos_hoja['foto'] : $datos_hoja['foto_hoja_vida'];
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=$url?>" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hoja de vida - Autorizar Documentos (<?=$datos['nombre'] . ' ' . $datos['apellido']?>)
					</h4>
				</div>
				<div class="card-body">
					<div class="row p-2">
						<div class="col-lg-12 form-group">
							<div class="row">
								<div class="col-lg-4">
									<div class="col-lg-12 form-group">
										<div class="p-4 text-center">
											<img src="<?=PUBLIC_PATH?>upload/<?=$foto_perfil?>" class="img-thumbnail img-fluid rounded" alt="" width="240">
										</div>
									</div>
								</div>
								<div class="col-lg-8 form-group">
									<div class="row">
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Identificacion</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos['documento']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Nombres</label>
											<input type="text" class="form-control" disabled value="<?=$datos['nombre']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Apellidos</label>
											<input type="text" class="form-control" disabled value="<?=$datos['apellido']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Telefono</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos['telefono']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Celular</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos_hoja['celular']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Correo electronico</label>
											<input type="email" class="form-control" disabled value="<?=$datos['correo']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Direcci&oacute;n</label>
											<input type="text" class="form-control" disabled value="<?=$datos_hoja['direccion']?>">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 form-group mt-2">
							<div class="table-responsive">
								<table class="table table-hover border">
									<thead class="text-center text-dark">
										<tr>
											<th colspan="4" class="text-success font-weight-bold h5">Documentos</th>
										</tr>
										<tr>
											<th>Documento</th>
											<th>Estado del documento</th>
										</tr>
									</thead>
									<tbody class="text-center">
										<?php
										$cant_documentos = 0;
										$cant_aprobados  = 0;
										$cant_denegados  = 0;
										foreach ($datos_documentos as $documento) {
											$id_documento  = $documento['id'];
											$nom_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];

											$ruta = PUBLIC_PATH . 'upload' . DS . $documento['documento'];

											$validar = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);

											$ver_aprobar = ($documento['autorizacion_uno'] == 1 || $documento['autorizacion_uno'] == 3) ? 'd-none' : '';
											$ver_denegar = ($documento['autorizacion_uno'] == 2) ? 'd-none' : '';

											$cant_documentos++;
											$cant_aprobados = ($documento['autorizacion_uno'] == 1 || $documento['autorizacion_uno'] == 3) ? $cant_aprobados + 1 : $cant_aprobados;
											$cant_denegados = ($documento['autorizacion_uno'] == 2) ? $cant_denegados + 1 : $cant_denegados;

											$ver_archivo = ($documento['autorizacion_uno'] == 3) ? 'd-none' : '';
											?>
											<tr>
												<td><?=$nom_documento?></td>
												<td><?=$validar?></td>
												<td>
													<a href="<?=$ruta?>" class="btn btn-info btn-sm <?=$ver_archivo?>" target="_blank">
														<i class="fa fa-eye"></i>
														&nbsp;
														Ver archivo
													</a>
												</td>
												<td>
													<?php
													if (empty($validar_aut_user)) {
														?>
														<div class="btn-group">
															<button class="btn btn-success btn-sm <?=$ver_aprobar?>" type="button" data-toggle="modal" data-target="#aprobar_documento_<?=$id_documento?>">
																<i class="fa fa-check"></i>
																&nbsp;
																Aprobar
															</button>
															<button class="btn btn-secondary btn-sm <?=$ver_aprobar?>" type="button" data-toggle="modal" data-target="#no_aplica_<?=$id_documento?>">
																<i class="fas fa-ban"></i>
																&nbsp;
																No Aplica
															</button>
															<button class="btn btn-danger btn-sm <?=$ver_denegar?>" type="button" data-toggle="modal" data-target="#denegar_documento_<?=$id_documento?>">
																<i class="fa fa-times"></i>
																&nbsp;
																Denegar
															</button>
														</div>
													<?php }?>
												</td>
											</tr>

											<div class="modal fade" id="aprobar_documento_<?=$id_documento?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Aprobar documento - (<?=$nom_documento?>)</h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_user" value="<?=$documento['id_user']?>">
																<input type="hidden" name="id_documento" value="<?=$id_documento?>">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_perfil" value="<?=$id_perfil_sesion?>">
																<input type="hidden" name="estado" value="1">
																<input type="hidden" name="nom_documento" value="<?=$nom_documento?>">
																<input type="hidden" name="enlace" value="<?=$id_url?>">
																<div class="row p-2">
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Observaci&oacute;n</label>
																		<textarea name="observacion" class="form-control" rows="5"></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-2 text-right">
																		<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-success btn-sm" type="submit">
																			<i class="fas fa-paper-plane"></i>
																			&nbsp;
																			Aprobar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>

											<div class="modal fade" id="no_aplica_<?=$id_documento?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">No aplica documento - (<?=$nom_documento?>)</h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_user" value="<?=$documento['id_user']?>">
																<input type="hidden" name="id_documento" value="<?=$id_documento?>">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_perfil" value="<?=$id_perfil_sesion?>">
																<input type="hidden" name="estado" value="3">
																<input type="hidden" name="nom_documento" value="<?=$nom_documento?>">
																<input type="hidden" name="enlace" value="<?=$id_url?>">
																<div class="row p-2">
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Observaci&oacute;n</label>
																		<textarea name="observacion" class="form-control" rows="5"></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-2 text-right">
																		<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-success btn-sm" type="submit">
																			<i class="fas fa-paper-plane"></i>
																			&nbsp;
																			Aprobar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>

											<div class="modal fade" id="denegar_documento_<?=$id_documento?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Denegar documento - (<?=$nom_documento?>)</h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_user" value="<?=$documento['id_user']?>">
																<input type="hidden" name="id_documento" value="<?=$id_documento?>">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_perfil" value="<?=$id_perfil_sesion?>">
																<input type="hidden" name="estado" value="2">
																<input type="hidden" name="nom_documento" value="<?=$nom_documento?>">
																<input type="hidden" name="enlace" value="<?=$id_url?>">
																<div class="row p-2">
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Observaci&oacute;n</label>
																		<textarea name="observacion" class="form-control" rows="5"></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-2 text-right">
																		<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-success btn-sm" type="submit">
																			<i class="fas fa-paper-plane"></i>
																			&nbsp;
																			Denegar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>

											<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<?php
						if ($cant_aprobados == $cant_documentos || $cant_denegados == $cant_documentos) {
							if (empty($validar_aut_user)) {
								?>
								<div class="col-lg-12 form-group mt-2 text-right">
									<form method="POST">
										<input type="hidden" name="id_log" value="<?=$id_log?>">
										<input type="hidden" name="cantidad_doc" value="<?=$cant_documentos?>">
										<input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
										<input type="hidden" name="id_user" value="<?=$id_usuario?>">
										<input type="hidden" name="enlace" value="<?=$id_url?>">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-save"></i>
											&nbsp;
											Finalizar revision
										</button>
									</form>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['estado'])) {
	$instancia->revisionDocumentosHojaControl();
}

if (isset($_POST['id_hoja'])) {
	$instancia->finalizarRevisionControl();
}