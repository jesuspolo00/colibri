<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlHoja::singleton_hoja();
$instancia_perfil  = ControlPerfil::singleton_perfil();
$instancia_usuario = ControlUsuario::singleton_usuario();

$tipo_sangre  = $instancia->tipoSangreControl();
$datos_genero = $instancia->sexoGeneroControl();
$estado_civil = $instancia->estadoCivilControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 63);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_usuario = base64_decode($_GET['id']);
	$id_url     = base64_decode($_GET['enlace']);

	$url = ($id_url == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
	$url = ($id_url == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

	$datos                    = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
	$datos_hoja               = $instancia->informacionHojaVidaControl($id_usuario);
	$datos_pregunta           = $instancia->preguntasHojaControl($datos_hoja['id']);
	$datos_academicos         = $instancia->informacionAcademicaControl($datos_hoja['id']);
	$datos_laborales          = $instancia->informacionLaboralControl($datos_hoja['id']);
	$datos_documentos         = $instancia->mostrarDocumentosTodosHojaControl($id_usuario);
	$datos_evaluacion_admin   = $instancia_usuario->mostrarDatosEvaluacionControl($id_usuario, 1);
	$datos_evaluacion_docente = $instancia_usuario->mostrarDatosEvaluacionControl($id_usuario, 2);

	$autorizacion_anual = $instancia_usuario->autorizacionAnioDocumentosControl($id_usuario, date('Y'));

	$evaluacion_anual_administrativa = $instancia_usuario->validarEvaluacionAnualUsuarioControl($id_usuario, date('Y'), 1);
	$evaluacion_anual_docente        = $instancia_usuario->validarEvaluacionAnualUsuarioControl($id_usuario, date('Y'), 2);

	$foto_perfil = (!empty($datos_hoja['foto'])) ? $datos_hoja['foto'] : $datos_hoja['foto_hoja_vida'];

	$salario      = (empty($datos_hoja['salario'])) ? 0 : $datos_hoja['salario'];
	$fecha_ultima = (empty($datos_hoja['fecha_edit'])) ? $datos_hoja['fechareg'] : $datos_hoja['fecha_edit'];

	$bg_documentos = 'btn-danger';
	$bg_autorizar  = 'btn-danger';
	$bg_evaluacion = 'btn-danger';
	$progress_bar  = 'style="width: 33%"';

	if ($datos_hoja['verificacion'] == 2) {
		$nom_estado_ver = 'Denegado';
		$bg_input       = 'bg-danger';
	}

	if ($datos_hoja['verificacion'] == 1) {
		$nom_estado_ver = 'Aprobado';
		$bg_input       = 'bg-success';
	}

	if ($datos_hoja['verificacion'] == 0) {
		$nom_estado_ver = 'Pendiente';
		$bg_input       = 'bg-warning';
	}

	if (!empty($datos_documentos)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-danger';
		$bg_evaluacion = 'btn-danger';
		$progress_bar  = 'style="width: 59%"';
	}
	if (!empty($autorizacion_anual)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-success';
		$bg_evaluacion = 'btn-danger';
		$progress_bar  = 'style="width: 85%"';
	}

	if (!empty($evaluacion_anual_administrativa)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-success';
		$bg_evaluacion = 'btn-success';
		$progress_bar  = 'style="width: 100%"';
	}

	if (!empty($evaluacion_anual_docente)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-success';
		$bg_evaluacion = 'btn-success';
		$progress_bar  = 'style="width: 100%"';
	}

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=$url?>" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Hoja de vida (<?=$datos['nombre'] . ' ' . $datos['apellido']?>)
						</h4>
						<h6 class="text-success float-right font-weight-bold">Fecha ultima actualizacion: <?=$fecha_ultima?></h6>
					</div>
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
							<input type="hidden" name="foto_ant" value="<?=$datos_hoja['foto']?>">
							<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
							<input type="hidden" name="enlace" value="<?=$id_url?>">
							<div class="row p-2">
								<div class="col-lg-12 form-group">
									<div class="row p-3">
										<div class="col-lg-2 text-center">
											<button class="btn btn-success btn-sm rounded-circle p-4 text-center" disabled type="button">
												<i class="fas fa-user-edit ml-1 fa-2x"></i>
											</button>
											<br>
											<label class="font-weight-bold">Ingreso</label>
										</div>
										<div class="col-lg-1">
											<hr class="mt-5 font-weight-bold text-dark">
										</div>
										<div class="col-lg-2 text-center">
											<button class="btn <?=$bg_documentos?> btn-sm rounded-circle p-4 text-center" disabled type="button">
												<i class="fas fa-folder-open fa-2x"></i>
											</button>
											<br>
											<label class="font-weight-bold">Documentos</label>
										</div>
										<div class="col-lg-1">
											<hr class="mt-5 font-weight-bold text-dark">
										</div>
										<div class="col-lg-2 text-center">
											<button class="btn <?=$bg_autorizar?> btn-sm rounded-circle p-4 text-center" disabled type="button">
												<i class="fas fa-check-double fa-2x"></i>
											</button>
											<br>
											<label class="font-weight-bold">Verificaci&oacute;n de documentos (Anual)</label>
										</div>
										<div class="col-lg-1">
											<hr class="mt-5 font-weight-bold text-dark">
										</div>
										<div class="col-lg-2 text-center">
											<button class="btn <?=$bg_evaluacion?> btn-sm rounded-circle p-4 text-center" disabled type="button">
												<i class="fas fa-user-edit ml-1 fa-2x"></i>
											</button>
											<br>
											<label class="font-weight-bold">Evaluaci&oacute;n (Anual)</label>
										</div>
										<div class="col-lg-12 mt-2 mb-2">
											<div class="progress">
												<div class="progress-bar bg-success" role="progressbar" <?=$progress_bar?> aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 form-group">
									<h5 class="text-success text-center text-uppercase font-weight-bold">Informaci&oacute;n Personal</h5>
									<hr>
								</div>
								<div class="col-lg-8 form-group">
									<div class="row">
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Nombres <span class="text-danger">*</span></label>
											<input type="text" class="form-control" name="nombre" required value="<?=$datos['nombre']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Apellidos <span class="text-danger">*</span></label>
											<input type="text" class="form-control" name="apellido" required value="<?=$datos['apellido']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Identificacion <span class="text-danger">*</span></label>
											<input type="text" class="form-control numeros" name="documento" required value="<?=$datos['documento']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Sexo <span class="text-danger">*</span></label>
											<select name="sexo" class="form-control" required>
												<option value="">Seleccione una opcion...</option>
												<?php
												foreach ($datos_genero as $genero) {
													$id_genero  = $genero['id'];
													$nom_genero = $genero['nombre'];

													$select_sexo = ($datos_hoja['sexo'] == $id_genero) ? 'selected' : '';

													?>
													<option value="<?=$id_genero?>"<?=$select_sexo?>><?=$nom_genero?></option>
												<?php }?>
											</select>
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Telefono</label>
											<input type="text" class="form-control numeros" name="telefono" value="<?=$datos['telefono']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Celular</label>
											<input type="text" class="form-control numeros" name="celular" value="<?=$datos_hoja['celular']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Correo electronico</label>
											<input type="email" class="form-control" name="correo" value="<?=$datos['correo']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Direcci&oacute;n <span class="text-danger">*</span></label>
											<input type="text" class="form-control" name="direccion" value="<?=$datos_hoja['direccion']?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<div class="p-4 text-center">
										<img src="<?=PUBLIC_PATH?>upload/<?=$foto_perfil?>" class="img-thumbnail img-fluid rounded" alt="" width="230">
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Lugar de residencia</label>
									<input type="text" class="form-control" name="lugar" value="<?=$datos_hoja['lugar']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Barrio</label>
									<input type="text" class="form-control" name="barrio" value="<?=$datos_hoja['barrio']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fecha de nacimiento <span class="text-danger">*</span></label>
									<input type="date" class="form-control" name="fecha_nac" value="<?=$datos_hoja['fecha_nac']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Lugar de nacimiento <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="lugar_nac" value="<?=$datos_hoja['lugar_nac']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Tipo de sangre <span class="text-danger">*</span></label>
									<select name="tipo_sangre" required class="form-control">
										<option value="">Seleccione una opcion...</option>
										<?php
										foreach ($tipo_sangre as $sangre) {
											$id_sangre  = $sangre['id'];
											$nom_sangre = $sangre['nombre'];

											$select_sangre = ($id_sangre == $datos_hoja['tipo_sangre']) ? 'selected' : '';
											?>
											<option value="<?=$id_sangre?>" <?=$select_sangre?>><?=$nom_sangre?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Foto 3X4</label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="foto" accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-12 form-group mt-4">
									<h5 class="font-weight-bold text-success text-center text-uppercase">Informaci&oacute;n Adicional</h5>
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Expedicion de documento <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="exp_documento" value="<?=$datos_hoja['exp_documento']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Estado civil <span class="text-danger">*</span></label>
									<select name="estado_civil" class="form-control" required>
										<option value="">Seleccione una opcion...</option>
										<?php
										foreach ($estado_civil as $estado) {
											$id_estado  = $estado['id'];
											$nom_estado = $estado['nombre'];

											$select_estado = ($id_estado == $datos_hoja['estado_civil']) ? 'selected' : '';
											?>
											<option value="<?=$id_estado?>" <?=$select_estado?>><?=$nom_estado?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Escalaf&oacute;n</label>
									<input type="text" class="form-control" name="escalafon" value="<?=$datos_hoja['escalafon']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nivel Academico</label>
									<input type="text" class="form-control" name="nivel_academico" value="<?=$datos_hoja['nivel_academico']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Calidad de desempeño</label>
									<input type="text" class="form-control" name="calidad_desempeno" value="<?=$datos_hoja['calidad_desempeno']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Especialidad</label>
									<input type="text" class="form-control" name="especialidad" value="<?=$datos_hoja['especialidad']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fecha de ingreso <span class="text-danger">*</span></label>
									<input type="date" class="form-control" name="fecha_ingreso" value="<?=$datos_hoja['fecha_ingreso']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Tiempo Laboral</label>
									<input type="text" class="form-control" name="tiempo_laboral" value="<?=$datos_hoja['tiempo_laboral']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Tipo de vinculaci&oacute;n</label>
									<input type="text" class="form-control" name="tipo_vinculacion" value="<?=$datos_hoja['tipo_vinculacion']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Origen Vinculaci&oacute;n</label>
									<input type="text" class="form-control" name="origen_vinculacion" value="<?=$datos_hoja['origen_vinculacion']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fuente de recursos</label>
									<input type="text" class="form-control" name="fuente_recursos" value="<?=$datos_hoja['fuente_recursos']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Salario <span class="text-danger">*</span></label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">$</span>
										</div>
										<input type="text" class="form-control numeros precio" name="salario" value="<?=number_format($salario)?>">
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Cargo</label>
									<input type="text" class="form-control" name="cargo" value="<?=$datos_hoja['cargo']?>">
								</div>
								<div class="col-lg-12 form-group text-right">
									<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_academica">
										<i class="fa fa-plus"></i>
										&nbsp;
										Agregar informaci&oacute;n acad&eacute;mica
									</button>
								</div>
								<div class="table-responsive">
									<table class="table table-hover border">
										<thead>
											<tr>
												<th colspan="8">
													<h5 class="font-weight-bold text-success text-center text-uppercase">Informaci&oacute;n Acad&eacute;mica</h5>
												</th>
											</tr>
											<tr class="text-center">
												<th scope="col">Instituci&oacute;n</th>
												<th scope="col">Ciudad</th>
												<th scope="col">Pais</th>
												<th scope="col">Nivel Acad&eacute;mico</th>
												<th scope="col" colspan="3">Detalle</th>
											</tr>
										</thead>
										<tbody class="info_academica">
											<?php
											foreach ($datos_academicos as $academico) {
												$id_academico = $academico['id'];
												$institucion  = $academico['institucion'];
												$ciudad       = $academico['ciudad'];
												$pais         = $academico['pais'];
												$nivel        = $academico['nivel'];
												$grado        = $academico['graduado'];
												$fecha_grado  = $academico['fecha_grado'];
												$titulo       = $academico['titulo'];
												?>
												<tr class="text-center academica_<?=$id_academico?>">
													<td><?=$institucion?></td>
													<td><?=$ciudad?></td>
													<td><?=$pais?></td>
													<td><?=$nivel?></td>
													<td>
														<span class="font-weight-bold">Graduado</span>
														<br>
														<?=$grado?>
													</td>
													<td>
														<span class="font-weight-bold">Fecha Graduaci&oacute;n</span>
														<br>
														<?=$fecha_grado?>
													</td>
													<td>
														<span class="font-weight-bold">T&iacute;tulo Alcanzado</span>
														<br>
														<?=$titulo?>
													</td>
													<td>
														<button class="btn btn-danger btn-sm eliminar" type="button" id="<?=$id_academico?>">
															<i class="fas fa-times"></i>
															&nbsp;
															Eliminar
														</button>
													</td>
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
								<div class="col-lg-12 form-group text-right mt-2">
									<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_laboral">
										<i class="fa fa-plus"></i>
										&nbsp;
										Agregar informaci&oacute;n laboral
									</button>
								</div>
								<div class="table-responsive">
									<table class="table table-hover border">
										<thead>
											<tr>
												<th colspan="8">
													<h5 class="font-weight-bold text-success text-center text-uppercase">Informaci&oacute;n Laboral</h5>
												</th>
											</tr>
											<tr class="text-center">
												<th scope="col">Empresa</th>
												<th scope="col">Ciudad</th>
												<th scope="col">Pais</th>
												<th scope="col">Cargo</th>
												<th scope="col">Tel&eacute;fono</th>
												<th scope="col">Direcci&oacute;n</th>
												<th scope="col">Ingreso</th>
												<th scope="col">Terminaci&oacute;n</th>
											</tr>
										</thead>
										<tbody class="info_laboral">
											<?php
											foreach ($datos_laborales as $laboral) {
												$id_laboral    = $laboral['id'];
												$empresa       = $laboral['empresa'];
												$ciudad        = $laboral['ciudad'];
												$pais          = $laboral['pais'];
												$cargo         = $laboral['cargo'];
												$telefono      = $laboral['telefono'];
												$direccion     = $laboral['direccion'];
												$fecha_ingreso = $laboral['fecha_ingreso'];
												$fecha_salida  = ($laboral['fecha_salida'] == '0000-00-00') ? '' : $laboral['fecha_salida'];
												?>
												<tr class="text-center laboral_<?=$id_laboral?>">
													<td><?=$empresa?></td>
													<td><?=$ciudad?></td>
													<td><?=$pais?></td>
													<td><?=$cargo?></td>
													<td><?=$telefono?></td>
													<td><?=$direccion?></td>
													<td><?=$fecha_ingreso?></td>
													<td><?=$fecha_salida?></td>
													<td>
														<button class="btn btn-danger btn-sm eliminar_laboral" type="button" id="<?=$id_laboral?>">
															<i class="fas fa-times"></i>
															&nbsp;
															Eliminar
														</button>
													</td>
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
								<div class="col-lg-12 form-group mt-4">
									<h5 class="font-weight-bold text-success text-center text-uppercase">Preguntas personalizadas</h5>
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fecha De Diligenciamiento <span class="text-danger">*</span></label>
									<input type="date" class="form-control" name="fecha_diligencia" value="<?=$datos_pregunta['fecha_diligencia']?>" required>
								</div>
								<div class="col-lg-8 form-group"></div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Diga Los Idiomas Diferentes Al Español Que Domina: </label>
									<textarea class="form-control" rows="5" name="idiomas"><?=$datos_pregunta['idiomas']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Especifique Nivel: (Regular, Bien, Muy Bien): </label>
									<textarea class="form-control" rows="5" name="idiomas_nivel"><?=$datos_pregunta['idiomas_nivel']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Referencia Laboral (Nombre, Empresa,Cargo, Teléfono ):</label>
									<textarea class="form-control" rows="5" name="referencia_laboral"><?=$datos_pregunta['referencia_laboral']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Autorizo A Codetec A Verificar Toda La Información Creada En Este Documento:</label>
									<textarea class="form-control" rows="5" name="autorizo"><?=$datos_pregunta['autorizo']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">En Caso De Emergencia Llamar A ( Nombre, Teléfono ):</label>
									<textarea class="form-control" rows="5" name="emergencia"><?=$datos_pregunta['emergencia']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Referencia Personal (Nombre, Empresa,Cargo, Teléfono ):</label>
									<textarea class="form-control" rows="5" name="referencia_personal"><?=$datos_pregunta['referencia_personal']?></textarea>
								</div>
								<div class="col-lg-6 form-group">
									<label class="font-weight-bold">Libreta Militar ( Si Aplica ) (Clase, Número ):</label>
									<textarea class="form-control" rows="5" name="libreta_militar"><?=$datos_pregunta['libreta_militar']?></textarea>
								</div>
								<div class="col-lg-12 form-group text-right mt-2">
									<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#nuevo_documento">
										<i class="fa fa-plus"></i>
										&nbsp;
										Agregar documento
									</button>
								</div>
								<?php
								if ($id_url == 1) {
									?>
									<div class="col-lg-12 form-group">
										<div class="table-responsive">
											<table class="table table-hover border">
												<thead class="text-center text-dark">
													<tr>
														<th colspan="6" class="font-weight-bold text-success h5 text-center text-uppercase">Documentos</th>
													</tr>
													<tr>
														<th>Documento</th>
														<th>Direcci&oacute;n Acad&eacute;mica</th>
														<th>Director/a de bienestar</th>
														<th>Departamento Contable</th>
													</tr>
												</thead>
												<tbody class="text-center">
													<?php
													foreach ($datos_documentos as $documento) {
														$id_documento   = $documento['id'];
														$tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
														$archivo        = $documento['documento'];
														$perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

														$validar = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);

														$span_academico = ($perfil == 16) ? $validar : '<span class="badge badge-secondary">N/A</span>';
														$span_contable  = ($perfil == 11) ? $validar : '<span class="badge badge-secondary">N/A</span>';
														$span_bienestar = ($perfil == 17) ? $validar : '<span class="badge badge-secondary">N/A</span>';

														$ver_archivo = ($documento['autorizacion_uno'] == 3) ? 'd-none' : '';

														?>
														<tr>
															<td><?=$tipo_documento?></td>
															<td><?=$span_academico?></td>
															<td><?=$span_bienestar?></td>
															<td><?=$span_contable?></td>
															<td>
																<div class="btn-group <?=$ver_archivo?>">
																	<a href="<?=PUBLIC_PATH?>upload/<?=$archivo?>" class="btn btn-info btn-sm" target="_blank">
																		<i class="fa fa-eye"></i>
																		&nbsp;
																		Ver archivo
																	</a>
																	<button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#documento_<?=$id_documento?>">
																		<i class="fa fa-upload"></i>
																		&nbsp;
																		Actualizar archivo
																	</button>
																</div>
															</td>
														</tr>
														<?php
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								<?php } else {
									?>
									<div class="col-lg-12 form-group">
										<div class="table-responsive">
											<table class="table table-hover border">
												<thead class="text-center text-dark">
													<tr>
														<th colspan="6" class="font-weight-bold text-success h5 text-center text-uppercase">Documentos</th>
													</tr>
													<tr>
														<th>Documento</th>
														<th>Documento Subido</th>
														<th>Documento Aprobado</th>
													</tr>
												</thead>
												<tbody class="text-center">
													<?php
													foreach ($datos_documentos as $documento) {
														$id_documento   = $documento['id'];
														$tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
														$archivo        = $documento['documento'];
														$perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

														$validar          = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);
														$verficacion_span = $instancia->spanEstadoAutorizacionControl($datos_hoja['verificacion']);

														$span_autoriza     = (!empty($documento['autorizacion_uno'])) ? $validar : '<span class="badge badge-secondary">N/A</span>';
														$span_verificacion = ($datos_hoja['verificacion'] == 1) ? $verficacion_span : '<span class="badge badge-secondary">N/A</span>';

														$ver_archivo = ($documento['autorizacion_uno'] == 3) ? 'd-none' : '';

														?>
														<tr>
															<td><?=$tipo_documento?></td>
															<td><span class="badge badge-success">Realizado</span></td>
															<td><?=$span_autoriza?></td>
															<td>
																<div class="btn-group <?=$ver_archivo?>">
																	<a href="<?=PUBLIC_PATH?>upload/<?=$archivo?>" class="btn btn-info btn-sm" target="_blank">
																		<i class="fa fa-eye"></i>
																		&nbsp;
																		Ver archivo
																	</a>
																	<button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#documento_<?=$id_documento?>">
																		<i class="fa fa-upload"></i>
																		&nbsp;
																		Actualizar archivo
																	</button>
																</div>
															</td>
														</tr>
														<?php
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								<?php }?>
								<div class="col-lg-12 form-group mt-4">
									<h5 class="font-weight-bold text-success text-center text-uppercase">Revisi&oacute;n de hoja de vida</h5>
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Quien verifica</label>
									<input type="text" class="form-control" disabled value="<?=$datos_hoja['nom_verifica']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Estado de verificaci&oacute;n</label>
									<input type="text" class="form-control text-white <?=$bg_input?>" value="<?=$nom_estado_ver?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fecha de verificaci&oacute;n</label>
									<input type="date" class="form-control" value="<?=date('Y-m-d', strtotime($datos_hoja['fecha_verificacion']))?>" disabled>
								</div>
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Observaciones</label>
									<textarea class="form-control" rows="5" disabled><?=$datos_hoja['observacion_verifica']?></textarea>
								</div>
								<?php
								if ($id_url == 1) {
									if ($datos['perfil'] != 3) {
										?>
										<div class="col-lg-12 form-group mt-4">
											<div class="table-responsive">
												<table class="table table-hover border">
													<thead class="text-center text-dark">
														<tr>
															<th colspan="5" class="font-weight-bold text-success h5 text-center text-uppercase">Evaluaciones de desempeño (Administrativo)</th>
														</tr>
														<tr>
															<th>A&ntilde;o</th>
															<th>Evaluador</th>
															<th>Puntuaci&oacute;n</th>
															<th>Desempe&ntilde;o</th>
														</tr>
													</thead>
													<tbody class="text-center">
														<?php
														foreach ($datos_evaluacion_admin as $evaluacion) {
															$anio       = $evaluacion['anio'];
															$puntuacion = ($evaluacion['suma_puntuacion'] / $evaluacion['cant_preguntas']);
															$evaluador  = $evaluacion['evaluador'];

															$span = ($puntuacion >= 1 && $puntuacion < 2) ? '<span class="badge badge-danger">Insuficiente</span>' : '';
															$span = ($puntuacion >= 2 && $puntuacion < 3) ? '<span class="badge badge-primary">B&aacute;sico</span>' : $span;
															$span = ($puntuacion >= 3 && $puntuacion < 4) ? '<span class="badge badge-warning">Regular</span>' : $span;
															$span = ($puntuacion >= 4 && $puntuacion < 5) ? '<span class="badge badge-info">Bueno</span>' : $span;
															$span = ($puntuacion == 5) ? '<span class="badge badge-success">Excelente</span>' : $span;

															?>
															<tr>
																<td><?=$anio?></td>
																<!-- <td><?=$evaluador?></td> -->
																<td>Orlando Rafael Mendoza Barrios</td>
																<td><?=number_format($puntuacion, 2)?></td>
																<td><?=$span?></td>
																<td>
																	<button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#evaluacionn_<?=$evaluacion['id_user'] . '_' . $anio?>">
																		<i class="fa fa-eye"></i>
																		&nbsp;
																		Ver detalles
																	</button>
																</td>
															</tr>


															<div class="modal fade" id="evaluacionn_<?=$evaluacion['id_user'] . '_' . $anio?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
																<div class="modal-dialog modal-lg">
																	<div class="modal-content">
																		<div class="modal-header">
																			<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Detalles evalucion a&ntilde;o <?=$anio?></h5>
																			<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																				<i class="fa fa-times"></i>
																				&nbsp;
																				Cerrar
																			</button>
																		</div>
																		<div class="modal-body">
																			<form method="POST">
																				<div class="row p-2">
																					<div class="col-lg-6 text-center border">
																						<label class="font-weight-bold">Pregunta</label>
																					</div>
																					<div class="col-lg-6 text-center border">
																						<label class="font-weight-bold">Puntuaci&oacute;n</label>
																					</div>
																					<?php
																					$evaluacion_administrativo = $instancia_usuario->mostrarDetalleEvaluacionControl($evaluacion['id_user'], $anio, 1);
																					foreach ($evaluacion_administrativo as $eva_admin) {
																						$nom_pregunta = $eva_admin['nom_pregunta'];
																						$puntuacion   = $eva_admin['puntuacion'];
																						?>
																						<div class="col-lg-6 border">
																							<label><?=$nom_pregunta?></label>
																						</div>
																						<div class="col-lg-6 text-center border">
																							<label><?=$puntuacion?></label>
																						</div>
																					<?php }?>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
										<?php
									}
									if ($datos['perfil'] == 3) {
										?>
										<div class="col-lg-12 form-group mt-4">
											<div class="table-responsive">
												<table class="table table-hover border">
													<thead class="text-center text-dark">
														<tr>
															<th colspan="5" class="font-weight-bold text-success h5 text-center text-uppercase">Evaluaciones de desempeño (Docente)</th>
														</tr>
														<tr>
															<th>A&ntilde;o</th>
															<th>Evaluador</th>
															<th>Puntuaci&oacute;n</th>
															<th>Desempe&ntilde;o</th>
														</tr>
													</thead>
													<tbody class="text-center">
														<?php
														foreach ($datos_evaluacion_docente as $evaluacion) {
															$anio       = $evaluacion['anio'];
															$puntuacion = ($evaluacion['suma_puntuacion'] / $evaluacion['cant_preguntas']);
															$evaluador  = $evaluacion['evaluador'];

															$span = ($puntuacion >= 1 && $puntuacion < 2) ? '<span class="badge badge-danger">Insuficiente</span>' : '';
															$span = ($puntuacion >= 2 && $puntuacion < 3) ? '<span class="badge badge-primary">B&aacute;sico</span>' : $span;
															$span = ($puntuacion >= 3 && $puntuacion < 4) ? '<span class="badge badge-warning">Regular</span>' : $span;
															$span = ($puntuacion >= 4 && $puntuacion < 5) ? '<span class="badge badge-info">Bueno</span>' : $span;
															$span = ($puntuacion == 5) ? '<span class="badge badge-success">Excelente</span>' : $span;

															?>
															<tr>
																<td><?=$anio?></td>
																<td><?=$evaluador?></td>
																<td><?=number_format($puntuacion, 2)?></td>
																<td><?=$span?></td>
																<td>
																	<button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#evaluacione_<?=$evaluacion['id_user'] . '_' . $anio?>">
																		<i class="fa fa-eye"></i>
																		&nbsp;
																		Ver detalles
																	</button>
																</td>
															</tr>

															<div class="modal fade" id="evaluacione_<?=$evaluacion['id_user'] . '_' . $anio?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
																<div class="modal-dialog modal-lg">
																	<div class="modal-content">
																		<div class="modal-header">
																			<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Detalles evalucion a&ntilde;o <?=$anio?></h5>
																			<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																				<i class="fa fa-times"></i>
																				&nbsp;
																				Cerrar
																			</button>
																		</div>
																		<div class="modal-body">
																			<form method="POST">
																				<div class="row p-2">
																					<div class="col-lg-6 text-center border">
																						<label class="font-weight-bold">Pregunta</label>
																					</div>
																					<div class="col-lg-6 text-center border">
																						<label class="font-weight-bold">Puntuaci&oacute;n</label>
																					</div>
																					<?php
																					$evaluacion_docente = $instancia_usuario->mostrarDetalleEvaluacionControl($evaluacion['id_user'], $anio, 2);
																					foreach ($evaluacion_docente as $eva_admin) {
																						$nom_pregunta = $eva_admin['nom_pregunta'];
																						$puntuacion   = $eva_admin['puntuacion'];
																						?>
																						<div class="col-lg-6 border">
																							<label><?=$nom_pregunta?></label>
																						</div>
																						<div class="col-lg-6 text-center border">
																							<label><?=$puntuacion?></label>
																						</div>
																					<?php }?>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									<?php }}?>
									<div class="col-lg-12 form-group mt-4">
										<a href="<?=BASE_URL?>imprimir/hoja/hoja_vida?id=<?=base64_encode($id_usuario)?>&enlace=<?=base64_encode($id_url)?>" class="btn btn-secondary btn-sm float-left" target="_blank">
											<i class="fa fa-print"></i>
											&nbsp;
											Imprimir
										</a>
										<button class="btn btn-success btn-sm float-right" type="submit">
											<i class="fa fa-save"></i>
											&nbsp;
											Guardar
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="nuevo_documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Subir nuevo documento</h5>
					</div>
					<div class="modal-body">
						<form method="POST" enctype="multipart/form-data">
							<input type="hidden" name="tipo_documento" value="0">
							<input type="hidden" name="id_user" value="<?=$id_usuario?>">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_hoja_documento" value="<?=$datos_hoja['id']?>">
							<input type="hidden" name="id_perfil" value="<?=$id_perfil_sesion?>">
							<input type="hidden" name="url" value="1">
							<div class="row p-2">
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Nombre de archivo <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="nom_archivo" required>
								</div>
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Archivo a subir <span class="text-danger">*</span></label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="documento_nuevo" id="documento_nuevo" accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label_documento_nuevo" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-12 form-group mt-4 text-right">
									<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
										<i class="fa fa-times"></i>
										&nbsp;
										Cancelar
									</button>
									<button class="btn btn-success btn-sm" type="submit">
										<i class="fa fa-upload"></i>
										&nbsp;
										Subir
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<?php
		foreach ($datos_documentos as $documento) {
			$id_documento   = $documento['id'];
			$tipo_documento = $documento['nom_tipo'];
			$archivo        = $documento['documento'];
			?>
			<div class="modal fade" id="documento_<?=$id_documento?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Subir <?=$tipo_documento?></h5>
						</div>
						<div class="modal-body">
							<form method="POST" enctype="multipart/form-data">
								<input type="hidden" name="tipo_documento" value="<?=$documento['tipo_doc']?>">
								<input type="hidden" name="id_user" value="<?=$id_usuario?>">
								<input type="hidden" name="id_log" value="<?=$id_log?>">
								<input type="hidden" name="id_hoja_actual" value="<?=$datos_hoja['id']?>">
								<input type="hidden" name="archivo" value="<?=$archivo?>">
								<input type="hidden" name="enlace" value="<?=$id_url?>">
								<div class="row p-2">
									<div class="col-lg-12 form-group">
										<label class="font-weight-bold"><?=$tipo_documento?> <span class="text-danger">*</span></label>
										<div class="custom-file pmd-custom-file-filled">
											<input type="file" class="custom-file-input file_input" name="documento" id="documento_<?=$id_documento?>" accept=".png, .jpg, .jpeg, .pdf">
											<label class="custom-file-label file_label_documento_<?=$id_documento?>" for="customfilledFile"></label>
										</div>
									</div>
									<div class="col-lg-12 form-group mt-4 text-right">
										<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
											<i class="fa fa-times"></i>
											&nbsp;
											Cancelar
										</button>
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-upload"></i>
											&nbsp;
											Subir
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		include_once VISTA_PATH . 'script_and_final.php';
		include_once VISTA_PATH . 'modulos' . DS . 'hoja' . DS . 'agregarAcademica.php';
		include_once VISTA_PATH . 'modulos' . DS . 'hoja' . DS . 'agregarLaboral.php';

		if (isset($_POST['id_hoja'])) {
			$instancia->actualizarHojaVidaUsuarioControl();
		}

		if (isset($_POST['id_hoja_actual'])) {
			$instancia->actualizarDocumentosControl();
		}

		if (isset($_POST['id_hoja_documento'])) {
			$instancia->subirNuevoDocumentoControl();
		}
	}
	?>
	<script src="<?=PUBLIC_PATH?>js/hoja/funcionesHoja.js"></script>