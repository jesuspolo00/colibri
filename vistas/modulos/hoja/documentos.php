<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';

$instancia        = ControlHoja::singleton_hoja();
$instancia_perfil = ControlPerfil::singleton_perfil();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 62);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_usuario = base64_decode($_GET['id']);
	$id_url     = base64_decode($_GET['enlace']);

	$url = ($id_url == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
	$url = ($id_url == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

	$datos      = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
	$datos_hoja = $instancia->informacionHojaVidaControl($id_usuario);
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=$url?>" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hoja de vida - Documentos (<?=$datos['nombre'] . ' ' . $datos['apellido']?>)
					</h4>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data">
						<input type="hidden" name="id_user" value="<?=$id_usuario?>">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
						<input type="hidden" name="enlace" value="<?=$id_url?>">
						<div class="row p-2">
							<div class="col-lg-12 form-group">
								<div class="row">
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Identificacion</label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos['documento']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Nombres</label>
										<input type="text" class="form-control" disabled value="<?=$datos['nombre']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Apellidos</label>
										<input type="text" class="form-control" disabled value="<?=$datos['apellido']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Telefono</label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos['telefono']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Celular</label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos_hoja['celular']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Correo electronico</label>
										<input type="email" class="form-control" disabled value="<?=$datos['correo']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Direcci&oacute;n</label>
										<input type="text" class="form-control" disabled value="<?=$datos_hoja['direccion']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-4">
								<h5 class="font-weight-bold text-success text-center text-uppercase">Documentos</h5>
								<hr>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Fotocopia de cedula <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="cedula" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_cedula" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="1">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">RUT <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="rut" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_rut" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="2">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Certificado de afiliacion EPS actualizado (maximo un mes de expedido) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="eps" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_eps" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="3">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Certificado de afiliacion ARL actualizado (maximo un mes de expedido) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="arl" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_arl" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="4">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Certificado de afiliacion PENSION actualizado (maximo un mes de expedido) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="pension" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_pension" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="5">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Fotocopia de la tarjeta profesional (Si es del area de la salud RETHUS) </label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="tarjeta" accept=".png, .jpg, .jpeg, .pdf">
									<label class="custom-file-label file_label_tarjeta" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="6">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Diploma de informacion pedagogica</label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="pedagogica" accept=".png, .jpg, .jpeg, .pdf">
									<label class="custom-file-label file_label_pedagogica" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="7">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Diploma y/o acta de grado de formacion profesional <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="profesional" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_profesional" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="8">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Experiencia docente (Minimo 2 a&ntilde;os)</label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="exp_docente" accept=".png, .jpg, .jpeg, .pdf">
									<label class="custom-file-label file_label_exp_docente" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="9">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Experiencia en el modulo que se le asigna (Personal nuevo <?=date('Y')?> minimo 1 a&ntilde;o)</label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="exp_modulo" accept=".png, .jpg, .jpeg, .pdf">
									<label class="custom-file-label file_label_exp_modulo" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="10">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Experiencia laboral en su campo (Minimo 2 a&ntilde;os) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="exp_laboral" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_exp_laboral" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="11">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Actualizacion academica reciente en su campo (Minimo 1 a&ntilde;o) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="act_academica" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_act_academica" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="12">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Foto 3X4 <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="foto" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_foto" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="13">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Contraloria <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="contraloria" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_contraloria" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="14">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Procuraduria <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="procura" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_procura" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="15">
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Antecedentes (policia) <span  class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento[]" id="policia" accept=".png, .jpg, .jpeg, .pdf" required>
									<label class="custom-file-label file_label_policia" for="customfilledFile"></label>
									<input type="hidden" name="tipo_doc[]" value="16">
								</div>
							</div>
							<div class="col-lg-12 form-group mt-4 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-upload"></i>
									&nbsp;
									Subir
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->subirDocumentosControl();
}
?>