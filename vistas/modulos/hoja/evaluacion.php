<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';

$instancia        = ControlHoja::singleton_hoja();
$instancia_perfil = ControlPerfil::singleton_perfil();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 65);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_usuario = base64_decode($_GET['id']);

	$datos          = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
	$datos_hoja     = $instancia->informacionHojaVidaControl($id_usuario);
	$datos_pregunta = $instancia->mostrarPreguntasEvaluacionControl(1);

	$foto_perfil = (!empty($datos_hoja['foto'])) ? $datos_hoja['foto'] : $datos_hoja['foto_hoja_vida'];
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>hoja/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hoja de vida - Evaluaci&oacute;n (<?=$datos['nombre'] . ' ' . $datos['apellido']?>)
					</h4>
				</div>
				<div class="card-body">
					<div class="row p-2">
						<div class="col-lg-12 form-group">
							<div class="row">
								<div class="col-lg-4">
									<div class="col-lg-12 form-group">
										<div class="p-4 text-center">
											<img src="<?=PUBLIC_PATH?>upload/<?=$foto_perfil?>" class="img-thumbnail img-fluid rounded" alt="" width="240">
										</div>
									</div>
								</div>
								<div class="col-lg-8 form-group">
									<div class="row">
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Identificacion</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos['documento']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Nombres</label>
											<input type="text" class="form-control" disabled value="<?=$datos['nombre']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Apellidos</label>
											<input type="text" class="form-control" disabled value="<?=$datos['apellido']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Telefono</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos['telefono']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Celular</label>
											<input type="text" class="form-control numeros" disabled value="<?=$datos_hoja['celular']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Correo electronico</label>
											<input type="email" class="form-control" disabled value="<?=$datos['correo']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Direcci&oacute;n</label>
											<input type="text" class="form-control" disabled value="<?=$datos_hoja['direccion']?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Cargo</label>
											<input type="text" class="form-control" disabled value="<?=$datos_hoja['cargo']?>">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<hr>
							<h5 class="font-weight-bold text-center text-success text-uppercase">Evaluaci&oacute;n desempe&ntilde;o personal administrativo</h5>
							<hr>
						</div>
						<div class="col-lg-12 form-group">
							<p>Para asignar los resultados se presenta frente a la descripci&oacute;n de cada par&aacute;metro a evaluar una guía de valoraci&oacute;n de cinco (5) grados definidos as&iacute;:</p>
							<ul class="ml-4">
								<li><strong>Excelente = 5</strong>, el desempe&ntilde;o del empleado evaluado cumple los patrones establecidos en el manual de funciones obteniendo un nivel m&aacute;ximo efeciencia y efectividad.</li>
								<li><strong>Bueno = 4</strong>, el evaluad posee par&aacute;ametros satisfactorios en su desempeño.</li>
								<li><strong>Regular = 3</strong>, el desempe&ntilde;o del evaluado se ajusta a los establecidos. </li>
								<li><strong>B&aacute;sico = 2</strong>, el desempe&ntilde;o del evaluado se ajusta los minimos establecidos.</li>
								<li><strong>Insuficiente = 1</strong>, el evaluado no alcanza los mininos establecidos.</li>
							</ul>
						</div>
						<div class="table-responsive">
							<form method="POST">
								<input type="hidden" name="id_log" value="<?=$id_log?>">
								<input type="hidden" name="id_user" value="<?=$id_usuario?>">
								<input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
								<table class="table table-hover border table-sm" width="100%" cellspacing="0">
									<thead class="text-center text-uppercase">
										<tr>
											<th class="text-success h5 font-weight-bold" colspan="3">Parametros a evaluar</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach ($datos_pregunta as $pregunta) {
											$id_pregunta  = $pregunta['id'];
											$nom_pregunta = $pregunta['pregunta'];
											?>
											<input type="hidden" name="pregunta[]" value="<?=$id_pregunta?>">
											<tr>
												<td class="p-3"><?=$nom_pregunta?></td>
												<td class="text-center">
													<input type="number" class="form-control numeros text-center" name="punt[]" required min="1" max="5">
												</td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
								<div class="col-lg-12 form-group mt-2 text-right">
									<button class="btn btn-success btn-sm" type="submit">
										&nbsp;
										<i class="fa fa-save"></i>
										Finalizar Evaluaci&oacute;n
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->finalizarEvaluacionControl();
}