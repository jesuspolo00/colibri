<div class="modal fade" id="agregar_academica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Agregar Informaci&oacute;n Acad&eacute;mica</h5>
      </div>
      <div class="modal-body">
        <form method="POST" id="form_academico">
          <input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Instituci&oacute;n <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="institucion" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Ciudad<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="ciudad" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Pais<span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="pais" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Nivel Acad&eacute;mico <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nivel" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Graduado <span class="text-danger">*</span></label>
              <div class="form-inline mt-1">
                <div class="custom-control custom-switch">
                  <input type="radio" class="custom-control-input" name="graduado" value="Si" id="si" required>
                  <label class="custom-control-label font-weight-bold" for="si">Si</label>
                </div>
                <div class="custom-control custom-switch ml-4">
                  <input type="radio" class="custom-control-input" name="graduado" value="No" id="no" required>
                  <label class="custom-control-label font-weight-bold" for="no">No</label>
                </div>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha de graduaci&oacute;n</label>
              <input type="date" class="form-control" name="fecha_grado">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Titulo alcanzado</label>
              <input type="text" class="form-control" name="titulo">
            </div>
            <div class="col-lg-12 form-group text-right mt-4">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-success btn-sm enviar_academico" type="button">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
