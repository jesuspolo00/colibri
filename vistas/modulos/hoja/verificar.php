<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlHoja::singleton_hoja();
$instancia_perfil  = ControlPerfil::singleton_perfil();
$instancia_usuario = ControlUsuario::singleton_usuario();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 66);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_usuario = base64_decode($_GET['id']);
	$id_url     = base64_decode($_GET['enlace']);

	$url = ($id_url == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
	$url = ($id_url == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

	$datos            = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
	$datos_hoja       = $instancia->informacionHojaVidaControl($id_usuario);
	$datos_pregunta   = $instancia->preguntasHojaControl($datos_hoja['id']);
	$datos_academicos = $instancia->informacionAcademicaControl($datos_hoja['id']);
	$datos_laborales  = $instancia->informacionLaboralControl($datos_hoja['id']);
	$datos_documentos = ($id_url == 1) ? $instancia->mostrarDocumentosHojaControl($id_usuario, 17) : $instancia->mostrarDocumentosHojaCidaProyectosControl($id_usuario);

	$autorizacion_anual = $instancia_usuario->autorizacionAnioDocumentosControl($id_usuario, date('Y'));

	$evaluacion_anual_administrativa = $instancia_usuario->validarEvaluacionAnualUsuarioControl($id_usuario, date('Y'), 1);
	$evaluacion_anual_docente        = $instancia_usuario->validarEvaluacionAnualUsuarioControl($id_usuario, date('Y'), 2);

	$foto_perfil = (!empty($datos_hoja['foto'])) ? $datos_hoja['foto'] : $datos_hoja['foto_hoja_vida'];

	$salario      = (empty($datos_hoja['salario'])) ? 0 : $datos_hoja['salario'];
	$fecha_ultima = (empty($datos_hoja['fecha_edit'])) ? $datos_hoja['fechareg'] : $datos_hoja['fecha_edit'];

	$bg_documentos = 'btn-danger';
	$bg_autorizar  = 'btn-danger';
	$bg_evaluacion = 'btn-danger';
	$progress_bar  = 'style="width: 33%"';

	if (!empty($datos_documentos)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-danger';
		$bg_evaluacion = 'btn-danger';
		$progress_bar  = 'style="width: 59%"';
	}
	if (!empty($autorizacion_anual)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-success';
		$bg_evaluacion = 'btn-danger';
		$progress_bar  = 'style="width: 85%"';
	}

	if (!empty($evaluacion_anual_administrativa) || !empty($evaluacion_anual_docente)) {
		$bg_documentos = 'btn-success';
		$bg_autorizar  = 'btn-success';
		$bg_evaluacion = 'btn-success';
		$progress_bar  = 'style="width: 100%"';
	}

}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=$url?>" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hoja de vida (<?=$datos['nombre'] . ' ' . $datos['apellido']?>)
					</h4>
					<h6 class="text-success float-right font-weight-bold">Fecha ultima actualizacion: <?=$fecha_ultima?></h6>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="enlace" value="<?=$id_url?>">
						<div class="row p-2">
							<div class="col-lg-12 form-group">
								<div class="row p-3">
									<div class="col-lg-2 text-center">
										<button class="btn btn-success btn-sm rounded-circle p-4 text-center" disabled type="button">
											<i class="fas fa-user-edit ml-1 fa-2x"></i>
										</button>
										<br>
										<label class="font-weight-bold">Ingreso</label>
									</div>
									<div class="col-lg-1">
										<hr class="mt-5 font-weight-bold text-dark">
									</div>
									<div class="col-lg-2 text-center">
										<button class="btn <?=$bg_documentos?> btn-sm rounded-circle p-4 text-center" disabled type="button">
											<i class="fas fa-folder-open fa-2x"></i>
										</button>
										<br>
										<label class="font-weight-bold">Documentos</label>
									</div>
									<div class="col-lg-1">
										<hr class="mt-5 font-weight-bold text-dark">
									</div>
									<div class="col-lg-2 text-center">
										<button class="btn <?=$bg_autorizar?> btn-sm rounded-circle p-4 text-center" disabled type="button">
											<i class="fas fa-check-double fa-2x"></i>
										</button>
										<br>
										<label class="font-weight-bold">Verificaci&oacute;n de documentos (Anual)</label>
									</div>
									<div class="col-lg-1">
										<hr class="mt-5 font-weight-bold text-dark">
									</div>
									<div class="col-lg-2 text-center">
										<button class="btn <?=$bg_evaluacion?> btn-sm rounded-circle p-4 text-center" disabled type="button">
											<i class="fas fa-user-edit ml-1 fa-2x"></i>
										</button>
										<br>
										<label class="font-weight-bold">Evaluaci&oacute;n (Anual)</label>
									</div>
									<div class="col-lg-12 mt-2 mb-2">
										<div class="progress">
											<div class="progress-bar bg-success" role="progressbar" <?=$progress_bar?> aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<h5 class="text-success text-center text-uppercase font-weight-bold">Informaci&oacute;n Personal</h5>
								<hr>
							</div>
							<div class="col-lg-8 form-group">
								<div class="row">
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Nombres <span class="text-danger">*</span></label>
										<input type="text" class="form-control" disabled value="<?=$datos['nombre']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Apellidos <span class="text-danger">*</span></label>
										<input type="text" class="form-control" disabled value="<?=$datos['apellido']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Identificacion <span class="text-danger">*</span></label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos['documento']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Sexo <span class="text-danger">*</span></label>
										<input type="text" class="form-control" disabled value="<?=$datos_hoja['sexo']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Telefono</label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos['telefono']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Celular</label>
										<input type="text" class="form-control numeros" disabled value="<?=$datos_hoja['celular']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Correo electronico</label>
										<input type="email" class="form-control" disabled value="<?=$datos['correo']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Direcci&oacute;n <span class="text-danger">*</span></label>
										<input type="text" class="form-control" disabled value="<?=$datos_hoja['direccion']?>">
									</div>
								</div>
							</div>
							<div class="col-lg-4 form-group">
								<div class="p-4 text-center">
									<img src="<?=PUBLIC_PATH?>upload/<?=$foto_perfil?>" class="img-thumbnail img-fluid rounded" alt="" width="230">
								</div>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Lugar de residencia</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['lugar']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Barrio</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['barrio']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de nacimiento <span class="text-danger">*</span></label>
								<input type="date" class="form-control" disabled value="<?=$datos_hoja['fecha_nac']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Lugar de nacimiento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['lugar_nac']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de sangre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['nom_sangre']?>">
							</div>
							<div class="col-lg-12 form-group mt-4">
								<h5 class="font-weight-bold text-success text-center text-uppercase">Informaci&oacute;n Adicional</h5>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Expedicion de documento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['exp_documento']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado civil <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['nom_estado']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Escalaf&oacute;n</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['escalafon']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nivel Academico</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['nivel_academico']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Calidad de desempeño</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['calidad_desempeno']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Especialidad</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['especialidad']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de ingreso <span class="text-danger">*</span></label>
								<input type="date" class="form-control" disabled value="<?=$datos_hoja['fecha_ingreso']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tiempo Laboral</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['tiempo_laboral']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de vinculaci&oacute;n</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['tipo_vinculacion']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Origen Vinculaci&oacute;n</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['origen_vinculacion']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fuente de recursos</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['fuente_recursos']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Salario <span class="text-danger">*</span></label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">$</span>
									</div>
									<input type="text" class="form-control numeros precio" disabled value="<?=number_format($salario)?>">
								</div>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Cargo</label>
								<input type="text" class="form-control" disabled value="<?=$datos_hoja['cargo']?>">
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#nuevo_documento">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar documento
								</button>
							</div>
							<?php
							if ($id_url == 1) {
								?>
								<div class="col-lg-12 form-group">
									<div class="table-responsive">
										<table class="table table-hover border">
											<thead class="text-center text-dark">
												<tr>
													<th colspan="6" class="font-weight-bold text-success h5 text-center text-uppercase">Documentos</th>
												</tr>
												<tr>
													<th>Documento</th>
													<th>Direcci&oacute;n Acad&eacute;mica</th>
													<th>Director/a de bienestar</th>
													<th>Departamento Contable</th>
												</tr>
											</thead>
											<tbody class="text-center">
												<?php
												foreach ($datos_documentos as $documento) {
													$id_documento   = $documento['id'];
													$tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
													$archivo        = $documento['documento'];
													$perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

													$validar = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);

													$span_academico = ($perfil == 16) ? $validar : '<span class="badge badge-secondary">N/A</span>';
													$span_contable  = ($perfil == 11) ? $validar : '<span class="badge badge-secondary">N/A</span>';
													$span_bienestar = ($perfil == 17) ? $validar : '<span class="badge badge-secondary">N/A</span>';

													?>
													<tr>
														<td><?=$tipo_documento?></td>
														<td><?=$span_academico?></td>
														<td><?=$span_bienestar?></td>
														<td><?=$span_contable?></td>
														<td>
															<div class="btn-group">
																<a href="<?=PUBLIC_PATH?>upload/<?=$archivo?>" class="btn btn-info btn-sm" target="_blank">
																	<i class="fa fa-eye"></i>
																	&nbsp;
																	Ver archivo
																</a>
															</div>
														</td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							<?php } else {
								?>
								<div class="col-lg-12 form-group">
									<div class="table-responsive">
										<table class="table table-hover border">
											<thead class="text-center text-dark">
												<tr>
													<th colspan="6" class="font-weight-bold text-success h5 text-center text-uppercase">Documentos</th>
												</tr>
												<tr>
													<th>Documento</th>
													<th>Documentos Subidos</th>
													<th>Documentos Autorizados</th>
												</tr>
											</thead>
											<tbody class="text-center">
												<?php
												foreach ($datos_documentos as $documento) {
													$id_documento   = $documento['id'];
													$tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
													$archivo        = $documento['documento'];
													$perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

													$validar          = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);
													$verficacion_span = $instancia->spanEstadoAutorizacionControl($datos_hoja['verificacion']);

													$span_autoriza     = (!empty($documento['autorizacion_uno'])) ? $validar : '<span class="badge badge-secondary">N/A</span>';
													$span_verificacion = ($datos_hoja['verificacion'] != 0) ? $verficacion_span : '<span class="badge badge-secondary">N/A</span>';

													?>
													<tr>
														<td><?=$tipo_documento?></td>
														<td><span class="badge badge-success">Realizado</span></td>
														<td><?=$span_autoriza?></td>
														<td>
															<div class="btn-group">
																<a href="<?=PUBLIC_PATH?>upload/<?=$archivo?>" class="btn btn-info btn-sm" target="_blank">
																	<i class="fa fa-eye"></i>
																	&nbsp;
																	Ver archivo
																</a>
															</div>
														</td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							<?php }?>
							<input type="hidden" name="id_user" value="<?=$id_usuario?>">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado de verificac&oacute;n <span class="text-danger">*</span></label>
								<select class="form-control" name="estado" required>
									<option value="">Seleccione una opcion...</option>
									<option value="1">Aprobado</option>
									<option value="2">Denegado</option>
								</select>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observacion</label>
								<textarea class="form-control" rows="5" name="observacion"></textarea>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar verificaci&oacute;n
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="nuevo_documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Subir nuevo documento</h5>
				</div>
				<div class="modal-body">
					<form method="POST" enctype="multipart/form-data">
						<input type="hidden" name="tipo_documento" value="0">
						<input type="hidden" name="id_user" value="<?=$id_usuario?>">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_hoja_documento" value="<?=$datos_hoja['id']?>">
						<input type="hidden" name="id_perfil" value="<?=$id_perfil_sesion?>">
						<input type="hidden" name="url" value="2">
						<div class="row p-2">
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Nombre de archivo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nom_archivo" required>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Archivo a subir <span class="text-danger">*</span></label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="documento_nuevo" id="documento_nuevo" accept=".png, .jpg, .jpeg, .pdf">
									<label class="custom-file-label file_label_documento_nuevo" for="customfilledFile"></label>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-4 text-right">
								<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
									<i class="fa fa-times"></i>
									&nbsp;
									Cancelar
								</button>
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-upload"></i>
									&nbsp;
									Subir
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_hoja_documento'])) {
		$instancia->subirNuevoDocumentoControl();
	}

	if (isset($_POST['id_hoja'])) {
		$instancia->verificarHojaVidaControl();
	}
