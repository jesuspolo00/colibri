<div class="modal fade" id="agregar_laboral" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Agregar Informaci&oacute;n Laboral</h5>
      </div>
      <div class="modal-body">
        <form method="POST" id="form_laboral">
          <input type="hidden" name="id_hoja" value="<?=$datos_hoja['id']?>">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Empresa <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="empresa" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="ciudad" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="pais" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Cargo <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="cargo" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Telefono</label>
              <input type="text" class="form-control numeros" name="telefono" >
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Direccion</label>
              <input type="text" class="form-control" name="direccion" >
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha de ingreso <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha_ingreso" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha de Terminaci&oacute;n</label>
              <input type="date" class="form-control" name="fecha_salida">
            </div>
            <div class="col-lg-12 form-group text-right mt-4">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-success btn-sm enviar_laboral" type="button">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
