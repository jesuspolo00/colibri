<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

if (isset($_GET['codigo'])) {

	$id_inventario = base64_decode($_GET['codigo']);

	$datos_hoja = $instancia->hojaVidaArticuloControl($id_inventario);
}

?>
<div class="container-fluid">
	<div class="row p-2">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>lector/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Reportar Articulo
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_usuario" value="0">
						<input type="hidden" name="id_articulo" value="<?=$id_inventario?>">
						<input type="hidden" name="estado" value="2">
						<input type="hidden" name="id_area" value="<?=$datos_hoja['id_area']?>">
						<input type="hidden" name="inicio" value="2">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$datos_hoja['descripcion']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$datos_hoja['nom_area']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Categoria <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$datos_hoja['nom_categoria']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Codigo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$datos_hoja['codigo']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de reporte <span class="text-danger">*</span></label>
								<input type="date" class="form-control" required value="<?=date('Y-m-d')?>" name="fecha_reporte">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observacion</label>
								<textarea class="form-control" rows="5" name="observacion"></textarea>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<a href="<?=BASE_URL?>lector/index" class="btn btn-danger btn-sm" type="submit">
									<i class="fa fa-times"></i>
									&nbsp;
									Cancelar
								</a>
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Reportar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_articulo'])) {
	$instancia->reportarArticuloControl();
}

if ($datos_hoja['estado'] != 1 && $datos_hoja['estado'] != 4 && $datos_hoja['estado'] != 7) {
	?>
	<script>
		swal({
			title: "Articulo no disponible",
			text: "El articulo no esta disponible para realizar reporte ya que se encuentra en el estado <?=$datos_hoja['nom_estado']?>",
			icon: "error",
			type: "error",
			button: "Regresar",
			closeOnClickOutside: false,
			showCancelButton: false,
			closeOnEsc: false,
			dangerMode: true,
		}).then((result) => {
			if (result) {
				window.location.replace("<?=BASE_URL?>lector/index");
			}
		});
	</script>
	<?php
}