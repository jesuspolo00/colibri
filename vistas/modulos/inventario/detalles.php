<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia      = ControlInventario::singleton_inventario();
$instancia_area = ControlAreas::singleton_areas();

$datos_zonas = $instancia_area->mostrarZonasControl();
$datos_area  = $instancia_area->mostrarAreasControl();

if (isset($_GET['id'])) {

	$id_inventario = base64_decode($_GET['id']);

	$datos_articulo = $instancia->hojaVidaArticuloControl($id_inventario);
	$datos_reporte  = $instancia->mostrarReportesControl($id_inventario);
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inventario/panel"  class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Detalles de articulo (<?=$datos_articulo['descripcion']?>)
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_inventario" value="<?=$id_inventario?>">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
								<input type="text" name="descripcion" class="form-control" value="<?=$datos_articulo['descripcion']?>" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Marca </label>
								<input type="text" name="marca" class="form-control" value="<?=$datos_articulo['marca']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Modelo </label>
								<input type="text" name="modelo" class="form-control" value="<?=$datos_articulo['modelo']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Cantidad  <span class="text-danger">*</span></label>
								<input type="number" min="1" class="form-control" name="cantidad" required value="<?=$datos_articulo['cantidad']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Codigo Inventario  <span class="text-danger">*</span></label>
								<input type="text" disabled class="form-control" required value="<?=$datos_articulo['codigo']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Zona <span class="text-danger">*</span></label>
								<select class="form-control" name="zona">
									<option value="<?=$datos_articulo['id_zona']?>" class="d-none" selected><?=$datos_articulo['nom_zona']?></option>
									<?php
									foreach ($datos_zonas as $zona) {
										$id_zona  = $zona['id'];
										$nom_zona = $zona['nombre'];
										?>
										<option value="<?=$id_zona?>"><?=$nom_zona?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Dependencia <span class="text-danger">*</span></label>
								<select class="form-control" name="area">
									<option value="<?=$datos_articulo['id_area']?>" class="d-none" selected><?=$datos_articulo['nom_area']?></option>
									<?php
									foreach ($datos_area as $area) {
										$id_area  = $area['id'];
										$nom_area = $area['nombre'];
										?>
										<option value="<?=$id_area?>"><?=$nom_area?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-12 form-group mt-2">
								<input type="hidden" value="<?=$datos_articulo['codigo']?>" name="codigo" id="codigo">
								<a href="<?=BASE_URL?>imprimir/inventario/codigo?id=<?=base64_encode($id_inventario)?>" class="btn btn-primary btn-sm" target="_blank">
									<i class="fas fa-barcode"></i>
									&nbsp;
									Descargar codigo de inventario
								</a>
							</div>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table class="table table-hover border" >
										<thead>
											<tr class="text-center font-weight-bold text-uppercase">
												<th colspan="10">
													Solicitudes de mantenimiento
												</th>
											</tr>
											<tr class="text-center font-weight-bold text-uppercase">
												<th>No. Reporte</th>
												<th>Bloque</th>
												<th>Zona</th>
												<th>Observacion reportada</th>
												<th>Fecha reporte</th>
												<th>Observacion Respuesta</th>
												<th>Fecha Respuesta</th>
												<th>Tipo de reporte</th>
											</tr>
										</thead>
										<tbody class="buscar text-uppercase">
											<?php
											foreach ($datos_reporte as $reporte) {
												$id_reporte            = $reporte['id'];
												$nom_zona              = $reporte['nom_zona'];
												$nom_area              = $reporte['nom_area'];
												$observacion           = $reporte['observacion'];
												$fecha_reporte         = $reporte['fecha_reporte'];
												$observacion_respuesta = $reporte['observacion_respuesta'];
												$fecha_respuesta       = (empty($reporte['fecha_respuesta'])) ? '' : date('Y-m-d', strtotime($reporte['fecha_respuesta']));

												$ver_solucion = ($reporte['estado'] == 4) ? '' : 'd-none';
												$span_estado  = ($reporte['estado'] == 4) ? '<span class="badge badge-success">Solucionado</span>' : '<span class="badge badge-warning">Pendiente de solucion</span>';
												$span_tipo = ($reporte['tipo_reporte'] == 1) ? '<span class="badge badge-danger">Correctivo</span>' : '<span class="badge badge-warning">Mantenimiento</span>';
												?>
												<tr class="text-center text-dark">
													<td><?=$id_reporte?></td>
													<td><?=$nom_zona?></td>
													<td><?=$nom_area?></td>
													<td><?=$observacion?></td>
													<td><?=date('Y-m-d', strtotime($fecha_reporte))?></td>
													<td><?=$observacion_respuesta?></td>
													<td><?=$fecha_respuesta?></td>
													<td><?=$span_tipo?></td>
													<td><?=$span_estado?></td>
													<td class="<?=$ver_solucion?>">
														<div class="btn-group">
															<a href="<?=BASE_URL?>imprimir/inventario/solucion?id=<?=base64_encode($id_reporte)?>" target="_blank" class="btn btn-success btn-sm" data-tooltip?="tooltip" title="Descargar Solucion" data-placement=bottom>
																<i class="fa fa-file-pdf"></i>
															</a>
														</div>
													</td>
												</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-sync"></i>
									&nbsp;
									Actualizar articulo
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->actualizarArticuloControl();
}