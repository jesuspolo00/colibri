<!-- Hardware -->
<div class="modal fade" id="agregar_hardware" tabindex="-1" role="dialog" aria-labelledby="agregar_hardware" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold text-success">
					<a href="<?=BASE_URL?>inventario/hojaVida?inventario=<?=base64_encode($id_inventario)?>"  class="text-decoration-none">
						<i class="fa fa-times text-success"></i>
					</a>
				&nbsp;Agregar hardware</h5>
				<button class="btn btn-info btn-sm float-right" type="button" id="btn_asignar">
					<i class="fas fa-sync-alt"></i>
					&nbsp;
					Asignar
				</button>
				<button class="btn btn-success btn-sm float-right" type="button" id="btn_agregar">
					<i class="fa fa-plus"></i>
					&nbsp;
					Agregar
				</button>
			</div>
			<form method="POST" id="harwd">
				<input type="hidden" name="id_user" value="<?=$id_log?>">
				<input type="hidden" name="area" value="<?=$datos_hoja['id_area']?>">
				<input type="hidden" name="categoria" value="2">
				<input type="hidden" name="id_inventario" value="<?=$id_inventario?>">
				<div class="modal-body border-0">
					<div class="row p-2">
						<div class="form-group col-lg-6">
							<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
							<input type="text" class="form-control" value="<?=$datos_hoja['nom_area']?>" disabled>
						</div>
						<div class="form-group col-lg-6">
							<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="descripcion" required>
						</div>
						<div class="form-group col-lg-6">
							<label class="font-weight-bold">Modelo</label>
							<input type="text" class="form-control" name="modelo">
						</div>
						<div class="form-group col-lg-6">
							<label class="font-weight-bold">Marca</label>
							<input type="text" class="form-control" name="marca">
						</div>
						<div class="form-group col-lg-6">
							<label class="font-weight-bold">Fecha adquisicion</label>
							<input type="date" class="form-control" name="fecha_compra">
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<a href="<?=BASE_URL?>inventario/hojaVida?inventario=<?=base64_encode($id_inventario)?>" class="btn btn-danger btn-sm">
						<i class="fa fa-times"></i>
						&nbsp;
						Cerrar
					</a>
					<button type="submit" class="btn btn-success btn-sm">
						<i class="fa fa-plus"></i>
						&nbsp;
						Agregar
					</button>
				</div>
			</form>

			<div class="modal-body border-0 p-4" id="list_hardw">
				<div class="table-responsive">
					<table class="table border table-sm" width="100%" cellspacing="0">
						<thead>
							<tr class="text-center font-weight-bold">
								<th>Descripcion</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Area</th>
								<th>Codigo</th>
							</tr>
						</thead>
						<tbody class="buscar text-uppercase">
							<?php
							foreach ($datos_componente as $componente) {
								$id_componente = $componente['id'];
								$descripcion   = $componente['descripcion'];
								$marca         = $componente['marca'];
								$modelo        = $componente['modelo'];
								$area          = $componente['nom_area'];
								$codigo        = $componente['codigo'];

								$ver = ($componente['id_area'] == $datos_hoja['id_area'] && $componente['asignado'] == 'no') ? '' : 'd-none';
								?>
								<tr class="text-center <?=$ver?> text-dark">
									<td><?=$descripcion?></td>
									<td><?=$marca?></td>
									<td><?=$modelo?></td>
									<td><?=$area?></td>
									<td><?=$codigo?></td>
									<td class="span-<?=$id_componente?>">
										<button type="button" class="btn btn-info btn-sm asignar_comp" id="<?=$id_componente?>" data-inventario="<?=$id_inventario?>" data-log="<?=$id_log?>">
											<i class="fas fa-sync-alt"></i>
											&nbsp;
											Asignar
										</button>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>




<!-- Modal -->
<div class="modal fade" id="agregar_soft" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold text-success">Agregar software</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST">
				<input type="hidden" name="id_log" value="<?=$id_log?>">
				<input type="hidden" name="id_inventario" value="<?=$id_inventario?>">
				<div class="modal-body border-0">
					<div class="col-lg-12 form-group">
						<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="descripcion_soft" maxlength="50" required>
					</div>
					<div class="col-lg-12 form-group">
						<label class="font-weight-bold">Version</label>
						<input type="text" class="form-control" name="version_soft" maxlength="50">
					</div>
					<div class="col-lg-12 form-group">
						<label class="font-weight-bold">Fabricante</label>
						<input type="text" class="form-control" name="fabricante_soft" maxlength="50">
					</div>
					<div class="col-lg-12 form-group">
						<label class="font-weight-bold">Licencia</label>
						<input type="text" class="form-control" name="licencia_soft" maxlength="50">
					</div>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cerrar
					</button>
					<button type="submit" class="btn btn-success btn-sm">
						<i class="fa fa-save"></i>
						&nbsp;
						Guardar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>