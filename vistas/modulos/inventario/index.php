<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlInventario::singleton_inventario();
$instancia_usuario   = ControlUsuario::singleton_usuario();
$instancia_area      = ControlAreas::singleton_areas();
$instancia_categoria = ControlCategorias::singleton_categoria();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_usuarios  = $instancia_usuario->mostrarUsuariosControl();
$datos_areas     = $instancia_area->mostrarAreasControl();
$datos_categoria = $instancia_categoria->mostrarCategoriasControl();
$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();
$datos_zonas     = $instancia_area->mostrarZonasControl();

if (isset($_POST['buscar'])) {
	$id_area         = $_POST['area'];
	$id_usuario      = $_POST['usuario'];
	$datos_articulos = $instancia->mostrarArticulosControl($id_area, $id_usuario);
} else {
	$datos_articulos = "";
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Inventario
					</h4>
				</div>
				<div class="card-body">
					<div class="row p-2">
						<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>inventario/panel">
							<div class="card border-left-primary shadow-sm h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="h5 mb-0 font-weight-bold text-gray-700">Listado inventario</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-clipboard-list fa-2x text-primary"></i>
										</div>
									</div>
								</div>
							</div>
						</a>
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 24);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="#" data-toggle="modal" data-target="#agregar_articulo">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Agregar articulo</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-plus-circle fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';

if (isset($_POST['descripcion'])) {
	$instancia->agregarArticuloControl();
}
?>
