<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

$datos_jornada = $instancia->mostrarJornadasControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 42);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['cobro'])) {

	$id_cobro = base64_decode($_GET['cobro']);

	$datos_cobro      = $instancia->mostrarDatosCuentaCobroControl($id_cobro);
	$datos_detalle    = $instancia->mostrarDetallesCuentasCobroControl($id_cobro);
	$datos_documentos = $instancia->mostrarDocumentosCuentasCobroControl($id_cobro);

	$ver_documentos = ($datos_cobro['cuenta_documentos'] == 0) ? 'd-none' : '';

	$ver_aprobar_cord = ($datos_cobro['estado'] == 1 && $id_perfil_sesion == 15) ? '' : 'd-none';
	$ver_aprobar_cont = ($datos_cobro['estado_contabilidad'] == 0 && $datos_cobro['estado'] == 2 && $id_perfil_sesion == 11) ? '' : 'd-none';
	$ver_archivo      = ($datos_cobro['estado'] == 2 && $datos_cobro['estado_contabilidad'] == 1) ? '' : 'd-none';

	if ($datos_cobro['estado'] == 0) {
		$bg_estado   = 'bg-secondary';
		$span_estado = 'Aún faltan completar pasos';
	}

	if ($datos_cobro['estado'] == 1) {
		$bg_estado   = 'bg-warning';
		$span_estado = 'Pendiente de aprobacion Coordinador';
	}

	if ($datos_cobro['estado'] == 2) {
		$bg_estado   = 'bg-success';
		$span_estado = 'Aprobado Coordinador';
	}

	if ($datos_cobro['estado'] == 3) {
		$bg_estado   = 'bg-danger';
		$span_estado = 'Denegado Coordinador';
	}

	if ($datos_cobro['estado_contabilidad'] == 0 && $datos_cobro['estado'] == 2) {
		$bg_estado   = 'bg-warning';
		$span_estado = 'Pendiente de aprobacion Contabilidad';
	}

	if ($datos_cobro['estado_contabilidad'] == 1 && $datos_cobro['estado'] == 2) {
		$bg_estado   = 'bg-success';
		$span_estado = 'Aprobado Contabilidad';
	}

	if ($datos_cobro['estado_contabilidad'] == 2 && $datos_cobro['estado'] == 2) {
		$bg_estado   = 'bg-success';
		$span_estado = 'Denegado Contabilidad';
	}

	$mes_asistencia = (!empty($datos_cobro['mes_cuenta_asistencia'])) ? mesesEspanol($datos_cobro['mes_cuenta_asistencia']) : 'N/A';
	$anio           = (!empty($datos_cobro['anio_cuenta_asistencia'])) ? $datos_cobro['anio_cuenta_asistencia'] : 'N/A';

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>distrito/cobro/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Detalles Cuenta de cobro No. <?=$id_cobro?> (<?=$mes_asistencia?> del <?=$anio?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_cuenta_cobro" value="<?=$id_cobro?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombre Completo</label>
									<input type="text" class="form-control" value="<?=$datos_cobro['usuario']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Raz&oacute;n Social </label>
									<input type="text" class="form-control" value="<?=$datos_cobro['razon']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Cedula o NIT</label>
									<input type="text" class="form-control" value="<?=$datos_cobro['documento']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Lugar</label>
									<input type="text" class="form-control" value="<?=$datos_cobro['lugar']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Direcci&oacute;n</label>
									<input type="text" class="form-control" value="<?=$datos_cobro['direccion']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Telefono</label>
									<input type="text" class="form-control numeros"  value="<?=$datos_cobro['telefono']?>" disabled>
								</div>
								<div class="col-lg-4 form-group d-none">
									<label class="font-weight-bold">Valor por hora</label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">$</span>
										</div>
										<input type="text" class="form-control numeros precio" value="<?=number_format($datos_cobro['valor'])?>" disabled>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Estado</label>
									<input type="text" class="form-control <?=$bg_estado?> text-white border-0" value="<?=$span_estado?>">
								</div>
								<?php
								if (!empty($datos_cobro['horas_coord'])) {
									?>
									<div class="col-lg-12 form-group">
										<hr>
										<h5 class="font-weight-bold text-center text-success text-uppercase">Proceso de coordinador</h5>
										<hr>
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Coordinador que aprueba</label>
										<input type="text" class="form-control" disabled value="<?=$datos_cobro['nom_coordinador']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Horas Aprobadas (Coordinador)</label>
										<input type="text" class="form-control" disabled value="<?=$datos_cobro['horas_coord']?>">
									</div>
									<div class="col-lg-12 form-group">
										<label class="font-weight-bold">Justificacion (Coordinador)</label>
										<textarea class="form-control" rows="5" disabled><?=$datos_cobro['justificacion_coord']?></textarea>
									</div>
								<?php }
								if ($datos_cobro['estado_contabilidad'] == 1 && $datos_cobro['estado'] == 2) {
									?>
									<div class="col-lg-12 form-group">
										<hr>
										<h5 class="font-weight-bold text-center text-success text-uppercase">Proceso de contabilidad</h5>
										<hr>
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Contabilidad que aprueba</label>
										<input type="text" class="form-control" disabled value="<?=$datos_cobro['nom_contabilidad']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Horas Aprobadas (Contabilidad)</label>
										<input type="text" class="form-control" disabled value="<?=$datos_cobro['horas_cont']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Horas a pagar (Contabilidad)</label>
										<input type="text" class="form-control" disabled value="<?=$datos_cobro['horas_pagar']?>">
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Valor por hora (Contabilidad)</label>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">$</span>
											</div>
											<input type="text" class="form-control numeros precio" value="<?=number_format($datos_cobro['hora_valor'])?>" disabled>
										</div>
									</div>
									<div class="col-lg-4 form-group">
										<label class="font-weight-bold">Valor total a pagar</label>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">$</span>
											</div>
											<input type="text" class="form-control numeros precio" value="<?=number_format($datos_cobro['hora_valor'] * $datos_cobro['horas_pagar'])?>" disabled>
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<label class="font-weight-bold">Justificacion (Contabilidad)</label>
										<textarea class="form-control" rows="5" disabled><?=$datos_cobro['justificacion_cont']?></textarea>
									</div>
									<?php
								}
								if ($datos_cobro['estado_contabilidad'] == 1) {
									?>
									<div class="col-lg-12 form-group">
										<hr>
										<h5 class="font-weight-bold text-center text-success text-uppercase">Documentacion</h5>
										<hr>
									</div>
									<div class="col-lg-4 form-group mt-2">
										<a href="<?=BASE_URL?>imprimir/cobro/cuentaCobro?cobro=<?=base64_encode($id_cobro)?>" target="_blank" class="btn btn-secondary btn-sm <?=$ver_archivo?>" style="margin-top: 6.5%;" data-tooltip="tooltip" title="Descargar Archivo" data-placement="bottom">
											<i class="fa fa-file-pdf"></i>
											&nbsp;
											Descargar Cuenta de cobro
										</a>
									</div>
									<div class="col-lg-12 form-group <?=$ver_documentos?>">
										<hr>
										<a href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['rut']?>" target="_blank" class="btn btn-primary btn-sm">
											Documento RUT
											&nbsp;
											<i class="fa fa-download"></i>
										</a>
										<a href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['cedula']?>" target="_blank" class="btn btn-primary btn-sm">
											Documento Cedula
											&nbsp;
											<i class="fa fa-download"></i>
										</a>
										<a href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['certificacion']?>" target="_blank" class="btn btn-primary btn-sm">
											Documento Certificacion Bancaria
											&nbsp;
											<i class="fa fa-download"></i>
										</a>
										<a href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['seguridad']?>" target="_blank" class="btn btn-primary btn-sm">
											Documento Seguridad Social
											&nbsp;
											<i class="fa fa-download"></i>
										</a>
									</div>
								<?php }?>
							</div>
							<div class="table-responsive mt-2">
								<table class="table table-hover border table-sm" width="100%" cellspacing="0">
									<thead>
										<tr class="text-center text-uppercase font-weight-bold">
											<th scope="col" colspan="12">Listado de asistencia</th>
										</tr>
										<tr class="text-center text-uppercase font-weight-bold">
											<th scope="col">Modulo</th>
											<th scope="col">Competencia</th>
											<th scope="col">Programa</th>
											<th scope="col">Sede</th>
											<th scope="col">Colegio</th>
											<th scope="col">Jornada</th>
											<th scope="col">Mes</th>
											<th scope="col">Dia de semana</th>
											<th scope="col">Fechas</th>
											<th scope="col">Ciclo</th>
											<th scope="col">Semestre</th>
											<th scope="col">Horas</th>
										</tr>
									</thead>
									<tbody class="buscar">
										<?php
										$sum_horas = 0;
										foreach ($datos_detalle as $detalle) {
											$id_detalle     = $detalle['id'];
											$modulo         = $detalle['modulo'];
											$competencia    = $detalle['competencia'];
											$programa       = $detalle['programa'];
											$sede           = $detalle['sede'];
											$jornada_id     = $detalle['jornada'];
											$colegio        = $detalle['colegio'];
											$ciclo          = $detalle['ciclo'];
											$semestre       = $detalle['semestre'];
											$horas          = $detalle['horas'];
											$id_asistencia  = $detalle['id_asistencia'];
											$jornada_nom    = $detalle['nom_jornada'];
											$mes_asistencia = mesesEspanol($detalle['mes_asistencia']);

											$sum_horas += $horas;

											$datos_dias        = $instancia->mostrarDiasSemanaAsistenciaControl($id_asistencia);
											$datos_dias_numero = $instancia->mostrarDiasSemanaNumeroControl($id_asistencia);

											$dias_semana        = '';
											$dias_semana_numero = '';

											foreach ($datos_dias as $dias) {
												$dias_semana .= diasEspanol($dias['dia']) . ', ';
											}

											foreach ($datos_dias_numero as $dias_numero) {
												$dias_semana_numero .= $dias_numero['dia'] . ', ';
											}

											?>
											<tr class="text-center fila_<?=$id_asistencia?>">
												<td><?=$modulo?></td>
												<td><?=$competencia?></td>
												<td><?=$programa?></td>
												<td><?=$sede?></td>
												<td><?=$colegio?></td>
												<td><?=$jornada_nom?></td>
												<td><?=$mes_asistencia?></td>
												<td><?=$dias_semana?></td>
												<td><?=$dias_semana_numero?></td>
												<td><?=$ciclo?></td>
												<td><?=$semestre?></td>
												<td><?=$horas?></td>
											</tr>
											<?php
										}
										?>
										<tr class="text-center font-weight-bold text-uppercase">
											<td colspan="11">Total Horas</td>
											<td><?=$sum_horas?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script src="<?=PUBLIC_PATH?>js/cobro/funcionesCobro.js"></script>