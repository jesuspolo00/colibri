<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

if (isset($_POST['buscar'])) {
	$buscar      = $_POST['buscar'];
	$fecha       = $_POST['fecha'];
	$datos_cobro = $instancia->buscarCuentaCobroControl($_POST['buscar'], $_POST['fecha']);
} else {
	$buscar      = '';
	$fecha       = '';
	$datos_cobro = $instancia->mostrarLimiteLibreCuentasCobroControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 43);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>distrito/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de Cuentas de cobro
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha de creacion" data-placement="top" value="<?=$fecha?>">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" value="<?=$buscar?>" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center text-uppercase font-weight-bold">
									<th scope="col">No. Cobro</th>
									<th scope="col">Nombre / Raz&oacute;n Social</th>
									<th scope="col">Documento / NIT</th>
									<th scope="col">A&ntilde;o</th>
									<th scope="col">Mes</th>
									<th scope="col">Valor</th>
									<th scope="col">Fecha Creacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_cobro as $cobro) {
									$id_cobro          = $cobro['id'];
									$usuario           = ($cobro['razon'] == '') ? $cobro['usuario'] : $cobro['razon'];
									$documento         = $cobro['documento'];
									$lugar             = $cobro['lugar'];
									$direccion         = $cobro['direccion'];
									$telefono          = $cobro['telefono'];
									$anio              = (!empty($cobro['anio_cuenta_asistencia'])) ? $cobro['anio_cuenta_asistencia'] : 'N/A';
									$mes               = $cobro['mes_cuenta_asistencia'];
									$valor             = number_format($cobro['valor']);
									$cuenta_detalle    = $cobro['cuenta_detalle'];
									$cuenta_documentos = $cobro['cuenta_documentos'];
									$estado            = $cobro['estado'];
									$fechareg          = $cobro['fechareg'];

									$mes_asistencia = (!empty($mes)) ? mesesEspanol($mes) : 'N/A';

									if ($cuenta_detalle == 0 && $cuenta_documentos == 0) {
										$ver_detalle    = '';
										$ver_documentos = 'd-none';
									}

									if ($cuenta_detalle == 1 && $cuenta_documentos == 0) {
										$ver_detalle    = 'd-none';
										$ver_documentos = '';
									}

									if ($cuenta_detalle == 1 && $cuenta_documentos == 1) {
										$ver_detalle    = 'd-none';
										$ver_documentos = 'd-none';
									}

									if ($estado == 0) {
										$ver_archivo = 'd-none';
										$span_estado = '<span class="badge badge-secondary">Aún faltan completar pasos</span>';
									}

									if ($estado == 1) {
										$ver_archivo = 'd-none';
										$span_estado = '<span class="badge badge-warning">Pendiente de aprobacion</span>';
									}

									if ($estado == 2) {
										$ver_archivo = '';
										$span_estado = '<span class="badge badge-success">Aprobado</span>';
									}

									if ($estado == 3) {
										$ver_archivo = 'd-none';
										$span_estado = '<span class="badge badge-danger">Denegado</span>';
									}

									?>
									<tr class="text-center">
										<td><?=$id_cobro?></td>
										<td><?=$usuario?></td>
										<td><?=$documento?></td>
										<td><?=$anio?></td>
										<td><?=$mes_asistencia?></td>
										<td>$<?=$valor?></td>
										<td><?=date('Y-m-d', strtotime($fechareg))?></td>
										<td><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>distrito/cobro/detalle?cobro=<?=base64_encode($id_cobro)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalle" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>