<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia = ControlRecibos::singleton_recibos();

$datos_metodos = $instancia->mostrarTipoPagoAprobacionControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 94);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Metodos de pago
					</h4>
					<div class="btn-group">
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_metodo_pago">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar metodo de pago
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-4"></div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<div class="input-group mb-3">
								<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
								<div class="input-group-append">
									<button class="btn btn-success btn-sm" type="submit">
										<i class="fa fa-search"></i>
										&nbsp;
										Buscar
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th>Metodo de pago</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody class="buscar text-center">
								<?php
								foreach ($datos_metodos as $metodos) {
									$id_metodo  = $metodos['id'];
									$nom_metodo = $metodos['nombre'];
									$estado     = $metodos['activo'];

									$span_estado = ($estado == 0) ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>';
									?>
									<tr>
										<td><?=$nom_metodo?></td>
										<td><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<button type="button" class="btn btn-secondary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#editar_<?=$id_metodo?>">
													<i class="fas fa-edit"></i>
												</button>
											</div>
										</td>
									</tr>

									<div class="modal fade" id="editar_<?=$id_metodo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Editar Metodo de pago</h5>
												</div>
												<div class="modal-body">
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_metodo" value="<?=$id_metodo?>">
														<div class="row p-2">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Metodo de pago <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="nombre_edit" required value="<?=$nom_metodo?>">
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Estado</label>
																<select class="form-control" name="estado">
																	<option value="1">Activo</option>
																	<option value="0">Inactivo</option>
																</select>
															</div>
															<div class="col-lg-12 form-group mt-4 text-right">
																<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																	<i class="fas fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fas fa-sync"></i>
																	&nbsp;
																	Actualizar
																</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require_once VISTA_PATH . 'modulos' . DS . 'metodo_pago' . DS . 'agregarMetodoPago.php';
require_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nombre'])) {
	$instancia->registrarMetodoPagoControl();
}

if (isset($_POST['nombre_edit'])) {
	$instancia->editarMetodoPagoControl();
}