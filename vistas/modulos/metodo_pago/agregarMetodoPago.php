<div class="modal fade" id="agregar_metodo_pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Metodo de pago</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Metodo de pago <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" required>
            </div>
            <div class="col-lg-12 form-group text-right mt-4">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fas fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-success btn-sm" type="submit">
                <i class="fas fa-save"></i>
                &nbsp;
                Registrar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
