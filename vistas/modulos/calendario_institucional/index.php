<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$datos_tipo = $instancia->mostrarTipoPlanControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 77);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Calendario Institucional
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>calendario_institucional/listado" class="btn btn-info btn-sm">
							<i class="fas fa-list"></i>
							&nbsp;
							Lista de calendario institucional
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row p-2">
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Linea de formaci&oacute;n <span class="text-danger">*</span></label>
								<textarea class="form-control" name="linea" rows="5"></textarea>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Temas</label>
								<textarea class="form-control" name="temas" rows="5"></textarea>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de plan <span class="text-danger">*</span></label>
								<select name="tipo" class="form-control" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_tipo as $tipo) {
										$id_tipo  = $tipo['id'];
										$nom_tipo = $tipo['nombre'];
										?>
										<option value="<?=$id_tipo?>"><?=$nom_tipo?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Poblaci&oacute;n Objetivo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="objetivo" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Recursos fisicos <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="recursos" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Responsable <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$nom_log_session?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha a ejecutar <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_ejecutar" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hora a ejecutar</label>
								<input type="time" class="form-control clockpicker" name="hora">
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->guardarPlanFormacionControl();
}