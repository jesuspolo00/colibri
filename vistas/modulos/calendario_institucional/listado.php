<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlAsistencia::singleton_asistencia();
$instancia_usuario = ControlUsuario::singleton_usuario();

$datos_usuario = $instancia_usuario->mostrarUsuariosControl();

if (isset($_POST['buscar'])) {
	$datos      = array('usuario' => $_POST['usuario'], 'fecha' => $_POST['fecha'], 'buscar' => $_POST['buscar'], 'arte' => $_POST['arte_filtro']);
	$datos_plan = $instancia->buscarPlanFormacionControl($datos);
} else {
	$datos_plan = $instancia->mostrarPlanFormacionControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 77);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>calendario_institucional/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Lista de calendario institucional
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/plan_formacion/listadoExcel" target="_blank" class="btn btn-success btn-sm">
							<i class="fas fa-file-excel"></i>
							&nbsp;
							Exportar EXCEL
						</a>
						<a href="<?=BASE_URL?>imprimir/plan_formacion/listadoPDF" target="_blank" class="btn btn-primary btn-sm">
							<i class="fas fa-file-pdf"></i>
							&nbsp;
							Exportar PDF
						</a>
						<a href="<?=BASE_URL?>calendario_institucional/index" class="btn btn-secondary btn-sm">
							<i class="fas fa-plus"></i>
							&nbsp;
							Agregar plan
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-3 form-group">
								<select name="usuario" class="form-control">
									<option value="" selected>Seleccione un usuario...</option>
									<?php
									foreach ($datos_usuario as $usuario) {
										$id_usuario = $usuario['id_user'];
										$nom_user   = $usuario['nombre'] . ' ' . $usuario['apellido'];
										?>
										<option value="<?=$id_usuario?>"><?=$nom_user?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-3 form-group">
								<select class="form-control" data-tooltip="tooltip" title="Arte" name="arte_filtro">
									<option select value="">Seleccione una opcion...</option>
									<option value="1">Arte Realizado</option>
									<option value="2">Arte no realizado</option>
								</select>
							</div>
							<div class="col-lg-3 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha">
							</div>
							<div class="col-lg-3 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm">
							<thead class="text-center">
								<tr>
									<th>No.</th>
									<th>Tipo de plan</th>
									<th>Linea de formaci&oacute;n</th>
									<th>Temas</th>
									<th>Pobalci&oacute;n objetivo</th>
									<th>Recursos Fisicos</th>
									<th>Responsable</th>
									<th>Fecha proyectada</th>
									<th>Hora proyectada</th>
									<th>Fecha ejecutada / cancelada</th>
									<th>Arte</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody class="buscar text-center">
								<?php
								foreach ($datos_plan as $plan) {
									$id_plan          = $plan['id'];
									$nom_tipo         = $plan['nom_tipo'];
									$linea            = $plan['linea'];
									$temas            = $plan['temas'];
									$objetivo         = $plan['objetivo'];
									$recursos         = $plan['recursos'];
									$responsable      = $plan['nom_usuario'];
									$fecha_proyectada = $plan['fecha_proyectada'];
									$hora_proyectada  = $plan['hora_proyectada'];
									$fecha_ejecutada  = $plan['fecha_ejecutada'];
									$arte             = $plan['arte'];

									$span_estado = ($plan['activo'] == 1) ? '<span class="badge badge-warning">Pendiente de ejecuci&oacute;n</span>' : '<span class="badge badge-success">Ejecutado</span>';
									$span_estado = ($plan['activo'] == 0) ? '<span class="badge badge-danger">Cancelado</span>' : $span_estado;

									$span_arte = ($arte == 2) ? '<span class="badge badge-danger">Arte no realizado</span>' : '<span class="badge badge-success">Arte realizado</span>';

									$ver_check_arte = ($arte == 2) ? '' : 'd-none';
									$ver_times_arte = ($arte == 1) ? '' : 'd-none';

									?>
									<tr class="plan_formacion_<?=$id_plan?>">
										<td><?=$id_plan?></td>
										<td><?=$nom_tipo?></td>
										<td><?=$linea?></td>
										<td><?=$temas?></td>
										<td><?=$objetivo?></td>
										<td><?=$recursos?></td>
										<td><?=$responsable?></td>
										<td><?=$fecha_proyectada?></td>
										<td><?=date('H:i:s A', strtotime($hora_proyectada))?></td>
										<td><?=$fecha_ejecutada?></td>
										<td class="span_arte_<?=$id_plan?>"><?=$span_arte?></td>
										<td><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>calendario_institucional/detalles?id=<?=base64_encode($id_plan)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
											</div>
										</td>
										<?php
										$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 90);
										if ($permiso) {
											?>
											<td>
												<div class="btn-group">
													<button class="btn btn-success btn-sm arte_realizado check_<?=$id_plan?> <?=$ver_check_arte?>" type="button" id="<?=$id_plan?>" data-tooltip="tooltip" data-placement="bottom" title="Arte realizado">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-danger btn-sm arte_no_realizado times_<?=$id_plan?> <?=$ver_times_arte?>" type="button" id="<?=$id_plan?>" data-tooltip="tooltip" data-placement="bottom" title="Arte no realizado">
														<i class="fas fa-times"></i>
													</button>
												</div>
											</td>
										<?php }?>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=BASE_URL?>public/js/plan_formacion/arte.js"></script>