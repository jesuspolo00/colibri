<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

if (isset($_GET['id'])) {

	$id_plan = base64_decode($_GET['id']);

	$datos_plan       = $instancia->mostrarDetallesPlanFormacionControl($id_plan);
	$datos_asistencia = $instancia->mostrarAsistenciaPlanFormacionControl($id_plan);

	$nom_estado = ($datos_plan['activo'] == 1) ? 'Pendiente de ejecuci&oacute;n' : 'Ejecutado';
	$nom_estado = ($datos_plan['activo'] == 0) ? 'Cancelado' : $nom_estado;

	$bg_estado = ($datos_plan['activo'] == 1) ? 'bg-warning' : 'bg-success';
	$bg_estado = ($datos_plan['activo'] == 0) ? 'bg-danger' : $bg_estado;

	$nom_arte = ($datos_plan['arte'] == 1) ? 'Arte realizado' : 'Arte no realizado';
	$bg_arte  = ($datos_plan['arte'] == 0) ? 'bg-success' : 'bg-danger';

	$fecha_titulo = ($datos_plan['activo'] == 0) ? 'Cancelado' : 'Ejecutado';
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 77);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>calendario_institucional/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Detalles del plan No. (<?=$id_plan?>)
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>calendario_institucional/listado" class="btn btn-info btn-sm">
							<i class="fas fa-list"></i>
							&nbsp;
							Lista de calendario institucional
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="row p-2">
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Linea de formaci&oacute;n <span class="text-danger">*</span></label>
							<textarea class="form-control" disabled rows="5"><?=$datos_plan['linea']?></textarea>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Temas</label>
							<textarea class="form-control" disabled rows="5"><?=$datos_plan['temas']?></textarea>
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Tipo de plan</label>
							<input type="text" class="form-control" disabled value="<?=$datos_plan['nom_tipo']?>">
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Poblaci&oacute;n Objetivo <span class="text-danger">*</span></label>
							<input type="text" class="form-control" disabled value="<?=$datos_plan['objetivo']?>">
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Recursos fisicos <span class="text-danger">*</span></label>
							<input type="text" class="form-control" value="<?=$datos_plan['linea']?>" disabled>
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Responsable <span class="text-danger">*</span></label>
							<input type="text" class="form-control" value="<?=$datos_plan['nom_usuario']?>" disabled>
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Fecha proyectada <span class="text-danger">*</span></label>
							<input type="date" class="form-control" disabled value="<?=$datos_plan['fecha_proyectada']?>">
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Hora proyectada <span class="text-danger">*</span></label>
							<input type="time" class="form-control" disabled value="<?=$datos_plan['hora_proyectada']?>">
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Estado</label>
							<input type="text" class="form-control <?=$bg_estado?> text-white" disabled value="<?=$nom_estado?>">
						</div>
						<div class="col-lg-4 form-group">
							<label class="font-weight-bold">Diseño de Arte</label>
							<input type="text" class="form-control <?=$bg_arte?> text-white" disabled value="<?=$nom_arte?>">
						</div>
						<?php
						if ($datos_plan['activo'] != 1) {
							?>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha <?=$fecha_titulo?></label>
								<input type="date" class="form-control" value="<?=$datos_plan['fecha_ejecutada']?>" disabled>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observaci&oacute;n</label>
								<textarea class="form-control" rows="5" disabled><?=$datos_plan['observacion']?></textarea>
							</div>
						<?php }?>
						<div class="col-lg-12 form-group mt-2">
							<a href="<?=BASE_URL?>imprimir/plan_formacion/codigoQR?id=<?=base64_encode($id_plan)?>" target="_blank" class="btn btn-primary btn-sm">
								<i class="fas fa-qrcode"></i>
								&nbsp;
								Descargar codigo QR
							</a>
						</div>
						<?php
						if ($datos_plan['activo'] == 1) {
							?>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#cancelar_plan">
									<i class="fa fa-times"></i>
									&nbsp;
									No ejecutada / Cancelada
								</button>
								<button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#ejecutar_plan">
									<i class="fa fa-check"></i>
									&nbsp;
									Ejecutada
								</button>
							</div>
						<?php }?>
					</div>
					<div class="table-responsive">
						<table class="table table-sm border">
							<thead class="text-center">
								<tr class="text-uppercase">
									<th colspan="6">Detalles asistencia plan de formaci&oacute;n</th>
								</tr>
								<tr>
									<th>Documento</th>
									<th>Nombre Completo</th>
									<th>Telefono</th>
									<th>Correo electronico</th>
									<th>Fecha</th>
									<th>Hora</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<?php
								foreach ($datos_asistencia as $asistencia) {
									$id_asistencia    = $asistencia['id'];
									$nom_usuario      = $asistencia['nom_usuario'];
									$documento        = $asistencia['documento'];
									$telefono         = $asistencia['telefono'];
									$correo           = $asistencia['correo'];
									$fecha_asistencia = date('Y-m-d', strtotime($asistencia['fechareg']));
									$hora_asistencia  = date('H:i:s A', strtotime($asistencia['fechareg']));
									?>
									<tr>
										<td><?=$documento?></td>
										<td><?=$nom_usuario?></td>
										<td><?=$telefono?></td>
										<td><?=$correo?></td>
										<td><?=$fecha_asistencia?></td>
										<td><?=$hora_asistencia?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="ejecutar_plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Plan de formaci&oacute;n - ejecutado</h5>
			</div>
			<div class="modal-body">
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="id_plan" value="<?=$id_plan?>">
					<input type="hidden" name="estado" value="2">
					<div class="row p-2">
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Fecha ejecutada <span class="text-danger">*</span></label>
							<input type="date" class="form-control" name="fecha" required>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Observaci&oacute;n</label>
							<textarea class="form-control" rows="5" name="observacion"></textarea>
						</div>
						<div class="col-lg-12 form-group mt-2 text-right">
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
								<i class="fas fa-times"></i>
								&nbsp;
								Cancelar
							</button>
							<button class="btn btn-success btn-sm" type="submit">
								<i class="fas fa-sync"></i>
								Actualizar estado
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="cancelar_plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Plan de formaci&oacute;n - cancelar</h5>
			</div>
			<div class="modal-body">
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="id_plan" value="<?=$id_plan?>">
					<input type="hidden" name="estado" value="0">
					<div class="row p-2">
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Fecha cancelada <span class="text-danger">*</span></label>
							<input type="date" class="form-control" name="fecha" required>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Observaci&oacute;n</label>
							<textarea class="form-control" rows="5" name="observacion"></textarea>
						</div>
						<div class="col-lg-12 form-group mt-2 text-right">
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
								<i class="fas fa-times"></i>
								&nbsp;
								Cancelar
							</button>
							<button class="btn btn-success btn-sm" type="submit">
								<i class="fas fa-sync"></i>
								Actualizar estado
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->actualizarEstadoPlanFormacionControl();
}