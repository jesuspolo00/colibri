<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'messages.php';

$instancia = ControlAsistencia::singleton_asistencia();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 77);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['token'])) {

	$token = base64_decode($_GET['token']);

	$datos_plan = $instancia->mostrarPlanFormacionDatosTokenControl($token);

	if ($datos_plan['activo'] != 1) {
		$title   = 'Asistencia no valida';
		$message = 'El enlace de asistencia de este plan de formacion ya ha caducado!';
		$enlace  = BASE_URL . 'lector' . DS . 'qr';
		alertDanger($title, $message, $enlace);
		die();
	} else {
		$asistencia = $instancia->tomarAsistenciaPlanFormacionControl($datos_plan['id'], $id_log);
	}

}
