<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$anio = (isset($_POST['anio']) && !empty($_POST['anio'])) ? $_POST['anio'] : date('Y');

if (isset($_POST['buscar'])) {
	$datos_usuarios = $instancia->buscarUsuariosHojaVidaControl($_POST['buscar']);
} else {
	$datos_usuarios = $instancia->mostrarLimiteUsuariosHojaVidaControl();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/recursos" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Evaluaciones realizadas
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>hoja/listado" class="btn btn-info btn-sm">
							<i class="fas fa-list"></i>
							&nbsp;
							Listado de hojas de vida
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group"></div>
							<div class="col-lg-4 form-group">
								<select name="anio" class="form-control">
									<option value="<?=$anio?>" selected><?=$anio?></option>
									<?php
									for ($i = date('Y') + 1; $i <= 2050; $i++) {
										?>
										<option value="<?=$i?>"><?=$i?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<?php
					if (isset($_POST['buscar'])) {
						?>
						<div class="col-lg-12 form-group text-right">
							<a href="<?=BASE_URL?>imprimir/evaluacion/evaluacionExcel?buscar=<?=$_POST['buscar']?>&anio=<?=$_POST['anio']?>" class="btn btn-secondary btn-sm" target="_blank">
								<i class="fas fa-file-excel"></i>
								&nbsp;
								Descargar Excel
							</a>
						</div>
					<?php }?>
					<div class="table-responsive">
						<table class="table table-hover table-sm border">
							<thead class="text-center">
								<th>Documento</th>
								<th>Nombre completo</th>
								<th>Correo</th>
								<th>Perfil</th>
								<th>Evaluaci&oacute;n Docente</th>
								<th>Puntuaci&oacute;n</th>
								<th>Evaluaci&oacute;n Administrativa</th>
								<th>Puntuaci&oacute;n</th>
							</thead>
							<tbody class="buscar text-center">
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user     = $usuario['id_user'];
									$nom_usuario = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$documento   = $usuario['documento'];
									$correo      = $usuario['correo'];
									$perfil      = $usuario['nom_perfil'];

									$anio_buscar = (isset($_POST['anio']) && !empty($_POST['anio'])) ? $_POST['anio'] : date('Y');

									$evaluacion_anual_administrativa = $instancia->mostrarPuntajeEvaluacionAnioControl($id_user, $anio_buscar, 1);
									$evaluacion_anual_docente        = $instancia->mostrarPuntajeEvaluacionAnioControl($id_user, $anio_buscar, 2);

									$punt_docente        = (!empty($evaluacion_anual_docente['suma_puntuacion'])) ? $evaluacion_anual_docente['suma_puntuacion'] / $evaluacion_anual_docente['cant_preguntas'] : 'N/A';
									$punt_administrativa = (!empty($evaluacion_anual_administrativa['suma_puntuacion'])) ? $evaluacion_anual_administrativa['suma_puntuacion'] / $evaluacion_anual_administrativa['cant_preguntas'] : 'N/A';

									$puntuacion_docente        = (!empty($evaluacion_anual_docente)) ? '<span class="badge badge-info">' . number_format($punt_docente, 2) . '</span>' : '<span class="badge badge-secondary">N/A</span>';
									$puntuacion_administrativa = (!empty($evaluacion_anual_administrativa)) ? '<span class="badge badge-info">' . number_format($punt_administrativa, 2) . '</span>' : '<span class="badge badge-secondary">N/A</span>';

									$span_docente        = (!empty($evaluacion_anual_docente)) ? '<span class="badge badge-success">Realizada - ' . $evaluacion_anual_docente['anio'] . '</span>' : '<span class="badge badge-secondary">No aplica</span>';
									$span_administrativo = (!empty($evaluacion_anual_administrativa)) ? '<span class="badge badge-success">Realizada - ' . $evaluacion_anual_administrativa['anio'] . '</span>' : '<span class="badge badge-secondary">No aplica</span>';
									?>
									<tr>
										<td><?=$documento?></td>
										<td><?=$nom_usuario?></td>
										<td><?=$correo?></td>
										<td><?=$perfil?></td>
										<td><?=$span_docente?></td>
										<td><?=$puntuacion_docente?></td>
										<td><?=$span_administrativo?></td>
										<td><?=$puntuacion_administrativa?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>