<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
?>
<div class="container-fluid">
	<div class="row p-2">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-success">
							<i class="fas fa-arrow-left"></i>
						</a>
						&nbsp;
						Lector - codigo de barras
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12 form-group">
							<div id="qrcode">
								<video id="video" playsinline autoplay class="w-100 h-50"></video>
							</div>
						</div>
						<div class="col-lg-12 form-group text-center">
							<button id="btn" class="btn btn-success btn-sm">
								<i class="fa fa-camera"></i>
								&nbsp;
								Escanear codgio
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/lector/funcionesLectorQr.js"></script>