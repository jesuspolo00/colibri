<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_programa = base64_decode($_GET['id']);

	$datos_programas_todos = $instancia->mostrarProgramasAsistenciaControl();

	$datos_programas   = $instancia->mostrarDatosProgramaIdControl($id_programa);
	$datos_competencia = $instancia->competenciasProgramaControl($id_programa);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>programa/listado" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Programa - (<?=$datos_programas['nombre']?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_programa" value="<?=$id_programa?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Programa <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="nom_programa" value="<?=$datos_programas['nombre']?>" required>
								</div>
								<div class="col-lg-12 form-group text-right mt-2">
									<button class="btn btn-success btn-sm" type="submit">
										<i class="fa fa-sync"></i>
										&nbsp;
										Actualizar programa
									</button>
								</div>
							</div>
						</form>

						<div class="col-lg-12 form-group text-right">
							<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_competencia">
								<i class="fa fa-plus"></i>
								&nbsp;
								Agregar competencia
							</button>
							<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_modulo">
								<i class="fa fa-plus"></i>
								&nbsp;
								Agregar asignatura
							</button>
						</div>

						<div class="col-lg-12 form-group mt-2">
							<h4 class="font-weight-bold text-success">Competencias</h4>
							<div class="accordion" id="accordionExample">
								<?php
								foreach ($datos_competencia as $competencia) {
									$id_competencia  = $competencia['id'];
									$nom_competencia = $competencia['nombre'];

									$datos_modulos = $instancia->modulosCompetenciaControl($id_competencia);

									if (count($datos_modulos) != 0) {
										?>
										<div class="card">
											<div class="card-header bg-transparent" id="headingOne">
												<div class="row">
													<div class="col-lg-10 form-group">
														<button class="btn btn-link text-left btn-block font-weight-bold" type="button" data-toggle="collapse" data-target="#comp_<?=$id_competencia?>">
															<?=$nom_competencia?>
														</button>
													</div>
													<div class="col-lg-2 form-group">
														<div class="btn-group float-right">
															<button class="btn btn-secondary btn-sm" type="button" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-toggle="modal" data-target="#editar_<?=$id_competencia?>">
																<i class="fas fa-edit"></i>
															</button>
														</div>
													</div>
												</div>
											</div>

											<div id="comp_<?=$id_competencia?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
												<div class="card-body">
													<div class="table-responsive">
														<table class="table table-hover border table-sm  mt-4" width="100%" cellspacing="0">
															<thead>
																<tr class="text-center text-dark">
																	<th>Asignatura</th>
																	<th>Horas teoricas</th>
																	<th>Horas practicas</th>
																	<th>Horas totales</th>
																	<th>Estado</th>
																</tr>
															</thead>
															<tbody class="text-center">

																<?php
																foreach ($datos_modulos as $modulos) {
																	$id_modulo             = $modulos['id'];
																	$nom_modulo            = $modulos['nombre'];
																	$horas_teoricas        = str_replace(',', '.', $modulos['horas_teoricas']);
																	$horas_practicas       = str_replace(',', '.', $modulos['horas_practicas']);
																	$horas_totales         = floatval($horas_teoricas) + floatval($horas_practicas);
																	$id_competencia_modulo = $modulos['id_competencia'];

																	$span_estado = ($modulos['activo'] == 0) ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>';

																	$ver_activo   = ($modulos['activo'] == 0) ? '' : 'd-none';
																	$ver_inactivo = ($modulos['activo'] == 1) ? '' : 'd-none';

																	?>
																	<tr>
																		<td><?=$nom_modulo?></td>
																		<td><?=$horas_teoricas?></td>
																		<td><?=$horas_practicas?></td>
																		<td><?=$horas_totales?></td>
																		<td id="fila_span_<?=$id_modulo?>"><?=$span_estado?></td>
																		<td>
																			<div class="btn-group">
																				<button class="btn btn-secondary btn-sm" type="button" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-toggle="modal" data-target="#editar_modulo_<?=$id_modulo?>">
																					<i class="fas fa-edit"></i>
																				</button>
																				<button class="btn btn-danger btn-sm inactivar_asig <?=$ver_inactivo?>" id="inact_<?=$id_modulo?>" data-id="<?=$id_modulo?>" data-estado="0" data-tooltip="tooltip" data-placement="bottom" title="Inactivar" type="button">
																					<i class="fas fa-times"></i>
																				</button>
																				<button class="btn btn-success btn-sm activar_asig <?=$ver_activo?>" id="act_<?=$id_modulo?>" data-id="<?=$id_modulo?>" data-estado="1" type="button" data-tooltip="tooltip" title="Activar" data-placement="bottom">
																					<i class="fas fa-check"></i>
																				</button>
																			</div>
																		</td>
																	</tr>


																	<div class="modal fade" id="editar_modulo_<?=$id_modulo?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog modal-lg">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="text-success font-weight-bold" id="exampleModalLabel">Editar Asignatura</h5>
																				</div>
																				<div class="modal-body">
																					<form method="POST">
																						<input type="hidden" name="id_modulo" value="<?=$id_modulo?>">
																						<input type="hidden" name="id_log" value="<?=$id_log?>">
																						<input type="hidden" name="programa" value="<?=$datos_programas['id']?>">
																						<div class="row p-2">
																							<div class="col-lg-12 form-group">
																								<label class="font-weight-bold">Programa <span class="text-danger">*</span></label>
																								<input type="text" class="form-control" disabled value="<?=$datos_programas['nombre']?>">
																							</div>
																							<div class="col-lg-12 form-group">
																								<label class="font-weight-bold">Competencia <span class="text-danger">*</span></label>
																								<select name="competencia" class="form-control">
																									<?php
																									foreach ($datos_competencia as $comp) {
																										$id_comp = $comp['id'];
																										$nom_cop = $comp['nombre'];

																										$select_comp = ($id_competencia == $id_comp) ? 'selected' : '';
																										?>
																										<option value="<?=$id_comp?>" <?=$select_comp?>><?=$nom_cop?></option>
																									<?php }?>
																								</select>
																							</div>
																							<div class="col-lg-6 form-group">
																								<label class="font-weight-bold">Asignatura <span class="text-danger">*</span></label>
																								<input type="text" class="form-control" name="nom_modulo_edit" required value="<?=$nom_modulo?>">
																							</div>
																							<div class="col-lg-6 form-group">
																								<label class="font-weight-bold">Horas teoricas</label>
																								<input type="text" class="form-control" name="horas_teoricas" value="<?=$horas_teoricas?>">
																							</div>
																							<div class="col-lg-6 form-group">
																								<label class="font-weight-bold">Horas practicas</label>
																								<input type="text" class="form-control" name="horas_practicas" value="<?=$horas_practicas?>">
																							</div>
																							<div class="col-lg-6 form-group">
																								<label class="font-weight-bold">Estado <span class="text-danger">*</span></label>
																								<select class="form-control" required name="estado">
																									<?php
																									$select_no = ($modulos['activo'] == 0) ? 'selected' : '';
																									$select_si = ($modulos['activo'] == 1) ? 'selected' : '';
																									?>
																									<option value="1" <?=$select_si?>>Activo</option>
																									<option value="0" <?=$select_no?>>Inactivo</option>
																								</select>
																							</div>
																							<div class="col-lg-12 form-group mt-2 text-right">
																								<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																									<i class="fas fa-times"></i>
																									&nbsp;
																									Cancelar
																								</button>
																								<button class="btn btn-success btn-sm" type="submit">
																									<i class="fas fa-sync"></i>
																									&nbsp;
																									Actualizar
																								</button>
																							</div>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>

																<?php }?>
															</tbody>
														</table>
													</div>
												</div>
											</div>


											<div class="modal fade" id="editar_<?=$id_competencia?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="text-success font-weight-bold" id="exampleModalLabel">Editar competencia</h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_competencia" value="<?=$id_competencia?>">
																<input type="hidden" name="programa_old" value="<?=$datos_programas['id']?>">
																<div class="row p-2">
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Programa <span class="text-danger">*</span></label>
																		<select name="programa_edit" class="form-control">
																			<?php
																			foreach ($datos_programas_todos as $programa) {
																				$id_program   = $programa['id'];
																				$nom_programa = $programa['nombre'];

																				$select_program = ($datos_programas['id'] == $id_program) ? 'selected' : '';

																				?>
																				<option value="<?=$id_program?>" <?=$select_program?>><?=$nom_programa?></option>
																			<?php }?>
																		</select>
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Competencia <span class="text-danger">*</span></label>
																		<textarea class="form-control" required name="nom_competencia_edit"><?=$nom_competencia?></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-2 text-right">
																		<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																			<i class="fas fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-success btn-sm">
																			<i class="fas fa-sync"></i>
																			&nbsp;
																			Actualizar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>


										</div>
									<?php }}?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		require_once VISTA_PATH . 'modulos' . DS . 'programa' . DS . 'agregarCompetencia.php';
		require_once VISTA_PATH . 'modulos' . DS . 'programa' . DS . 'agregarModulo.php';
		include_once VISTA_PATH . 'script_and_final.php';

		if (isset($_POST['nom_programa'])) {
			$instancia->editarProgramaControl();
		}

		if (isset($_POST['nom_competencia'])) {
			$instancia->guardarCompetenciaControl();
		}

		if (isset($_POST['nom_competencia_edit'])) {
			$instancia->editarCompetenciaControl();
		}

		if (isset($_POST['nom_modulo'])) {
			$instancia->guardarModuloControl();
		}

		if (isset($_POST['id_modulo'])) {
			$instancia->editarModuloControl();
		}
	}
	?>
	<script src="<?=PUBLIC_PATH?>js/asistencia/funcionesAsistencia.js"></script>