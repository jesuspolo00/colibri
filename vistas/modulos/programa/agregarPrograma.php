<div class="modal fade" id="agregar_programa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Programa</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
           <div class="col-lg-12 form-group">
             <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
             <input type="text" class="form-control" name="nom_programa" required>
           </div>
           <div class="col-lg-12 form-group text-right mt-2">
             <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
               <i class="fa fa-times"></i>
               &nbsp;
               Cancelar
             </button>
             <button class="btn btn-success btn-sm" type="submit">
               <i class="fa fa-save"></i>
               &nbsp;
               Guardar
             </button>
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>
