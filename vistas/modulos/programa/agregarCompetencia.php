<div class="modal fade" id="agregar_competencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Competencia</h5>
			</div>
			<div class="modal-body">
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="programa" value="<?=$datos_programas['id']?>">
					<div class="row p-2">
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Programa</label>
							<input type="text" class="form-control" disabled value="<?=$datos_programas['nombre']?>">
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Nombre de la competencia <span class="text-danger">*</span></label>
							<textarea type="text" class="form-control" rows="5" name="nom_competencia" required></textarea>
						</div>
						<div class="col-lg-12 form-group text-right mt-2">
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
								<i class="fa fa-times"></i>
								&nbsp;
								Cancelar
							</button>
							<button class="btn btn-success btn-sm" type="submit">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>