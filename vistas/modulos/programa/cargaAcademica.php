<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:../login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia         = ControlAsistencia::singleton_asistencia();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_cobro   = ControlCobro::singleton_cobro();

$datos_programas = $instancia->mostrarProgramasAsistenciaControl();
$datos_usuarios  = $instancia_usuario->mostrarProfesoresControl();
$datos_jornada   = $instancia_cobro->mostrarJornadasControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
if (!$permiso) {
  include_once VISTA_PATH . 'modulos' . DS . '403.php';
  exit();
}
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow-sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h4 class="m-0 font-weight-bold text-success">
            <a href="<?=BASE_URL?>programa/index" class="text-decoration-none">
              <i class="fa fa-arrow-left text-success"></i>
            </a>
            &nbsp;
            Asignar carga Academica
          </h4>
        </div>
        <div class="card-body">
          <form method="POST">
            <div class="row p-2">
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Programas <span class="text-danger">*</span></label>
                <select class="form-control" required name="programa" id="programa">
                  <option value="" selected>Seleccione una opcion...</option>
                  <?php
                  foreach ($datos_programas as $programa) {
                    $id_programa  = $programa['id'];
                    $nom_programa = $programa['nombre'];
                    ?>
                    <option value="<?=$id_programa?>"><?=$nom_programa?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Competencias <span class="text-danger">*</span></label>
                <select class="form-control" required name="competencias" id="competencias">
                  <option value="" selected>Seleccione una opcion...</option>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Asignaturas <span class="text-danger">*</span></label>
                <select class="form-control" required name="modulos" id="modulos">
                  <option value="" selected>Seleccione una opcion...</option>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
                <select name="profesor" class="form-control">
                  <option value="" selected>Seleccione una opcion...</option>
                  <?php
                  foreach ($datos_usuarios as $usuario) {
                    $id_usuario  = $usuario['id_user'];
                    $nom_usuario = $usuario['nombre'] . ' ' . $usuario['apellido'];
                    ?>
                    <option value="<?=$id_usuario?>"><?=$nom_usuario?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Tipo de asignaci&oacute;n <span class="text-danger">*</span></label>
                <select class="form-control" name="tipo" required>
                  <option value="" selected>Seleccione una opcion...</option>
                  <option value="1">Distrito</option>
                  <option value="2">Codetec</option>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Jornada <span class="text-danger">*</span></label>
                <select name="jornada" class="form-control" required>
                  <option value="" selected>Seleccione una opcion...</option>
                  <?php
                  foreach ($datos_jornada as $jornada) {
                    $id_jornada  = $jornada['id'];
                    $nom_jornada = $jornada['nombre'];
                    ?>
                    <option value="<?=$id_jornada?>"><?=$nom_jornada?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">A&ntilde;o <span class="text-danger">*</span></label>
                <select name="anio" class="form-control" required>
                  <option value="">Seleccione una opcion...</option>
                  <option value="<?=date('Y')?>" selected><?=date('Y')?></option>
                  <?php
                  for ($i = date('Y') + 1; $i <= 2050; $i++) {
                    ?>
                    <option value="<?=$i?>"><?=$i?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Mes Inicio (Semestre)<span class="text-danger">*</span></label>
                <select name="mes_inicio" class="form-control" required>
                  <option value="" selected>Seleccione una opcion...</option>
                  <option value="1">Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Mes Fin (Semestre)<span class="text-danger">*</span></label>
                <select name="mes_fin" class="form-control" required>
                  <option value="" selected>Seleccione una opcion...</option>
                  <option value="1">Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Sede (Opcional)</label>
                <input type="text" name="sede" class="form-control">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Colegio (Opcional)</label>
                <input type="text" name="colegio" class="form-control">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Ciclo (Opcional)</label>
                <input type="text" name="ciclo" class="form-control">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas teoricas</label>
                <input type="text" class="form-control horas" disabled id="horas_teoricas">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas practicas</label>
                <input type="text" class="form-control horas" disabled id="horas_practicas">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas totales</label>
                <input type="text" class="form-control horas" disabled id="horas_totales">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas teoricas (Asignar)</label>
                <input type="number" step="any" class="form-control horas" id="horas_teoricas_asig" required>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas practicas (Asignar)</label>
                <input type="number" step="any" class="form-control horas" id="horas_practicas_asig" required>
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas totales (Asignar)</label>
                <input type="text" disabled class="form-control horas" id="horas_totales_asig">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas teoricas (Restantes)</label>
                <input type="text" class="form-control horas" disabled id="horas_teoricas_rest">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas practicas (Restantes)</label>
                <input type="text" class="form-control horas" disabled id="horas_practicas_rest">
              </div>
              <div class="col-lg-4 form-group">
                <label class="font-weight-bold">Horas totales (Restantes)</label>
                <input type="text" class="form-control horas" disabled id="horas_totales_rest">
              </div>
              <div class="col-lg-12 form-group text-right mt-2">
                <button class="btn btn-success btn-sm" type="button">
                  <i class="fas fa-save"></i>
                  &nbsp;
                  Guardar
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
  $instancia->asignarCargaAcedmicaControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/programa/funcionesPrograma.js"></script>