<div class="modal fade" id="agregar_modulo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Asignatura</h5>
			</div>
			<div class="modal-body">
				<form method="POST">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="programa" value="<?=$datos_programas['id']?>">
					<div class="row p-2">
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Programa</label>
							<input type="text" class="form-control" disabled value="<?=$datos_programas['nombre']?>">
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Competencia <span class="text-danger">*</span></label>
							<select name="competencia" class="form-control" required>
								<option value="">Seleccione una opcion...</option>
								<?php
								foreach ($datos_competencia as $competencia) {
									$id_competencia  = $competencia['id'];
									$nom_competencia = $competencia['nombre'];
									?>
									<option value="<?=$id_competencia?>"><?=$nom_competencia?></option>
								<?php }?>
							</select>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Asignatura <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="nom_modulo" required>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Horas teoricas</label>
							<input type="text" class="form-control" name="horas_teoricas">
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Horas practicas</label>
							<input type="text" class="form-control" name="horas_practicas">
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Estado <span class="text-danger">*</span></label>
							<select class="form-control" required name="estado">
								<option value="1">Activo</option>
								<option value="0">Inactivo</option>
							</select>
						</div>
						<div class="col-lg-12 form-group text-right mt-2">
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
								<i class="fa fa-times"></i>
								&nbsp;
								Cancelar
							</button>
							<button class="btn btn-success btn-sm" type="submit">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>