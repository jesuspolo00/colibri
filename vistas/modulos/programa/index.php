<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia         = ControlAsistencia::singleton_asistencia();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_cobro   = ControlCobro::singleton_cobro();

if (isset($_POST['buscar'])) {
	$datos       = array('anio' => $_POST['anio'], 'docente' => $_POST['docente'], 'buscar' => $_POST['buscar'], 'id' => $id_log);
	$datos_carga = $instancia->buscarCargasAcademicasControl($datos);
} else {
	$datos_carga = $instancia->mostrarLimiteCargaAcademicaControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Carga academica
					</h4>
					<div class="btn-group">
						<a class="btn btn-info btn-sm" href="<?=BASE_URL?>programa/listado">
							<i class="fas fa-cog"></i>
							&nbsp;
							Configurar programa
						</a>
						<a class="btn btn-secondary btn-sm" href="<?=BASE_URL?>programa/cargaAcademica">
							<i class="fa fa-plus"></i>
							&nbsp;
							Asignar carga academica
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="anio" class="form-control" data-tooltip="tooltip" title="A&ntilde;o">
									<?php
									for ($i = date('Y') - 1; $i <= 2050; $i++) {
										$selected = (date('Y-m-d') == $i) ? 'selected' : '';
										?>
										<option value="<?=$i?>" <?=$selected?>><?=$i?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<select name="docente" class="form-control" data-tooltip="tooltip" title="Docente">
									<option value="" selected>Seleccione un docente...</option>
									<?php
									foreach ($datos_usuarios as $usuarios) {
										$id_usuario  = $usuarios['id_user'];
										$nom_docente = $usuarios['nombre'] . ' ' . $usuarios['apellido'];
										?>
										<option value="<?=$id_usuario?>"><?=$nom_docente?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Asignatura</th>
									<th scope="col">Competencia</th>
									<th scope="col">Horas teoricas</th>
									<th scope="col">Horas practicas</th>
									<th scope="col">Horas totales</th>
									<th scope="col">Horas teoricas asignadas por coordinador</th>
									<th scope="col">Horas practicas asignadas por coordinador</th>
									<th scope="col">Horas totales asignadas por coordinador</th>
									<th scope="col">Horas teoricas aprobadas por Direccion academica</th>
									<th scope="col">Horas practicas aprobadas por Direccion academica</th>
									<th scope="col">Horas totales aprobadas por Direccion academica</th>
									<th scope="col">Identificacion docente</th>
									<th scope="col">Docente</th>
									<th scope="col">Programa</th>
									<th scope="col">Semestre</th>
									<th scope="col">Jornada</th>
								</tr>
							</thead>
							<tbody class="buscar">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>