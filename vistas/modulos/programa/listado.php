<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

if (isset($_POST['buscar'])) {
	$datos_programa = $instancia->buscarProgramaControl($_POST['buscar']);
} else {
	$datos_programa = $instancia->mostrarLimiteProgramasControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>programa/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Programas
					</h4>
					<div class="btn-group">
						<button class="btn btn-secondary btn-sm" type="button" data-target="#agregar_programa" data-toggle="modal">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Programa
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Programa</th>
									<th scope="col">Cantidad de competencias</th>
									<th scope="col">Cantidad de asignaturas</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_programa as $programa) {
									$id_programa          = $programa['id'];
									$nom_programa         = $programa['nombre'];
									$cantidad_competencia = $programa['cantidad_competencia'];
									$cantidad_modulos     = $programa['cantidad_modulos'];
									?>
									<tr class="text-center">
										<td><?=$nom_programa?></td>
										<td><?=$cantidad_competencia?></td>
										<td><?=$cantidad_modulos?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>programa/detallePrograma?id=<?=base64_encode($id_programa)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'modulos' . DS . 'programa' . DS . 'agregarPrograma.php';
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nom_programa'])) {
	$instancia->guardarProgramaControl();
}