<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia         = ControlAprobacion::singleton_aprobacion();
$isntancia_recibos = ControlRecibos::singleton_recibos();

$datos_proyecto = $isntancia_recibos->mostrarTodosProyectosControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 17);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['aprobacion'])) {
	$id_solicitud = base64_decode($_GET['aprobacion']);

	$datos_aprobacion = $instancia->mostrarInformacionAprobacionPagoControl($id_solicitud);
	$datos_archivos   = $instancia->mostrarArchivosAprobacionControl($id_solicitud);

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>aprobacion/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Solicitud de aprobacion de pagos No. <?=$id_solicitud?> - Archivos
						</h4>
					</div>
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data">
							<input type="hidden" name="id_aprobacion" value="<?=$id_solicitud?>">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Documento de quien realiza</label>
									<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['documento']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombre de quien realiza</label>
									<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['nom_user']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Proyecto <span class="text-danger">*</span></label>
									<select name="proyecto" class="form-control" required>
										<?php
										foreach ($datos_proyecto as $proyecto) {
											$id_proyecto  = $proyecto['id'];
											$nom_proyecto = $proyecto['nombre'];

											$select = ($id_proyecto == $datos_aprobacion['id_proyecto']) ? 'selected' : '';
											?>
											<option value="<?=$id_proyecto?>" <?=$select?>><?=$nom_proyecto?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombre Archivo <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="nom_archivo" value="<?=$datos_aprobacion['nom_archivo']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Valor <span class="text-danger">*</span></label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text">$</span>
										</div>
										<input type="text" class="form-control precio" name="valor" value="<?=number_format($datos_aprobacion['valor'])?>" required>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Fecha solicitado</label>
									<input type="date" class="form-control" name="fecha_sol" value="<?=date('Y-m-d', strtotime($datos_aprobacion['fechareg']))?>">
								</div>
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Archivos a enviar</label>
									<input type="file" class="file" name="archivo[]" multiple accept=".jpg, .png, .jpeg, .xlsx, .pdf, .docx, .xls, .doc">
								</div>
								<div class="col-lg-12 form-group mt-4">
									<?php
									if ($datos_aprobacion['archivo'] != '') {
										?>
										<a href="<?=PUBLIC_PATH?>upload/<?=$datos_aprobacion['archivo']?>" class="btn btn-primary btn-sm" target="_blank">
											<i class="fa fa-download"></i>
											&nbsp;
											Descargar Archivo No. 1
										</a>
									<?php } else {
										$cont = 1;
										foreach ($datos_archivos as $archivos) {
											$nombre_archivo = $archivos['archivo'];
											?>
											<a href="<?=PUBLIC_PATH?>upload/<?=$nombre_archivo?>" class="btn btn-primary btn-sm" target="_blank">
												<i class="fa fa-download"></i>
												&nbsp;
												Descargar Archivo No. <?=$cont++?>
											</a>
										<?php }}?>
									</div>
									<div class="col-lg-12 form-group mt-2">
										<label class="font-weight-bold">Concepto</label>
										<textarea class="form-control" name="concepto" rows="5"><?=$datos_aprobacion['concepto']?></textarea>
									</div>
									<div class="col-lg-12 form-group text-right mt-2">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-save"></i>
											&nbsp;
											Guardar
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		include_once VISTA_PATH . 'script_and_final.php';

		if (isset($_POST['id_aprobacion'])) {
			$instancia->editarAprobacionControl();
		}
	}
?>