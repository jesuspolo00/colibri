<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';

$instancia = ControlAprobacion::singleton_aprobacion();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 37);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['aprobacion'])) {
	$id_solicitud = base64_decode($_GET['aprobacion']);
	$url          = ($_GET['enlace'] == 1) ? 'index' : 'solicitudUsuario';

	$datos_aprobacion = $instancia->mostrarInformacionWsAprobacionPagoControl($id_solicitud);
	$datos_archivos   = $instancia->mostrarArchivosWsAprobacionControl($id_solicitud);

	if ($datos_aprobacion['estado'] == 1) {
		$bg  = 'bg-warning text-white';
		$txt = 'Pendiente';
	}

	if ($datos_aprobacion['estado'] == 2) {
		$bg  = 'bg-success text-white';
		$txt = 'Aprobado';
	}

	if ($datos_aprobacion['estado'] == 3) {
		$bg  = 'bg-danger text-white';
		$txt = 'Denegado';
	}
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>aprobacion_ws/<?=$url?>" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Solicitud de aprobacion de pagos No. <?=$id_solicitud?> - Archivos
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Documento de quien realiza</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['documento']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre de quien realiza</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['nom_user']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Proyecto</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['nom_proyecto']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre Archivo</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['nom_archivo']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Valor</label>
								<input type="text" class="form-control" disabled value="$<?=number_format($datos_aprobacion['valor'])?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha solicitado</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['fechareg']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de pago</label>
								<input type="text" class="form-control" disabled value="<?=$datos_aprobacion['nom_tipo_pago']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado</label>
								<input type="text" disabled class="form-control <?=$bg?>" value="<?=$txt?>">
							</div>
							<div class="col-lg-4 form-group"></div>
							<div class="col-lg-4 form-group mt-2">
								<?php
								if ($datos_aprobacion['archivo'] != '') {
									?>
									<a href="<?=PUBLIC_PATH?>upload/<?=$datos_aprobacion['archivo']?>" class="btn btn-primary btn-sm" target="_blank">
										<i class="fa fa-download"></i>
										&nbsp;
										Descargar Archivo No. 1
									</a>
								<?php } else {
									$cont = 1;
									foreach ($datos_archivos as $archivos) {
										$nombre_archivo = $archivos['archivo'];
										?>
										<a href="<?=PUBLIC_PATH?>upload/<?=$nombre_archivo?>" class="btn btn-primary btn-sm" target="_blank">
											<i class="fa fa-download"></i>
											&nbsp;
											Descargar Archivo No. <?=$cont++?>
										</a>
									<?php }}?>
								</div>
								<div class="col-lg-12 form-group mt-2">
									<label class="font-weight-bold">Concepto</label>
									<textarea class="form-control" disabled rows="5"><?=$datos_aprobacion['concepto']?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		include_once VISTA_PATH . 'script_and_final.php';
	}
?>