<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia        = ControlAprobacion::singleton_aprobacion();
$instancia_recibo = ControlRecibos::singleton_recibos();

$datos_proyectos = $instancia_recibo->mostrarTodosProyectosWsControl();

if (isset($_POST['buscar'])) {
	$datos           = array('fecha' => $_POST['fecha'], 'proyecto' => $_POST['proyecto'], 'buscar' => $_POST['buscar']);
	$datos_solicitud = $instancia->buscarSolicitudAprobacionWsUsuarioControl($datos);
} else {
	$datos_solicitud = $instancia->mostrarAprobacionWsUsuarioControl($id_log);
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 37);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>aprobacion_ws/solicitud" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Aprobacion de pagos
					</h4>
					<div class="btn-group">
						<a class="btn btn-primary btn-sm" href="<?=BASE_URL?>aprobacion_ws/solicitud">
							<i class="fa fa-plus"></i>
							&nbsp;
							Solicitar aprobacion de pago
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select class="form-control" name="proyecto">
									<option value="" selected>Seleccione un proyecto...</option>
									<?php
									foreach ($datos_proyectos as $proyecto) {
										$id_proyecto  = $proyecto['id'];
										$nom_proyecto = $proyecto['nombre'];
										?>
										<option value="<?=$id_proyecto?>"><?=$nom_proyecto?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha Solicitado" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center text-uppercase font-weight-bold">
									<th scope="col">No. Solicitud</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Proyecto Asociado</th>
									<th scope="col">Archivo</th>
									<th scope="col">Concepto</th>
									<th scope="col">Valor</th>
									<th scope="col">Fecha Solicitado</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_solicitud as $solicitud) {
									$id_solicitud = $solicitud['id'];
									$documento    = $solicitud['documento'];
									$nom_completo = $solicitud['nom_user'];
									$correo       = $solicitud['correo'];
									$nom_estado   = $solicitud['estado_sol'];
									$nom_archivo  = $solicitud['nom_archivo'];
									$concepto     = $solicitud['concepto'];
									$nom_proyecto = $solicitud['nom_proyecto'];
									$fecha        = date('Y-m-d', strtotime($solicitud['fechareg']));
									$valor        = ($solicitud['valor'] == '') ? 0 : number_format($solicitud['valor']);

									if ($solicitud['estado'] == 1) {
										$span_estado = '<span class="badge badge-warning">' . $nom_estado . '</span>';
									}

									if ($solicitud['estado'] == 2) {
										$span_estado = '<span class="badge badge-success">' . $nom_estado . '</span>';
									}

									if ($solicitud['estado'] == 3) {
										$span_estado = '<span class="badge badge-danger">' . $nom_estado . '</span>';
									}

									$url_archivo = PUBLIC_PATH . 'upload' . DS . $solicitud['archivo'];

									if ($solicitud['id_user'] == $id_log) {

										?>
										<tr class="text-center">
											<td><?=$id_solicitud?></td>
											<td><?=$nom_completo?></td>
											<td><?=$correo?></td>
											<td><?=$nom_proyecto?></td>
											<td><?=$nom_archivo?></td>
											<td><?=$concepto?></td>
											<td>$<?=$valor?></td>
											<td><?=$fecha?></td>
											<td><?=$span_estado?></td>
											<td>
												<div class="btn-group">
													<a href="<?=BASE_URL?>aprobacion_ws/archivo?aprobacion=<?=base64_encode($id_solicitud)?>&enlace=2" data-tooltip="tooltip" title="Ver archivo" data-placement="bottom" class="btn btn-primary btn-sm">
														<i class="fa fa-eye"></i>
													</a>
												</div>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>