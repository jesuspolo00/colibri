<?php
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';

$instancia = ControlAprobacion::singleton_aprobacion();

if (isset($_GET['token'])) {

	$token = $_GET['token'];

	$token_usado = $instancia->verificarWsTokenUsoControl($token);

	if ($token_usado['token_usado'] == 0) {

		$aprobar_permiso = $instancia->aprobarWsSolicitudPagoControl($token_usado['id'], 3);

		if ($aprobar_permiso == true) {
			?>
			<div class="container">
				<div class="row p-2">
					<div class="col-lg-3"></div>
					<div class="col-lg-6 mt-10">
						<div class="card shadow bg-success border-success">
							<div class="card-body text-center">
								<i class="fas fa-check-circle text-white fa-4x mt-4"></i>
								<h3 class="font-weight-bold text-center text-white mt-4">WS Aprobacion de pago No. <?=$token_usado['id']?> Aceptada</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}

	} else {
		?>
		<div class="container">
			<div class="row p-2">
				<div class="col-lg-3"></div>
				<div class="col-lg-6 mt-10">
					<div class="card shadow bg-danger border-danger text-white">
						<div class="card-body text-center">
							<i class="fas fa-hourglass-half fa-4x mt-4"></i>
							<h3 class="font-weight-bold text-center mt-4 text-white">Enlace Vencido</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script>
	$(".loader").hide();
</script>