<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'renovacion' . DS . 'ControlRenovacion.php';

$instancia = ControlRenovacion::singleton_renovacion();

$datos_anio = $instancia->mostrarAniosRenovacionControl();

if (isset($_POST['buscar'])) {
	$datos            = array('buscar' => $_POST['buscar'], 'anio' => $_POST['anio']);
	$datos_renovacion = $instancia->buscarRenovacionesControl($datos);
} else {
	$datos_renovacion = $instancia->mostrarLimitesRenovacionesControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 33);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/academica" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Renovaciones
					</h4>
					<div class="btn-group">
						<!-- <a href="<?=BASE_URL?>imprimir/reporteUsuario" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a> -->
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_renovacion">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar renovacion
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group"></div>
							<div class="col-lg-4 form-group">
								<select name="anio" class="form-control">
									<option value="" selected>Seleccione un a&ntilde;o...</option>
									<?php
									foreach ($datos_anio as $anio) {
										$nom_anio = $anio['anio'];
										?>
										<option value="<?=$nom_anio?>"><?=$nom_anio?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Renovacion</th>
									<th scope="col">Programa</th>
									<th scope="col">Numero Resolucion</th>
									<th scope="col">Fecha Renovacion</th>
									<th scope="col">Fecha Proxima Renovacion</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_renovacion as $renovacion) {
									$id_renovacion = $renovacion['id'];
									$nombre        = $renovacion['nombre'];
									$fecha_inicio  = $renovacion['fecha_inicio'];
									$fecha_fin     = $renovacion['fecha_fin'];
									$observacion   = $renovacion['observacion'];
									$numero        = $renovacion['numero'];

									if (date('Y-m-d') > $fecha_fin) {
										$span = '<span class="badge badge-danger">Expirado</span>';
									}

									if (date('Y-m-d') < $fecha_fin) {
										$span = '<span class="badge badge-success">Vigente</span>';
									}

									$fecha_fin_mes = date("Y-m", strtotime($fecha_fin . "- 1 month"));

									if ($fecha_fin_mes <= date('Y-m')) {
										$span = '<span class="badge badge-warning">Por Vencer</span>';
									}

									$ver_evidencia = (empty($renovacion['evidencia'])) ? 'd-none' : '';
									$ver_archivo   = (empty($renovacion['archivo'])) ? 'd-none' : '';

									if (empty($renovacion['archivo']) && empty($renovacion['evidencia'])) {
										$span_archivo = '<span class="badge badge-danger">Falta Archivo y Evidencia</span>';
									}

									if (!empty($renovacion['archivo']) && empty($renovacion['evidencia'])) {
										$span_archivo = '<span class="badge badge-danger">Falta Evidencia</span>';
									}

									if (empty($renovacion['archivo']) && !empty($renovacion['evidencia'])) {
										$span_archivo = '<span class="badge badge-danger">Falta Archivo</span>';
									}

									if (!empty($renovacion['archivo']) && !empty($renovacion['evidencia'])) {
										$span_archivo = '';
									}

									?>
									<tr class="text-center">
										<td><?=$id_renovacion?></td>
										<td><?=$nombre?></td>
										<td><?=$numero?></td>
										<td><?=$fecha_inicio?></td>
										<td><?=$fecha_fin?></td>
										<td><?=$observacion?></td>
										<td>
											<?=$span_archivo?>
											<?=$span?>
										</td>
										<td>
											<div class="btn-group">
												<?php
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 34);
												if ($permiso) {
													?>
													<button class="btn btn-success btn-sm" type="button" data-tooltip="tooltip" title="Editar Renovacion" data-placement="bottom" data-toggle="modal" data-target="#editar<?=$id_renovacion?>">
														<i class="fa fa-edit"></i>
													</button>
												<?php }?>
												<a href="<?=PUBLIC_PATH?>upload/<?=$renovacion['archivo']?>" class="btn btn-primary btn-sm <?=$ver_archivo?>" data-tooltip="tooltip" title="Ver archivo" target="_blank" data-placement="bottom">
													<i class="fas fa-file-image"></i>
												</a>
												<a href="<?=PUBLIC_PATH?>upload/<?=$renovacion['evidencia']?>" class="btn btn-info btn-sm <?=$ver_evidencia?>" data-tooltip="tooltip" title="Ver evidencia" target="_blank" data-placement="bottom">
													<i class="fas fa-eye"></i>
												</a>
											</div>
										</td>
									</tr>


									<div class="modal fade" id="editar<?=$id_renovacion?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Editar Renovacion No. <?=$id_renovacion?></h5>
												</div>
												<div class="modal-body">
													<form method="POST"  enctype="multipart/form-data">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_renovacion" value="<?=$id_renovacion?>">
														<input type="hidden" name="archivo_ant" value="<?=$renovacion['archivo']?>">
														<input type="hidden" name="evidencia_ant" value="<?=$renovacion['evidencia']?>">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Nombre Programa <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="nom_edit" value="<?=$nombre?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Numero Resolucion <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="numero_edit" value="<?=$numero?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Renovacion <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_inicio_edit" value="<?=$fecha_inicio?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Proxima Renovacion <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_fin_edit" value="<?=$fecha_fin?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Archivo </label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" name="archivo_edit" id="archivo<?=$id_renovacion?>" accept=".png, .jpg, .jpeg, .pdf">
																	<label class="custom-file-label file_label_archivo<?=$id_renovacion?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Evidencia </label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" name="evidencia_edit" id="evidencia<?=$id_renovacion?>" accept=".png, .jpg, .jpeg, .pdf">
																	<label class="custom-file-label file_label_evidencia<?=$id_renovacion?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea name="observacion_edit" class="form-control" rows=5><?=$observacion?></textarea>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fa fa-edit"></i>
																	&nbsp;
																	Guardar
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'renovacion' . DS . 'agregarRenovacion.php';

if (isset($_POST['nombre'])) {
	$instancia->agregarRenovacionControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia->editarRenovacionControl();
}