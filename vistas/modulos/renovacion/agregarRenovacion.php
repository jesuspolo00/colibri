<div class="modal fade" id="agregar_renovacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Renovacion</h5>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Nombre Programa <span class="text-danger">*</span></label>
              <input type="text" name="nombre" class="form-control" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Numero de resolucion <span class="text-danger">*</span></label>
              <input type="text" name="numero" class="form-control" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha de renovacion <span class="text-danger">*</span></label>
              <input type="date" name="fecha_inicio" class="form-control" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha de proxima renovacion <span class="text-danger">*</span></label>
              <input type="date" name="fecha_fin" class="form-control" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Archivo <span class="text-danger">*</span></label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" name="archivo" id="archivo" required accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label_archivo" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Evidencia</label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" name="evidencia" id="evidencia" accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label_evidencia" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Observacion</label>
              <textarea class="form-control" name="observacion" rows="5"></textarea>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
               <i class="fa fa-times"></i>
               &nbsp;
               Cancelar
             </button>
             <button class="btn btn-success btn-sm" type="submit">
              <i class="fa fa-save"></i>
              &nbsp;
              Guardar
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
