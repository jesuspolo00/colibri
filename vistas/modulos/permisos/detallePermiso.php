<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia = ControlPermisos::singleton_permisos();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 15);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['permiso'])) {

	$id_permiso = base64_decode($_GET['permiso']);
	$enlace     = base64_decode($_GET['enlace']);

	$datos_permiso = $instancia->mostrarDatosPermisoIdControl($id_permiso);

	$hora_inicio = date('g:i A', strtotime($datos_permiso['hora_inicio']));
	$hora_fin    = ($datos_permiso['hora_fin'] == '00:00:00') ? '--' : date('g:i A', strtotime($datos_permiso['hora_fin']));
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>permisos/<?=$enlace?>" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Detalle permiso laboral No. <?=$id_permiso?>
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Documento</label>
								<input type="text" class="form-control" disabled value="<?=$datos_permiso['documento']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre Completo</label>
								<input type="text" class="form-control" disabled value="<?=$datos_permiso['nom_user']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Correo</label>
								<input type="text" class="form-control" disabled value="<?=$datos_permiso['correo']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" class="form-control" disabled value="<?=$datos_permiso['telefono']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha Solicitado</label>
								<input type="text" class="form-control" disabled value="<?=$datos_permiso['fecha']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hora Inicio</label>
								<input type="text" class="form-control" disabled value="<?=$hora_inicio?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hora Fin</label>
								<input type="text" class="form-control" disabled value="<?=$hora_fin?>">
							</div>
							<?php
							if ($datos_permiso['documento_soporte'] != '') {?>
								<div class="col-lg-8 form-group">
									<button class="btn btn-primary btn-sm" style="margin-top: 4.6%;" type="button" data-toggle="modal" data-target="#archivo">
										<i class="fa fa-eye"></i>
										&nbsp;
										Documentacion Soporte
									</button>
								</div>
							<?php }?>
							<div class="col-lg-12 form-group mt-2">
								<label class="font-weight-bold">Solicito la aprobacion para ausentarme laboralmente por motivos</label>
								<textarea class="form-control" rows="5" disabled><?=$datos_permiso['motivo']?></textarea>
							</div>
							<?php if ($datos_permiso['token_usado'] == 0 && $enlace == 'listado') {
								$permiso      = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 20);
								$ver_aprobado = ($permiso) ? '' : 'd-none';
								?>
								<div class="col-lg-12 form-group mt-2 text-right <?=$ver_aprobado?>">
									<button class="btn btn-danger btn-sm denegar" id="<?=$datos_permiso['token']?>" type="button">
										<i class="fa fa-times"></i>
										&nbsp;
										Denegar
									</button>
									<button class="btn btn-success btn-sm aprobar" id="<?=$datos_permiso['token']?>" type="button">
										<i class="fa fa-check"></i>
										&nbsp;
										Aprobar
									</button>
								</div>
							<?php }?>
						</div>

						<div class="modal fade" id="archivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Documento Soporte</h4>
										<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close">
											<i class="fa fa-times"></i>
											&nbsp;
											Cerrar
										</button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-lg-12 form-group">
												<iframe src="<?=PUBLIC_PATH?>upload/<?=$datos_permiso['documento_soporte']?>" style="width: 100%; height: 500px;"></iframe>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script src="<?=PUBLIC_PATH?>js/permisos/funcionesListadoPermisos.js"></script>