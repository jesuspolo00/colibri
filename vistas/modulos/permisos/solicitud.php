<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia        = ControlPermisos::singleton_permisos();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil = $instancia_perfil->mostrarDatosPerfilControl($id_log);

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 15);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/recursos" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Solicitud de permiso laboral - Anexo personal
					</h4>
					<?php
					$permiso     = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 21);
					$ver_listado = ($permiso) ? '' : 'd-none';
					?>
					<div class="btn-group">
						<a href="<?=BASE_URL?>permisos/listadoUsuario" class="btn btn-secondary btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Mis solicitudes
						</a>
						<a href="<?=BASE_URL?>permisos/listado" class="btn btn-info btn-sm <?=$ver_listado?>">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado de solciitudes de permisos
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data" class="formValidation">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_user" value="<?=$datos_perfil['id_user']?>">
						<div class="row">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_perfil['documento']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" disabled value="<?=$datos_perfil['nombre'] . ' ' . $datos_perfil['apellido']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Sitio de trabajo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" required name="sitio">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de solicitud <span class="text-danger">*</span></label>
								<input type="date" name="fecha" value="<?=date('Y-m-d')?>" class="form-control">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hora Inicio <span class="text-danger">*</span></label>
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="time" class="form-control" value="<?=date('H:m')?>" name="hora_inicio" required>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hora Final</label>
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="time" class="form-control" name="hora_fin">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Solicito la aprobacion para ausentarme laboralmente por motivos <span class="text-danger">*</span></label>
								<textarea class="form-control" rows="5" name="motivo" required></textarea>
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Anexo documentacion soporte (opcional)</label>
							<div class="custom-file">
								<input type="file" class="file input-sm" data-preview-file-type="any" name="documento" accept=".jpg, .png, .jpeg, .xlsx, .pdf, .docx, .xls, .doc">
							</div>
						</div>
						<div class="col-lg-12 form-group text-right mt-4">
							<button class="btn btn-success btn-sm buttonFormValidation" type="submit">
								<i class="fa fa-save"></i>
								&nbsp;
								Solicitar
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_user'])) {
	$instancia->registrarPermisoLaboralControl();
}
?>