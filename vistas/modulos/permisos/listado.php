<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlPermisos::singleton_permisos();
$instancia_usuario = ControlUsuario::singleton_usuario();

if (isset($_POST['buscar'])) {
	$datos         = array('usuario' => $_POST['usuario'], 'fecha' => $_POST['fecha'], 'buscar' => $_POST['buscar']);
	$datos_permiso = $instancia->buscarPermisosSolicitadosControl($datos);
} else {
	$datos_permiso = $instancia->mostrarListadoPermisosControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 15);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>permisos/solicitud" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de solicitud de permisos laborales
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/reporteListadoPermisos" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a>
						<a href="<?=BASE_URL?>permisos/solicitud" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Solicitar permiso
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="usuario" class="form-control">
									<option value="" selected>Seleccione un usuario...</option>
									<?php
									foreach ($datos_usuarios as $usuario) {
										$id_user  = $usuario['id_user'];
										$nom_user = $usuario['nombre'] . ' ' . $usuario['apellido'];
										?>
										<option value="<?=$id_user?>"><?=$nom_user?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control" name="fecha" data-tooltip="tooltip" title="Fecha Solicitado" data-placement="top" data-trigger="hover">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. permiso</th>
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Motivo</th>
									<th scope="col">Fecha solicitado</th>
									<th scope="col">Hora inicio</th>
									<th scope="col">Hora fin</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_permiso as $listado) {
									$id_permiso  = $listado['id'];
									$documento   = $listado['documento'];
									$nom_user    = $listado['nom_user'];
									$correo      = $listado['correo'];
									$motivo      = $listado['motivo'];
									$fecha_sol   = $listado['fecha'];
									$hora_inicio = date('g:i A', strtotime($listado['hora_inicio']));
									$hora_fin    = ($listado['hora_fin'] == '00:00:00') ? '--' : date('g:i A', strtotime($listado['hora_fin']));
									$token       = $listado['token'];

									$span = '';

									if ($listado['estado'] == 2) {
										$span = '<span class="badge badge-success">Aprobada</span>';
									}

									if ($listado['estado'] == 3) {
										$span = '<span class="badge badge-danger">Denegada</span>';
									}

									if ($listado['token_usado'] == 0) {
										$ver_botones = '';
									} else {
										$ver_botones = 'd-none';
									}
									?>
									<tr class="text-center">
										<td><?=$id_permiso?></td>
										<td><?=$documento?></td>
										<td><?=$nom_user?></td>
										<td><?=$correo?></td>
										<td><?=$motivo?></td>
										<td><?=$fecha_sol?></td>
										<td><?=$hora_inicio?></td>
										<td><?=$hora_fin?></td>
										<td>
											<?=$span?>
										</td>
										<td>
											<a href="<?=BASE_URL?>permisos/detallePermiso?permiso=<?=base64_encode($id_permiso)?>&enlace=<?=base64_encode('listado')?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalle" data-placement="bottom" data-trigger="hover">
												<i class="fa fa-eye"></i>
											</a>
										</td>
										<?php
										$permiso      = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 20);
										$ver_aprobado = ($permiso) ? '' : 'd-none';
										?>
										<td class="fila_<?=$token?> <?=$ver_aprobado?>">
											<div class="btn-group <?=$ver_botones?>">
												<button class="btn btn-success btn-sm aprobar" id="<?=$token?>" type="button" data-tooltip="tooltip" title="Aprobar permiso" data-placement="bottom" data-trigger="hover">
													<i class="fa fa-check"></i>
												</button>
												<button class="btn btn-danger btn-sm denegar" id="<?=$token?>" type="button" data-tooltip="tooltip" title="Denegar permiso" data-placement="bottom" data-trigger="hover">
													<i class="fa fa-times"></i>
												</button>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/permisos/funcionesListadoPermisos.js"></script>