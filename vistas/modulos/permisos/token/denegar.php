<?php
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia = ControlPermisos::singleton_permisos();

if (isset($_GET['token'])) {

	$token = $_GET['token'];

	$token_usado = $instancia->verificarTokenUsoControl($token);

	if ($token_usado['token_usado'] == 0) {

		$aprobar_permiso = $instancia->denegarPermisoControl($token_usado['id']);

		if ($aprobar_permiso == true) {
			?>
			<div class="container">
				<div class="row p-2">
					<div class="col-lg-3"></div>
					<div class="col-lg-6 mt-10">
						<div class="card shadow bg-danger border-danger">
							<div class="card-body text-center">
								<i class="fas fa-times-circle text-white fa-4x mt-4"></i>
								<h3 class="font-weight-bold text-center text-white mt-4">Permiso No. <?=$token_usado['id']?>  Denegado</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}

	} else {
		?>
		<div class="container">
			<div class="row p-2">
				<div class="col-lg-3"></div>
				<div class="col-lg-6 mt-10">
					<div class="card shadow bg-danger border-danger text-white">
						<div class="card-body text-center">
							<i class="fas fa-hourglass-half fa-4x mt-4"></i>
							<h3 class="font-weight-bold text-center mt-4 text-white">Enlace Vencido</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script>
	$(".loader").hide();
</script>