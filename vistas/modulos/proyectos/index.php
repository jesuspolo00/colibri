<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia           = ControlRecibos::singleton_recibos();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 12);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_POST['buscar'])) {
	$buscar_ambos = $_POST['buscar'];

	$datos_proyectos = $instancia->buscarProyectosControl($buscar_ambos);
} else {
	$datos_proyectos = $instancia->mostrarProyectosControl();

}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Proyectos
					</h4>
					<div class="btn-group">
						<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#agregar_proyecto">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Proyecto
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="Buscar..." aria-describedby="basic-addon2" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Proyecto</th>
									<th scope="col">Nombre</th>
									<th scope="col">Ciudad</th>
									<th scope="col">Cliente</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_proyectos as $proyecto) {
									$id_proyecto      = $proyecto['id_proyecto'];
									$nombre           = $proyecto['nom_proyecto'];
									$ciudad           = $proyecto['ciudad_proyecto'];
									$nombre_proveedor = $proyecto['proveedor'];
									$activo           = $proyecto['activo'];
									$id_proveedor_pro = $proyecto['id_proveedor'];
									$nit_proveedor    = $proyecto['nit_proveedor'];

									$ver = ($activo == 0) ? 'd-none' : '';
									?>
									<tr class="text-center <?=$ver?>">
										<td><?=$id_proyecto?></td>
										<td><?=$nombre?></td>
										<td><?=$ciudad?></td>
										<td><?=$nombre_proveedor?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<button class="btn btn-success btn-sm" type="button" data-tooltip="tootip" title="Editar" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#editar_proyecto<?=$id_proyecto?>">
													<i class="fa fa-edit"></i>
												</button>
												<button class="btn btn-danger btn-sm" type="button" data-tooltip="tootip" title="Inactivar" data-placement="bottom" data-trigger="hover">
													<i class="fa fa-times"></i>
												</button>
											</div>
										</td>
									</tr>

									<!--Agregar usuario-->
									<div class="modal fade" id="editar_proyecto<?=$id_proyecto?>" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
										<div class="modal-dialog modal-lg p-2" role="document">
											<div class="modal-content">
												<form method="POST" id="form_enviar">
													<input type="hidden" value="<?=$id_log?>" name="id_log">
													<input type="hidden" value="<?=$id_proyecto?>" name="id_proyecto">
													<div class="modal-header p-3">
														<h4 class="modal-title text-success font-weight-bold">Editar proyecto</h4>
													</div>
													<div class="modal-body border-0">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Nombre del proyecto <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="nombre_edit" value="<?=$nombre?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="ciudad_edit" value="<?=$ciudad?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cliente del proyecto <span class="text-danger">*</span></label>
																<select class="form-control" required="" name="cliente_edit">
																	<option value="<?=$id_proveedor_pro?>" selected="" class="d-none"><?=$nombre_proveedor . ' (' . $nit_proveedor . ')'?></option>
																	<?php
																	foreach ($datos_proveedor as $proveedor) {
																		$id_proveedor     = $proveedor['id_proveedor'];
																		$nombre_proveedor = $proveedor['nombre'];
																		$nit              = $proveedor['num_identificacion'];
																		$estado           = $proveedor['estado'];

																		$ver = ($estado == 0) ? 'd-none' : '';
																		?>
																		<option value="<?=$id_proveedor?>" class="<?=$ver?>"><?=$nombre_proveedor . ' (' . $nit . ')'?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-12 form-group text-right">
																<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cerrar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fa fa-save"></i>
																	&nbsp;
																	Guardar
																</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'proyectos' . DS . 'agregarProyecto.php';

if (isset($_POST['nombre'])) {
	$instancia->registrarProyectosControl();
}

if (isset($_POST['nombre_edit'])) {
	$instancia->editarProyectoControl();
}
?>