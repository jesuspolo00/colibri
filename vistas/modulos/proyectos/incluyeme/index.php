<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil       = $instancia_perfil->mostrarPerfilesControl();
$datos_centro_costo = $instancia->mostrarCentroCostoControl();

if (isset($_POST['buscar'])) {
	$datos_usuarios = $instancia->buscarUsuariosHojaVidaIncluyemeControl($_POST['buscar']);
} else {
	$datos_usuarios = $instancia->mostrarLimiteUsuariosHojaVidaIncluyemeControl();
}

$id_url = 1;

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 79);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/proyectos" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Hojas de vida - Incluyeme hoy
					</h4>
					<div class="btn-group">
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 64);
						if ($permiso) {
							?>
							<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_usuario">
								<i class="fa fa-plus"></i>
								&nbsp;
								Agregar usuario
							</button>
							<?php
						}?>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Perfil</th>
									<th scope="col">Progreso</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user         = $usuario['id_user'];
									$documento       = $usuario['documento'];
									$nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$correo          = $usuario['correo'];
									$telefono        = $usuario['telefono'];
									$user            = $usuario['user'];
									$perfil          = $usuario['nom_perfil'];
									$activo          = $usuario['estado'];

									$anio_registro = date('Y', strtotime($usuario['fecha_registro']));

									$autorizacion_documentos_primera = $instancia->autorizacionAnioDocumentosControl($id_user, $anio_registro);
									$autorizacion_anual              = $instancia->autorizacionAnioDocumentosControl($id_user, date('Y'));

									$evaluacion_anual_administrativa = $instancia->validarEvaluacionAnualUsuarioControl($id_user, date('Y'), 1);
									$evaluacion_anual_docente        = $instancia->validarEvaluacionAnualUsuarioControl($id_user, date('Y'), 2);

									$autorizacion_academica = $instancia->mostrarAutorizacionPerfilControl($id_user, date('Y'), 0);

									$ver_subir_documentos = (empty($usuario['documentos'])) ? '' : 'd-none';

									$ver_autorizar = ($usuario['documentos'] != 0) ? '' : 'd-none';
									$ver_autorizar = ($autorizacion_anual['cantidad_doc'] == $usuario['documentos']) ? 'd-none' : $ver_autorizar;

									$ver_evaluacion_admin = ($autorizacion_anual['cantidad_doc'] == $usuario['documentos']) ? '' : 'd-none';
									$ver_evaluacion_admin = (!empty($evaluacion_anual_administrativa)) ? 'd-none' : $ver_evaluacion_admin;

									$ver_evaluacion_docente = ($autorizacion_anual['cantidad_doc'] == $usuario['documentos']) ? '' : 'd-none';
									$ver_evaluacion_docente = (!empty($evaluacion_anual_docente)) ? 'd-none' : $ver_evaluacion_docente;

									if ($id_url == 1) {
										$ver_aprobar = (!empty($autorizacion_academica) && $usuario['verificacion'] == 0) ? '' : 'd-none';
									} else {
										$ver_aprobar = ($usuario['verificacion'] == 0) ? '' : 'd-none';
									}

									$span_documentos         = (empty($usuario['documentos'])) ? '<span class="badge badge-danger">Faltan documentos</span>' : '<span class="badge badge-success">Documentos subidos</span>';
									$span_autorizacion       = (empty($autorizacion_anual)) ? '<span class="badge badge-danger">Falta autorizaci&oacute;n documentos</span>' : '<span class="badge badge-success">Autorizaci&oacute;n realizada</span>';
									$span_evaluacion         = (empty($evaluacion_anual_administrativa)) ? '<span class="badge badge-danger">Falta evaluaci&oacute;n administrativa</span>' : '<span class="badge badge-success">Evaluaci&oacute;n administrativa realizada</span>';
									$span_evaluacion_docente = (empty($evaluacion_anual_docente)) ? '<span class="badge badge-danger">Falta evaluaci&oacute;n docente</span>' : '<span class="badge badge-success">Evaluaci&oacute;n docente realizada</span>';
									$span_aprobacion         = ($usuario['verificacion'] == 0) ? '<span class="badge badge-danger">Falta revision</span>' : '<span class="badge badge-success">Revision realizada</span>';

									$url_hoja = ($usuario['perfil'] == 5) ? BASE_URL . 'proveedor/hojaRegistro?proveedor=' . base64_encode($id_user) : BASE_URL . 'hoja/index?id=' . base64_encode($id_user) . '&enlace=' . base64_encode(2);

									?>
									<tr class="text-center text-dark">
										<td><?=$documento?></td>
										<td><?=$nombre_completo?></td>
										<td><?=$correo?></td>
										<td><?=$perfil?></td>
										<td>
											<?=$span_documentos?>
											<?=$span_autorizacion?>
											<?=$span_aprobacion?>
										</td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<?php
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 82);
												if ($permiso) {
													?>
													<a href="<?=$url_hoja?>" class="btn btn-info btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Actualizar Informacion">
														<i class="fa fa-eye"></i>
													</a>
													<?php
												}
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 81);
												if ($permiso) {
													?>
													<a href="<?=BASE_URL?>hoja/documentos?id=<?=base64_encode($id_user)?>&enlace=<?=base64_encode(2)?>" class="btn btn-success btn-sm <?=$ver_subir_documentos?>" data-tooltip="tooltip" data-placement="bottom" title="Subir documentos">
														<i class="fa fa-upload"></i>
													</a>
													<?php
												}
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 83);
												if ($permiso) {
													?>
													<a href="<?=BASE_URL?>hoja/autorizar?id=<?=base64_encode($id_user)?>&enlace=<?=base64_encode(2)?>" class="btn btn-secondary btn-sm <?=$ver_autorizar?>" data-tooltip="tooltip" data-placement="bottom" title="Autorizar Documentos">
														<i class="fas fa-clipboard-check"></i>
													</a>
													<?php
												}
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 84);
												if ($permiso) {
													?>
													<a href="<?=BASE_URL?>hoja/verificar?id=<?=base64_encode($id_user)?>&enlace=<?=base64_encode(2)?>" class="btn btn-success btn-sm <?=$ver_aprobar?>" data-tooltip="tooltip" title="Aprobar - Denegar" data-placement="bottom">
														<i class="fas fa-user-check"></i>
													</a>
													<?php
												}
												?>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
	include_once VISTA_PATH . 'modulos' . DS . 'proyectos' . DS . 'incluyeme' . DS . 'agregarUsuario.php';

	if (isset($_POST['documento'])) {
		$instancia->registrarUsuarioControl();
	}
	?>
	<script src="<?=PUBLIC_PATH?>js/usuarios/funcionesUsuarios.js"></script>