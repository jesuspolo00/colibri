<!--Agregar usuario-->
<div class="modal fade" id="agregar_proyecto" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg p-2" role="document">
        <div class="modal-content">
            <form method="POST" id="form_enviar">
                <input type="hidden" value="<?=$id_log?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-success font-weight-bold">Agregar proyecto</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row p-2">
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Nombre del proyecto <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="ciudad" required>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Cliente del proyecto <span class="text-danger">*</span></label>
                            <select class="form-control" required name="cliente">
                                <option value="" selected="">Selecciona una opcion...</option>
                                <?php
                                foreach ($datos_proveedor as $proveedor) {
                                    $id_proveedor     = $proveedor['id_proveedor'];
                                    $nombre_proveedor = $proveedor['nombre'];
                                    $nit              = $proveedor['num_identificacion'];
                                    $estado           = $proveedor['estado'];

                                    $ver = ($estado == 0) ? 'd-none' : '';
                                    ?>
                                    <option value="<?=$id_proveedor?>" class="<?=$ver?>"><?=$nombre_proveedor . ' (' . $nit . ')'?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-12 form-group text-right">
                            <button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
                                <i class="fa fa-times"></i>
                                &nbsp;
                                Cerrar
                            </button>
                            <button class="btn btn-success btn-sm" type="submit">
                                <i class="fa fa-save"></i>
                                &nbsp;
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>