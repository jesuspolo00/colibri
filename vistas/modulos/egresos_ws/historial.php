<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'ingresos' . DS . 'ControlIngresos.php';

$instancia = ControlIngresos::singleton_ingresos();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 28);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['usuario'])) {

	$id_user           = base64_decode($_GET['usuario']);
	$datos_usuario     = $instancia->mostrarDatosUsuariosIngresosControl($id_user);
	$historial_ingreso = $instancia->mostrarHistorialEgresosWsUsuarioControl($id_user);

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>egresos_ws/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							WS Egresos Usuario - (<?=$datos_usuario['documento'] . ' - ' . $datos_usuario['nombre']?>)
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="docuemnto" value="<?=$datos_usuario['documento']?>" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nombre" value="<?=$datos_usuario['nombre']?>" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="ciudad" value="<?=$datos_usuario['ciudad']?>" required>
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<a href="<?=BASE_URL?>imprimir/reporteEgresosUsuarioWs?usuario=<?=base64_encode($id_user)?>" target="_blank" class="btn btn-success btn-sm">
									<i class="fa fa-file-excel"></i>
									&nbsp;
									Descargar historial
								</a>
								<button class="btn btn-primary btn-sm" disabled type="submit">
									<i class="fas fa-sync-alt"></i>
									&nbsp;
									Actualizar datos usuario
								</button>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-uppercase text-center font-weight-bold">
										<th scope="col" colspan="6">Historial de egresos</th>
									</tr>
									<tr class="text-center text-uppercase font-weight-bold">
										<th scope="col">No. egreso</th>
										<th scope="col">Concepto</th>
										<th scope="col">Fecha de egreso</th>
										<th scope="col">Metodo de pago</th>
										<th scope="col">Valor</th>
										<th scope="col">Observacion</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($historial_ingreso as $historial) {
										$id_egreso    = $historial['id'];
										$nom_user     = $historial['nom_user'];
										$nom_concepto = $historial['nom_concepto'];
										$nom_pago     = $historial['nom_pago'];
										$fecha        = $historial['fecha'];
										$valor        = number_format($historial['valor']);
										$doc_user     = $historial['doc_user'];
										$observacion  = $historial['observacion'];
										$activo       = $historial['activo'];

										if ($activo == 0) {
											$ver_recibo  = 'd-none';
											$span_estado = '<span class="badge badge-danger">Anulado</span>';
										} else {
											$ver_recibo  = '';
											$span_estado = '';
										}
										?>
										<tr class="text-center">
											<td><?=$id_egreso?></td>
											<td><?=$nom_concepto?></td>
											<td><?=$fecha?></td>
											<td><?=$nom_pago?></td>
											<td>$<?=$valor?></td>
											<td><?=$observacion?></td>
											<td><?=$span_estado?></td>
											<td class="<?=$ver_recibo?>">
												<div class="btn-group">
													<a href="<?=BASE_URL?>imprimir/egresos_ws?egreso=<?=base64_encode($id_egreso)?>" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Recibo" data-placement="bottom" target="_blank">
														<i class="fas fa-money-check"></i>
													</a>
												</div>
											</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}
?>