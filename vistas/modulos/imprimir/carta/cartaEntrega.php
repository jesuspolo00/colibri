<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'carta' . DS . 'ControlCarta.php';

$instancia = ControlCarta::singleton_carta();

if (isset($_GET['carta'])) {
    $id_carta = base64_decode($_GET['carta']);

    $datos_carta     = $instancia->mostrarInformacionCartaControl($id_carta);
    $datos_articulos = $instancia->mostrarArticulosCartaControl($id_carta);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }

    }

    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Recibo de Egresos');
    $pdf->SetSubject('Recibo de Egresos');
    $pdf->SetKeywords('Recibo de Egresos');
    $pdf->AddPage();

    $pdf->Ln(5);
    $pdf->Cell(5);
    $pdf->Image(PUBLIC_PATH . 'img/logo_wind.png', '', '', 50, 11);

    $pdf->Ln(3);
    $pdf->Cell(145);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
    $pdf->Cell(20, 6, date('d / m / Y', strtotime($datos_carta['fecha_entrega'])), 0, 0, 'L');

    $pdf->Ln(20);
    $pdf->Cell(75);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->Cell(20, 6, 'Acta de entrega No.' . $id_carta, 0, 0, 'L');

    $pdf->Ln(15);
    $pdf->Cell(80);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Cell(20, 6, 'Recibi conforme', 0, 0, 'L');

    $tabla_art = '
    <table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
    <tr style="text-align:center; font-weight:bold;">
    <th>Articulo</th>
    <th>Cantidad</th>
    </tr>
    ';

    foreach ($datos_articulos as $articulos) {
        $nom_articulo = $articulos['articulo'];
        $cantidad     = $articulos['cantidad'];

        $tabla_art .= '
        <tr style="text-align:center;">
        <td>' . $nom_articulo . '</td>
        <td>' . $cantidad . '</td>
        </tr>
        ';
    }

    $tabla_art .= '
    </table>

    <br>
    <p style="text-align: left;">
    <span style="font-weight:bold;">Observacion:</span>
    ' . $datos_carta['observacion'] . '
    </p>
    ';

    $pdf->Ln(16);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_art, true, false, true, false, '');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->Ln(15);
    $pdf->Cell(1);
    $pdf->Cell(20, 6, 'Nombre Completo: _________________________________________________________', 0, 0, 'L');

    $pdf->Ln(15);
    $pdf->Cell(1);
    $pdf->Cell(20, 6, 'Firma y número de documento: _________________________________________________________', 0, 0, 'L');

    ob_clean();
    $pdf->Output('carta_entrega_' . date('Y-m-d-H-i-s') . '.pdf', 'I');

}
