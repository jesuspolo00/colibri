<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$instancia   = ControlAsistencia::singleton_asistencia();

$datos_plan = $instancia->mostrarPlanesFormacionControl();

$spreadsheet->getProperties()
->setTitle('Listado de plan de formacion')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:J1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:J')->applyFromArray($estilos_datos);

foreach (range('A', 'J') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'N°')
->setCellValue('B1', 'LINEA DE FORMACIÓN')
->setCellValue('C1', 'TEMAS')
->setCellValue('D1', 'POBLACIÓN OBJETIVO')
->setCellValue('E1', 'RECURSOS FISICOS')
->setCellValue('F1', 'RESPONSABLE')
->setCellValue('G1', 'FECHA PROYECTADA')
->setCellValue('H1', 'HORA PROYECTADA')
->setCellValue('I1', 'FECHA EJECUTADA')
->setCellValue('J1', 'ESTADO');

$cont = 2;

foreach ($datos_plan as $plan) {

    $id_plan          = $plan['id'];
    $linea            = $plan['linea'];
    $temas            = $plan['temas'];
    $objetivo         = $plan['objetivo'];
    $recursos         = $plan['recursos'];
    $responsable      = $plan['nom_usuario'];
    $fecha_proyectada = $plan['fecha_proyectada'];
    $hora_proyectada  = $plan['hora_proyectada'];
    $fecha_ejecutada  = $plan['fecha_ejecutada'];

    $span_estado = ($plan['activo'] == 1) ? 'Pendiente de ejecución' : 'Ejecutado';
    $span_estado = ($plan['activo'] == 0) ? 'Cancelado' : $span_estado;

    $sheet->setCellValue('A' . $cont, $id_plan)
    ->setCellValue('B' . $cont, $linea)
    ->setCellValue('C' . $cont, $temas)
    ->setCellValue('D' . $cont, $objetivo)
    ->setCellValue('E' . $cont, $recursos)
    ->setCellValue('F' . $cont, $responsable)
    ->setCellValue('G' . $cont, $fecha_proyectada)
    ->setCellValue('H' . $cont, $hora_proyectada)
    ->setCellValue('I' . $cont, $fecha_ejecutada)
    ->setCellValue('J' . $cont, $span_estado);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Listado_plan_formacion.xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
