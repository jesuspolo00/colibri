<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'phpqrcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

if (isset($_GET['id'])) {
    $id_plan    = base64_decode($_GET['id']);
    $datos_plan = $instancia->mostrarDetallesPlanFormacionControl($id_plan);
}

class MYPDF extends TCPDF
{
    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
        // Define el encabezado si es necesario
        // Por ejemplo: $this->Image($this->logo, 10, 10, 40, 10);
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(50);
        $this->Image(PUBLIC_PATH . 'img/logo.png', '', '', 30, 7, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
    }
}

// Crea un objeto PDF
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Establece información del documento (meta)
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Código Calendario Institucional');
$pdf->SetSubject('Código Calendario Institucional');
$pdf->SetKeywords('Código Calendario Institucional');
$pdf->AddPage('P', 'A5');

// set style for barcode
$style = array(
    'border'        => 0,
    'vpadding'      => '0',
    'hpadding'      => '0',
    'fgcolor'       => array(0, 0, 0),
    'bgcolor'       => false, //array(255,255,255)
    'module_width'  => 1, // width of a single module in points
    'module_height' => 1, // height of a single module in points
);

// QRCODE,H : QR-CODE Best error correction
$pdf->write2DBarcode(BASE_URL . 'calendario_institucional/asistencia?token=' . base64_encode($datos_plan['token']), 'QRCODE,L', 13, 25, 150, 150, $style, 'N');

$pdf->SetFont('', '', 15);

$pdf->Ln(20);
$pdf->Cell(130, 7, $datos_plan['nom_tipo'] . ' No. ' . $id_plan, 0, 0, 'C');

$pdf->Ln(8);
$pdf->Cell(130, 7, 'RESPONSABLE: ' . $datos_plan['nom_usuario'], 0, 0, 'C');

// Genera el PDF y lo muestra en el navegador
$pdf->Output('codigo' . date('Y-m-d-H-i-s') . '.pdf', 'I');
