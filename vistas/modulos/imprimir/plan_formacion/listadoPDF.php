<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'phpqrcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$datos_plan = $instancia->mostrarPlanesFormacionControl();

class MYPDF extends TCPDF
{
    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
        // Define el encabezado si es necesario
        // Por ejemplo: $this->Image($this->logo, 10, 10, 40, 10);
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

// Crea un objeto PDF
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Establece información del documento (meta)
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Listado de plan de formacion');
$pdf->SetSubject('Listado de plan de formacion');
$pdf->SetKeywords('Listado de plan de formacion');
$pdf->AddPage('L');

$encabezado = '
<table border="1" cellpadding="8" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight: bold;">
<th rowspan="2">
<br>
<br>
<img src="' . PUBLIC_PATH . 'img/logo.png" width="150">
</th>
<th colspan="3">CORPORACION TECNICA DE ESTUDIOS ESPECIALIZADOS DEL CARIBE 
<br>PLAN DE FORMACION</th>
</tr>
<tr style="text-align:center; font-weight: bold;">
<th>Versión: 3</th>
<th>Fecha: 2024-01-02</th>
<th>Código: RG-GER-13</th>
</tr>
';

$encabezado .= '</table>';

$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($encabezado, true, false, true, false, '');

$tabla = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; background-color:#c4d79b; font-weight: bold;">
<th>N°</th>
<th>Linea de formaci&oacute;n</th>
<th>Temas</th>
<th>Pobalci&oacute;n objetivo</th>
<th>Recursos Fisicos</th>
<th>Responsable</th>
<th>Fecha proyectada</th>
<th>Hora proyectada</th>
<th>Fecha ejecutada / cancelada</th>
<th>Estado</th>
</tr>
';

foreach ($datos_plan as $plan) {

    $id_plan          = $plan['id'];
    $linea            = $plan['linea'];
    $temas            = $plan['temas'];
    $objetivo         = $plan['objetivo'];
    $recursos         = $plan['recursos'];
    $responsable      = $plan['nom_usuario'];
    $fecha_proyectada = $plan['fecha_proyectada'];
    $hora_proyectada  = $plan['hora_proyectada'];
    $fecha_ejecutada  = $plan['fecha_ejecutada'];

    $span_estado = ($plan['activo'] == 1) ? '<span class="badge badge-warning">Pendiente de ejecuci&oacute;n</span>' : '<span class="badge badge-success">Ejecutado</span>';
    $span_estado = ($plan['activo'] == 0) ? '<span class="badge badge-danger">Cancelado</span>' : $span_estado;

    $tabla .= '
    <tr style="text-align:center;">
    <td>' . $id_plan . '</td>
    <td>' . $linea . '</td>
    <td>' . $temas . '</td>
    <td>' . $objetivo . '</td>
    <td>' . $recursos . '</td>
    <td>' . $responsable . '</td>
    <td>' . $fecha_proyectada . '</td>
    <td>' . date('H:i:sA', strtotime($hora_proyectada)) . '</td>
    <td>' . $fecha_ejecutada . '</td>
    <td>' . $span_estado . '</td>
    </tr>
    ';

}

$tabla .= '</table>';

$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla, true, false, true, false, '');

$pdf->Output('Lista_plan_formacion.pdf', 'I');
