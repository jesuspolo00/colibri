<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'phpqrcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_GET['id'])) {
    $id_area    = base64_decode($_GET['id']);
    $datos_area = $instancia->mostrarDatosAreaControl($id_area);
}

class MYPDF extends TCPDF
{
    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
        // Define el encabezado si es necesario
        // Por ejemplo: $this->Image($this->logo, 10, 10, 40, 10);
    }

    public function Footer()
    {
        /*$this->SetY(-15);
    $this->SetFillColor(127);
    $this->SetTextColor(127);
    $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
    $this->Cell(50);
    $this->Image(PUBLIC_PATH . 'img/logo.png', '', '', 30, 7, '', '', 'T', false, 90, '', false, false, 1, false, false, false);*/

}
}

// Crea un objeto PDF
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Establece información del documento (meta)
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Código Zonas');
$pdf->SetSubject('Código Zonas');
$pdf->SetKeywords('Código Zonas');
$pdf->SetMargins(0, 0, 0); // Márgenes izquierdo, superior y derecho en 0
$pdf->SetAutoPageBreak(false, 0); // Desactivar salto de página automático y margen inferior

$pdf->AddPage('P', array(130, 120));

// set style for barcode
$style = array(
    'border'        => 0,
    'vpadding'      => '0',
    'hpadding'      => '0',
    'fgcolor'       => array(0, 0, 0),
    'bgcolor'       => false, //array(255,255,255)
    'module_width'  => 1, // width of a single module in points
    'module_height' => 1, // height of a single module in points
);

// QRCODE,H : QR-CODE Best error correction
$pdf->write2DBarcode(BASE_URL . 'areas/reporteQR?id=' . base64_encode($datos_area['id']), 'QRCODE,L', 14, 10, 95, 95, $style, 'N');

$pdf->Ln(5);
$pdf->SetFont('', '', 20);
$pdf->Cell(120, 10, $datos_area['nombre'], 0, 0, 'C');

// Genera el PDF y lo muestra en el navegador
$pdf->Output('codigo' . date('Y-m-d-H-i-s') . '.pdf', 'I');
