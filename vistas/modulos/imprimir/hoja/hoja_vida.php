<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlHoja::singleton_hoja();
$instancia_perfil  = ControlPerfil::singleton_perfil();
$instancia_usuario = ControlUsuario::singleton_usuario();

if (isset($_GET['id'])) {

    $id_usuario = base64_decode($_GET['id']);
    $id_url     = base64_decode($_GET['enlace']);

    $datos                    = $instancia_perfil->mostrarDatosPerfilControl($id_usuario);
    $datos_hoja               = $instancia->informacionHojaVidaControl($id_usuario);
    $datos_pregunta           = $instancia->preguntasHojaControl($datos_hoja['id']);
    $datos_academicos         = $instancia->informacionAcademicaControl($datos_hoja['id']);
    $datos_laborales          = $instancia->informacionLaboralControl($datos_hoja['id']);
    $datos_documentos         = $instancia->mostrarDocumentosTodosHojaControl($id_usuario);
    $datos_evaluacion_admin   = $instancia_usuario->mostrarDatosEvaluacionControl($id_usuario, 1);
    $datos_evaluacion_docente = $instancia_usuario->mostrarDatosEvaluacionControl($id_usuario, 2);

    $salario = (empty($datos_hoja['salario'])) ? 0 : $datos_hoja['salario'];

    $titulo = ($id_url == 1) ? 'HOJA DE VIDA INSTITUCIONAL' : 'HOJA DE VIDA';

    $empresa     = ($id_url == 1) ? 'CODETEC' : 'INCLUYEME HOY';
    $empresa     = ($id_url == 3) ? 'CDV' : $empresa;

    $foto_perfil = (!empty($datos_hoja['foto'])) ? $datos_hoja['foto'] : $datos_hoja['foto_hoja_vida'];

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }

}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Hoja de vida');
$pdf->SetSubject('Hoja de vida');
$pdf->SetKeywords('Hoja de vida');
$pdf->AddPage();

$pdf->Ln(2);

if ($id_url == 1) {
    $pdf->Cell(1);
    $pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 45, 10, '', '', 'C', false, 90, '', false, false, 1, false, false, false);
}
if ($id_url == 2) {
    $pdf->Cell(1);
    $pdf->Image(PUBLIC_PATH . 'img/logo_incluyeme_hoy.png', '', '', 42, 9, '', '', 'C', false, 90, '', false, false, 1, false, false, false);
}
if ($id_url == 3) {
    $pdf->Cell(10);
    $pdf->Image(PUBLIC_PATH . 'img/logo_cdv.png', '', '', 30, 12, '', '', 'C', false, 90, '', false, false, 1, false, false, false);
}
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, $empresa, 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, $titulo, 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GER-01', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 5', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2023-01-02', 'B', 0, 'C');

$pdf->Ln(11);
$pdf->Cell(139);
$pdf->Image(PUBLIC_PATH . 'upload/' . $foto_perfil, '', '', 47, 47, '', '', 'C', false, 80, '', false, false, 1, false, false, false);

$info_personal = '<table border="1" cellpadding="4" style="font-size:0.85em; width:70%;">
<tr style="font-weight: bold; background-color:  #c1c5c8  ;">
<th colspan="2">INFORMACI&Oacute;N PERSONAL</th>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Nombres: </span> ' . $datos['nombre'] . '</td>
<td><span style="font-weight: bold;">Apellidos: </span> ' . $datos['apellido'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Identificacion: </span>' . $datos['documento'] . '</td>
<td><span style="font-weight: bold;">Sexo: </span>' . $datos_hoja['nom_genero'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Tel&eacute;fono: </span>' . $datos['telefono'] . '</td>
<td><span style="font-weight: bold;">Celular: </span>' . $datos_hoja['celular'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Correo electr&oacute;nico: </span>' . $datos['correo'] . '</td>
<td><span style="font-weight: bold;">Direcci&oacute;n: </span>' . $datos_hoja['direccion'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Lugar de residencia: </span>' . $datos_hoja['lugar'] . '</td>
<td><span style="font-weight: bold;">Barrio: </span>' . $datos_hoja['barrio'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Fecha de nacimiento: </span>' . date('d/m/Y', strtotime($datos_hoja['fecha_nac'])) . '</td>
<td><span style="font-weight: bold;">Lugar de nacimiento: </span>' . $datos_hoja['lugar_nac'] . '</td>
</tr>
</table>';

$pdf->Ln(1);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($info_personal, true, false, true, false, '');

$info_adicional = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
<tr style="font-weight: bold; background-color:  #c1c5c8  ;">
<th colspan="2">INFORMACI&Oacute;N ADICIONAL</th>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Exped. Documento: </span> ' . $datos_hoja['exp_documento'] . '</td>
<td><span style="font-weight: bold;">Estado civil: </span> ' . $datos_hoja['nom_estado'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Escalaf&oacute;n: </span>' . $datos_hoja['escalafon'] . '</td>
<td><span style="font-weight: bold;">Nivel Acad&eacute;mico: </span>' . $datos_hoja['nivel_academico'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Calidad de Desempe&ntilde;o: </span>' . $datos_hoja['calidad_desempeno'] . '</td>
<td><span style="font-weight: bold;">Especialidad: </span>' . $datos_hoja['especialidad'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Fecha de Ingreso: </span>' . date('d/m/Y', strtotime($datos_hoja['fecha_ingreso'])) . '</td>
<td><span style="font-weight: bold;">Tipo de Vinculaci&oacute;n: </span>' . $datos_hoja['tiempo_laboral'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Origen Vinculaci&oacute;n: </span>' . $datos_hoja['tipo_vinculacion'] . '</td>
<td><span style="font-weight: bold;">Barrio: </span>' . $datos_hoja['origen_vinculacion'] . '</td>
</tr>
<tr style="text-align:left;">
<td><span style="font-weight: bold;">Fuente de Recursos: </span>' . $datos_hoja['fuente_recursos'] . '</td>
<td><span style="font-weight: bold;">Salario: </span>$' . number_format($salario) . '</td>
</tr>
<tr style="text-align:left;">
<td colspan="2"><span style="font-weight: bold;">Cargo: </span>' . $datos_hoja['cargo'] . '</td>
</tr>
</table>';

$pdf->Ln(-4);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($info_adicional, true, false, true, false, '');

$info_academica = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
<tr style="font-weight: bold; background-color:  #c1c5c8  ;">
<th colspan="7">INFORMACI&Oacute;N ACAD&Eacute;MICA</th>
</tr>
<tr style="text-align:center; font-weight: bold;">
<th>Instituci&oacute;n</th>
<th>Ciudad</th>
<th>Pais</th>
<th>Nivel Acad&eacute;mico</th>
<th colspan="3">Detalle</th>
</tr>
';

foreach ($datos_academicos as $academico) {
    $id_academico = $academico['id'];
    $institucion  = $academico['institucion'];
    $ciudad       = $academico['ciudad'];
    $pais         = $academico['pais'];
    $nivel        = $academico['nivel'];
    $grado        = $academico['graduado'];
    $fecha_grado  = date('d/m/Y', strtotime($academico['fecha_grado']));
    $titulo       = $academico['titulo'];

    $info_academica .= '
    <tr style="text-align: left;">
    <td>' . $institucion . '</td>
    <td>' . $ciudad . '</td>
    <td>' . $pais . '</td>
    <td>' . $nivel . '</td>
    <td>
    <span style="font-weight: bold;">Graduado</span>
    <br>
    ' . $grado . '
    </td>
    <td>
    <span style="font-weight: bold;">Fecha Graduaci&oacute;n</span>
    <br>
    ' . $fecha_grado . '
    </td>
    <td>
    <span style="font-weight: bold;">T&iacute;tulo Alcanzado</span>
    <br>
    ' . $titulo . '
    </td>
    </tr>
    ';

}

$info_academica .= '</table>';

$pdf->Ln(1);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($info_academica, true, false, true, false, '');

$info_laboral = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
<tr style="font-weight: bold; background-color:  #c1c5c8  ;">
<th colspan="8">INFORMACI&Oacute;N LABORAL</th>
</tr>
<tr style="text-align:center; font-weight: bold;">
<th>Empresa</th>
<th>Ciudad</th>
<th>Pais</th>
<th>Cargo</th>
<th>Tel&eacute;fono</th>
<th>Direcci&oacute;n</th>
<th>Ingreso</th>
<th>Terminaci&oacute;n</th>
</tr>
';

foreach ($datos_laborales as $laboral) {
    $id_laboral    = $laboral['id'];
    $empresa       = $laboral['empresa'];
    $ciudad        = $laboral['ciudad'];
    $pais          = $laboral['pais'];
    $cargo         = $laboral['cargo'];
    $telefono      = $laboral['telefono'];
    $direccion     = $laboral['direccion'];
    $fecha_ingreso = date('d/m/Y', strtotime($laboral['fecha_ingreso']));
    $fecha_salida  = ($laboral['fecha_salida'] == '0000-00-00') ? '' : date('d/m/Y', strtotime($laboral['fecha_salida']));

    $info_laboral .= '
    <tr style="text-align: left;">
    <td>' . $empresa . '</td>
    <td>' . $ciudad . '</td>
    <td>' . $pais . '</td>
    <td>' . $cargo . '</td>
    <td>' . $telefono . '</td>
    <td>' . $direccion . '</td>
    <td>' . $fecha_ingreso . '</td>
    <td>' . $fecha_salida . '</td>
    </tr>
    ';

}

$info_laboral .= '</table>';

$pdf->Ln(-4);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($info_laboral, true, false, true, false, '');

$info_laboral = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
<tr style="font-weight: bold; background-color:  #c1c5c8  ;">
<th colspan="2">PREGUNTAS PERSONALIZADAS</th>
</tr>
<tr style="text-align:left;">
<th><span style="font-weight: bold;">Diga Los Idiomas Diferentes Al Español Que Domina: </span> <br>' . $datos_pregunta['idiomas'] . '</th>
<th><span style="font-weight: bold;">Especifique Nivel: (Regular, Bien, Muy Bien): </span> <br>' . $datos_pregunta['idiomas_nivel'] . '</th>
</tr>
<tr style="text-align:left;">
<th><span style="font-weight: bold;">Referencia Laboral (Nombre, Empresa,Cargo, Teléfono ): </span> <br>' . $datos_pregunta['referencia_laboral'] . '</th>
<th><span style="font-weight: bold;">Autorizo A Codetec A Verificar Toda La Información Creada En Este Documento: </span> <br>' . $datos_pregunta['autorizo'] . '</th>
</tr>
<tr style="text-align:left;">
<th><span style="font-weight: bold;">En Caso De Emergencia Llamar A ( Nombre, Teléfono ): </span> <br>' . $datos_pregunta['emergencia'] . '</th>
<th><span style="font-weight: bold;">Referencia Personal (Nombre, Empresa,Cargo, Teléfono ): </span> <br>' . $datos_pregunta['referencia_personal'] . '</th>
</tr>
<tr style="text-align:left;">
<th><span style="font-weight: bold;">Libreta Militar ( Si Aplica ) (Clase, Número ): </span> <br>' . $datos_pregunta['libreta_militar'] . '</th>
</tr>
</table>';

$pdf->Ln(-4);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($info_laboral, true, false, true, false, '');

if ($id_url == 1) {

    $tabla_documentos = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
    <tr style="font-weight: bold; background-color:  #c1c5c8  ;">
    <th colspan="4">DOCUMENTOS</th>
    </tr>
    <tr style="font-weight: bold; text-align: center; font-size: 0.98em;">
    <th>DOCUMENTO</th>
    <th>DIRECCIÓN ACADÉMICA</th>
    <th>DIRECTOR/A BIENESTAR</th>
    <th>DEPARTAMENTO CONTABLE</th>
    </tr>';

    foreach ($datos_documentos as $documento) {
        $id_documento   = $documento['id'];
        $tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
        $archivo        = $documento['documento'];
        $perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

        $validar = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);

        $span_academico = ($perfil == 16) ? $validar : '<span class="badge badge-secondary">N/A</span>';
        $span_contable  = ($perfil == 11) ? $validar : '<span class="badge badge-secondary">N/A</span>';
        $span_bienestar = ($perfil == 17) ? $validar : '<span class="badge badge-secondary">N/A</span>';

        $tabla_documentos .= '
        <tr>
        <td>' . $tipo_documento . '</td>
        <td style="text-align: center; font-weight: bold; font-size: 0.9em;">' . $span_academico . '</td>
        <td style="text-align: center; font-weight: bold; font-size: 0.9em;">' . $span_bienestar . '</td>
        <td style="text-align: center; font-weight: bold; font-size: 0.9em;">' . $span_contable . '</td>
        </tr>
        ';

    }

    $tabla_documentos .= '</table>';

    $pdf->Ln(4);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_documentos, true, false, true, false, '');

} else {

    $tabla_documentos = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
    <tr style="font-weight: bold; background-color:  #c1c5c8  ;">
    <th colspan="3">DOCUMENTOS</th>
    </tr>
    <tr style="font-weight: bold; text-align: center; font-size: 0.98em;">
    <th>DOCUMENTO</th>
    <th>DOCUMENTO SUBIDO</th>
    <th>DOCUMENTO APROBADO</th>
    </tr>';

    foreach ($datos_documentos as $documento) {
        $id_documento   = $documento['id'];
        $tipo_documento = (empty($documento['nombre_archivo'])) ? $documento['nom_tipo'] : $documento['nombre_archivo'];
        $archivo        = $documento['documento'];
        $perfil         = (empty($documento['perfil_aut']) && $documento['perfil_aut'] == 0) ? $documento['perfil_autoriza'] : $documento['perfil_aut'];

        $validar          = $instancia->spanEstadoAutorizacionControl($documento['autorizacion_uno']);
        $verficacion_span = $instancia->spanEstadoAutorizacionControl($datos_hoja['verificacion']);

        $span_autoriza     = (!empty($documento['autorizacion_uno'])) ? $validar : '<span class="badge badge-secondary">N/A</span>';
        $span_verificacion = ($datos_hoja['verificacion'] != 0) ? $verficacion_span : '<span class="badge badge-warning">N/A</span>';

        $tabla_documentos .= '
        <tr>
        <td>' . $tipo_documento . '</td>
        <td style="text-align: center; font-weight: bold; font-size: 0.9em;">Aprobado</td>
        <td style="text-align: center; font-weight: bold; font-size: 0.9em;">' . $span_autoriza . '</td>
        </tr>
        ';

    }

    $tabla_documentos .= '</table>';

    $pdf->Ln(4);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_documentos, true, false, true, false, '');

}

if ($id_url == 1) {

    $tabla_evaluacion_admin = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
    <tr style="font-weight: bold; background-color:  #c1c5c8  ;">
    <th colspan="4">EVALUACIONES (Administrativa)</th>
    </tr>
    <tr style="font-weight: bold; text-align: center; font-size: 0.98em;">
    <th>A&Ntilde;O</th>
    <th>EVALUADOR</th>
    <th>PUNTUACI&Oacute;N</th>
    <th>DESEMPE&Ntilde;O</th>
    </tr>';

    foreach ($datos_evaluacion_admin as $evaluacion) {
        $anio       = $evaluacion['anio'];
        $puntuacion = ($evaluacion['suma_puntuacion'] / $evaluacion['cant_preguntas']);
        $evaluador  = $evaluacion['evaluador'];

        $span = ($puntuacion >= 1 && $puntuacion < 2) ? '<span class="badge badge-danger">Insuficiente</span>' : '';
        $span = ($puntuacion >= 2 && $puntuacion < 3) ? '<span class="badge badge-primary">B&aacute;sico</span>' : $span;
        $span = ($puntuacion >= 3 && $puntuacion < 4) ? '<span class="badge badge-warning">Regular</span>' : $span;
        $span = ($puntuacion >= 4 && $puntuacion < 5) ? '<span class="badge badge-info">Bueno</span>' : $span;
        $span = ($puntuacion == 5) ? '<span class="badge badge-success">Excelente</span>' : $span;

        $tabla_evaluacion_admin .= '
        <tr style="text-align:center;">
        <td>' . $anio . '</td>
        <td>' . $evaluador . '</td>
        <td>' . number_format($puntuacion, 2) . '</td>
        <td>' . $span . '</td>
        </tr>
        ';

    }

    $tabla_evaluacion_admin .= '</table>';

    $pdf->Ln(1);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_evaluacion_admin, true, false, true, false, '');

    $tabla_evaluacion_docente = '<table border="1" cellpadding="4" style="font-size:0.85em; width:98%;">
    <tr style="font-weight: bold; background-color:  #c1c5c8  ;">
    <th colspan="4">EVALUACIONES (Docente)</th>
    </tr>
    <tr style="font-weight: bold; text-align: center; font-size: 0.98em;">
    <th>A&Ntilde;O</th>
    <th>EVALUADOR</th>
    <th>PUNTUACI&Oacute;N</th>
    <th>DESEMPE&Ntilde;O</th>
    </tr>';

    foreach ($datos_evaluacion_docente as $evaluacion) {
        $anio       = $evaluacion['anio'];
        $puntuacion = ($evaluacion['suma_puntuacion'] / $evaluacion['cant_preguntas']);
        $evaluador  = $evaluacion['evaluador'];

        $span = ($puntuacion >= 1 && $puntuacion < 2) ? '<span class="badge badge-danger">Insuficiente</span>' : '';
        $span = ($puntuacion >= 2 && $puntuacion < 3) ? '<span class="badge badge-primary">B&aacute;sico</span>' : $span;
        $span = ($puntuacion >= 3 && $puntuacion < 4) ? '<span class="badge badge-warning">Regular</span>' : $span;
        $span = ($puntuacion >= 4 && $puntuacion < 5) ? '<span class="badge badge-info">Bueno</span>' : $span;
        $span = ($puntuacion == 5) ? '<span class="badge badge-success">Excelente</span>' : $span;

        $tabla_evaluacion_docente .= '
        <tr style="text-align:center;">
        <td>' . $anio . '</td>
        <td>' . $evaluador . '</td>
        <td>' . number_format($puntuacion, 2) . '</td>
        <td>' . $span . '</td>
        </tr>
        ';

    }

    $tabla_evaluacion_docente .= '</table>';

    $pdf->Ln(1);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_evaluacion_docente, true, false, true, false, '');
}

$pdf->Output('Hoja_vida_' . $datos['nombre'] . '_' . $datos['apellido'] . '.pdf', 'I');
