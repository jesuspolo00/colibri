<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

if (isset($_GET['id'])) {

    $id_inventario = base64_decode($_GET['id']);

    $datos_hoja = $instancia->hojaVidaArticuloControl($id_inventario);
}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {

    }

    public function Footer()
    {
        /*$this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');*/
    }
}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetMargins(0, 0, 0); // Márgenes izquierdo, superior y derecho en 0
$pdf->SetAutoPageBreak(false, 0); // Desactivar salto de página automático y margen inferior

$pdf->AddPage('P', array(100, 100));

$pdf->SetFont('', '', 20);

$pdf->Ln(15);
$pdf->Cell(100, 10, $datos_hoja['descripcion'], 0, 0, 'C');

$pdf->SetFont('', '', 15);

$pdf->Ln(8);
$pdf->Cell(100, 5, $datos_hoja['nom_area'], 0, 0, 'C');

/*-----------------------------------------------------*/

$style = array(
    'position'     => 'C',
    'align'        => 'C',
    'stretch'      => false,
    'fitwidth'     => true,
    'cellfitalign' => '',
    'border'       => true,
    'hpadding'     => '5',
    'vpadding'     => '5',
    'fgcolor'      => array(0, 0, 0),
    'bgcolor'      => false, //array(255,255,255),
    'text'         => true,
    'font'         => 'helvetica',
    'fontsize'     => 8,
    'stretchtext'  => 4,
);

// CODE 39
$pdf->Ln(15);

$pdf->write1DBarcode(strval($datos_hoja['codigo']), 'c128', '', '', '', 35, 1, $style, 'N', 'C');

$pdf->Output('codigo_' . $id_inventario . '.pdf', 'I');
