<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

if (isset($_GET['id'])) {

    $id_reporte = base64_decode($_GET['id']);

    $datos_reporte = $instancia->mostrarInformacionReporteSolucionControl($id_reporte);

    $titulo = 'SOLICITUD DE MANTENIMIENTO';

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {

    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->AddPage();

$pdf->Ln(0);
$pdf->Cell(3);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 40, 9, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, $titulo, 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GER-03', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 5', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2023-01-02', 'B', 0, 'C');

$encabezado = '
<table border="0" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:left;">
<td><b>Reportado por: </b>' . $datos_reporte['nom_user'] . '</td>
<td><b>Fecha Reportado: </b>' . $datos_reporte['fecha_reporte'] . '</td>
<td><b>Fecha Solucion: </b>' . $datos_reporte['fecha_respuesta'] . '</td>
</tr>
</table>
';

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($encabezado, true, false, true, false, '');

$tabla_ub = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th>Bloque</th>
<th>Area</th>
<th>Descripcion</th>
<th>Cantidad</th>
</tr>
<tr style="text-align:center;">
<td>' . $datos_reporte['nom_zona'] . '</td>
<td>' . $datos_reporte['nom_area'] . '</td>
<td>' . $datos_reporte['nom_inventario'] . '</td>
<td>' . $datos_reporte['cantidad'] . '</td>
</tr>
<tr>
<td colspan="4">
<p><b>Observacion:</b> ' . $datos_reporte['observacion'] . '</p>
</td>
</tr>
<tr>
<td colspan="4">
<p><b>Observacion Solucion:</b> ' . $datos_reporte['observacion_respuesta'] . '</p>
</td>
</tr>
</table>
';


$pdf->Ln(2);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_ub, true, false, true, false, '');

$pdf->Ln(3);
$encabezado = '
<table cellpadding="2" cellspacing="10" style="width: 87%; font-size: 0.9em;">
<tr>
<td style="width: 33%; text-align: center;">' . $datos_reporte['nom_user'] . '<br>_________________________<br><b>Reportado Por</b></td>
<td style="width: 33%; text-align: center;">' . $datos_reporte['nom_solucion'] . '<br>_________________________<br><b>Aprobado Por</b></td>
<td style="width: 33%; text-align: center;">El Técnico<br>_________________________<br><b>Solucionado Por</b></td>
</tr>
</table>
';

$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->Cell(10);
$pdf->writeHTMLCell(200, 0, '', '', $encabezado, '', 1, 0, true, 'L', true);

$pdf->Output('Reporte_solucion_' . $id_reporte . '.pdf', 'I');
