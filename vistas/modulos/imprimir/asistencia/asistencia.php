<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'numeros.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia_perfil = ControlPerfil::singleton_perfil();
$instancia        = ControlAsistencia::singleton_asistencia();

if (isset($_GET['asistencia'])) {

    $id_asistencia = base64_decode($_GET['asistencia']);

    $datos_asistencia = $instancia->mostrarDatosAsistenciaControl($id_asistencia);
    $datos_detalle    = $instancia->mostrarDetallesAsistenciaControl($id_asistencia);
    $datos_usuario    = $instancia_perfil->mostrarDatosPerfilControl($datos_asistencia['id_log']);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }

    }

    $pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Asistencia');
    $pdf->SetSubject('Asistencia');
    $pdf->SetKeywords('Asistencia');
    $pdf->AddPage();

    $encabezado = '
    <table border="1" cellpadding="5" style="font-size:1em; width:98%;">
    <tr style="text-align:center;">
    <td colspan="1" rowspan="2">
    <br>
    <br>
    <br>
    <img src="' . PUBLIC_PATH . 'img/logo.jpg" width="100">
    </td>
    <td colspan="5" style="text-align: center; font-weight: bold;">
    CORPORACION TECNICA DE ESTUDIOS ESPECIALIZADOS DEL CARIBE
    <br>
    <br>
    ASISTENCIA DOCENTE
    </td>
    </tr>
    <tr style="text-align: center; font-weight: bold;">
    <td colspan="2">Versión: 2</td>
    <td>Fecha: 01/08/2016</td>
    <td colspan="2">Código: RG-EFT-03</td>
    </tr>
    </table>
    ';

    $pdf->Ln(5);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($encabezado, true, false, true, false, '');

    $titulos = '
    <table border="1" cellpadding="5" style="font-size:0.95em; width:98%;">
    <tr style="text-align:center;">
    <td colspan="1" style="background-color: #BCD35F; font-weight: bold;">
    PROGRAMA
    </td>
    <td colspan="3">
    ' . $datos_asistencia['nom_programa'] . '
    </td>
    <td colspan="1" style="background-color: #BCD35F; font-weight: bold;">
    DOCENTE
    </td>
    <td colspan="3">
    ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
    </td>
    </tr>
    <tr style="text-align:center;">
    <td colspan="2" style="background-color: #BCD35F; font-weight: bold;">
    MODULO DE FORMACION
    </td>
    <td colspan="3">
    ' . $datos_asistencia['nom_modulo'] . '
    </td>
    <td colspan="2" style="background-color: #BCD35F; font-weight: bold;">
    HORAS PROGRAMADAS
    </td>
    <td colspan="2">
    ' . $datos_asistencia['horas'] . '
    </td>
    </tr>
    </table>
    ';

    $pdf->Ln(1);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($titulos, true, false, true, false, '');

    $contenido = '
    <table border="1" cellpadding="5" style="font-size:0.8em; width:98%;">
    <tr style="text-align:center; font-weight: bold;">
    <td colspan="3" style="background-color: #BCD35F; width: 15%;">
    FECHA
    </td>
    <td colspan="1" rowspan="2" style="background-color: #BCD35F;">
    HORA ENTRADA
    </td>
    <td colspan="1" rowspan="2" style="background-color: #BCD35F;">
    HORA SALIDA
    </td>
    <td colspan="1" rowspan="2" style="background-color: #BCD35F; width: 10%;">
    NUMERO DE HORAS
    </td>
    <td colspan="1" rowspan="2" style="background-color: #BCD35F;  width: 37.5%;">
    ACTIVIDAD O TEMTICA DESARROLLADA
    </td>
    <td colspan="1" rowspan="2" style="background-color: #BCD35F;">
    FIRMA DOCENTE
    </td>
    </tr>
    <tr style="text-align:center; font-weight: bold;">
    <td colspan="1" style="background-color: #BCD35F;">
    DD
    </td>
    <td colspan="1" style="background-color: #BCD35F;">
    MM
    </td>
    <td colspan="1" style="background-color: #BCD35F;">
    AAAA
    </td>
    </tr>';

    foreach ($datos_detalle as $detalle) {

        $id_detalle   = $detalle['id'];
        $hora_entrada = $detalle['hora_entrada'];
        $hora_salida  = $detalle['hora_salida'];
        $horas        = $detalle['horas'];
        $tematica     = $detalle['tematica'];
        $dia          = date('d', strtotime($detalle['fecha']));
        $mes          = date('m', strtotime($detalle['fecha']));
        $anio         = date('Y', strtotime($detalle['fecha']));

        $contenido .= '
        <tr style="text-align:center; font-size:1.1em;">
        <td>' . $dia . '</td>
        <td>' . $mes . '</td>
        <td>' . $anio . '</td>
        <td>' . date("g:i a", strtotime($hora_entrada)) . '</td>
        <td>' . date("g:i a", strtotime($hora_salida)) . '</td>
        <td>' . $horas . '</td>
        <td>' . strtoupper($tematica) . '</td>
        <td>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</td>
        </tr>
        ';
    }

    $contenido .= '
    </table>
    ';

    $pdf->Ln(1);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($contenido, true, false, true, false, '');

    $pdf->Output('asistencia.pdf', 'I');

}
