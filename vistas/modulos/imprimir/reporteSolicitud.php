<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';

$instancia = ControlSolicitud::singleton_solicitud();

$datos_solicitud = $instancia->mostrarSolicitudesControl();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
    ->setTitle('Reporte solicitud')
    ->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:G1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:G')->applyFromArray($estilos_datos);

foreach (range('A', 'G') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No')
    ->setCellValue('B1', 'AREA')
    ->setCellValue('C1', 'USUARIO')
    ->setCellValue('D1', 'JUSTIFICACION')
    ->setCellValue('E1', 'PROVEEDOR')
    ->setCellValue('F1', 'FECHA SOLICITADO / APLAZADO')
    ->setCellValue('G1', 'ESTADO');

$cont = 2;

foreach ($datos_solicitud as $solicitud) {
    $id_solicitud  = $solicitud['id'];
    $id_area       = $solicitud['id_area'];
    $id_user       = $solicitud['id_user'];
    $nom_user      = $solicitud['nom_usuario'];
    $nom_proveedor = $solicitud['nom_proveedor'];
    $nom_area      = $solicitud['area_nom'];
    $estado        = $solicitud['estado'];
    $justificacion = $solicitud['justificacion'];
    $activo        = $solicitud['activo'];
    $motivo        = $solicitud['motivo'];

    $texto = ($activo == 0) ? $motivo : $justificacion;

    $fechareg   = ($estado == 3 || $estado == 4) ? $solicitud['fecha_aplazado'] : $solicitud['fecha_solicitud'];
    $estado_nom = '';

    if ($estado == 0) {
        $estado_nom = 'Pendiente de revision';
    }

    if ($estado == 1) {
        $estado_nom = 'Aprobada';
    }

    if ($estado == 2) {
        $estado_nom = 'Rechazada';
    }

    if ($estado == 3) {
        $estado_nom = 'Aplazada';
    }

    if ($estado == 4) {
        $estado_nom = 'Aprobada - pendiente';
    }

    $sheet->setCellValue('A' . $cont, $id_solicitud)
        ->setCellValue('B' . $cont, $nom_area)
        ->setCellValue('C' . $cont, $nom_user)
        ->setCellValue('D' . $cont, $texto)
        ->setCellValue('E' . $cont, $nom_proveedor)
        ->setCellValue('F' . $cont, $fechareg)
        ->setCellValue('G' . $cont, $estado_nom);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Solicitudes_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
