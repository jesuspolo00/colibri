<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$instancia = ControlInventario::singleton_inventario();

$datos_articulos = $instancia->mostrarTodosArticulosExcelControl();

$spreadsheet->getProperties()
->setTitle('Reporte inventario')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:G1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:G')->applyFromArray($estilos_datos);

foreach (range('A', 'G') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No')
->setCellValue('B1', 'AREA')
->setCellValue('C1', 'DESCRIPCION')
->setCellValue('D1', 'MARCA')
->setCellValue('E1', 'CODIGO')
->setCellValue('F1', 'CATEGORIA')
->setCellValue('G1', 'ESTADO');

$cont    = 2;
$continv = 1;

foreach ($datos_articulos as $articulo) {
    $id_articulo  = $articulo['id'];
    $descripcion  = $articulo['descripcion'];
    $area         = $articulo['nom_area'];
    $estado       = $articulo['nom_estado'];
    $usuario      = $articulo['nom_user'];
    $marca        = $articulo['marca'];
    $codigo       = $articulo['codigo'];
    $id_usuario   = $articulo['id_user'];
    $id_categoria = $articulo['id_categoria'];

    $sheet->setCellValue('A' . $cont, $continv)
    ->setCellValue('B' . $cont, $area)
    ->setCellValue('C' . $cont, $descripcion)
    ->setCellValue('D' . $cont, $marca)
    ->setCellValue('E' . $cont, $codigo)
    ->setCellValue('F' . $cont, $articulo['nom_categoria'])
    ->setCellValue('G' . $cont, $estado);

    $cont++;
    $continv++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Inventario_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
