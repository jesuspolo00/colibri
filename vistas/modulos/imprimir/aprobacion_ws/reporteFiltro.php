<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$instancia = ControlAprobacion::singleton_aprobacion();

if (isset($_GET['fecha'])) {
    $fecha    = base64_decode($_GET['fecha']);
    $proyecto = base64_decode($_GET['proyecto']);
    $buscar   = base64_decode($_GET['buscar']);

    $datos           = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $buscar);
    $datos_solicitud = $instancia->buscarSolicitudesWsAprobacionControl($datos);
}

$spreadsheet->getProperties()
->setTitle('Reporte aprobacion de pagos FILTRO')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:I1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:I')->applyFromArray($estilos_datos);

foreach (range('A', 'I') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No. SOLICITUD')
->setCellValue('B1', 'USUARIO SOLICITA')
->setCellValue('C1', 'CORREO')
->setCellValue('D1', 'PROYECTO ASOCIADO')
->setCellValue('E1', 'ARCHIVO')
->setCellValue('F1', 'CONCEPTO')
->setCellValue('G1', 'VALOR')
->setCellValue('H1', 'FECHA SOLICITADO')
->setCellValue('I1', 'ESTADO');

$cont = 2;

foreach ($datos_solicitud as $solicitud) {
    $id_solicitud = $solicitud['id'];
    $documento    = $solicitud['documento'];
    $nom_completo = $solicitud['nom_user'];
    $correo       = $solicitud['correo'];
    $nom_estado   = $solicitud['estado_sol'];
    $nom_archivo  = $solicitud['nom_archivo'];
    $concepto     = $solicitud['concepto'];
    $nom_proyecto = $solicitud['nom_proyecto'];
    $fecha        = date('Y-m-d', strtotime($solicitud['fechareg']));
    $token        = $solicitud['token'];
    $valor        = ($solicitud['valor'] == '') ? 0 : number_format($solicitud['valor'], 1, ',', '.');
    $comprobante  = $solicitud['comprobante'];

    $sheet->setCellValue('A' . $cont, $id_solicitud)
    ->setCellValue('B' . $cont, $nom_completo)
    ->setCellValue('C' . $cont, $correo)
    ->setCellValue('D' . $cont, $nom_proyecto)
    ->setCellValue('E' . $cont, $nom_archivo)
    ->setCellValue('F' . $cont, $concepto)
    ->setCellValue('G' . $cont, $valor)
    ->setCellValue('H' . $cont, $fecha)
    ->setCellValue('I' . $cont, $nom_estado);

    //$sheet->getStyle('G' . $cont)->getNumberFormat()->setFormatCode('###.###,###');

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Aprobaciones_WS_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
