<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia->mostrarProveedoresControl();

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

// create a PDF object
$pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Proveedores');
$pdf->SetSubject('Proveedores');
$pdf->SetKeywords('Proveedores');
$pdf->AddPage();

$pdf->Ln(0);
$pdf->Cell(3);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 45, 10, '', '', 'T', false, 100, '', false, false, 0, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(50);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(225, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(50);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(225, 5, 'LISTADO DE PROVEEDORES EXTERNOS', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(50);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(75, 5, 'Codigo: RG-GEC-03', 'B', 0, 'C');
$pdf->Cell(75, 5, 'Version: 5', 'B', 0, 'C');
$pdf->Cell(75, 5, 'Fecha Version: 2023-01-02', 'B', 0, 'C');

$tabla = '
<table border="1" cellpadding="3" style="font-size:0.9em; width:98%;">
<tr style="text-align:center; font-weight: bold;">
<td colspan="10">INFORMACI&Oacute;N PROVEEDORES</td>
</tr>
<tr style="text-align:center; font-weight: bold;">
<td>No</td>
<td>FECHA INGRESO</td>
<td>NOMBRE O RAZON SOCIAL</td>
<td>IDENTIFICACION</td>
<td>NTELEFONO</td>
<td>CONTACTO</td>
<td>TELEFONO CONTACTO</td>
<td>DETALLE PRODUCTO</td>
<td>DOCUMENTOS</td>
<td>EVALUACION ANUAL</td>
</tr>
';

$continv = 1;

foreach ($datos_proveedor as $proveedor) {
    $id_proveedor   = $proveedor['id_proveedor'];
    $nombre         = $proveedor['razon_social'];
    $identificacion = $proveedor['num_identificacion'];
    $telefono       = $proveedor['telefono'];
    $nom_contacto   = $proveedor['contacto'];
    $tel_contacto   = $proveedor['telefono_contacto'];
    $detalle        = $proveedor['detalle_producto'];
    $fecha_ingreso  = $proveedor['fecha_ingreso'];

    $documentos_proveedor = $instancia->validarDocumentosProveedorControl($id_proveedor);
    $evaluacion_proveedor = $instancia->validarEvaluacionProveedorControl($id_proveedor);

    $documentos = ($documentos_proveedor['contar'] == 0) ? 'Faltan Documentos' : 'Todos los documentos subidos';
    $evaluacion = ($evaluacion_proveedor['id'] == '') ? 'No realizada' : 'Realizada';

    if ($evaluacion == 'No realizada') {
        $puntos_eval = '';
    } else {
        $puntos_eval = ' (Puntuacion: ' . ($evaluacion_proveedor['pregunta_1'] + $evaluacion_proveedor['pregunta_2'] + $evaluacion_proveedor['pregunta_3'] + $evaluacion_proveedor['pregunta_4'] + $evaluacion_proveedor['pregunta_5']) / 5 . ')';
    }

    $tabla .= '
    <tr style="text-align:center;">
    <td>' . $continv . '</td>
    <td>' . $fecha_ingreso . '</td>
    <td>' . $nombre . '</td>
    <td>' . $identificacion . '</td>
    <td>' . $telefono . '</td>
    <td>' . $nom_contacto . '</td>
    <td>' . $tel_contacto . '</td>
    <td>' . $detalle . '</td>
    <td>' . $documentos . '</td>
    <td>' . $evaluacion . $puntos_eval . '</td>
    </tr>
    ';

    $continv++;
}

$tabla .= '</table>';

$pdf->Ln(20);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla, true, false, true, false, '');

$pdf->Output('Proveedores' . date('Y-m-d-H-i-s') . '.pdf', 'I');
