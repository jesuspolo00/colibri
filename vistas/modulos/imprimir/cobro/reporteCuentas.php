<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

if (isset($_GET['buscar'])) {

    $buscar = $_GET['buscar'];
    $anio   = $_GET['anio'];
    $mes    = (empty($_GET['mes'])) ? 0 : $_GET['mes'];

    $datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $buscar);

    $datos_cobro = $instancia->buscarCuentaCobroControl($datos);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }

    }

    $pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Cuenta de cobro');
    $pdf->SetSubject('Cuenta de cobro');
    $pdf->SetKeywords('Cuenta de cobro');
    $pdf->AddPage();

    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
    $pdf->Ln(1);
    $pdf->Cell(50);
    $pdf->Cell(180, 6, 'CORPORACION TECNICA DE ESTUDIOS ESPECIALIZADOS DEL CARIBE "CODETEC"', 0, 0, 'C');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
    $pdf->Ln(6);
    $pdf->Cell(50);
    $pdf->Cell(180, 6, 'NÒMINA DE PRESTACION DE SERVICIO DOCENTES', 0, 0, 'C');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
    $pdf->Ln(6);
    $pdf->Cell(50);
    $pdf->Cell(180, 6, 'MES DE ' . strtoupper(mesesEspanol($mes)) . ' ' . $anio, 0, 0, 'C');

    $tabla_datos = '
    <table border="1" cellpadding="4" style="font-size:1em; width:98%;">
    <tr style="font-weight:bold; text-align:center;">
    <th>No. Cuenta Cobro</th>
    <th>Identificaci&oacute;n</th>
    <th>Apellidos y nombre</th>
    <th>Tipo</th>
    <th>Valor hora</th>
    <th>Horas ' . strtoupper(mesesEspanol($mes)) . '</th>
    <th>Subtotal a pagar</th>
    <th>Valor a pagar</th>
    </tr>';

    $sum_total    = 0;
    $sum_subtotal = 0;
    $horas_total  = 0;

    foreach ($datos_cobro as $cobro) {
        $id_cobro              = $cobro['id'];
        $usuario               = ($cobro['razon'] == '') ? $cobro['usuario'] : $cobro['razon'];
        $documento             = $cobro['documento'];
        $lugar                 = $cobro['lugar'];
        $direccion             = $cobro['direccion'];
        $telefono              = $cobro['telefono'];
        $anio                  = (!empty($cobro['anio_cuenta_asistencia'])) ? $cobro['anio_cuenta_asistencia'] : 'N/A';
        $mes                   = $cobro['mes_cuenta_asistencia'];
        $valor                 = number_format($cobro['valor']);
        $cuenta_detalle        = $cobro['cuenta_detalle'];
        $cuenta_documentos     = $cobro['cuenta_documentos'];
        $estado                = $cobro['estado'];
        $fechareg              = $cobro['fechareg'];
        $tipo                  = ($cobro['tipo'] == 1) ? 'Distrito' : 'Codetec';
        $horas_asistencia      = $cobro['horas_aprobadas_asistencia'];
        $horas_aprobadas_coord = (empty($cobro['horas_coord'])) ? 'N/A' : $cobro['horas_coord'];
        $horas_aprobadas_cont  = (empty($cobro['horas_cont'])) ? 'N/A' : $cobro['horas_cont'];
        $hora_valor            = (empty($cobro['hora_valor'])) ? $cobro['valor'] : $cobro['hora_valor'];
        $horas_pagar           = (empty($cobro['horas_pagar'])) ? 'N/A' : $cobro['horas_pagar'];

        if ($cobro['estado_contabilidad'] == 1) {

            $sum_total += ($horas_pagar * $hora_valor);
            $sum_subtotal += ($horas_pagar * $hora_valor);
            $horas_total += $horas_pagar;

            $tabla_datos .= '
            <tr style="text-align:center; font-weight:normal;">
            <td>' . $id_cobro . '</td>
            <td>' . $documento . '</td>
            <td>' . $usuario . '</td>
            <td>' . $tipo . '</td>
            <td>$' . number_format($hora_valor) . '</td>
            <td>' . $horas_pagar . '</td>
            <td>$' . number_format($horas_pagar * $hora_valor) . '</td>
            <td>$' . number_format($horas_pagar * $hora_valor) . '</td>
            </tr>
            ';

        }

    }

    $tabla_datos .= '
    <tr style="text-align:center; font-weight:bold;">
    <th colspan="5">TOTAL HORAS</th>
    <th>' . $horas_total . '</th>
    <th>$' . number_format($sum_subtotal) . '</th>
    <th>$' . number_format($sum_total) . '</th>
    </tr>
    </table>
    ';

    $pdf->Ln(12);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_datos, true, false, true, false, '');

    $tabla_firmas = '
    <table border="0" cellpadding="4" style="font-size:0.8em; width:98%;">
    <tr style="font-weight:bold; text-align:center;">
    <th>_______________________________________________</th>
    <th>_______________________________________________</th>
    </tr>
    <tr style="font-weight:bold; text-align:center;">
    <th>ERNESTO A. EGUIS RUIZ</th>
    <th>ORLANDO R. MENDOZA BARRIOS</th>
    </tr>
    <tr style="font-weight:bold; text-align:center;">
    <th>CONTADOR</th>
    <th>DIRECTOR ADMINISTRATIVO Y FINANCIERO</th>
    </tr>
    </table>
    ';

    $pdf->Ln(15);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_firmas, true, false, true, false, '');

    ob_clean();
    $pdf->Output('reporte_cuentas' . $anio . '_' . $mes . 'pdf', 'I');
}
