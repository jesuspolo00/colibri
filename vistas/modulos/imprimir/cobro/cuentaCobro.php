<?php
ob_start();
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

if (isset($_GET['cobro'])) {

    $id_cobro = base64_decode($_GET['cobro']);

    $datos_cobro   = $instancia->mostrarDatosCuentaCobroControl($id_cobro);
    $datos_detalle = $instancia->mostrarDetallesCuentasCobroControl($id_cobro);

    $dia_aprobado  = date('d', strtotime($datos_cobro['fecha_aprobado']));
    $mes_aprobado  = date('m', strtotime($datos_cobro['fecha_aprobado']));
    $anio_aprobado = date('Y', strtotime($datos_cobro['fecha_aprobado']));

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }

}

$pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Cuenta de cobro');
$pdf->SetSubject('Cuenta de cobro');
$pdf->SetKeywords('Cuenta de cobro');
$pdf->AddPage();

$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
$pdf->Ln(1);
$pdf->Cell(50);
$pdf->Cell(60, 6, 'DIA(' . $dia_aprobado . ') MES(' . $mes_aprobado . ') AÑO(' . $anio_aprobado . ')', 0, 0, 'L');

$pdf->SetFont(PDF_FONT_NAME_MAIN, 'BU', 13);
$pdf->Ln(5);
$pdf->Cell(5);
$pdf->Cell(60, 6, 'CUENTA DE COBRO', 0, 0, 'L');
$pdf->Cell(146.5);
$pdf->Cell(60, 6, 'No. ' . $id_cobro, 0, 0, 'R');

$tabla_cabecera = '
<table border="1" cellpadding="4" style="font-size:1.1em; width:98%;">
<tr style="">
<th style="width:25%; text-align:left; font-weight:bold;">NOMBRES Y APELLIDOS:</th>
<th style="width:75%; text-align:center;">' . strtoupper($datos_cobro['usuario']) . '</th>
</tr>
<tr style="">
<th style="text-align:left; width:25%; font-weight:bold;">O RAZÓN SOCIAL:</th>
<th style="text-align:center; width:75%;">' . strtoupper($datos_cobro['razon']) . '</th>
</tr>
<tr style="">
<th style="text-align:left; width:25%; font-weight:bold;">CEDULA O NIT:</th>
<th style="text-align:center; width:25%;">' . $datos_cobro['documento'] . '</th>
<th style="text-align:center; width:25%; font-weight:bold;">LUGAR DE EXPEDICION:</th>
<th style="text-align:center; width:25%;">' . strtoupper($datos_cobro['lugar']) . '</th>
</tr>
<tr style="">
<th style="text-align:left; width:25%; font-weight:bold;">DIRECCION: </th>
<th style="text-align:center; width:25%;">' . strtoupper($datos_cobro['direccion']) . '</th>
<th style="text-align:center; width:25%; font-weight:bold;">TELEFONO:</th>
<th style="text-align:center; width:25%;">' . $datos_cobro['telefono'] . '</th>
</tr>
<tr style="">
<th style="text-align:left; width:25%; font-weight:bold;">VALOR DE:</th>
<th style="text-align:center; width:75%;">$' . number_format($datos_cobro['hora_valor'] * $datos_cobro['horas_pagar']) . '</th>
</tr>
<tr style="">
<th style="text-align:left; width:25%; font-weight:bold;">VALOR EN LETRAS:</th>
<th style="text-align:center; width:75%;">' . convertirnumeroletra($datos_cobro['hora_valor'] * $datos_cobro['horas_pagar']) . '</th>
</tr>
</table>';

$pdf->Ln(8);
$pdf->Cell(5);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_cabecera, true, false, true, false, '');

$tabla_datos = '
<table border="1" cellpadding="4" style="font-size:1em; width:98%;">
<tr style="font-weight:bold; text-align:center;">
<th>MODULO</th>
<th>PROGRAMA</th>
<th>SEDE</th>
<th>COLEGIO</th>
<th>JORNADA</th>
<th>DIA DE SEMANAS</th>
<th>FECHAS</th>
<th>CICLO</th>
<th>SEMESTRE</th>
<th>HORAS</th>
</tr>';

$total_hora = 0;

foreach ($datos_detalle as $detalle) {
    $id_detalle    = $detalle['id'];
    $modulo        = $detalle['modulo'];
    $programa      = $detalle['programa'];
    $sede          = $detalle['sede'];
    $jornada_id    = $detalle['jornada'];
    $colegio       = $detalle['colegio'];
    $ciclo         = $detalle['ciclo'];
    $semestre      = $detalle['semestre'];
    $horas         = $detalle['horas'];
    $id_asistencia = $detalle['id_asistencia'];
    $nom_jornada   = $detalle['nom_jornada'];

    $datos_dias        = $instancia->mostrarDiasSemanaAsistenciaControl($id_asistencia);
    $datos_dias_numero = $instancia->mostrarDiasSemanaNumeroControl($id_asistencia);

    $dias_semana        = '';
    $dias_semana_numero = '';

    foreach ($datos_dias as $dias) {
        $dias_semana .= diasEspanol($dias['dia']) . ', ';
    }

    foreach ($datos_dias_numero as $dias_numero) {
        $dias_semana_numero .= $dias_numero['dia'] . ', ';
    }

    $total_hora += $horas;

    $tabla_datos .= '
    <tr style="text-align:center;">
    <th>' . $modulo . '</th>
    <th>' . $programa . '</th>
    <th>' . $sede . '</th>
    <th>' . $colegio . '</th>
    <th>' . $nom_jornada . '</th>
    <th>' . $dias_semana . '</th>
    <th>' . $dias_semana_numero . '</th>
    <th>' . $ciclo . '</th>
    <th>' . $semestre . '</th>
    <th>' . $horas . '</th>
    </tr>
    ';
}

$tabla_datos .= '
<tr style="text-align:center; font-weight:bold;">
<th colspan="9">TOTAL HORAS</th>
<th>' . $total_hora . '</th>
</tr>
</table>
';

$pdf->Ln(-3);
$pdf->Cell(5);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_datos, true, false, true, false, '');

$tabla_aprobacion = '
<table border="1" cellpadding="4" style="font-size:1em; width:98%;">
<tr style="font-weight:bold; text-align:center;">
<th>HORAS APROBADAS (COORDINADOR)</th>
<th>HORAS APROBADAS (CONTABILIDAD)</th>
<th>HORAS A PAGAR (CONTABILIDAD)</th>
<th>VALOR POR HORA</th>
<th>TOTAL A PAGAR</th>
</tr>
<tr style="text-align:center;">
<td>' . $datos_cobro['horas_coord'] . '</td>
<td>' . $datos_cobro['horas_cont'] . '</td>
<td>' . $datos_cobro['horas_pagar'] . '</td>
<td>$' . number_format($datos_cobro['hora_valor']) . '</td>
<td>$' . number_format($datos_cobro['hora_valor'] * $datos_cobro['horas_pagar']) . '</td>
</tr>
</table>
';

$pdf->Ln(2);
$pdf->Cell(5);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_aprobacion, true, false, true, false, '');

$pdf->Ln(-3);
$pdf->Cell(5);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
$pdf->MultiCell(267, 15, '->Según articulo 383 parágrafo 2. No tengo 2 o más trabajadores asociados a mi actividad.
    Los docentes que devenguen menos del salario minimo no aportan seguridad social de acuerdo al derecho 3032 art 9 parágrafo.', 0, 'L', 0, 0);

$firmas = '
<table border="0" cellpadding="4" style="font-size:1.1em; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th colspan="3">_________________________________</th>
<th colspan="3">_________________________________</th>
<th colspan="3">_________________________________</th>
</tr>
<tr style="text-align:center; font-weight:bold;">
<th colspan="3">FIRMA DEL DOCENTE</th>
<th colspan="3">FIRMA DEL COORDINADOR</th>
<th colspan="3">FIRMA DE CONTABILIDAD</th>
</tr>
</table>
';

$pdf->Ln(25);
$pdf->Cell(5);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($firmas, true, false, true, false, '');

ob_clean();
$pdf->Output('cuenta_cobro_no_' . $id_cobro . '.pdf', 'I');
