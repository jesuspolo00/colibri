<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'ingresos' . DS . 'ControlIngresos.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$instancia = ControlIngresos::singleton_ingresos();

$datos_egresos = $instancia->todosEgresosControl();

$spreadsheet->getProperties()
    ->setTitle('Reporte ingresos')
    ->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:I1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:I')->applyFromArray($estilos_datos);

foreach (range('A', 'I') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No. EGRESO')
    ->setCellValue('B1', 'DOCUMENTO')
    ->setCellValue('C1', 'NOMBRE COMPLETO')
    ->setCellValue('D1', 'CONCEPTO')
    ->setCellValue('E1', 'FECHA DE EGRESO')
    ->setCellValue('F1', 'METODO DE PAGO')
    ->setCellValue('G1', 'REALIZADO POR')
    ->setCellValue('H1', 'VALOR')
    ->setCellValue('I1', 'ESTADO');

$cont = 2;

foreach ($datos_egresos as $egreso) {
    $id_egreso    = $egreso['id'];
    $nom_user     = $egreso['nom_user'];
    $nom_concepto = $egreso['nom_concepto'];
    $nom_pago     = $egreso['nom_pago'];
    $fecha        = $egreso['fecha'];
    $valor        = $egreso['valor'];
    $doc_user     = $egreso['doc_user'];
    $activo       = ($egreso['activo'] == 0) ? 'Anulado' : '';

    $sheet->setCellValue('A' . $cont, $id_egreso)
        ->setCellValue('B' . $cont, $doc_user)
        ->setCellValue('C' . $cont, $nom_user)
        ->setCellValue('D' . $cont, $nom_concepto)
        ->setCellValue('E' . $cont, $fecha)
        ->setCellValue('F' . $cont, $nom_pago)
        ->setCellValue('G' . $cont, $egreso['usuario_realiza'])
        ->setCellValue('H' . $cont, $valor)
        ->setCellValue('I' . $cont, $activo);

    $sheet->getStyle('H' . $cont)->getNumberFormat()->setFormatCode('###,###,###');

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Ingresos_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
