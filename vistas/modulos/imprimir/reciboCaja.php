<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlRecibos::singleton_recibos();

if (isset($_GET['recibo'])) {

    $id_recibo    = base64_decode($_GET['recibo']);
    $datos_recibo = $instancia->mostrarDetallesReciboControl($id_recibo);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

// create a PDF object
    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->setData('encabezado.png');
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Inventario');
    $pdf->SetSubject('Inventario');
    $pdf->SetKeywords('Inventario');
    $pdf->AddPage();
    for ($i = 0; $i < 2; $i++) {

        $cell = 5;

        $valor = (empty($datos_recibo['valor'])) ? 0 : $datos_recibo['valor'];

        $pdf->Ln(8);
        $pdf->Cell(16);
        /*$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 50, 10);*/

        $pdf->Ln(-3);
        $pdf->Cell(115);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
        $pdf->Cell(70, 10, 'COMPROBANTE DE EGRESO', 1, 1, 'C', 0, '', 1);
        $pdf->Cell(115);
        $pdf->Cell(70, 5, 'No. ' . $id_recibo, 1, 1, 'C', 0, '', 1);

        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 11.5);
        $pdf->Ln(8);
        $pdf->Cell($cell);
        $pdf->MultiCell(180, 6, 'Proyecto: ' . $datos_recibo['nom_proyecto'], 1, 'L', 0, 1);

        $pdf->Cell($cell);
        $pdf->MultiCell(60, 6, 'Ciudad: ' . $datos_recibo['ciudad'], 1, 'L', 0, 0);
        $pdf->MultiCell(60, 6, 'Fecha: ' . $datos_recibo['fecha_pago'], 1, 'L', 0, 0);
        $pdf->MultiCell(60, 6, '$ ' . number_format($valor), 1, 'R', 0, 1);

        $pdf->Cell($cell);
        $pdf->MultiCell(180, 10, 'Pagado a: ' . $datos_recibo['nom_proveedor'], 1, 'L', 0, 1);

        $pdf->Cell($cell);
        $pdf->MultiCell(180, 15, 'Por concepto de: ' . $datos_recibo['concepto'], 1, 'L', 0, 1);

        $pdf->Cell($cell);
        $pdf->MultiCell(180, 10, 'La suma de: ' . strtoupper(convertirnumeroletra($datos_recibo['valor'])), 1, 'L', 0, 1);

        if ($datos_recibo['tipo_pago'] == 1) {
            $banco    = $datos_recibo['banco'];
            $cheque   = $datos_recibo['numero_cheque'];
            $efectivo = '';
        }
        if ($datos_recibo['tipo_pago'] == 2) {
            $banco    = $datos_recibo['banco'];
            $cheque   = '';
            $efectivo = '';
        }
        if ($datos_recibo['tipo_pago'] == 3) {
            $banco    = '';
            $cheque   = '';
            $efectivo = 'Cancelacion inmediata';
        }

        $pdf->Cell($cell);
        $pdf->MultiCell(60, 15, 'Cheque No: ' . $cheque, 1, 'L', 0, 0);
        $pdf->MultiCell(60, 15, 'Banco: ' . $banco, 1, 'L', 0, 0);
        $pdf->MultiCell(60, 15, 'Efectivo: ' . $efectivo, 1, 'L', 0, 1);

        $pdf->Ln(10);
        $pdf->Cell(125);
        $pdf->MultiCell(60, 10, 'Firma', 0, 'L', 0, 1);
        $pdf->Cell(125);
        $pdf->MultiCell(60, 10, 'C.C', 0, 'L', 0, 1);
        $pdf->Ln(15);
    }

    $pdf->Ln(-145);
    $pdf->Cell(1);
    $pdf->MultiCell(190, 10, '-------------------------------------------------------------------------------------------------------------------------------------', 0, 'C', 0, 1);}

    $pdf->Output('reciboCaja.pdf', 'I');
