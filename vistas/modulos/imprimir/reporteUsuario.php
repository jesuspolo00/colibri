<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$instancia   = ControlUsuario::singleton_usuario();

$datos_usuarios = $instancia->mostrarUsuariosControl();

$spreadsheet->getProperties()
->setTitle('Reporte usuarios')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:I1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:I')->applyFromArray($estilos_datos);

foreach (range('A', 'I') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No')
->setCellValue('B1', 'DOCUMENTO')
->setCellValue('C1', 'NOMBRE COMPLETO')
->setCellValue('D1', 'DESCRIPCION')
->setCellValue('E1', 'CORREO')
->setCellValue('F1', 'TELEFONO')
->setCellValue('G1', 'USUARIO')
->setCellValue('H1', 'PERFIL')
->setCellValue('I1', 'ESTADO');

$cont    = 2;
$continv = 1;


foreach ($datos_usuarios as $usuario) {
    $id_user         = $usuario['id_user'];
    $documento       = $usuario['documento'];
    $nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
    $correo          = $usuario['correo'];
    $telefono        = $usuario['telefono'];
    $user            = $usuario['user'];
    $perfil          = $usuario['nom_perfil'];
    $activo          = $usuario['estado'];

    $activo = ($activo == 0) ? 'Inactivo' : 'Activo';

    if ($usuario['perfil'] != 5 && $usuario['estado'] != 0) {

        $sheet->setCellValue('A' . $cont, $continv)
        ->setCellValue('B' . $cont, $documento)
        ->setCellValue('C' . $cont, $nombre_completo)
        ->setCellValue('D' . $cont, $correo)
        ->setCellValue('E' . $cont, $telefono)
        ->setCellValue('F' . $cont, $telefono)
        ->setCellValue('G' . $cont, $user)
        ->setCellValue('H' . $cont, $perfil)
        ->setCellValue('I' . $cont, $activo);

        $cont++;
        $continv++;
    }

}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Usuario_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
