<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia->mostrarProveedoresControl();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
->setTitle('Reporte proveedor')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:J1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:J')->applyFromArray($estilos_datos);

foreach (range('A', 'J') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No')
->setCellValue('B1', 'FECHA INGRESO')
->setCellValue('C1', 'NOMBRE O RAZON SOCIAL')
->setCellValue('D1', 'IDENTIFICACION')
->setCellValue('E1', 'TELEFONO')
->setCellValue('F1', 'CONTACTO')
->setCellValue('G1', 'TELEFONO CONTACTO')
->setCellValue('H1', 'DETALLE PRODUCTO')
->setCellValue('I1', 'DOCUMENTOS')
->setCellValue('J1', 'EVALUACION ANUAL');

$cont    = 2;
$continv = 1;

foreach ($datos_proveedor as $proveedor) {
    $id_proveedor   = $proveedor['id_proveedor'];
    $nombre         = $proveedor['razon_social'];
    $identificacion = $proveedor['num_identificacion'];
    $telefono       = $proveedor['telefono'];
    $nom_contacto   = $proveedor['contacto'];
    $tel_contacto   = $proveedor['telefono_contacto'];
    $detalle        = $proveedor['detalle_producto'];
    $fecha_ingreso  = $proveedor['fecha_ingreso'];

    $documentos_proveedor = $instancia->validarDocumentosProveedorControl($id_proveedor);
    $evaluacion_proveedor = $instancia->validarEvaluacionProveedorControl($id_proveedor);

    $documentos = ($documentos_proveedor['contar'] == 0) ? 'Faltan Documentos' : 'Todos los documentos subidos';
    $evaluacion = ($evaluacion_proveedor['id'] == '') ? 'No realizada' : 'Realizada';

    if ($evaluacion == 'No realizada') {
        $puntos_eval = '';
    } else {
        $puntos_eval = ' (Puntuacion: ' . ($evaluacion_proveedor['pregunta_1'] + $evaluacion_proveedor['pregunta_2'] + $evaluacion_proveedor['pregunta_3'] + $evaluacion_proveedor['pregunta_4'] + $evaluacion_proveedor['pregunta_5']) / 5 . ')';
    }

    $sheet->setCellValue('A' . $cont, $continv)
    ->setCellValue('B' . $cont, $fecha_ingreso)
    ->setCellValue('C' . $cont, $nombre)
    ->setCellValue('D' . $cont, $identificacion)
    ->setCellValue('E' . $cont, $telefono)
    ->setCellValue('F' . $cont, $nom_contacto)
    ->setCellValue('G' . $cont, $tel_contacto)
    ->setCellValue('H' . $cont, $detalle)
    ->setCellValue('I' . $cont, $documentos)
    ->setCellValue('J' . $cont, $evaluacion . $puntos_eval);

    $cont++;
    $continv++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Proveedor_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
