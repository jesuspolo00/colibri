<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$instancia = ControlAreas::singleton_areas();

if (isset($_GET['buscar'])) {
    $datos            = array('buscar' => $_GET['buscar'], 'fecha' => $_GET['fecha'], 'zona' => $_GET['zona']);
    $datos_inspeccion = $instancia->buscarInspeccionesLimiteControl($datos);
}

$spreadsheet->getProperties()
->setTitle('Reporte Inspecciones Zonas')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:G1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:G')->applyFromArray($estilos_datos);

foreach (range('A', 'G') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No. INSPECCION')
->setCellValue('B1', 'USUARIO REPORTA')
->setCellValue('C1', 'ZONA')
->setCellValue('D1', 'AREA')
->setCellValue('E1', 'COMPONENTE')
->setCellValue('F1', 'FECHA INSPECCION')
->setCellValue('G1', 'OBSERVACION');

$cont = 2;

foreach ($datos_inspeccion as $inspeccion) {
    $id_inspeccion = $inspeccion['id'];
    $nom_area      = $inspeccion['nom_area'];
    $nom_categoria = $inspeccion['nom_categoria'];
    $fecha         = $inspeccion['fecha'];
    $observacion   = $inspeccion['observacion'];
    $nom_zona      = $inspeccion['nom_zona'];
    $nom_user      = $inspeccion['nom_user'];

    $sheet->setCellValue('A' . $cont, $id_inspeccion)
    ->setCellValue('B' . $cont, $nom_user)
    ->setCellValue('C' . $cont, $nom_zona)
    ->setCellValue('D' . $cont, $nom_area)
    ->setCellValue('E' . $cont, $nom_categoria)
    ->setCellValue('F' . $cont, $fecha)
    ->setCellValue('G' . $cont, $observacion);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Inspeccion_" . date('Y-m-d-H-i-s') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
