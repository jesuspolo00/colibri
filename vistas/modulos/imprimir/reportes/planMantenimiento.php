<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_GET['id'])) {

    $id_plan = base64_decode($_GET['id']);

    $datos_plan    = $instancia->mostrarInformacionPlanControl($id_plan);
    $datos_detalle = $instancia->mostrarDetallesPlanControl($id_plan);

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {

    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Plan mantenimiento');
$pdf->SetSubject('Plan mantenimiento');
$pdf->SetKeywords('Plan mantenimiento');
$pdf->AddPage();

$pdf->Ln(0);
$pdf->Cell(3);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 40, 9, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, 'PLAN DE MANTENIMIENTO', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GER-07', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 4', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2024-01-02', 'B', 0, 'C');

$encabezado = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:left;">
<td style="background-color:#c4d79b; width: 25%;"><b>Fecha de vigencia del plan: </b></td>
<td style="background-color:white; width: 75%;">Del ' . fechaCastellano($datos_plan['fecha_inicio']) . ' hasta el ' . fechaCastellano($datos_plan['fecha_fin']) . '</td>
</tr>
</table>
';

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($encabezado, true, false, true, false, '');

$tabla_detalle = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; background-color:#c4d79b; font-weight: bold;">
<th>DESCRIPCI&Oacute;N</th>
<th>FECHA</th>
<th>UBICACI&Oacute;N</th>
<th>OBSERVACION</th>
<th>MANTENIMIENTO PREVENTIVO REALIZADO</th>
<th>OBSERVACION RESPUESTA</th>
</tr>
';

foreach ($datos_detalle as $detalle) {
    $id_detalle            = $detalle['id'];
    $nom_categoria         = $detalle['nom_categoria'];
    $fecha_mant            = $detalle['fecha_mant'];
    $nom_zona              = $detalle['nom_zona'];
    $observacion           = $detalle['observacion'];
    $observacion_respuesta = $detalle['observacion_respuesta'];

    $span_estado = ($detalle['estado'] == 1) ? '<span class="badge badge-success">Realizado</span>' : '<span class="badge badge-warning">Pendiente de revision</span>';
    $span_estado = ($datos_plan['fecha_fin'] < date('Y-m-d') && $detalle['estado'] != 1) ? '<span class="badge badge-danger">No Realizado</span>' : $span_estado;

    $tabla_detalle .= '
    <tr style="text-align:center;">
    <td>' . $nom_categoria . '</td>
    <td>' . $fecha_mant . '</td>
    <td>' . $nom_zona . '</td>
    <td>' . $observacion . '</td>
    <td>' . $span_estado . '</td>
    <td>' . $observacion_respuesta . '</td>
    </tr>
    ';

}

$tabla_detalle .= '</table>';

$pdf->Ln(1);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_detalle, true, false, true, false, '');

$encabezado = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:left;">
<td style="background-color:#c4d79b; width: 25%;"><b>Aprobado por: </b></td>
<td style="background-color:white; width: 75%;">' . $datos_plan['nom_user'] . '</td>
</tr>
<tr style="text-align:left;">
<td style="background-color:#c4d79b; width: 25%;"><b>Cargo: </b></td>
<td style="background-color:white; width: 75%;">' . $datos_plan['cargo'] . '</td>
</tr>
</table>
';

$pdf->Ln(1);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($encabezado, true, false, true, false, '');

$pdf->Output('Plan_mantenimiento_' . $id_plan . '.pdf', 'I');
