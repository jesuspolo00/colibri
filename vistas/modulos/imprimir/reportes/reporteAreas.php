<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_GET['id'])) {

    $id_inspeccion = base64_decode($_GET['id']);

    $datos_inspeccion = $instancia->mostrarInformacionInspeccionControl($id_inspeccion);

    $titulo = ($datos_inspeccion['estado'] == 2) ? 'MANTENIMIENTO' : 'INSPECCION';

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {

    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Reporte');
$pdf->SetSubject('Reporte');
$pdf->SetKeywords('Reporte');
$pdf->AddPage();

$pdf->Ln(0);
$pdf->Cell(3);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 40, 9, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, $titulo . ' DE AREA', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GER-03', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 3', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2022-03-15', 'B', 0, 'C');

$encabezado = '
<table border="0" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center;">
<td><b>Reportado por: </b>' . $datos_inspeccion['nom_user'] . '</td>
<td></td>
<td><b>Fecha Inspeccion: </b>' . $datos_inspeccion['fecha'] . '</td>
</tr>
</table>
';

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($encabezado, true, false, true, false, '');

$tabla_ub = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th>Zona</th>
<th>Area</th>
<th>Componente</th>
</tr>
<tr style="text-align:center;">
<td>' . $datos_inspeccion['nom_zona'] . '</td>
<td>' . $datos_inspeccion['nom_area'] . '</td>
<td>' . $datos_inspeccion['nom_categoria'] . '</td>
</tr>
<tr>
<td colspan="3">
<p><b>Observacion:</b> ' . $datos_inspeccion['observacion'] . '</p>
</td>
</tr>
</table>
';

$pdf->Ln(2);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_ub, true, false, true, false, '');

$pdf->Ln(3);
$encabezado = '
<table cellpadding="2" cellspacing="10" style="width: 87%; font-size: 0.9em;">
<tr>
<td style="width: 33%; text-align: center;">' . $datos_inspeccion['nom_user'] . '<br>_________________________<br><b>Reportado Por</b></td>
<td style="width: 33%; text-align: center;"><br><br>_________________________<br><b>Aprobado Por</b></td>
<td style="width: 33%; text-align: center;"><br><br>_________________________<br><b>Solucionado por</b></td>
</tr>
</table>
';

$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->Cell(10);
$pdf->writeHTMLCell(200, 0, '', '', $encabezado, '', 1, 0, true, 'L', true);

$pdf->Output('reporte' . date('Y-m-d-H-i-s') . '.pdf', 'I');
