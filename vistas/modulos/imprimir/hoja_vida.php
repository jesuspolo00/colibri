<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

if (isset($_GET['inventario'])) {

    $id_inventario = base64_decode($_GET['inventario']);

    $datos_articulos = $instancia->hojaVidaArticuloControl($id_inventario);
    $datos_reporte   = $instancia->mostrarReportesControl($id_inventario);
    $datos_hardware  = $instancia->mostrarHardwareInventarioControl($id_inventario);
    $datos_software  = $instancia->mostrarSoftwareInventarioControl($id_inventario);

    $fecha_update = ($datos_articulos['fecha_update'] == '') ? $datos_articulos['fecha_hoja'] : $datos_articulos['fecha_update'];

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

// create a PDF object
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Hoja Vida');
$pdf->SetSubject('Hoja Vida');
$pdf->SetKeywords('Hoja Vida');
$pdf->AddPage();

$pdf->Ln(0);
$pdf->Cell(3);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 45, 10, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, 'HOJA DE VIDA DE EQUIPO', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GER-04', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 5', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2024-01-02', 'B', 0, 'C');

$pdf->Ln(20);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Informacion general del equipo', 1, 0, 'C');

$ln = 6;

$pdf->Ln($ln + 3);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Nombre: ' . $datos_articulos['descripcion'], 0, 0, 'L');
$pdf->Cell(90, 5, 'Fecha de actualizacion: ' . $fecha_update, 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Marca: ' . $datos_articulos['marca'], 0, 0, 'L');
$pdf->Cell(90, 5, 'Modelo: ' . $datos_articulos['modelo'], 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Codigo inventario: ' . $datos_articulos['codigo'], 0, 0, 'L');
$pdf->Cell(90, 5, 'Estado: ' . $datos_articulos['nom_estado'], 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Dependencia: ' . $datos_articulos['nom_area'], 0, 0, 'L');
$pdf->Cell(90, 5, 'Bloque: ' . $datos_articulos['nom_zona'], 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Fecha de adquisicion: ' . $datos_articulos['fecha_compra'], 0, 0, 'L');
$pdf->Cell(90, 5, 'Proveedor: ' . $datos_articulos['nom_proveedor'], 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Frecuencia de mantenimiento: ' . $datos_articulos['frecuencia_mantenimiento'] . ' MESES', 0, 0, 'L');
$pdf->Cell(90, 5, 'Fecha vencimiento garantia: ' . $datos_articulos['fecha_garantia'], 0, 0, 'L');

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(90, 5, 'Contacto garantia: ' . $datos_articulos['contacto_garantia'], 0, 0, 'L');

/*-------------------Hardware----------------------*/
$pdf->Ln($ln + 3);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Componentes de hardware', 1, 0, 'C');

$tabla_hard = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th>Descripcion</th>
<th>Modelo</th>
<th>Marca</th>
<th>Codigo</th>
</tr>
';

foreach ($datos_hardware as $hard) {
    $id_hard = $hard['id'];
    $nombre  = $hard['descripcion'];
    $marca   = $hard['marca'];
    $modelo  = $hard['modelo'];
    $codigo  = $hard['codigo'];

    $tabla_hard .= '
    <tr style="text-align:center;">
    <td>' . $nombre . '</td>
    <td>' . $marca . '</td>
    <td>' . $modelo . '</td>
    <td>' . $codigo . '</td>
    </tr>
    ';
}

$tabla_hard .= '
</table>
';

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_hard, true, false, true, false, '');
/*--------------------------------------------------------*/

/*-------------------Software----------------------*/
$pdf->Ln(4);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Componentes de software', 1, 0, 'C');

$tabla_soft = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th>Descripcion</th>
<th>Version</th>
<th>Fabricante</th>
<th>Licencia</th>
</tr>
';

foreach ($datos_software as $software) {
    $descripcion_soft = $software['descripcion'];
    $version          = $software['version'];
    $fabricante       = $software['fabricante'];
    $licencia         = $software['licencia'];

    $tabla_soft .= '
    <tr style="text-align:center;">
    <td>' . $descripcion_soft . '</td>
    <td>' . $version . '</td>
    <td>' . $fabricante . '</td>
    <td>' . $licencia . '</td>
    </tr>
    ';
}

$tabla_soft .= '
</table>
';

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_soft, true, false, true, false, '');
/*-----------------------------------------------------*/

/*-------------------Ubicacion----------------------*/
$pdf->Ln(4);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(180, 5, 'Ubicacion y asignacion del equipo', 1, 0, 'C');

$tabla_ub = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th>Area responsable</th>
<th>Fecha de asignacion</th>
</tr>
<tr style="text-align:center;">
<td>' . $datos_articulos['nom_area'] . '</td>
<td>' . $datos_articulos['fecha_compra'] . '</td>
</tr>
</table>
';

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_ub, true, false, true, false, '');
/*-----------------------------------------------------*/

/*-------------------Ubicacion----------------------*/
$tabla_reporte = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th colspan="9">CONTROL DE MANTENIMIENTOS</th>
</tr>
<tr style="text-align:center; font-weight:bold;">
<th>No. Reporte</th>
<th>Bloque</th>
<th>Zona</th>
<th>Observacion reportada</th>
<th>Fecha reporte</th>
<th>Observacion Respuesta</th>
<th>Fecha Respuesta</th>
<th>Tipo de reporte</th>
<th>Estado</th>
</tr>
';

foreach ($datos_reporte as $reporte) {
    $id_reporte            = $reporte['id'];
    $nom_zona              = $reporte['nom_zona'];
    $nom_area              = $reporte['nom_area'];
    $observacion           = $reporte['observacion'];
    $fecha_reporte         = $reporte['fecha_reporte'];
    $observacion_respuesta = $reporte['observacion_respuesta'];
    $fecha_respuesta       = (empty($reporte['fecha_respuesta'])) ? '' : date('Y-m-d', strtotime($reporte['fecha_respuesta']));

    $ver_solucion = ($reporte['estado'] == 4) ? '' : 'd-none';
    $span_estado  = ($reporte['estado'] == 4) ? '<span class="badge badge-success">Solucionado</span>' : '<span class="badge badge-warning">Pendiente de solucion</span>';
    $span_tipo    = ($reporte['tipo_reporte'] == 1) ? '<span class="badge badge-danger">Correctivo</span>' : '<span class="badge badge-warning">Mantenimiento</span>';

    $tabla_reporte .= '
    <tr style="text-align:center;">
    <td>' . $id_reporte . '</td>
    <td>' . $nom_zona . '</td>
    <td>' . $nom_area . '</td>
    <td>' . $observacion . '</td>
    <td>' . date('Y-m-d', strtotime($fecha_reporte)) . '</td>
    <td>' . $observacion_respuesta . '</td>
    <td>' . $fecha_respuesta . '</td>
    <td>' . $span_tipo . '</td>
    <td>' . $span_estado . '</td>
    </tr>
    ';
}

$tabla_reporte .= '
</table>
';

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla_reporte, true, false, true, false, '');
/*-----------------------------------------------------*/

$style = array(
    'position'     => 'C',
    'align'        => 'C',
    'stretch'      => false,
    'fitwidth'     => true,
    'cellfitalign' => '',
    'border'       => false,
    'hpadding'     => 'auto',
    'vpadding'     => 'auto',
    'fgcolor'      => array(0, 0, 0),
    'bgcolor'      => false, //array(255,255,255),
    'text'         => true,
    'font'         => 'helvetica',
    'fontsize'     => 8,
    'stretchtext'  => 4,
);

// CODE 39
$pdf->Ln($ln + 8);
$pdf->write1DBarcode($datos_articulos['codigo'], 'C39', '', '', '', 18, 0.4, $style, 'N', 'C');
$pdf->Output('hoja_vida_' . date('Y-m-d-H-i-s') . '.pdf', 'I');
