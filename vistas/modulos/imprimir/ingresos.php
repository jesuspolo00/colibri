<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'ingresos' . DS . 'ControlIngresos.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlIngresos::singleton_ingresos();

if (isset($_GET['ingreso'])) {

    $id_ingreso = base64_decode($_GET['ingreso']);

    $datos_ingreso = $instancia->mostrarDatosIngresoIdControl($id_ingreso);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }

    }

    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Recibo de Ingresos');
    $pdf->SetSubject('Recibo de Ingresos');
    $pdf->SetKeywords('Recibo de Ingresos');
    $pdf->AddPage('P', 'A4');

    for ($i = 0; $i < 2; $i++) {

        $pdf->Ln(15);
        $pdf->Cell(8);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
        $pdf->Cell(20, 6, 'No: ' . $id_ingreso, 0, 0, 'L');


        $pdf->Ln(1.5);
        $pdf->Cell(48);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
        $pdf->Cell(70, 6, '', 0, 0, 'C');
        $pdf->Cell(70, 6, 'COMPROBANTE DE EGRESO No. ' . $id_ingreso, 0, 0, 'C');

        $tabla = '
        <table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
        <tr>
        <td><span style="font-weight: bold;">CEDULA: </span>' . $datos_ingreso['doc_user'] . '</td>
        <td><span style="font-weight: bold;">CIUDAD: </span>' . $datos_ingreso['nom_ciudad'] . '</td>
        </tr>
        <tr>
        <td><span style="font-weight: bold;">FECHA: </span>' . fechaCastellano($datos_ingreso['fecha']) . '</td>
        <td><span style="font-weight: bold;">POR CONCEPTO DE: </span>' . $datos_ingreso['nom_concepto'] . '</td>
        </tr>
        <tr>
        <td><span style="font-weight: bold;">VALOR: $</span>' . number_format($datos_ingreso['valor']) . '</td>
        <td><span style="font-weight: bold;">BENEFICIARIO: </span>' . $datos_ingreso['nom_user'] . '</td>
        </tr>
        <tr>
        <td colspan="2"><span style="font-weight: bold;">LA SUMA DE: </span>' . strtoupper(convertirnumeroletra($datos_ingreso['valor'])) . ' PESOS</td>
        </tr>';

        if ($datos_ingreso['metodo_pago'] == 5) {
            $tabla .= '
            <tr>
            <td><span style="font-weight: bold;">BANCO: ' . strtoupper($datos_ingreso['banco']) . '</span></td>
            <td><span style="font-weight: bold;">No. CHEQUE: </span>' . strtoupper($datos_ingreso['cheque']) . '</td>
            </tr>
            ';
        }

        $tabla .= '
        <tr>
        <td><span style="font-weight: bold;">TIPO DE PAGO: </span>' . strtoupper($datos_ingreso['nom_pago']) . '</td>
        <td><span style="font-weight: bold;">REALIZADO POR: </span>' . strtoupper($datos_ingreso['usuario_realiza']) . '</td>
        </tr>
        <tr>
        <td colspan="2"><span style="font-weight: bold;">OBSERVACION: </span>' . strtoupper($datos_ingreso['observacion']) . '</td>
        </tr>
        </table>
        ';

        $pdf->Ln(10);
        $pdf->Cell(6);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
        $pdf->writeHTML($tabla, true, false, true, false, '');

        $firma = '
        <table cellpadding="3" style="font-size:8.5px; width:98%;">
        <tr>
        <td style="font-weight: bold;">FIRMA Y SELLO</td>
        </tr>
        <tr>
        <td>
        <br>
        <br>
        <br>
        _____________________________________________________
        </td>
        </tr>
        </table>
        ';

        $pdf->Ln(5);
        $pdf->Cell(6);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
        $pdf->writeHTML($firma, true, false, true, false, '');

        $pdf->Ln(15);
        $pdf->Cell(5);
        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
        $pdf->MultiCell(180, 6, '-----------------------------------------------------------------------------------------------------------', 0, 'C', 0, 0, '', '', true);

        $pdf->Ln(10);
    }

    $pdf->Output('recibo_ingreso_' . $id_ingreso . '.pdf', 'I');
}
