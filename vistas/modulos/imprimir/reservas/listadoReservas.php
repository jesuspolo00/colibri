<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'reservas' . DS . 'ControlReserva.php';

$instancia = ControlReserva::singleton_reserva();

$datos_reserva = $instancia->mostrarAreasReservaHoyControl();

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }

}

$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Recibo de Egresos');
$pdf->SetSubject('Recibo de Egresos');
$pdf->SetKeywords('Recibo de Egresos');
$pdf->AddPage();

$tabla = '
<table cellpadding="3" style="width:98%;" border="1">
<tr style="text-transform:uppercase; text-align:center; font-weight: bold; font-size:1em;">
<th colspan="4">Listado de reservas</th>
</tr>';
foreach ($datos_reserva as $areas) {
    $id_area             = $areas['id'];
    $nom_area            = $areas['nombre'];
    $fecha_reserva_salon = $areas['fecha_reserva'];

    $tabla .= '<tr style="text-transform:uppercase; text-align:center; font-weight: bold; font-size:0.9em;">
    <th scope="col" colspan="4">' . $nom_area . '</th>
    </tr>
    <tr style="text-transform:uppercase; text-align:center; font-weight: bold; font-size:0.9em;">
    <th scope="col">Usuario Reserva</th>
    <th scope="col">Fecha Reserva</th>
    <th scope="col">Hora Reserva</th>
    <th scope="col">Detalle Reserva</th>
    </tr>
    ';

    $datos_detalle = $instancia->detalleReservaSalonControl($id_area);
    foreach ($datos_detalle as $detalle) {
        $id_reserva      = $detalle['id'];
        $fecha_reserva   = $detalle['fecha_reserva'];
        $horas           = $detalle['horas'];
        $detalle_reserva = $detalle['detalle'];
        $nom_user        = $detalle['nom_user'];

        $tabla .= '
        <tr style="text-transform:uppercase; text-align:center; font-size:0.9em;">
        <td>' . $nom_user . '</td>
        <td>' . $fecha_reserva . '</td>
        <td>' . $horas . '</td>
        <td>' . $detalle_reserva . '</td>
        </tr>
        ';

    }

}

$tabla .= '</table>';

$pdf->Ln(5);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla, true, false, true, false, '');

$pdf->Output('recibo_ingreso_' . $id_egreso . '.pdf', 'I');
