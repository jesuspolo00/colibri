<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

if (isset($_GET['buscar'])) {

    $datos_usuarios = $instancia->buscarUsuariosHojaVidaControl($_GET['buscar']);
}

$spreadsheet->getProperties()
->setTitle('Listado de plan de formacion')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:H1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:H')->applyFromArray($estilos_datos);

foreach (range('A', 'H') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'DOCUMENTO')
->setCellValue('B1', 'NOMBRE COMPLETO')
->setCellValue('C1', 'CORREO')
->setCellValue('D1', 'PERFIL')
->setCellValue('E1', 'EVALUACION DOCENTE')
->setCellValue('F1', 'PUNTUACION')
->setCellValue('G1', 'EVALUACION ADMINISTRATIVA')
->setCellValue('H1', 'PUNTUACION');

$cont = 2;

foreach ($datos_usuarios as $usuario) {
    $id_user     = $usuario['id_user'];
    $nom_usuario = $usuario['nombre'] . ' ' . $usuario['apellido'];
    $documento   = $usuario['documento'];
    $correo      = $usuario['correo'];
    $perfil      = $usuario['nom_perfil'];

    $anio_buscar = (isset($_GET['anio']) && !empty($_GET['anio'])) ? $_GET['anio'] : date('Y');

    $evaluacion_anual_administrativa = $instancia->mostrarPuntajeEvaluacionAnioControl($id_user, $anio_buscar, 1);
    $evaluacion_anual_docente        = $instancia->mostrarPuntajeEvaluacionAnioControl($id_user, $anio_buscar, 2);

    $punt_docente        = (!empty($evaluacion_anual_docente['suma_puntuacion'])) ? $evaluacion_anual_docente['suma_puntuacion'] / $evaluacion_anual_docente['cant_preguntas'] : 'N/A';
    $punt_administrativa = (!empty($evaluacion_anual_administrativa['suma_puntuacion'])) ? $evaluacion_anual_administrativa['suma_puntuacion'] / $evaluacion_anual_administrativa['cant_preguntas'] : 'N/A';

    $puntuacion_docente        = (!empty($evaluacion_anual_docente)) ? number_format($punt_docente, 2) : 'N/A';
    $puntuacion_administrativa = (!empty($evaluacion_anual_administrativa)) ? number_format($punt_administrativa, 2) : 'N/A';

    $span_docente        = (!empty($evaluacion_anual_docente)) ? 'Realizada - ' . $evaluacion_anual_docente['anio'] : 'No realizada / No aplica';
    $span_administrativo = (!empty($evaluacion_anual_administrativa)) ? 'Realizada - ' . $evaluacion_anual_administrativa['anio'] : 'No realizada / No aplica';

    $sheet->setCellValue('A' . $cont, $documento)
    ->setCellValue('B' . $cont, $nom_usuario)
    ->setCellValue('C' . $cont, $correo)
    ->setCellValue('D' . $cont, $perfil)
    ->setCellValue('E' . $cont, $span_docente)
    ->setCellValue('F' . $cont, $puntuacion_docente)
    ->setCellValue('G' . $cont, $span_administrativo)
    ->setCellValue('H' . $cont, $puntuacion_administrativa);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Listado_evaluaciones.xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
