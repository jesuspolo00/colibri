<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia = ControlPermisos::singleton_permisos();

$datos_permiso = $instancia->mostrarListadoTodosPermisosControl();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
->setTitle('Reporte Solicitud de permisos')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:I1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:I')->applyFromArray($estilos_datos);

foreach (range('A', 'I') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No. PERMISO')
    ->setCellValue('B1', 'DOCUMENTO')
    ->setCellValue('C1', 'NOMBRE COMPLETO')
    ->setCellValue('D1', 'CORREO')
    ->setCellValue('E1', 'TELEFONO')
    ->setCellValue('F1', 'FECHA SOLICITADO')
    ->setCellValue('G1', 'HORA INICIO')
    ->setCellValue('H1', 'HORA FIN')
    ->setCellValue('I1', 'ESTADO');

$cont = 2;

foreach ($datos_permiso as $listado) {
    $id_permiso  = $listado['id'];
    $documento   = $listado['documento'];
    $nom_user    = $listado['nom_user'];
    $correo      = $listado['correo'];
    $telefono    = $listado['telefono'];
    $fecha_sol   = $listado['fecha'];
    $hora_inicio = $listado['hora_inicio'];
    $hora_fin    = $listado['hora_fin'];
    $token       = $listado['token'];

    if ($listado['estado'] == 1) {
        $estado = 'Pendiente de revision';
    }

    if ($listado['estado'] == 2) {
        $estado = 'Aprobada';
    }

    if ($listado['estado'] == 3) {
        $estado = 'Denegada';
    }

    $sheet->setCellValue('A' . $cont, $id_permiso)
    ->setCellValue('B' . $cont, $documento)
    ->setCellValue('C' . $cont, $nom_user)
    ->setCellValue('D' . $cont, $correo)
    ->setCellValue('E' . $cont, $telefono)
    ->setCellValue('F' . $cont, $fecha_sol)
    ->setCellValue('G' . $cont, $hora_inicio)
    ->setCellValue('H' . $cont, $hora_fin)
    ->setCellValue('I' . $cont, $estado);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');
$spreadsheet->setActiveSheetIndex(0);

$fileName = "Reporte_Listado_Permisos_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
