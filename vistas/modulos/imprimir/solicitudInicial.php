<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlSolicitud::singleton_solicitud();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

if (isset($_GET['solicitud'])) {

    $id_solicitud = base64_decode($_GET['solicitud']);

    $datos_solicitud    = $instancia->mostrarDatosSolicitudInicialIdControl($id_solicitud);
    $productos          = $instancia->mostrarProdcutosSolicitudInicialControl($id_solicitud);
    $datos_veriifcacion = $instancia->mostrarDatosVerificacionInicialControl($id_solicitud);
    $datos_proveedor    = $instancia_proveedor->mostrarDatosProveedorIdControl($datos_solicitud['id_proveedor']);

    $cumple_cant    = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['cantidad']) && $datos_veriifcacion['cantidad'] == 'Si') ? 'X' : '';
    $no_cumple_cant = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['cantidad']) && $datos_veriifcacion['cantidad'] == 'No') ? 'X' : '';

    $cumple_calidad    = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['calidad']) && $datos_veriifcacion['calidad'] == 'Si') ? 'X' : '';
    $no_cumple_calidad = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['calidad']) && $datos_veriifcacion['calidad'] == 'No') ? 'X' : '';

    $cumple_precio    = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['precios']) && $datos_veriifcacion['precios'] == 'Si') ? 'X' : '';
    $no_cumple_precio = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['precios']) && $datos_veriifcacion['precios'] == 'No') ? 'X' : '';

    $cumple_plazo    = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['plazos']) && $datos_veriifcacion['plazos'] == 'Si') ? 'X' : '';
    $no_cumple_plazo = (is_array($datos_veriifcacion) && isset($datos_veriifcacion['plazos']) && $datos_veriifcacion['plazos'] == 'No') ? 'X' : '';

}

class MYPDF extends TCPDF
{

    public function setData($logo)
    {
        $this->logo = $logo;
    }

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
    }
}

// create a PDF object
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
$pdf->SetCreator(PDF_CREATOR);
$pdf->setData('encabezado.png');
$pdf->SetAuthor('Jesus Polo');
$pdf->SetTitle('Solicitud');
$pdf->SetSubject('Solicitud');
$pdf->SetKeywords('Solicitud');
$pdf->AddPage();

$pdf->Ln(-6);
$pdf->Cell(10);
$pdf->Cell(320, 5, 'No. ' . $datos_solicitud['consecutivo'], '', 0, 'C');
$pdf->Ln(10);
$pdf->Cell(1);
$pdf->Image(PUBLIC_PATH . 'img/logo.png', '', '', 45, 10, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
$pdf->Ln(-5);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(142.5, 5, 'CODETEC', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
$pdf->Cell(142.5, 5, 'SOLICITUD DE PRODUCTOS Y SERVICIOS SUMINISTRADOS EXTERNAMENTE', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->Cell(45);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
$pdf->Cell(47.5, 5, 'Codigo: RG-GEC-05', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Version: 4', 'B', 0, 'C');
$pdf->Cell(47.5, 5, 'Fecha Version: 2023-01-02', 'B', 0, 'C');

$tabla = '
<table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
<tr style="text-align:center; font-weight:bold;">
<th style="width: 10%;">Cantidad</th>
<th style="width: 90%;">Descripcion</th>
</tr>
';

$subtotal = 0;
$iva      = $datos_solicitud['iva'];

foreach ($productos as $producto) {
    $id_producto = $producto['id'];
    $nombre      = $producto['producto'];
    $cantidad    = $producto['cantidad'];
    $precio      = $producto['precio'];

    $tabla .= '
    <tr style="text-align: center;">
    <td>' . $cantidad . '</td>
    <td>' . $nombre . '</td>
    </tr>
    ';

}

$tabla .= '
<tr>
<td  style="width: 100%;"><span style="font-weight:bold;">Nombre de quien elabora:</span></td>
</tr>
<tr>
<td  style="width: 50%;"><span style="font-weight:bold;">Nombre de quien solicita:</span> ' . $datos_solicitud['nom_usuario'] . '</td>
<td  style="width: 50%;"><span style="font-weight:bold;">Firma del director financiero:</span></td>
</tr>
</table>
';

$pdf->Ln(15);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->writeHTML($tabla, true, false, true, false, '');

$ln = 5;
$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-Codetec se reserva el derecho de rechazar los productos  o servicios de esta orden de compra por cualquier causa que la haga insatisfactoria, dentro de los 30 dias  siguientes al recibo de la misma.', 0, 'L', 0, 0, '', '', true);

$pdf->Ln($ln + 3);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-Recepcion de productos o servicios solamente en horarios de  oficina.', 0, 'L', 0, 0, '', '', true);

$pdf->Ln($ln);
$pdf->Cell(6);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 7);
$pdf->MultiCell(180, 5, '-El despacho de esta orden de compra implica aceptacion de estas condiciónes.', 0, 'L', 0, 0, '', '', true);

$nombre_archivo = PUBLIC_PATH_ARCH . 'upload' . DS . 'solicitud_inicial_' . md5($id_solicitud);

$pdf->Output($nombre_archivo . '.pdf', 'I');
