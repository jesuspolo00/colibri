<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlAsistencia::singleton_asistencia();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 40);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['asistencia'])) {

	$id_asistencia = base64_decode($_GET['asistencia']);

	$datos_asistencia = $instancia->mostrarDatosAsistenciaControl($id_asistencia);
	$datos_detalle    = $instancia->mostrarDetallesAsistenciaControl($id_asistencia);

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>codetec/asistencia/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Asistencia - (<?=mesesEspanol($datos_asistencia['mes_asistencia']);?> del <?=$datos_asistencia['anio_asistencia']?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_asistencia" value="<?=$id_asistencia?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Programa</label>
									<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_programa']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Competencia</label>
									<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_competencia']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Modulo de formaci&oacute;n</label>
									<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_modulo']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Horas Programadas/Restantes <span class="text-danger">*</span></label>
									<input type="text" class="form-control numeros" name="horas_programadas" value="<?=$datos_asistencia['horas_realizadas']?>" readonly required>
								</div>
								<div class="col-lg-12 form-group text-right">
									<button class="btn btn-primary btn-sm agregar_producto" type="button">
										<i class="fa fa-plus"></i>
										&nbsp;
										Agregar Fila
									</button>
								</div>
							</div>
							<div class="table-responsive mt-2">
								<table class="table table-hover border table-sm" width="100%" cellspacing="0">
									<thead>
										<tr class="text-center font-weight-bold">
											<th scope="col" colspan="6">Registro de asistencia</th>
										</tr>
										<tr class="text-center text-uppercase font-weight-bold">
											<th scope="col">Fecha</th>
											<th scope="col">Hora Entrada</th>
											<th scope="col">Hora Salida</th>
											<th scope="col">Numero de horas</th>
											<th scope="col">Actividad o tematica desarrollada</th>
										</tr>
									</thead>
									<tbody class="buscar">
										<?php
										$cont = 0;
										foreach ($datos_detalle as $detalle) {
											$id_detalle   = $detalle['id'];
											$fecha        = $detalle['fecha'];
											$hora_entrada = $detalle['hora_entrada'];
											$hora_salida  = $detalle['hora_salida'];
											$horas        = $detalle['horas'];
											$tematica     = $detalle['tematica'];

											$ver_eliminar = ($cont == 0) ? 'd-none' : '';

											?>
											<tr class="text-center" id="fila<?=$id_detalle?>">
												<td><input type="date" class="form-control" name="fecha[]" required value="<?=$fecha?>"></td>
												<td><input type="time" class="form-control hora_entrada" data-id="<?=$id_detalle?>" id="hora_entrada_<?=$id_detalle?>" name="hora_entrada[]" required value="<?=$hora_entrada?>"></td>
												<td><input type="time" class="form-control hora_salida" data-id="<?=$id_detalle?>" id="hora_salida_<?=$id_detalle?>" name="hora_salida[]" value="<?=$hora_salida?>" required></td>
												<td><input type="text" class="form-control numeros text-center hora_diferencia" data-id="<?=$id_detalle?>" id="hora_diferencia_<?=$id_detalle?>" readonly name="horas[]" required maxlength="2" minlength="1" value="<?=$horas?>"></td>
												<td><input type="text" class="form-control" name="tematica[]" value="<?=$tematica?>" required></td>
												<td>
													<div class="btn-group">
														<button class="btn btn-danger btn-sm remover_input <?=$ver_eliminar?>" id="<?=$id_detalle?>" type="button">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</td>
											</tr>
											<?php
											$cont++;
										}
										?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-lg-12 form-group mt-2">
									<button class="btn btn-danger btn-sm finalizar float-left" type="button" id="<?=$id_asistencia?>">
										<i class="fas fa-check-circle"></i>
										&nbsp;
										Finalizar Asistencia
									</button>
									<button class="btn btn-success btn-sm float-right" type="submit">
										<i class="fa fa-edit"></i>
										&nbsp;
										Editar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_log'])) {
		$instancia->editarAsistenciaControl();
	}

}
?>
<script src="<?=PUBLIC_PATH?>js/asistencia/funcionesAsistencia.js"></script>