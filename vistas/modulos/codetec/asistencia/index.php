<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlAsistencia::singleton_asistencia();

if (isset($_POST['buscar'])) {
	$datos_asistencia = $instancia->buscarAsistenciaControl($id_log, $buscar);
} else {
	$datos_asistencia = $instancia->mostrarLimiteAsistenciaControl($id_log);
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 40);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>codetec/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Asistencias - Codetec
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>codetec/asistencia/agregarAsistencia" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar asistencia
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Programa</th>
									<th scope="col">Competencia</th>
									<th scope="col">Modulo de formaci&oacute;n</th>
									<th scope="col">Horas programadas</th>
									<th scope="col">A&ntilde;o asistencia</th>
									<th scope="col">Mes Asistencia</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_asistencia as $asistencia) {
									$id_asistencia     = $asistencia['id'];
									$nom_programa      = $asistencia['nom_programa'];
									$nom_competencia   = $asistencia['nom_competencia'];
									$nom_modulo        = $asistencia['nom_modulo'];
									$horas_programadas = $asistencia['horas'];
									$mes_asistencia    = $asistencia['mes_asistencia'];
									$anio_asistencia   = $asistencia['anio_asistencia'];
									$visto_bueno       = $asistencia['visto_bueno'];
									$finalizado        = $asistencia['finalizado'];
									$tipo              = $asistencia['tipo'];

									$mes_asistencia = mesesEspanol($mes_asistencia);

									if ($finalizado == 0) {
										$ver_detalle = 'd-none';
										$ver_pdf     = 'd-none';
										$ver_editar  = '';
										$span        = '<span class="badge badge-secondary">Pendiente de finalizacion</span>';
									}

									if ($finalizado == 1 && $visto_bueno == 0) {
										$ver_detalle = 'd-none';
										$ver_pdf     = 'd-none';
										$ver_editar  = 'd-none';
										$span        = '<span class="badge badge-warning">Pendiente de visto bueno</span>';
									}

									if ($finalizado == 1 && $visto_bueno == 1) {
										$ver_detalle = 'd-none';
										$ver_pdf     = 'd-none';
										$ver_editar  = 'd-none';
										$span        = '<span class="badge badge-danger">Denegada</span>';
									}

									if ($finalizado == 1 && $visto_bueno == 2) {
										$ver_detalle = '';
										$ver_pdf     = '';
										$ver_editar  = 'd-none';
										$span        = '<span class="badge badge-success">Aprobada</span>';
									}

									if ($tipo == 2) {

										?>
										<tr class="text-center">
											<td><?=$nom_programa?></td>
											<td><?=$nom_modulo?></td>
											<td><?=$nom_competencia?></td>
											<td><?=$horas_programadas?></td>
											<td><?=$anio_asistencia?></td>
											<td><?=$mes_asistencia?></td>
											<td><?=$span?></td>
											<td>
												<div class="btn-group">
													<a href="<?=BASE_URL?>codetec/asistencia/detalles?asistencia=<?=base64_encode($id_asistencia)?>" class="btn btn-success btn-sm <?=$ver_editar?>" data-tooltip="tooltip" title="Editar Asistencia" data-placement="bottom">
														<i class="fa fa-edit"></i>
													</a>
													<a href="<?=BASE_URL?>codetec/asistencia/detalleAsistencia?asistencia=<?=base64_encode($id_asistencia)?>" class="btn btn-info btn-sm <?=$ver_detalle?>" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
														<i class="fa fa-eye"></i>
													</a>
													<a href="<?=BASE_URL?>imprimir/asistencia/asistencia?asistencia=<?=base64_encode($id_asistencia)?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_pdf?>" data-tooltip="tooltip" title="Imprimir Asistencia" data-placement="bottom">
														<i class="fa fa-file-pdf"></i>
													</a>
												</div>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>