<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$datos_programas = $instancia->mostrarCargaAcademicaAsignadaControl($id_log, 2);

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 40);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>codetec/asistencia/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Agregar Asistencia
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" id="log" value="<?=$id_log?>">
						<input type="hidden" name="tipo" id="tipo" value="2">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Programa <span class="text-danger">*</span></label>
								<select name="programa" class="form-control" id="programa" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_programas as $programa) {
										$id_programa  = $programa['id_programa'];
										$nom_programa = $programa['nom_programa'];
										?>
										<option value="<?=$id_programa?>"><?=$nom_programa?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Competencias <span class="text-danger">*</span></label>
								<select name="competencia" class="form-control" id="competencias" required>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Modulos <span class="text-danger">*</span></label>
								<select name="modulo" class="form-control" id="modulos" required>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Horas Programadas/Restantes <span class="text-danger">*</span></label>
								<input type="text" class="form-control numeros" readonly id="horas" name="horas_programadas" required maxlength="2" minlength="1">
							</div>
							<div class="col-lg-12 form-group text-right">
								<button class="btn btn-secondary btn-sm agregar_producto" type="button">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar Fila
								</button>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col" colspan="6">Registro de asistencia</th>
									</tr>
									<tr class="text-center text-uppercase font-weight-bold">
										<th scope="col">Fecha</th>
										<th scope="col">Hora Entrada</th>
										<th scope="col">Hora Salida</th>
										<th scope="col">Numero de horas</th>
										<th scope="col">Actividad o tematica desarrollada</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<tr class="text-center">
										<td><input type="date" class="form-control" name="fecha[]" required></td>
										<td><input type="time" class="form-control hora_entrada" id="hora_entrada_123789456" data-id="123789456" name="hora_entrada[]" required></td>
										<td><input type="time" class="form-control hora_salida" id="hora_salida_123789456" data-id="123789456" name="hora_salida[]" required></td>
										<td><input type="text" class="form-control numeros text-center hora_diferencia" name="horas[]" data-id="123789456" id="hora_diferencia_123789456" required maxlength="2" minlength="1" value="0" readonly></td>
										<td><input type="text" class="form-control" name="tematica[]" required></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->guardarAsistenciaControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/asistencia/funcionesAsistencia.js"></script>