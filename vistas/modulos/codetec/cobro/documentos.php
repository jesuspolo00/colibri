<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 42);
if (!$permiso) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['cobro'])) {

    $id_cobro = base64_decode($_GET['cobro']);

    $datos_cobro = $instancia->mostrarDatosCuentaCobroControl($id_cobro);

    $documento_cedula = $instancia->validarDocumentoUsuarioControl($id_log, 1);
    $documento_rut    = $instancia->validarDocumentoUsuarioControl($id_log, 2);

    $ver_rut    = (empty($documento_rut)) ? '' : 'd-none';
    $ver_cedula = (empty($documento_cedula)) ? '' : 'd-none';

    $documentos_cobro = '';

    $mes_asistencia = (!empty($datos_cobro['mes_cuenta_asistencia'])) ? mesesEspanol($datos_cobro['mes_cuenta_asistencia']) : 'N/A';
    $anio           = (!empty($datos_cobro['anio_cuenta_asistencia'])) ? $datos_cobro['anio_cuenta_asistencia'] : 'N/A';
    ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>codetec/cobro/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Documentos Cuenta de cobro No. <?=$id_cobro?> (<?=$mes_asistencia?> del <?=$anio?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data">
							<input type="hidden" name="id_cuenta_cobro" value="<?=$id_cobro?>">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="rut_doc" value="<?=$documento_rut['documento']?>">
							<input type="hidden" name="cedula_doc" value="<?=$documento_cedula['documento']?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group <?=$ver_rut?>">
									<label class="font-weight-bold">RUT <span class="text-danger">*</span></label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="rut" id="rut" required accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label_rut" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-4 form-group <?=$ver_cedula?>">
									<label class="font-weight-bold">Cedula <span class="text-danger">*</span></label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="cedula" id="cedula" required accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label_cedula" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Certificacion Bancaria <span class="text-danger">*</span></label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="certificacion" id="certificacion" required accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label_certificacion" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Seguridad Social (Mensual) <span class="text-danger">*</span></label>
									<div class="custom-file pmd-custom-file-filled">
										<input type="file" class="custom-file-input file_input" name="seguridad" id="seguridad" required accept=".png, .jpg, .jpeg, .pdf">
										<label class="custom-file-label file_label_seguridad" for="customfilledFile"></label>
									</div>
								</div>
								<div class="col-lg-12 form-group text-right mt-2">
									<button type="submit" class="btn btn-success btn-sm">
										<i class="fas fa-upload"></i>
										&nbsp;
										Subir Documentos
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['id_cuenta_cobro'])) {
        $instancia->subirDocumentosCuentaCobroControl();
    }
}
?>