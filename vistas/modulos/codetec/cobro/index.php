<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

if (isset($_POST['buscar'])) {
	$buscar      = $_POST['buscar'];
	$fecha       = $_POST['fecha'];
	$datos_cobro = $instancia->buscarCuentaUsuarioCobroControl($_POST['buscar'], $_POST['fecha'], $id_log);
} else {
	$buscar      = '';
	$fecha       = '';
	$datos_cobro = $instancia->mostrarLimiteCuentasCobroControl($id_log);
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 42);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>codetec/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Cuentas de cobro - Codetec
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>codetec/cobro/agregarCobro" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Cuenta de cobro
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" value="<?=$fecha?>" class="form-control" data-tooltip="tooltip" title="Fecha de creacion" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" value="<?=$buscar?>" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Nombre / Raz&oacute;n Social</th>
									<th scope="col">Documento / NIT</th>
									<th scope="col">A&ntilde;o</th>
									<th scope="col">Mes</th>
									<th scope="col">Horas</th>
									<th scope="col">Fecha de creacion</th>
									<th scope="col">Coordinador</th>
									<th scope="col">Horas Aprobadas (Coordinador)</th>
									<th scope="col">Contabilidad</th>
									<th scope="col">Horas Aprobadas (Contabilidad)</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_cobro as $cobro) {
									$id_cobro              = $cobro['id'];
									$usuario               = ($cobro['razon'] == '') ? $cobro['usuario'] : $cobro['razon'];
									$documento             = $cobro['documento'];
									$lugar                 = $cobro['lugar'];
									$direccion             = $cobro['direccion'];
									$telefono              = $cobro['telefono'];
									$anio                  = (!empty($cobro['anio_cuenta_asistencia'])) ? $cobro['anio_cuenta_asistencia'] : 'N/A';
									$mes                   = $cobro['mes_cuenta_asistencia'];
									$valor                 = number_format($cobro['valor']);
									$cuenta_detalle        = $cobro['cuenta_detalle'];
									$cuenta_documentos     = $cobro['cuenta_documentos'];
									$estado                = $cobro['estado'];
									$fechareg              = $cobro['fechareg'];
									$tipo                  = $cobro['tipo'];
									$horas_asistencia      = $cobro['horas_aprobadas_asistencia'];
									$horas_aprobadas_coord = (empty($cobro['horas_coord'])) ? 'N/A' : $cobro['horas_coord'];
									$horas_aprobadas_cont  = (empty($cobro['horas_cont'])) ? 'N/A' : $cobro['horas_cont'];

									$mes_asistencia = (!empty($mes)) ? mesesEspanol($mes) : 'N/A';

									$ver_info = ($cuenta_detalle == 1 && $cuenta_documentos == 1 && $estado == 2) ? '' : 'd-none';

									$ver_archivo = ($cuenta_detalle == 1 && $cuenta_documentos == 1 && $estado == 2 && $cobro['estado_contabilidad'] == 1) ? '' : 'd-none';

									$ver_documentos = ($cuenta_detalle == 1 && $cuenta_documentos == 0) ? '' : 'd-none';

									$ver_detalle = ($cuenta_detalle == 0) ? '' : 'd-none';

									$span_estado = ($estado == 0) ? '<span class="badge badge-secondary">Aún faltan completar pasos</span>' : '';
									$span_estado = ($estado == 1) ? '<span class="badge badge-warning">Pendiente de aprobacion</span>' : $span_estado;
									$span_estado = ($estado == 2) ? '<span class="badge badge-success">Aprobado</span>' : $span_estado;
									$span_estado = ($estado == 3) ? '<span class="badge badge-danger">Denegado</span>' : $span_estado;

									$span_estado_cont = ($cobro['estado_contabilidad'] == 0) ? '<span class="badge badge-warning">Pendiente de aprobacion</span>' : '';
									$span_estado_cont = ($cobro['estado_contabilidad'] == 1) ? '<span class="badge badge-success">Aprobado</span>' : $span_estado_cont;
									$span_estado_cont = ($cobro['estado_contabilidad'] == 2) ? '<span class="badge badge-danger">Denegado</span>' : $span_estado_cont;

									if ($tipo == 2) {

										?>
										<tr class="text-center">
											<td><?=$usuario?></td>
											<td><?=$documento?></td>
											<td><?=$anio?></td>
											<td><?=$mes_asistencia?></td>
											<td><?=$horas_asistencia?></td>
											<td><?=date('Y-m-d', strtotime($fechareg))?></td>
											<td><?=$span_estado?></td>
											<td><?=$horas_aprobadas_coord?></td>
											<td><?=$span_estado_cont?></td>
											<td><?=$horas_aprobadas_cont?></td>
											<td>
												<div class="btn-group">
													<a href="<?=BASE_URL?>codetec/cobro/detalle?cobro=<?=base64_encode($id_cobro)?>" class="btn btn-secondary btn-sm <?=$ver_info?>" data-tooltip="tooltip" title="Ver detalle" data-placement="bottom">
														<i class="fa fa-info-circle"></i>
													</a>
													<a href="<?=BASE_URL?>codetec/cobro/detalleCobro?cobro=<?=base64_encode($id_cobro)?>" class="btn btn-info btn-sm <?=$ver_detalle?>" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
														<i class="fa fa-eye"></i>
													</a>
													<a href="<?=BASE_URL?>codetec/cobro/documentos?cobro=<?=base64_encode($id_cobro)?>" class="btn btn-primary btn-sm <?=$ver_documentos?>" data-tooltip="tooltip" title="Subir Documentos" data-placement="bottom">
														<i class="fas fa-file-upload"></i>
													</a>
													<a href="<?=BASE_URL?>imprimir/cobro/cuentaCobro?cobro=<?=base64_encode($id_cobro)?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_archivo?>" data-tooltip="tooltip" title="Descargar Archivo" data-placement="bottom">
														<i class="fa fa-file-pdf"></i>
													</a>
												</div>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>