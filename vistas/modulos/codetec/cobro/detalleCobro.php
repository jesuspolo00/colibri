<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlCobro::singleton_cobro();

$datos_jornada = $instancia->mostrarJornadasControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 42);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['cobro'])) {

	$id_cobro = base64_decode($_GET['cobro']);

	$datos_cobro    = $instancia->mostrarDatosCuentaCobroControl($id_cobro);
	$datos_detalle  = $instancia->mostrarDetallesCuentasCobroControl($id_cobro);
	$mes_asistencia = (!empty($datos_cobro['mes_cuenta_asistencia'])) ? mesesEspanol($datos_cobro['mes_cuenta_asistencia']) : 'N/A';
	$anio           = (!empty($datos_cobro['anio_cuenta_asistencia'])) ? $datos_cobro['anio_cuenta_asistencia'] : 'N/A';

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>codetec/cobro/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Detalles Cuenta de cobro No. <?=$id_cobro?> (<?=$mes_asistencia?> del <?=$anio?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_cuenta_cobro" value="<?=$id_cobro?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombre Completo</label>
									<input type="text" class="form-control" value="<?=$datos_cobro['usuario']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Raz&oacute;n Social </label>
									<input type="text" class="form-control" value="<?=$datos_cobro['razon']?>" name="razon">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Cedula o NIT <span class="text-danger">*</span></label>
									<input type="text" class="form-control" value="<?=$datos_cobro['documento']?>" name="documento" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Lugar <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="lugar" value="<?=$datos_cobro['lugar']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Direcci&oacute;n <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="direccion" value="<?=$datos_cobro['direccion']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
									<input type="text" class="form-control numeros" name="telefono" value="<?=$datos_cobro['telefono']?>" required>
								</div>
								<div class="col-lg-4 form-group d-none">
									<label class="font-weight-bold">Valor <span class="text-danger">*</span></label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">$</span>
										</div>
										<input type="text" class="form-control numeros precio" value="<?=number_format($datos_cobro['valor'])?>" name="valor">
									</div>
								</div>
								<div class="col-lg-12 form-group text-right">
									<a class="btn btn-primary btn-sm" href="<?=BASE_URL?>codetec/cobro/agregarAsistencia?cobro=<?=base64_encode($id_cobro)?>">
										<i class="fa fa-plus"></i>
										&nbsp;
										Agregar Asistencia
									</a>
								</div>
							</div>
							<div class="table-responsive mt-2">
								<table class="table table-hover border table-sm" width="100%" cellspacing="0">
									<thead>
										<tr class="text-center text-uppercase font-weight-bold">
											<th scope="col" colspan="12">Listado de asistencia</th>
										</tr>
										<tr class="text-center font-weight-bold">
											<th scope="col">Modulo</th>
											<th scope="col">Programa</th>
											<th scope="col">Sede</th>
											<th scope="col">Colegio</th>
											<th scope="col">Jornada</th>
											<th scope="col">Mes</th>
											<th scope="col">Dia de semana</th>
											<th scope="col">Fechas</th>
											<th scope="col">Ciclo</th>
											<th scope="col">Semestre</th>
											<th scope="col">Horas</th>
										</tr>
									</thead>
									<tbody class="buscar">
										<?php
										foreach ($datos_detalle as $detalle) {
											$id_detalle     = $detalle['id'];
											$modulo         = $detalle['modulo'];
											$programa       = $detalle['programa'];
											$sede           = (empty($detalle['sede'])) ? $detalle['sede_coord'] : $detalle['sede'];
											$jornada_id     = $detalle['jornada'];
											$mes_asistencia = mesesEspanol($detalle['mes_asistencia']);
											$colegio        = (empty($detalle['colegio'])) ? $detalle['colegio_coord'] : $detalle['colegio'];
											$ciclo          = (empty($detalle['ciclo'])) ? $detalle['ciclo_coord'] : $detalle['ciclo'];
											$semestre       = (empty($detalle['semestre'])) ? $detalle['semestre_coord'] : $detalle['semestre'];
											$horas          = $detalle['horas'];
											$id_asistencia  = $detalle['id_asistencia'];

											$datos_dias        = $instancia->mostrarDiasSemanaAsistenciaControl($id_asistencia);
											$datos_dias_numero = $instancia->mostrarDiasSemanaNumeroControl($id_asistencia);

											$dias_semana        = '';
											$dias_semana_numero = '';

											foreach ($datos_dias as $dias) {
												$dias_semana .= diasEspanol($dias['dia']) . ', ';
											}

											foreach ($datos_dias_numero as $dias_numero) {
												$dias_semana_numero .= $dias_numero['dia'] . ', ';
											}

											?>
											<input type="hidden" name="asistencia[]" value="<?=$id_asistencia?>">
											<tr class="text-center fila_<?=$id_asistencia?>">
												<td><?=$modulo?></td>
												<td><?=$programa?></td>
												<td>
													<input type="text" class="form-control text-center" name="sede[]" value="<?=$sede?>">
												</td>
												<td>
													<input type="text" class="form-control text-center" name="colegio[]" value="<?=$colegio?>">
												</td>
												<td>
													<input type="hidden" name="jornada" value="<?=$detalle['id_jornada']?>">
													<?=$detalle['nom_jornada']?>
												</td>
												<td><?=$mes_asistencia?></td>
												<td><?=$dias_semana?></td>
												<td><?=$dias_semana_numero?></td>
												<td>
													<input type="text" class="form-control text-center" name="ciclo[]" value="<?=$ciclo?>">
												</td>
												<td>
													<input type="text" class="form-control text-center" name="semestre[]" value="<?=$semestre?>">
												</td>
												<td><?=$horas?></td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-lg-12 form-group mt-2 text-right">
									<button class="btn btn-success btn-sm" type="submit">
										Siguiente Paso
										&nbsp;
										<i class="fa fa-arrow-right"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_cuenta_cobro'])) {
		$instancia->guardarDetallesCuentaCobroControl();
	}
}
?>