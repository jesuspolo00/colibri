<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';
require_once CONTROL_PATH . 'cobro' . DS . 'ControlCobro.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia            = ControlCobro::singleton_cobro();
$instancia_asistencia = ControlAsistencia::singleton_asistencia();
$instancia_perfil     = ControlPerfil::singleton_perfil();

$datos_programas = $instancia_asistencia->mostrarCargaAcademicaAsignadaControl($id_log, 2);
$datos_usuario   = $instancia_perfil->mostrarDatosPerfilControl($id_log);
$datos_cuenta    = $instancia->mostrarDatosCuentaCobroIdControl($id_log);

if (isset($_POST['programa'])) {
	$id_programa_select = $_POST['programa'];
} else {
	$id_programa_select = 0;
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 42);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>codetec/cobro/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Agregar Cuenta de cobro
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="tipo" value="2">
						<div class="row">
							<div class="col-lg-4 form-group d-none">
								<label class="font-weight-bold">Programa <span class="text-danger">*</span></label>
								<select name="programa" class="form-control">
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_programas as $programa) {
										$id_programa  = $programa['id_programa'];
										$nom_programa = $programa['nom_programa'];

										$select = ($id_programa == $id_programa_select) ? 'selected' : '';
										?>
										<option value="<?=$id_programa?>" <?=$select?>><?=$nom_programa?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">A&ntilde;o</label>
								<select name="anio" class="form-control">
									<?php
									for ($i = date('Y') - 1; $i <= 2050; $i++) {
										$selected = (date('Y-m-d') == $i) ? 'selected' : '';
										?>
										<option value="<?=$i?>" <?=$selected?>><?=$i?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Mes <span class="text-danger">*</span></label>
								<select name="mes" class="form-control" required>
									<option value="" selected>Seleccione una opcion...</option>
									<option value="1">Enero</option>
									<option value="2">Febrero</option>
									<option value="3">Marzo</option>
									<option value="4">Abril</option>
									<option value="5">Mayo</option>
									<option value="6">Junio</option>
									<option value="7">Julio</option>
									<option value="8">Agosto</option>
									<option value="9">Septiembre</option>
									<option value="10">Octubre</option>
									<option value="11">Noviembre</option>
									<option value="12">Diciembre</option>
								</select>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fas fa-filter"></i>
									&nbsp;
									Filtrar
								</button>
							</div>
						</div>
					</form>
					<?php
					if (isset($_POST['programa'])) {

						$datos = array('programa' => $_POST['programa'], 'anio' => $_POST['anio'], 'mes' => $_POST['mes'], 'id_log' => $_POST['id_log'], 'tipo' => 2);

						$datos_asistencia = $instancia_asistencia->mostrarAsistenciasFiltroControl($datos);
						?>
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="tipo" value="2">
							<div class="row p-2">
								<div class="col-lg-12 form-group">
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombres y Apellidos <span class="text-danger">*</span></label>
									<input type="text" class="form-control" value="<?=$datos_usuario['nombre'] . ' ' . $datos_usuario['apellido']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Raz&oacute;n Social</label>
									<input type="text" class="form-control" name="razon">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Cedula o NIT <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="documento" value="<?=$datos_cuenta['documento']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Lugar de expedici&oacute;n <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="lugar" value="<?=$datos_cuenta['lugar']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Direcci&oacute;n <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="direccion" value="<?=$datos_cuenta['direccion']?>" required>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
									<input type="text" class="form-control numeros" name="telefono" value="<?=$datos_cuenta['telefono']?>" required>
								</div>
								<div class="col-lg-4 form-group d-none">
									<label class="font-weight-bold">Valor por hora</label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">$</span>
										</div>
										<input type="text" class="form-control numeros precio" name="valor" value="0">
									</div>
								</div>
							</div>
							<div class="table-responsive mt-2">
								<table class="table table-hover border table-sm" width="100%" cellspacing="0">
									<thead>
										<tr class="text-center text-uppercase font-weight-bold">
											<th scope="col" colspan="7">Listado de asistencia</th>
										</tr>
										<tr class="text-center text-uppercase font-weight-bold">
											<th></th>
											<th scope="col">Programa</th>
											<th scope="col">Competencia</th>
											<th scope="col">Modulo de formaci&oacute;n</th>
											<th scope="col">Horas Aprobadas</th>
											<th scope="col">A&ntilde;o asistencia</th>
											<th scope="col">Mes Asistencia</th>
											<th scope="col">Coordinador</th>
										</tr>
									</thead>
									<tbody class="buscar">
										<?php
										$cont = 0;
										foreach ($datos_asistencia as $asistencia) {
											$id_asistencia     = $asistencia['id'];
											$nom_programa      = $asistencia['nom_programa'];
											$nom_competencia   = $asistencia['nom_competencia'];
											$nom_modulo        = $asistencia['nom_modulo'];
											$horas_programadas = $asistencia['horas_aprobadas_coord'];
											$mes_asistencia    = $asistencia['mes_asistencia'];
											$anio_asistencia   = $asistencia['anio_asistencia'];
											$visto_bueno       = $asistencia['visto_bueno'];
											$finalizado        = $asistencia['finalizado'];
											$tipo              = $asistencia['tipo'];

											$mes_asistencia = mesesEspanol($mes_asistencia);

											if ($finalizado == 0) {
												$ver_pdf    = 'd-none';
												$ver_editar = '';
												$span       = '<span class="badge badge-secondary">Pendiente de finalizacion</span>';
											}

											if ($finalizado == 1 && $visto_bueno == 0) {
												$ver_pdf    = 'd-none';
												$ver_editar = 'd-none';
												$span       = '<span class="badge badge-warning">Pendiente de visto bueno</span>';
											}

											if ($finalizado == 1 && $visto_bueno == 1) {
												$ver_pdf    = 'd-none';
												$ver_editar = 'd-none';
												$span       = '<span class="badge badge-danger">Denegada</span>';
											}

											if ($finalizado == 1 && $visto_bueno == 2) {
												$ver_pdf    = '';
												$ver_editar = 'd-none';
												$span       = '<span class="badge badge-success">Visto Bueno</span>';
											}

											$required = ($cont == 0) ? '' : '';

											if ($tipo == 2) {
												?>
												<tr class="text-center">
													<td>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input check" id="<?=$id_asistencia?>" name="asistencia[]" value="<?=$id_asistencia?>" <?=$required?>>
															<label class="custom-control-label" for="<?=$id_asistencia?>"></label>
														</div>
													</td>
													<td><?=$nom_programa?></td>
													<td><?=$nom_competencia?></td>
													<td><?=$nom_modulo?></td>
													<td><?=$horas_programadas?></td>
													<td><?=$anio_asistencia?></td>
													<td><?=$mes_asistencia?></td>
													<td><?=$span?></td>
												</tr>
												<?php
												$cont++;
											}
										}
										?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-lg-12 form-group mt-2">
									<button class="btn btn-success btn-sm float-right" type="submit">
										Siguiente Paso
										&nbsp;
										<i class="fa fa-arrow-right"></i>
									</button>
								</div>
							</div>
						</form>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['asistencia'])) {
	$instancia->generarCuentaCobroControl();
}
?>