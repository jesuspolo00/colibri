<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia          = ControlSolicitud::singleton_solicitud();
$instancia_areas    = ControlAreas::singleton_areas();
$instancia_usuarios = ControlUsuario::singleton_usuario();

$datos_usuarios = $instancia_usuarios->mostrarProveedoresControl();

if (isset($_POST['buscar'])) {
	$datos = array('proveedor' => $_POST['proveedor'], 'fecha' => $_POST['fecha'], 'buscar' => $_POST['buscar']);

	$datos_solicitud = $instancia->mostrarSolicitudBuscarControl($datos);
} else {
	$datos_solicitud = $instancia->mostratLimiteSolicitudesControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 11);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>solicitud/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de solicitudes
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/reporteSolicitud" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a>
						<a href="<?=BASE_URL?>solicitud/index" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Solicitud
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select class="form-control" name="proveedor" data-tooltip="tooltip" title="Proveedor">
									<option value="" selected>Seleccione un proveedor...</option>
									<?php
									foreach ($datos_usuarios as $usuario) {
										$id_user   = $usuario['id_user'];
										$nombre    = $usuario['nombre'] . ' ' . $usuario['apellido'];
										$documento = $usuario['documento'];
										?>
										<option value="<?=$id_user?>"><?=$nombre . '(' . $documento . ')'?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control" name="fecha" data-tooltip="tooltip" title="Fecha">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. solicitud</th>
									<th scope="col">Area</th>
									<th scope="col">Quien solicita</th>
									<th scope="col">Justificacion</th>
									<th scope="col">Proveedor</th>
									<th scope="col">Fecha solicitado / aplazado</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_solicitud as $solicitud) {
									$id_solicitud  = $solicitud['id'];
									$consecutivo   = $solicitud['consecutivo'];
									$id_area       = $solicitud['id_area'];
									$id_user       = $solicitud['id_user'];
									$nom_user      = $solicitud['nom_usuario'];
									$nom_proveedor = $solicitud['nom_proveedor'];
									$nom_area      = $solicitud['area_nom'];
									$estado        = $solicitud['estado'];
									$justificacion = $solicitud['justificacion'];
									$activo        = $solicitud['activo'];
									$motivo        = $solicitud['motivo'];

									$texto = ($activo == 0) ? $motivo : $justificacion;

									$fechareg = ($estado == 3 || $estado == 4) ? $solicitud['fecha_aplazado'] : $solicitud['fecha_solicitud'];

									$ver_confirmar    = 'd-none';
									$ver_imprimir     = 'd-none';
									$span             = '';
									$ver_verificacion = 'd-none';
									$ver_anular       = '';
									$editar           = '';

									if ($estado == 1) {
										$span             = '<span class="badge badge-success">Aprobada</span>';
										$ver_confirmar    = '';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
										$ver_anular       = 'd-none';
									}

									if ($estado == 1 && $solicitud['generado'] == 1) {
										$span             = '<span class="badge badge-primary">Orden de compra</span>';
										$ver_confirmar    = '';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
										$ver_anular       = 'd-none';
									}

									if ($estado == 2) {
										$span             = '<span class="badge badge-danger">Rechazada</span>';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
										$ver_anular       = 'd-none';
									}

									if ($estado == 3) {
										$span             = '<span class="badge badge-warning">Aplazada</span>';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
									}

									if ($estado == 4) {
										$span             = '<span class="badge badge-secondary">Aprobada - pendiente</span>';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = '';
									}

									if ($estado == 0 && $solicitud['generado'] == 0) {
										$span             = '<span class="badge badge-warning">Solicitada</span>';
										$editar           = 'd-none';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$ver_anular       = '';
									}

									$ver_confirmar = ($solicitud['id_proveedor'] != 0) ? '' : 'd-none';

									$datos_veriifcacion = $instancia->mostrarDatosVerificacionControl($id_solicitud);

									if ($estado == 1 && $solicitud['generado'] == 1 && !empty($datos_veriifcacion)) {
										$span             = '<span class="badge badge-info">Compra finalizada</span>';
										$editar           = 'd-none';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$ver_anular       = 'd-none';
									}

									if (empty($datos_veriifcacion) && $estado == 1 && $solicitud['generado'] == 1) {
										$ver_verificacion = '';
									} else {
										$ver_verificacion = 'd-none';
									}

									if ($estado == 0) {
										$ver_verificacion = 'd-none';
									}

									if ($fechareg == date('Y-m-d') && $estado == 3 && $solicitud['generado'] == 0) {
										$ver_confirmar = '';
									}

									if ($fechareg == date('Y-m-d') && $estado == 4 && $solicitud['generado'] == 0) {
										$ver_confirmar = '';
									}

									if ($solicitud['generado'] == 0 && $estado != 0) {
										$ver_confirmar    = '';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
										$ver_anular       = 'd-none';
									} else if ($solicitud['generado'] == 1) {
										$ver_confirmar = 'd-none';
										$ver_imprimir  = '';
										$editar        = '';
									}

									if ($activo == 0) {
										$span_proceso     = '<span class="badge badge-danger">Anulada</span>';
										$span             = '<span class="badge badge-danger">Anulada</span>';
										$ver_confirmar    = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$ver_anular       = 'd-none';
										$editar           = 'd-none';
									}

									?>
									<tr class="text-center">
										<td><?=$consecutivo?></td>
										<td><?=$nom_area?></td>
										<td><?=$nom_user?></td>
										<td><?=$texto?></td>
										<td><?=$nom_proveedor?></td>
										<td><?=date('Y-m-d', strtotime($fechareg))?></td>
										<td class="span_<?=$id_solicitud?>"><?=$span?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<a href="<?=BASE_URL?>solicitud/confirmar?solicitud=<?=base64_encode($id_solicitud)?>" class="btn btn-success btn-sm <?=$ver_confirmar?> confirmar_<?=$id_solicitud?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Generar orden de compra">
													<i class="fa fa-check-double"></i>
												</a>
												<a href="<?=BASE_URL?>solicitud/editar?solicitud=<?=base64_encode($id_solicitud)?>" class="btn btn-secondary btn-sm <?=$editar?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Editar solicitud">
													<i class="fa fa-edit"></i>
												</a>
												<a class="btn btn-info btn-sm <?=$ver_verificacion?>" href="<?=BASE_URL?>solicitud/verificar?solicitud=<?=base64_encode($id_solicitud)?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Verificar productos">
													<i class="fas fa-tasks"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/solicitud?solicitud=<?=base64_encode($id_solicitud)?>" class="btn btn-primary btn-sm <?=$ver_imprimir?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Orden de compra" target="_blank">
													<i class="fas fa-file-pdf"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/solicitudInicial?solicitud=<?=base64_encode($id_solicitud)?>" target="_blank" class="btn btn-warning btn-sm" data-tooltip="tooltip" data-placement="bottom"data-trigger="hover" title="Solicitud inicial">
													<i class="far fa-file-pdf"></i>
												</a>
											</div>
										</td>
										<?php
										$permiso        = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 19);
										$ver_aprobacion = ($permiso) ? '' : 'd-none';
										?>
										<td class="sol_<?=$id_solicitud?> <?=$ver_anular?> <?=$ver_aprobacion?>">
											<div class="btn-group">
												<button class="btn btn-success btn-sm aprobar_solicitud" type="button" data-tooltip="tooltip" title="Aprobar Solicitud" data-placement="bottom" data-log="<?=$id_log?>" id="<?=$id_solicitud?>">
													<i class="fa fa-check"></i>
												</button>
												<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#anular<?=$id_solicitud?>" data-tooltip="tooltip" data-placement="bottom" title="Anular solicitud">
													<i class="fa fa-times"></i>
												</button>
											</div>
										</td>
										<?php
										/*Permiso temporal para usuario Sindy*/
										/*if ($id_log == 4) {*/
											?>
											<!-- <td>
												<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#anular<?=$id_solicitud?>" data-tooltip="tooltip" data-placement="bottom" title="Anular solicitud">
													<i class="fa fa-times"></i>
												</button>
											</td> -->
										<?php /*}*/ ?>
									</tr>



									<!-- Modal -->
									<div class="modal fade" id="anular<?=$id_solicitud?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Anular solicitud (#<?=$id_solicitud?>)</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_solicitud" value="<?=$id_solicitud?>">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<div class="modal-body">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Motivo de anulacion</label>
																<textarea maxlength="1300" required rows="5" class="form-control" cols="4" name="motivo"></textarea>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-success btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Enviar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['motivo'])) {
	$instancia->anularSolicitudControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/solicitud/funcionesSolicitud.js"></script>