<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'carta' . DS . 'ControlCarta.php';

$instancia = ControlCarta::singleton_carta();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 35);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>wind/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Actas de entrega
					</h4>
					<div class="btn-group">
						<a class="btn btn-info btn-sm" href="<?=BASE_URL?>carta/listado">
							<i class="fa fa-list"></i>
							&nbsp;
							Listado de actas de entrega
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha Entrega <span class="text-danger">*</span></label>
								<input type="date" name="fecha" class="form-control">
							</div>
							<div class="col-lg-12 form-group text-right">
								<button class="btn btn-secondary btn-sm agregar_producto" type="button">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar Articulo
								</button>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col" colspan="2">Listado de Articulos</th>
									</tr>
									<tr class="text-center text-uppercase font-weight-bold">
										<th scope="col">Descripcion</th>
										<th scope="col">Cantidad</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<tr>
										<td>
											<input type="text" class="form-control" name="producto[]" required>
										</td>
										<td>
											<input type="text" class="form-control numeros text-center" name="cantidad[]" required>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Observacion</label>
							<textarea class="form-control" name="observacion" rows="5"></textarea>
						</div>
						<div class="row p-2">
							<div class="col-lg-12 form-group text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Generar Acta de entrega
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->generarCartaEntregaControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/carta/funcionesCarta.js"></script>