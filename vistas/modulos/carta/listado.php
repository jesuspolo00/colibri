<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'carta' . DS . 'ControlCarta.php';

$instancia = ControlCarta::singleton_carta();

if (isset($_POST['buscar'])) {
	$datos        = array('buscar' => $_POST['buscar'], 'fecha' => $_POST['fecha']);
$datos_cartas = $instancia->buscarCartasControl($datos);
} else {
	$datos_cartas = $instancia->mostrarLimitesCartasControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 35);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado actas de entrega
					</h4>
					<div class="btn-group">

					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4"></div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control" name="fecha" data-tooltip="tooltip" title="Fecha Entrega" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center text-uppercase font-weight-bold">
									<th scope="col">Generado Por</th>
									<th scope="col">Fecha Entrega</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_cartas as $carta) {
									$id_carta    = $carta['id'];
									$nom_user    = $carta['nom_user'];
									$fecha       = $carta['fecha_entrega'];
									$observacion = $carta['observacion'];
									$activo      = $carta['activo'];

									if ($activo != 0) {
										?>
										<tr class="text-center fila_<?=$id_carta?>">
											<td><?=$nom_user?></td>
											<td><?=$fecha?></td>
											<td><?=$observacion?></td>
											<td>
												<div class="btn-group <?=$ver_opciones?>">
													<a class="btn btn-success btn-sm" href="<?=BASE_URL?>carta/editar?carta=<?=base64_encode($id_carta)?>" data-tooltip="tooltip" title="Editar" data-placement="bottom">
														<i class="fa fa-edit"></i>
													</a>
													<a class="btn btn-primary btn-sm" href="<?=BASE_URL?>imprimir/carta/cartaEntrega?carta=<?=base64_encode($id_carta)?>" data-tooltip="tooltip" title="Carta de entrega" data-placement="bottom" target="_blank">
														<i class="fas fa-file-pdf"></i>
													</a>
													<button class="btn btn-danger btn-sm eliminar_carta" id="<?=$id_carta?>" data-tooltip="tooltip" data-placement="bottom" title="Eliminar">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/carta/funcionesCarta.js"></script>