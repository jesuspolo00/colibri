<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia        = ControlAprobacion::singleton_aprobacion();
$instancia_recibo = ControlRecibos::singleton_recibos();

$datos_tipo_pago = $instancia_recibo->mostrarTipoPagoAprobacionControl();

if (isset($_POST['buscar'])) {
	$datos                 = array('fecha' => $_POST['fecha'], 'proyecto' => 32, 'buscar' => $_POST['buscar']);
	$datos_solicitud       = $instancia->buscarSolicitudesAidaBarriosAprobacionControl($datos);
	$ver_descargar_filtros = '';
	$ver_descargar         = 'd-none';
} else {
	$datos_solicitud       = $instancia->mostrarSolicitudesAidaBarriosAprobacionControl();
	$ver_descargar_filtros = 'd-none';
	$ver_descargar         = '';
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 93);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>aida/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Aprobacion de pagos - Aida Barrios
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/aida/aprobacion/reporte" target="_blank" class="btn btn-success btn-sm <?=$ver_descargar?>">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a>
						<a class="btn btn-secondary btn-sm" href="<?=BASE_URL?>aida/aprobacion/solicitud">
							<i class="fa fa-plus"></i>
							&nbsp;
							Solicitar aprobacion de pago
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha Solicitado" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="col-lg-12 form-group mt-2 text-right <?=$ver_descargar_filtros?>">
						<a class="btn btn-success btn-sm" href="<?=BASE_URL?>imprimir/aida/aprobacion/reporteFiltro?fecha=<?=base64_encode($_POST['fecha'])?>&proyecto=<?=base64_encode(32)?>&buscar=<?=base64_encode($_POST['buscar'])?>" target="_bank">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Filtros
						</a>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center text-uppercase font-weight-bold">
									<th scope="col">No. Solicitud</th>
									<th scope="col">Usuario Solicita</th>
									<th scope="col">Correo</th>
									<th scope="col">Proyecto Asociado</th>
									<th scope="col">Archivo</th>
									<th scope="col">Concepto</th>
									<th scope="col">Valor</th>
									<th scope="col">Fecha Solicitado</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_solicitud as $solicitud) {
									$id_solicitud = $solicitud['id'];
									$documento    = $solicitud['documento'];
									$nom_completo = $solicitud['nom_user'];
									$correo       = $solicitud['correo'];
									$nom_estado   = $solicitud['estado_sol'];
									$nom_archivo  = $solicitud['nom_archivo'];
									$concepto     = $solicitud['concepto'];
									$nom_proyecto = $solicitud['nom_proyecto'];
									$fecha        = date('Y-m-d', strtotime($solicitud['fechareg']));
									$token        = $solicitud['token'];
									$valor        = ($solicitud['valor'] == '') ? 0 : number_format($solicitud['valor']);
									$comprobante  = $solicitud['comprobante'];

									if ($solicitud['estado'] == 1) {
										$ver_botones = '';
										$ver_pdf     = 'd-none';
										$subir_pdf   = 'd-none';
										$ver_remover = 'd-none';
										$span_estado = '<span class="badge badge-warning">' . $nom_estado . '</span>';
									}

									if ($solicitud['estado'] == 2) {
										$ver_pdf     = 'd-none';
										$subir_pdf   = '';
										$ver_botones = 'd-none';
										$ver_remover = '';
										$span_estado = '<span class="badge badge-success">' . $nom_estado . '</span>';
									}

									if ($solicitud['estado'] == 3) {
										$ver_botones = 'd-none';
										$ver_pdf     = 'd-none';
										$subir_pdf   = 'd-none';
										$ver_remover = '';
										$span_estado = '<span class="badge badge-danger">' . $nom_estado . '</span>';
									}

									if (!empty($comprobante) && $solicitud['estado'] == 2) {
										$ver_pdf   = '';
										$subir_pdf = 'd-none';
									}

									if (empty($comprobante) && $solicitud['estado'] == 2) {
										$ver_pdf   = 'd-none';
										$subir_pdf = '';
									}

									$url_archivo = PUBLIC_PATH . 'upload' . DS . $solicitud['archivo'];

									?>
									<tr class="text-center">
										<td><?=$id_solicitud?></td>
										<td><?=$nom_completo?></td>
										<td><?=$correo?></td>
										<td><?=$nom_proyecto?></td>
										<td><?=$nom_archivo?></td>
										<td><?=$concepto?></td>
										<td>$<?=$valor?></td>
										<td><?=$fecha?></td>
										<td  class="span_<?=$token?>"><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>aida/aprobacion/archivo?aprobacion=<?=base64_encode($id_solicitud)?>&enlace=1" data-tooltip="tooltip" title="Ver archivo" data-placement="bottom" class="btn btn-info btn-sm">
													<i class="fa fa-eye"></i>
												</a>
												<?php
												$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 52);
												if ($permiso) {
													?>
													<a href="<?=BASE_URL?>aida/aprobacion/editar?aprobacion=<?=base64_encode($id_solicitud)?>" class="btn btn-success btn-sm" data-tooltip="tooltip" title="Editar" data-placement="bottom">
														<i class="fa fa-edit"></i>
													</a>
													<?php
												}
												?>
												<button type="button" data-toggle="modal" data-target="#subir_pdf<?=$id_solicitud?>" class="btn btn-primary btn-sm <?=$subir_pdf?>" data-tooltip="tooltip" title="Subir comprobante de pago" data-placement="bottom">
													<i class="fa fa-upload"></i>
												</button>
												<a href="<?=PUBLIC_PATH?>upload/<?=$comprobante?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_pdf?>" data-tooltip="tooltip" title="Ver comprobante de pago" data-placement="bottom">
													<i class="fa fa-file-pdf"></i>
												</a>
											</div>
										</td>
										<td class="<?=$ver_remover?> btn_remover_<?=$token?>">
											<button class="btn btn-secondary btn-sm remover" id="<?=$token?>" data-tooltip="tooltip" title="Remover opcion" data-placement="bottom">
												<i class="fas fa-sync"></i>
											</button>
										</td>
										<?php
										$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 53);
										if ($permiso) {
											?>
											<td class="botones_<?=$token?> <?=$ver_botones?>">
												<div class="btn-group">
													<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#aprobar_<?=$id_solicitud?>" type="button" data-tooltip="tooltip" title="Aprobar Solicitud" data-placement="bottom">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-danger btn-sm denegar" id="<?=$token?>" type="button" data-tooltip="tooltip" title="Denegar Solicitud" data-placement="bottom">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</td>
										<?php }?>
									</tr>



									<div class="modal fade" id="subir_pdf<?=$id_solicitud?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Subir Comprobante de pago - Solicitud No. <?=$id_solicitud?></h5>
												</div>
												<div class="modal-body">
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id_solicitud" value="<?=$id_solicitud?>">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<div class="row p-2">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Comprobante de pago <span class="text-danger">*</span></label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" name="archivo" id="<?=$id_solicitud?>" required accept=".png, .jpg, .jpeg, .pdf">
																	<label class="custom-file-label file_label_<?=$id_solicitud?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fa fa-upload"></i>
																	&nbsp;
																	Subir
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

									<div class="modal fade modal_tipo_pago" id="aprobar_<?=$id_solicitud?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Aprobar solicitud No. <?=$id_solicitud?></h5>
												</div>
												<div class="modal-body">
													<div class="row p-2">
														<div class="col-lg-12 form-group">
															<label class="font-weight-bold">Tipo de pago <span class="text-danger">*</span></label>
															<select name="tipo_pago" id="tipo_pago_<?=$token?>" class="form-control" required>
																<option value="" selected>Seleccione una opcion...</option>
																<?php
																foreach ($datos_tipo_pago as $tipo_pago) {
																	$id_tipo_pago = $tipo_pago['id'];
																	$nom_tipo     = $tipo_pago['nombre'];

																	if($tipo_pago['estado'] == 1){
																	?>
																	<option value="<?=$id_tipo_pago?>"><?=$nom_tipo?></option>
																<?php }}?>
															</select>
														</div>
														<div class="col-lg-12 form-group mt-2 text-right">
															<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<input type="hidden" id="id_log" value="<?=$id_log?>">
															<button class="btn btn-success btn-sm aprobar" id="<?=$token?>" type="button">
																<i class="fa fa-check"></i>
																&nbsp;
																Aprobar
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_solicitud'])) {
	$instancia->subirAidaBarriosComprobanteControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/aida/funcionesAprobacion.js"></script>