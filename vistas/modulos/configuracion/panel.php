<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Panel de control - Modulos
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>areas/index">
								<div class="card border-left-orange shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Area / Oficina</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-map-marker-alt fa-2x text-orange"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 9);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>categorias/index">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Categorias</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-cubes fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 12);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>proyectos/index">
								<div class="card border-left-purple shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Proyectos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-pencil-ruler fa-2x text-purple"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 49);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>programa/index">
								<div class="card border-left-green-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Carga Academica</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-cubes fa-2x text-green-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 94);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>metodo_pago/index">
								<div class="card border-left-blue-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Metodos Pago</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-credit-card fa-2x text-blue-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>