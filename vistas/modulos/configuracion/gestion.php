<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Gestion de recursos - Modulos
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>proveedor/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Proveedores</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-truck-loading fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 5);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>inventario/index">
								<div class="card border-left-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Inventario</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-barcode fa-2x text-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/index">
								<div class="card border-left-danger shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Inspeccion y mantenimientos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-tools fa-2x text-danger"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<?php
						}
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 38);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reservas/index">
								<div class="card border-left-blue shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Reservar Salon</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar-alt fa-2x text-blue"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 39);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reservas/programacion">
								<div class="card border-left-blue-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado de reservas</div>
											</div>
											<div class="col-auto">
												<i class="far fa-calendar-alt fa-2x text-blue-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 77);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>calendario_institucional/index">
								<div class="card border-left-green-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Calendario institucional</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar fa-2x text-green-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>