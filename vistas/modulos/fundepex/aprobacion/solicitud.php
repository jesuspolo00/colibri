<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia         = ControlAprobacion::singleton_aprobacion();
$isntancia_recibos = ControlRecibos::singleton_recibos();

$datos_proyecto = $isntancia_recibos->mostrarTodosProyectosFundepexControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 55);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>fundepex/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Solicitud para aprobacion de pagos
					</h4>
					<div class="btn-group">
						<?php
						$ver_listado = ($_SESSION['rol'] == 10 || $_SESSION['rol'] == 1 && $permiso) ? '' : 'd-none';
						?>
						<a class="btn btn-secondary btn-sm" href="<?=BASE_URL?>fundepex/aprobacion/solicitudUsuario">
							<i class="fa fa-eye"></i>
							&nbsp;
							Mis solicitudes
						</a>
						<a class="btn btn-info btn-sm <?=$ver_listado?>" href="<?=BASE_URL?>fundepex/aprobacion/index">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado de solicitudes
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data" class="formValidation">
						<div class="row p-2">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Proyecto <span class="text-danger">*</span></label>
								<select name="proyecto" id="" required class="form-control">
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_proyecto as $proyecto) {
										$id_proyecto  = $proyecto['id'];
										$nom_proyecto = $proyecto['nombre'];
										?>
										<option value="<?=$id_proyecto?>"><?=$nom_proyecto?> - (<?=$proyecto['proveedor']?>)</option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre del archivo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nom_archivo" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Valor <span class="text-danger">*</span></label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
									</div>
									<input type="text" class="form-control precio" name="valor" required>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Archivos a enviar <span class="text-danger">*</span></label>
								<input type="file" class="file" name="archivo[]" multiple required accept=".jpg, .png, .jpeg, .xlsx, .pdf, .docx, .xls, .doc">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Concepto de solicitud (opcional)</label>
								<textarea class="form-control" name="concepto" rows="5"></textarea>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm buttonFormValidation" type="submit">
									<i class="fas fa-paper-plane"></i>
									&nbsp;
									Enviar solicitud
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nom_archivo'])) {
	$instancia->registrarSolicitudFundepexAprobacionControl();
}
?>