<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia = ControlProveedor::singleton_proveedor();

$datos_prov = $instancia->mostrarProveedoresControl();

if (isset($_POST['buscar'])) {
	$datos_proveedor = $instancia->mostrarPorveedorBuscarControl($_POST['buscar']);
} else {
	$datos_proveedor = $instancia->mostrarLimiteProveedoresControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 10);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Proveedores
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/proveedor/reporteProveedorPdf" target="_blank" class="btn btn-dark btn-sm">
							<i class="fa fa-file-pdf"></i>
							&nbsp;
							Descargar Reporte (PDF)
						</a>
						<a href="<?=BASE_URL?>imprimir/proveedor/reporteProveedor" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte (Excel)
						</a>
						<a href="<?=BASE_URL?>proveedor/registro" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Registrar proveedor
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus" autocomplete="off">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Fecha ingreso</th>
									<th scope="col">Nombre o Razon social</th>
									<th scope="col">Identificacion</th>
									<th scope="col">Telefono</th>
									<th scope="col">Contacto</th>
									<th scope="col">Telefono contacto</th>
									<th scope="col">Detalle producto</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_proveedor as $proveedor) {
									$id_proveedor   = $proveedor['id_proveedor'];
									$nombre         = $proveedor['razon_social'];
									$identificacion = $proveedor['num_identificacion'];
									$telefono       = $proveedor['telefono'];
									$nom_contacto   = $proveedor['contacto'];
									$tel_contacto   = $proveedor['telefono_contacto'];
									$detalle        = $proveedor['detalle_producto'];
									$fecha_ingreso  = $proveedor['fecha_ingreso'];

									$documentos_proveedor = $instancia->validarDocumentosProveedorControl($id_proveedor);
									$evaluacion_proveedor = $instancia->validarEvaluacionProveedorControl($id_proveedor);

									$ver_documentos = ($documentos_proveedor['contar'] >= 1) ? 'd-none' : '';

									$ver_evaluacion = (empty($evaluacion_proveedor) && $documentos_proveedor['contar'] >= 1) ? '' : 'd-none';

									$span_evaluacion = (empty($evaluacion_proveedor['id'])) ? '<span class="badge badge-danger">Falta evaluacion</span>' : '<span class="badge badge-success">Evaluacion realizada</span>';
									$span_documentos = ($documentos_proveedor['contar'] > 1) ? '<span class="badge badge-success">Documentos Subidos</span>' : '<span class="badge badge-danger">Faltan Documentos</span>';

									if (empty($evaluacion_proveedor)) {
										$puntos_eval = '';
									} else {
										$puntos_eval = ' (Puntuacion: ' . ($evaluacion_proveedor['pregunta_1'] + $evaluacion_proveedor['pregunta_2'] + $evaluacion_proveedor['pregunta_3'] + $evaluacion_proveedor['pregunta_4'] + $evaluacion_proveedor['pregunta_5']) / 5 . ')';
									}

									?>
									<tr class="text-center text-dark">
										<td><?=$fecha_ingreso?></td>
										<td>
											<a href="<?=BASE_URL?>proveedor/hojaRegistro?proveedor=<?=base64_encode($id_proveedor)?>"><?=$nombre?></a>
										</td>
										<td><?=$identificacion?></td>
										<td><?=$telefono?></td>
										<td><?=$nom_contacto?></td>
										<td><?=$tel_contacto?></td>
										<td><?=$detalle?></td>
										<td>
											<?=$span_evaluacion?>
											<?=$span_documentos?>
										</td>
										<td class="">
											<div class="btn-group">
												<a href="<?=BASE_URL?>proveedor/hojaRegistro?proveedor=<?=base64_encode($id_proveedor)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Hoja de registro" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
												<a href="<?=BASE_URL?>proveedor/evaluacion?proveedor=<?=base64_encode($id_proveedor)?>" class="btn btn-success btn-sm <?=$ver_evaluacion?>" data-tooltip="tooltip" data-placement="bottom" title="Evaluacion (<?=date('Y')?>)">
													<i class="fas fa-clipboard-list"></i>
												</a>
												<a href="<?=BASE_URL?>proveedor/documentos?proveedor=<?=base64_encode($id_proveedor)?>" class="btn btn-primary btn-sm <?=$ver_documentos?>" data-tooltip="tooltip" data-placement="bottom" title="Subir Documentos">
													<i class="fas fa-upload"></i>
												</a>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>