<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'reservas' . DS . 'ControlReserva.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia       = ControlReserva::singleton_reserva();
$instancia_areas = ControlAreas::singleton_areas();

$datos_areas = $instancia_areas->mostrarAreasReservablesControl();

if (isset($_POST['buscar'])) {
	$datos         = array('buscar' => $_POST['buscar'], 'area' => $_POST['salon'], 'fecha' => $_POST['fecha']);
	$fecha_hoy     = $_POST['fecha'];
	$datos_reserva = $instancia->buscarAreasReservaControl($datos);
} else {
	$fecha_hoy     = date('Y-m-d');
	$datos_reserva = $instancia->mostrarAreasReservaHoyControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 39);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Programacion Diaria
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/reservas/listadoReservas" class="btn btn-primary btn-sm" target="_blank">
							<i class="fas fa-file-pdf"></i>
							&nbsp;
							Descargar listado (PDF)
						</a>
						<a href="<?=BASE_URL?>reservas/index" class="btn btn-secondary btn-sm">
							<i class="fas fa-calendar-alt"></i>
							&nbsp;
							Reservar salon
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="salon" class="form-control" data-tooltip="tooltip" title="Area" data-placement="top">
									<option value="" selected>Selecciona un area...</option>
									<?php
									foreach ($datos_areas as $areas) {
										$id_area_sel = $areas['id'];
										$nom_area    = $areas['nombre'];
										?>
										<option value="<?=$id_area_sel?>"><?=$nom_area?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control" name="fecha" required data-tooltip="tooltip" title="Fecha Reserva" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<?php
					if (isset($_POST['buscar'])) {
						?>
						<div class="col-lg-12 form-group text-right">
							<a href="<?=BASE_URL?>imprimir/reservas/listadoReservaFiltro?buscar=<?=$_POST['buscar']?>&area=<?=$_POST['area']?>&fecha=<?=$_POST['fecha']?>" target="_blank" class="btn btn-primary btn-sm">
								<i class="fas fa-file-pdf"></i>
								&nbsp;
								Descargar listado filtro (PDF)
							</a>
						</div>
					<?php }?>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<?php
							foreach ($datos_reserva as $areas) {
								$id_area             = $areas['id'];
								$nom_area            = $areas['nombre'];
								$fecha_reserva_salon = $areas['fecha_reserva'];
								?>
								<thead>
									<tr class="text-center text-uppercase font-weight-bold">
										<th scope="col" colspan="5"><?=$nom_area?></th>
									</tr>
									<tr class="text-center">
										<th scope="col">Usuario Reserva</th>
										<th scope="col">Fecha Reserva</th>
										<th scope="col">Hora Reserva</th>
										<th scope="col">Detalle Reserva</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									$datos_detalle = $instancia->detalleReservaSalonControl($id_area, $_POST['fecha']);
									foreach ($datos_detalle as $detalle) {
										$id_reserva      = $detalle['id'];
										$fecha_reserva   = $detalle['fecha_reserva'];
										$horas           = $detalle['horas'];
										$detalle_reserva = $detalle['detalle'];
										$nom_user        = $detalle['nom_user'];
										?>
										<tr class="text-center">
											<td><?=$nom_user?></td>
											<td><?=$fecha_reserva?></td>
											<td><?=$horas?></td>
											<td><?=$detalle_reserva?></td>
										</tr>
										<?php
									}
									?>
								</tbody>
							<?php }?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>