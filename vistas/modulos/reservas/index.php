<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'reservas' . DS . 'ControlReserva.php';

$instancia       = ControlReserva::singleton_reserva();
$instancia_areas = ControlAreas::singleton_areas();

$datos_horas = $instancia->mostrarTodasHorasControl();
$datos_areas = $instancia_areas->mostrarAreasReservablesControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 38);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Reservar Salon
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reservas/programacion" class="btn btn-info btn-sm">
							<i class="far fa-calendar-alt"></i>
							&nbsp;
							Listado de reservas
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row p-2">
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Salon <span class="text-danger">*</span></label>
								<select name="salon" class="form-control" required id="salon">
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_areas as $areas) {
										$id_area  = $areas['id'];
										$nom_area = $areas['nombre'];
										?>
										<option value="<?=$id_area?>"><?=$nom_area?></option>
									<?php }?>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Fecha a reservar <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_reserva" required id="fecha">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Horas <span class="text-danger">*</span></label>
								<div class="col-lg-12 form-group">
									<div class="form-inline" id="horas">
										<?php
										foreach ($datos_horas as $horas) {
											$id_hora = $horas['id'];
											$hora    = $horas['horas'];
											?>
											<div class="custom-control custom-switch ml-2">
												<input type="checkbox" class="custom-control-input" name="hora[]" value="<?=$id_hora?>" id="<?=$id_hora?>">
												<label class="custom-control-label" for="<?=$id_hora?>"><?=$hora?></label>
											</div>
											<?php
										}
										?>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Detalle de la reserva</label>
								<textarea name="detalle"  rows="5" class="form-control"></textarea>
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['salon'])) {
	$instancia->reservarAreaControl();
}

?>
<script src="<?=PUBLIC_PATH?>js/reservas/funcionesReservas.js"></script>