<div class="container-fluid">
	<div class="row">
		<a class="col-md-2 mb-4 text-decoration-none" href="<?=BASE_URL?>lector/index">
			<div class="card border-left-dark shadow-sm h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gary-700">Lector de barra</div>
						</div>
						<div class="col-auto">
							<img src="<?=PUBLIC_PATH?>img/barcode.png" class="img-fluid" width="40">
						</div>
					</div>
				</div>
			</div>
		</a>
		<a class="col-md-2 mb-4 text-decoration-none" href="<?=BASE_URL?>lector/qr">
			<div class="card border-left-dark shadow-sm h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gary-700">Lector de QR</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-qrcode fa-2x fa-rotate-90 text-dark"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>