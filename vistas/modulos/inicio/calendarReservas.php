<div class="col-lg-12">
	<div class="card shadow-sm mb-4">
		<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			<h4 class="m-0 font-weight-bold text-success">
				Calendario de reservas
			</h4>
			<div class="btn-group">
				<a href="<?=BASE_URL?>reservas/programacion" class="btn btn-info btn-sm">
					<i class="fas fa-list"></i>
					&nbsp;
					Listado de reservas
				</a>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12 form-group">
					<div id="calendar">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/reservas/calendario.js"></script>