<div class="col-lg-12">
	<div class="card shadow-sm mb-4">
		<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			<h4 class="m-0 font-weight-bold text-success">
				Calendario Institucional
			</h4>
			<div class="btn-group">
				<a href="<?=BASE_URL?>calendario_institucional/listado" class="btn btn-info btn-sm">
					<i class="fas fa-list"></i>
					&nbsp;
					Listado de calendario institucional
				</a>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12 form-group">
					<div id="calendarPlan">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/plan_formacion/calendario.js"></script>