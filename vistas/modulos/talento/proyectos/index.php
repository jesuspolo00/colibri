<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';

$instancia           = ControlRecibos::singleton_recibos();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 59);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_POST['buscar'])) {
	$buscar_ambos = $_POST['buscar'];

	$datos_proyectos = $instancia->buscarProyectosTalentoControl($buscar_ambos);
} else {
	$datos_proyectos = $instancia->mostrarProyectosTalentoControl();

}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>fundepex/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Proyectos
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>proyectos/index" class="btn btn-info btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado de proyectos
						</a>
						<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#agregar_proyecto">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Proyecto
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="Buscar..." aria-describedby="basic-addon2" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Proyecto</th>
									<th scope="col">Ciudad</th>
									<th scope="col">Cliente</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_proyectos as $proyecto) {
									$id_proyecto      = $proyecto['id_proyecto'];
									$nombre           = $proyecto['nom_proyecto'];
									$ciudad           = $proyecto['ciudad_proyecto'];
									$nombre_proveedor = $proyecto['proveedor'];
									$activo           = $proyecto['activo'];
									$id_proveedor_pro = $proyecto['id_proveedor'];
									$nit_proveedor    = $proyecto['nit_proveedor'];
									$recibo           = $proyecto['recibo'];

									$ver_descargar = ($recibo != '') ? '' : 'd-none';
									$ver           = ($activo == 0) ? 'd-none' : '';
									?>
									<tr class="text-center <?=$ver?>">
										<td><?=$nombre?></td>
										<td><?=$ciudad?></td>
										<td><?=$nombre_proveedor?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<a href="<?=BASE_URL?>recibos/listado?proyecto=<?=base64_encode($id_proyecto)?>&enlace=<?=base64_encode(3)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Historial de recibos">
													<i class="fa fa-eye"></i>
												</a>
												<a href="<?=BASE_URL?>recibos/generar?proyecto=<?=base64_encode($id_proyecto)?>&enlace=<?=base64_encode(3)?>" class="btn btn-success btn-sm" data-tooltip="tooltip" title="Generar recibo de caja" data-placement="bottom" data-trigger="hover">
													<i class="fas fa-file-invoice-dollar"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/reciboCaja?recibo=<?=base64_encode($recibo)?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_descargar?>" data-tooltip="tooltip" title="Descargar ultimo recibo generado" data-placement="left" data-trigger="hover">
													<i class="fa fa-download"></i>
												</a>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'proyectos' . DS . 'agregarProyecto.php';

if (isset($_POST['nombre'])) {
	$instancia->registrarProyectosControl();
}
