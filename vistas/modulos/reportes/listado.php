<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

$datos_reporte = $instancia->mostrarTodosReportesControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 29);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de Reportes
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Reporte</th>
									<th scope="col">Bloque</th>
									<th scope="col">Area</th>
									<th scope="col">Descripcion</th>
									<th scope="col">Cantidad</th>
									<th scope="col">Codigo</th>
									<th scope="col">Fecha reportado</th>
									<th scope="col">Observacion</th>
									<th scope="col">Tipo de reporte</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_reporte as $dano) {
									$id_dano     = $dano['id'];
									$area        = $dano['nom_area'];
									$descripcion = $dano['nom_inventario'];
									$marca       = $dano['marca'];
									$modelo      = $dano['modelo'];
									$codigo      = $dano['codigo'];
									$estado      = $dano['estado'];
									$observacion = $dano['observacion'];
									$id_usuario  = $dano['id_user'];
									$id_articulo = $dano['id_inventario'];
									$nom_zona    = $dano['nom_zona'];
									$cantidad    = $dano['cantidad'];

									$span = ($dano['tipo_reporte'] == 1) ? '<span class="badge badge-danger">Correctivo</span>' : '<span class="badge badge-warning">Mantenimiento</span>';
									?>
									<tr class="text-uppercase text-center text-dark">
										<td><?=$id_dano?></td>
										<td><?=$nom_zona?></td>
										<td><?=$area?></td>
										<td><?=$descripcion?></td>
										<td><?=$cantidad?></td>
										<td><?=$codigo?></td>
										<td><?=$dano['fecha_reporte']?></td>
										<td class="text-lowercase"><?=$observacion?></td>
										<td><?=$span?></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Solucionar reporte" data-toggle="modal" data-target="#solucion<?=$id_dano?>">
													<i class="fas fa-check"></i>
												</button>
												<a href="<?=BASE_URL?>imprimir/inventario/reporte?id=<?=base64_encode($id_dano)?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar Reporte" data-placement="bottom">
													<i class="fa fa-file-pdf"></i>
												</a>
											</div>
										</td>
									</tr>

									<!-- Modal -->
									<div class="modal fade" id="solucion<?=$id_dano?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Solucionar Reporte</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_usuario" value="<?=$id_usuario?>">
													<input type="hidden" name="id_reporte" value="<?=$id_dano?>">
													<input type="hidden" name="id_articulo" value="<?=$id_articulo?>
													">
													<input type="hidden" name="id_area" value="<?=$dano['id_area']?>
													">
													<div class="modal-body border-0 p-4">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$descripcion?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$area?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Reportado <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$dano['fecha_reporte']?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cantidad <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$cantidad?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Codigo <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$codigo?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha solucion <span class="text-danger">*</span></label>
																<input type="date" class="form-control" name="fecha_reporte" required>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion del reporte</label>
																<textarea class="form-control" disabled rows="5" cols="5"><?=$dano['observacion']?></textarea>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion de solcuion</label>
																<textarea class="form-control" name="observacion" rows="5" cols="5"></textarea>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-success btn-sm">
															<i class="fa fa-check"></i>
															&nbsp;
															Solucionar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_reporte'])) {
	$instancia->solucionarReporteControl();
}
?>