<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 32);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['zona'])) {

	$id_zona = base64_decode($_GET['zona']);

	$datos_zona      = $instancia->mostrarDatosZonaControl($id_zona);
	$datos_historial = $instancia->mostrarHistorialInspeccionesControl($id_zona);

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>reportes/historial" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Historial de zona - <?=$datos_zona['nombre']?>
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Zona <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$datos_zona['nombre']?>" disabled>
							</div>
							<div class="col-lg-12 form-group">
								<a href="<?=BASE_URL?>imprimir/zonas/codigoQR?id=<?=base64_encode($id_zona)?>" class="btn btn-primary btn-sm" target="_blank">
									<i class="fas fa-qrcode"></i>
									&nbsp;
									Descargar codigo QR
								</a>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center text-uppercase font-weight-bold">
										<th colspan="10" class="text-success">Historial de Inspecciones</th>
									</tr>
									<tr class="text-center font-weight-bold">
										<td></td>
										<th scope="col">No. Inspeccion</th>
										<th scope="col">Usuario Reporta</th>
										<th scope="col">Area</th>
										<th scope="col">Componente</th>
										<th scope="col">Fecha Inspeccion</th>
										<th scope="col">Observacion</th>
										<th scope="col">Fecha Respuesta</th>
										<th scope="col">Observacion Respuesta</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_historial as $inspeccion) {
										$id_inspeccion   = $inspeccion['id'];
										$nom_area        = $inspeccion['nom_area'];
										$nom_categoria   = $inspeccion['nom_categoria'];
										$fecha           = $inspeccion['fecha'];
										$observacion     = $inspeccion['observacion'];
										$nom_zona        = $inspeccion['nom_zona'];
										$nom_user        = $inspeccion['nom_user'];
										$fecha_respuesta = $inspeccion['fecha_respuesta'];
										$obs_respuesta   = $inspeccion['observacion_respuesta'];

										$span_reporte         = ($inspeccion['estado'] == 2) ? '<span class="badge badge-warning">Mantenimiento</span>' : '<span class="badge badge-danger">Correctivo</span>';
										$span_solucionado     = (!empty($inspeccion['id_respuesta'])) ? '<span class="badge badge-success">Solucionado</span>' : '';
										$ver_reporte_solucion = (!empty($inspeccion['id_respuesta'])) ? '' : 'd-none';
										$ver_reporte          = (!empty($inspeccion['id_respuesta'])) ? 'd-none' : '';
										?>
										<tr class="text-center">
											<td><?=$span_reporte?> | <?=$span_solucionado?></td>
											<td><?=$id_inspeccion?></td>
											<td><?=$nom_user?></td>
											<td><?=$nom_area?></td>
											<td><?=$nom_categoria?></td>
											<td><?=$fecha?></td>
											<td><?=$observacion?></td>
											<td><?=$fecha_respuesta?></td>
											<td><?=$obs_respuesta?></td>
											<td>
												<div class="btn-group">
													<a href="<?=BASE_URL?>imprimir/reportes/solucion?id=<?=base64_encode($id_inspeccion)?>" class="btn btn-success btn-sm <?=$ver_reporte_solucion?>" data-tooltip="tooltip" title="Descargar Solucion" data-placement="bottom" target="_blank">
														<i class="fa fa-file-pdf"></i>
													</a>
													<a href="<?=BASE_URL?>imprimir/reportes/reporteAreas?id=<?=base64_encode($id_inspeccion)?>" class="btn btn-primary btn-sm <?=$ver_reporte?>" data-tooltip="tooltip" title="Descargar Reporte" data-placement="bottom" target="_blank">
														<i class="fa fa-file-pdf"></i>
													</a>
												</div>
											</td>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		include_once VISTA_PATH . 'script_and_final.php';
	}
	?>
