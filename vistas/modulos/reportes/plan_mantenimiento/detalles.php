<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 70);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_plan = base64_decode($_GET['id']);

	$datos_plan    = $instancia->mostrarInformacionPlanControl($id_plan);
	$datos_detalle = $instancia->mostrarDetallesPlanControl($id_plan);

	$disabled    = ($datos_plan['estado'] == 1) ? 'disabled' : '';
	$ver_guardar = ($datos_plan['estado'] == 1) ? 'd-none' : '';

	$nom_estado = ($datos_plan['estado'] == 1) ? 'Realizado' : 'Pendiente de revision';
	$nom_estado = ($datos_plan['fecha_fin'] < date('Y-m-d') && $datos_plan['estado'] != 1) ? 'No realizado' : $nom_estado;

	$bg_estado = ($datos_plan['estado'] == 1) ? 'bg-success' : 'bg-warning';
	$bg_estado = ($datos_plan['fecha_fin'] < date('Y-m-d') && $datos_plan['estado'] != 1) ? 'bg-danger' : $bg_estado;
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/plan_mantenimiento/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Detalles plan de mantenimiento No. (<?=$id_plan?>)
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_plan" value="<?=$id_plan?>">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Usuario</label>
								<input type="text" class="form-control" disabled value="<?=$datos_plan['nom_user']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha inicio</label>
								<input type="date" class="form-control" disabled value="<?=$datos_plan['fecha_inicio']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha fin</label>
								<input type="text" class="form-control" disabled value="<?=$datos_plan['fecha_fin']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado</label>
								<input type="text" class="form-control text-white <?=$bg_estado?>" disabled value="<?=$nom_estado?>">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observacion</label>
								<textarea class="form-control" rows="5" disabled><?=$datos_plan['observacion_general']?></textarea>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center text-uppercase font-weight-bold text-success">
										<td colspan="8" class="text-success">Detalles del plan de mantenimiento</td>
									</tr>
									<tr class="text-center font-weight-bold">
										<th scope="col">Descripci&oacute;n</th>
										<th scope="col">Fecha Mantenimiento</th>
										<th scope="col">Ubicaci&oacute;n</th>
										<th scope="col">Observaci&oacute;n</th>
										<th scope="col">Mantenimiento PREVENTIVO realizado <span class="text-danger">*</span></th>
										<th scope="col">Observaci&oacute;n</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_detalle as $detalle) {
										$id_detalle    = $detalle['id'];
										$nom_categoria = $detalle['nom_categoria'];
										$fecha_mant    = $detalle['fecha_mant'];
										$nom_zona      = $detalle['nom_zona'];
										$observacion   = $detalle['observacion'];

										$select_si = ($detalle['estado'] == 1) ? 'selected' : '';
										$select_no = ($detalle['estado'] == 2) ? 'selected' : '';
										?>
										<input type="hidden" name="id_detalle[]" value="<?=$id_detalle?>">
										<tr class="text-center">
											<td><?=$nom_categoria?></td>
											<td><?=$fecha_mant?></td>
											<td><?=$nom_zona?></td>
											<td><?=$observacion?></td>
											<td>
												<select name="estado[]" <?=$disabled?> class="form-control" required>
													<option value="" selected>Seleccione una opcio&oacute;n...</option>
													<option value="1" <?=$select_si?>>Si</option>
													<option value="2" <?=$select_no?>>No</option>
												</select>
											</td>
											<td>
												<textarea name="observacion_respuesta[]" class="form-control" <?=$disabled?> rows="2"><?=$detalle['observacion_respuesta']?></textarea>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Observacion general (Respuesta)</label>
							<textarea name="observacion_respuesta_general" class="form-control" <?=$disabled?> rows="5"><?=$datos_plan['observacion_respuesta']?></textarea>
						</div>
						<div class="col-lg-12 form-group text-right mt-2">
							<button class="btn btn-success btn-sm <?=$ver_guardar?>" type="submit">
								<i class="fa fa-check"></i>
								&nbsp;
								Finalizar plan
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->finalizarPlanMantenimientoControl();
}