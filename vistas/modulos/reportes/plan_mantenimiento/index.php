<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia            = ControlAreas::singleton_areas();
$instancia_categorias = ControlCategorias::singleton_categoria();

$datos_areas      = $instancia->mostrarAreasControl();
$datos_zonas      = $instancia->mostrarZonasControl();
$datos_categorias = $instancia->mostrarComponentesControl();
$datos_categoria  = $instancia_categorias->mostrarCategoriasControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 69);
if (!$permiso) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Plan de mantenimientos
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reportes/plan_mantenimiento/listado" class="btn btn-info btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado de mantenimientos
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre quien realiza <span class="text-danger">*</span></label>
								<input type="text" class="form-control" value="<?=$nom_log_session?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha inicio plan <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_inicio" required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha fin plan <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_fin" required>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observaci&oacute;n general</label>	
								<textarea class="form-control" rows="5" name="observacion_general"></textarea>
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_mantenimiento">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar mantenimiento
								</button>
							</div>
							<div class="col-lg-12 form-group mt-2">
								<div class="table-responsive">
									<table class="table table-hover border table-sm" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th scope="col">Descripci&oacute;n</th>
												<th scope="col">Fecha Mantenimiento</th>
												<th scope="col">Ubicaci&oacute;n</th>
												<th scope="col">Observaci&oacute;n</th>
											</tr>
										</thead>
										<tbody class="buscar text-center">
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'reportes' . DS . 'plan_mantenimiento' . DS . 'agregarMantenimiento.php';

if (isset($_POST['id_log'])) {
    $instancia->guardarPlanMantenimientoControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/reportes/funcionesMantenimiento.js"></script>