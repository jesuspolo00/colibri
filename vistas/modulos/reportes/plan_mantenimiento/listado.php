<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_POST['buscar'])) {
    $datos = array('fecha' => $_POST['fecha'], 'buscar' => $_POST['buscar']);
} else {
    $datos_listado = $instancia->mostrarListadoLimitePlanMantenimientoControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 70);
if (!$permiso) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-success">
                        <a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-success"></i>
                        </a>
                        &nbsp;
                        Listado plan de mantenimientos
                    </h4>
                    <div class="btn-group">
                        <a href="<?=BASE_URL?>reportes/plan_mantenimiento/index" class="btn btn-secondary btn-sm">
                            <i class="fa fa-plus"></i>
                            &nbsp;
                            Agregar plan
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4 form-group">
                                <input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha" data-placement="top">
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
                                    <div class="input-group-append">
                                        <button class="btn btn-success btn-sm" type="submit">
                                            <i class="fa fa-search"></i>
                                            &nbsp;
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No. Plan</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Fecha inicio</th>
                                    <th scope="col">Fecha fin</th>
                                    <th scope="col">Observacion</th>
                                    <th scope="col">Estado</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_listado as $listado) {
                                    $id_listado   = $listado['id'];
                                    $nom_user     = $listado['nom_user'];
                                    $fecha_inicio = $listado['fecha_inicio'];
                                    $fecha_fin    = $listado['fecha_fin'];
                                    $observacion  = $listado['observacion_general'];

                                    $span_estado = ($listado['estado'] == 1) ? '<span class="badge badge-success">Realizado</span>' : '<span class="badge badge-warning">Pendiente de revision</span>';
                                    $span_estado = ($listado['fecha_fin'] < date('Y-m-d') && $listado['estado'] != 1) ? '<span class="badge badge-danger">No Realizado</span>' : $span_estado;

                                    ?>
                                    <tr class="text-center">
                                        <td><?=$id_listado?></td>
                                        <td><?=$nom_user?></td>
                                        <td><?=$fecha_inicio?></td>
                                        <td><?=$fecha_fin?></td>
                                        <td><?=$observacion?></td>
                                        <td><?=$span_estado?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-info btn-sm" href="<?=BASE_URL?>reportes/plan_mantenimiento/detalles?id=<?=base64_encode($id_listado)?>" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="<?=BASE_URL?>imprimir/reportes/planMantenimiento?id=<?=base64_encode($id_listado)?>" class="btn btn-primary btn-sm" target="_blank" data-tooltip="tooltip" title="Descargar PDF" data-placement="bottom">
                                                    <i class="fa fa-file-pdf"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

?>