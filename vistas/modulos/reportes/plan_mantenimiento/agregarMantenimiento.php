<div class="modal fade" id="agregar_mantenimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar Mantenimiento</h5>
      </div>
      <div class="modal-body">
        <form id="mantenimiento">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Descripci&oacute;n <span class="text-danger">*</span></label>
              <select id="categoria" class="form-control" required>
                <option value="" selected>Seleccione una opci&oacute;n...</option>
                <?php
                foreach ($datos_categoria as $categoria) {
                  $id_categoria  = $categoria['id'];
                  $nom_categoria = $categoria['nombre'];
                  ?>
                  <option value="<?=$id_categoria?>"><?=$nom_categoria?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Ubicaci&oacute;n <span class="text-danger">*</span></label>
              <select id="zona" class="form-control" required>
                <option value="" selected>Seleccione una opci&oacute;n...</option>
                <?php
                foreach ($datos_zonas as $zona) {
                  $id_zona  = $zona['id'];
                  $nom_zona = $zona['nombre'];
                  ?>
                  <option value="<?=$id_zona?>"><?=$nom_zona?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha mantenimiento <span class="text-danger">*</span></label>
              <input type="date" class="form-control" id="fecha_mant" required>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Observaci&oacute;n</label>
              <textarea class="form-control" rows="5" id="observacion"></textarea>
            </div>
            <div class="col-lg-12 form-group text-right mt-2">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cerrar
              </button>
              <button class="btn btn-success btn-sm guardar" type="button">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
