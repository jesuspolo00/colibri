<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';

$instancia            = ControlAreas::singleton_areas();
$instancia_categorias = ControlCategorias::singleton_categoria();

$datos_areas      = $instancia->mostrarAreasControl();
$datos_zonas      = $instancia->mostrarZonasControl();
$datos_categorias = $instancia->mostrarComponentesControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 30);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Inspeccion de Areas (Individual)
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reportes/listadoAreas" class="btn btn-info btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado - Correctivas
						</a>
						<a href="<?=BASE_URL?>reportes/listadoAreasMant" class="btn btn-warning btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado - Mantenimientos
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="enlace" value="0">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Zonas <span class="text-danger">*</span></label>
								<select name="zona" class="form-control" required>
									<option value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_zonas as $zonas) {
										$id_zona  = $zonas['id'];
										$nom_zona = $zonas['nombre'];
										?>
										<option value="<?=$id_zona?>"><?=$nom_zona?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Area / Oficina <span class="text-danger">*</span></label>
								<select name="area" class="form-control" required>
									<option value="">Selecciona una opcion...</option>
									<?php
									foreach ($datos_areas as $areas) {
										$id_area  = $areas['id'];
										$nom_area = $areas['nombre'];

										if ($areas['activo'] == 1) {
											?>
											<option value="<?=$id_area?>"><?=$nom_area?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Componente <span class="text-danger">*</span></label>
								<select class="form-control" name="categoria" required>
									<option value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_categorias as $categorias) {
										$id_categ  = $categorias['id'];
										$nom_categ = $categorias['nombre'];

										if ($categorias['activo'] == 1) {
											?>
											<option value="<?=$id_categ?>"><?=$nom_categ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de inspeccion <span class="text-danger">*</span></label>
								<select name="tipo" class="form-control" required>
									<option value="" selected>Seleccione una opcion...</option>
									<option value="1">Correctiva</option>
									<option value="2">Mantenimiento</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de inspeccion <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha" required value="<?=date('Y-m-d')?>">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Evidencia (opcional)</label>
								<div class="custom-file pmd-custom-file-filled">
									<input type="file" class="custom-file-input file_input" name="evidencia" accept=".png, .jpg, .jpeg">
									<label class="custom-file-label file_label" for="customfilledFile"></label>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observacion</label>
								<textarea class="form-control" rows="5" name="observacion"></textarea>
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['area'])) {
	$instancia->registrarInspeccionAreaControl();
}
?>