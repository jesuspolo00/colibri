<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 7);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/gestion" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Inspecciones y mantenimientos - Modulos
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 89);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/miReportes">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Mis Reportes</div>
											</div>
											<div class="col-auto">
												<i class="far fa-list-alt fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 29);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/listado">
								<div class="card border-left-primary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado Reportes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-list-ol fa-2x text-primary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 30);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/areas">
								<div class="card border-left-blue-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Inspeccion Areas (Individual)</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-search-location fa-2x text-blue-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 67);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/areas_general/index">
								<div class="card border-left-blue-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Inspeccion Areas (General)</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-search-location fa-2x text-blue-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 31);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/listadoAreas">
								<div class="card border-left-blue shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado Inspecciones (Individual)</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-blue"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 68);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/areas_general/listado">
								<div class="card border-left-blue shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado Inspecciones (General)</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-blue"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 32);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/historial">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Historial Zonas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-history fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 69);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/plan_mantenimiento/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Plan Mantenimientos</div>
											</div>
											<div class="col-auto">
												<i class="far fa-calendar-alt fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 70);
						if ($permiso) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>reportes/plan_mantenimiento/listado">
								<div class="card border-left-orange-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-700">Listado Plan Mantenimientos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar-week fa-2x text-orange-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>