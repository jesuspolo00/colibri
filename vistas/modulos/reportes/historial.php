<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_POST['buscar'])) {

} else {
	$datos_zonas = $instancia->mostrarZonasControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 32);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Historial de zonas
					</h4>
					<div class="btn-group">
						<div class="dropdown">
							<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
								Opciones
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="<?=BASE_URL?>reportes/areas">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar Inspeccion
								</a>
								<a href="<?=BASE_URL?>reportes/listadoAreas" class="dropdown-item">
									<i class="fa fa-eye"></i>
									&nbsp;
									Listado - Correctivos
								</a>
								<a href="<?=BASE_URL?>reportes/listadoAreasMant" class="dropdown-item">
									<i class="fa fa-eye"></i>
									&nbsp;
									Listado - Mantenimientos
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Zona</th>
									<th scope="col">Nombre</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_zonas as $zona) {
									$id_zona  = $zona['id'];
									$nom_zona = $zona['nombre'];
									?>
									<tr class="text-center">
										<td><?=$id_zona?></td>
										<td>
											<a href="<?=BASE_URL?>reportes/historialZona?zona=<?=base64_encode($id_zona)?>"><?=$nom_zona?></a>
										</td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>reportes/historialZona?zona=<?=base64_encode($id_zona)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver historial de inspecciones" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/zonas/codigoQR?id=<?=base64_encode($id_zona)?>" class="btn btn-primary btn-sm" target="_blank" data-tooltip="tooltip" title="Descargar QR" data-placement="bottom">
													<i class="fas fa-qrcode"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>