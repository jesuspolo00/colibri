<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

$datos_reporte = $instancia->mostrarTodosReportesUsuarioControl($id_log);

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 89);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Mis Reportes
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Reporte</th>
									<th scope="col">Bloque</th>
									<th scope="col">Area</th>
									<th scope="col">Descripcion</th>
									<th scope="col">Cantidad</th>
									<th scope="col">Codigo</th>
									<th scope="col">Fecha reportado</th>
									<th scope="col">Observacion</th>
									<th scope="col">Tipo de reporte</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_reporte as $dano) {
									$id_dano     = $dano['id'];
									$area        = $dano['nom_area'];
									$descripcion = $dano['nom_inventario'];
									$marca       = $dano['marca'];
									$modelo      = $dano['modelo'];
									$codigo      = $dano['codigo'];
									$estado      = $dano['estado'];
									$observacion = $dano['observacion'];
									$id_usuario  = $dano['id_user'];
									$id_articulo = $dano['id_inventario'];
									$nom_zona    = $dano['nom_zona'];
									$cantidad    = $dano['cantidad'];

									$span        = ($dano['tipo_reporte'] == 1) ? '<span class="badge badge-danger">Correctivo</span>' : '<span class="badge badge-warning">Mantenimiento</span>';
									$span_estado = ($estado == 2) ? '<span class="badge badge-warning">Pendiente</span>' : '<span class="badge badge-success">Solucionado</span>';

									$url_reporte = ($estado == 2) ? BASE_URL . 'imprimir/inventario/reporte?id=' . base64_encode($id_dano) : BASE_URL . 'imprimir/inventario/solucion?id=' . base64_encode($id_dano);
									$color_pdf   = ($estado == 2) ? 'btn-primary' : 'btn-success';
									?>
									<tr class="text-uppercase text-center text-dark">
										<td><?=$id_dano?></td>
										<td><?=$nom_zona?></td>
										<td><?=$area?></td>
										<td><?=$descripcion?></td>
										<td><?=$cantidad?></td>
										<td><?=$codigo?></td>
										<td><?=$dano['fecha_reporte']?></td>
										<td class="text-lowercase"><?=$observacion?></td>
										<td><?=$span?></td>
										<td><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<a href="<?=$url_reporte?>" target="_blank" class="btn <?=$color_pdf?> btn-sm" data-tooltip="tooltip" title="Descargar Reporte" data-placement="bottom">
													<i class="fa fa-file-pdf"></i>
												</a>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';