<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$datos_zonas = $instancia->mostrarZonasControl();

$ver_descargar = 'd-none';

if (isset($_POST['buscar'])) {
	$datos            = array('buscar' => $_POST['buscar'], 'fecha' => $_POST['fecha'], 'zona' => $_POST['zona']);
	$datos_inspeccion = $instancia->buscarInspeccionesMantLimiteControl($datos);
	$ver_descargar    = '';
} else {
	$datos_inspeccion = $instancia->mostrarInspeccionesMantLimiteControl();
	$ver_descargar    = 'd-none';
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 30);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/listadoAreas" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de inspecciones - Mantenimientos
					</h4>
					<div class="btn-group">
						<div class="dropdown">
							<a href="<?=BASE_URL?>imprimir/reportes/reporteAreasExcelMant" target="_blank" class="btn btn-success btn-sm">
								<i class="fa fa-file-excel"></i>
								&nbsp;
								Descargar Reporte (Todo)
							</a>
							<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
								Opciones
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="<?=BASE_URL?>reportes/areas">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar Inspeccion
								</a>
								<a href="<?=BASE_URL?>reportes/listadoAreas" class="dropdown-item">
									<i class="fa fa-eye"></i>
									&nbsp;
									Listado - Correctivos
								</a>
								<a href="<?=BASE_URL?>reportes/historial" class="dropdown-item">
									<i class="fas fa-history"></i>
									&nbsp;
									Historial Zonas
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="zona" class="form-control" data-tooltip="tooltip" title="Area" data-placement="top">
									<option value="">Seleccione una zona...</option>
									<?php
									foreach ($datos_zonas as $zonas) {
										$id_zona  = $zonas['id'];
										$nom_zona = $zonas['nombre'];
										?>
										<option value="<?=$id_zona?>"><?=$nom_zona?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha inspeccion" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<div class="col-lg-12 form-group <?=$ver_descargar?>" style="margin-bottom: 3.5%;">
							<a href="<?=BASE_URL?>imprimir/reportes/reporteAreasExcelBuscMant?buscar=<?=$_POST['buscar']?>&fecha=<?=$_POST['fecha']?>&zona=<?=$_POST['zona']?>" class="btn btn-success btn-sm float-right">
								<i class="fa fa-file-excel"></i>
								&nbsp;
								Descargar filtro
							</a>
						</div>
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Cantidad Reportado</th>
									<th scope="col">Usuario Reporta</th>
									<th scope="col">Zona</th>
									<th scope="col">Area</th>
									<th scope="col">Componente</th>
									<th scope="col">Fecha Inspeccion</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_inspeccion as $inspeccion) {
									$id_inspeccion      = $inspeccion['id'];
									$nom_area           = $inspeccion['nom_area'];
									$nom_categoria      = $inspeccion['nom_categoria'];
									$fecha              = $inspeccion['fecha'];
									$observacion        = $inspeccion['observacion'];
									$nom_zona           = $inspeccion['nom_zona'];
									$nom_user           = $inspeccion['nom_user'];
									$cantidad_reportado = $inspeccion['cantidad_reportado'];
									$id_area            = $inspeccion['id_area'];
									$id_zona            = $inspeccion['id_zona'];
									$id_componente      = $inspeccion['id_categoria'];
									?>
									<tr class="text-center">
										<td><?=$cantidad_reportado?></td>
										<td><?=$nom_user?></td>
										<td><?=$nom_zona?></td>
										<td><?=$nom_area?></td>
										<td><?=$nom_categoria?></td>
										<td><?=$fecha?></td>
										<td><?=$observacion?></td>
										<td>
											<button class="btn btn-info btn-sm" type="button" data-tooltip="tooltip" title="Ver evidencia" data-placement="bottom" data-toggle="modal" data-target="#evidencia_<?=$id_inspeccion?>">
												<i class="fa fa-eye"></i>
											</button>
										</td>
										<td>
											<div class="btn-group">
												<button class="btn btn-success btn-sm" type="button" data-tooltip="tooltip" title="Solucionar" data-placement="bottom" data-toggle="modal" data-target="#solucionar<?=$id_inspeccion?>" >
													<i class="fa fa-check"></i>
												</button>
												<a href="<?=BASE_URL?>imprimir/reportes/reporteAreas?id=<?=base64_encode($id_inspeccion)?>" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar Reporte" data-placement="bottom" target="_blank">
													<i class="fa fa-file-pdf"></i>
												</a>
											</div>
										</td>
									</tr>

									<div class="modal fade" id="solucionar<?=$id_inspeccion?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Solucion Inspeccion No. <?=$id_inspeccion?></h5>
												</div>
												<div class="modal-body">
													<form method="POST">
														<input type="hidden" name="id_area" value="<?=$id_area?>">
														<input type="hidden" name="id_zona" value="<?=$id_zona?>">
														<input type="hidden" name="id_componente" value="<?=$id_componente?>">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="url" value="2">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Zona <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$nom_zona?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$nom_area?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Componente <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$nom_categoria?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Reportado <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$fecha?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Solucion <span class="text-danger">*</span></label>
																<input type="date" name="fecha_solucion" class="form-control" required>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion Respuesta</label>
																<textarea class="form-control" name="observacion_respuesta" rows="5"></textarea>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fa fa-check"></i>
																	&nbsp;
																	Solucionar
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

									<div class="modal fade" id="evidencia_<?=$id_inspeccion?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Evidencia Inspeccion No. <?=$id_inspeccion?></h5>
												</div>
												<div class="modal-body">
													<div class="row p-2">
														<div class="col-lg-12 form-group">
															<iframe src="<?=PUBLIC_PATH?>upload/<?=$inspeccion['evidencia']?>" style="width:100%; height: 500px;"></iframe>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->solucionarInspeccionControl();
}