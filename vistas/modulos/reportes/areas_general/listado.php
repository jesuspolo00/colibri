<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_POST['buscar'])) {
	$datos            = array('buscar' => $_POST['buscar'], 'fecha' => $_POST['fecha'], 'estado' => $_POST['estado']);
	$datos_inspeccion = $instancia->buscarInspeccionGeneralControl($datos);
} else {
	$datos_inspeccion = $instancia->mostrarInspeccionGeneralLimiteControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 68);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Listado de inspecciones (Generales)
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reportes/areas_general/index" class="btn btn-secondary btn-sm">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar inspecci&oacute;n
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="estado" class="form-control">
									<option value="" selected>Seleccione un estado...</option>
									<option value="0">Pendientes</option>
									<option value="1">Solucionados</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha inspeccion" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. inspecci&oacute;n</th>
									<th scope="col">Cantidad Reportado</th>
									<th scope="col">Usuario Reporta</th>
									<th scope="col">Fecha Inspeccion</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_inspeccion as $inspeccion) {
									$id_inspeccion    = $inspeccion['id'];
									$cantidad         = $inspeccion['cantidad'];
									$fecha_inspeccion = $inspeccion['fecha_inspeccion'];
									$nom_reporta      = $inspeccion['nom_usuario'];

									$bg_pdf   = ($inspeccion['estado'] == 1) ? 'btn-success' : 'btn-primary';
									$text_pdf = ($inspeccion['estado'] == 0) ? 'Descargar reporte' : 'Descargar soluci&oacute;n';

									$span = ($inspeccion['estado'] == 0) ? '<span class="badge badge-warning">Pendiente</span>' : '<span class="badge badge-success">Solucionado</span>';

									?>
									<tr class="text-center">
										<td><?=$id_inspeccion?></td>
										<td><?=$cantidad?></td>
										<td><?=$nom_reporta?></td>
										<td><?=$fecha_inspeccion?></td>
										<td><?=$span?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>reportes/areas_general/detalle?id=<?=base64_encode($id_inspeccion)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/reportes/reporteAreaGeneral?id=<?=base64_encode($id_inspeccion)?>" target="_blank" class="btn <?=$bg_pdf?> btn-sm" data-tooltip="tooltip" title="<?=$text_pdf?>" data-placement="bottom">
													<i class="fa fa-file-pdf"></i>
												</a>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->solucionarInspeccionControl();
}