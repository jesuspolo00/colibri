<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia            = ControlAreas::singleton_areas();

$datos_areas      = $instancia->mostrarAreasControl();
$datos_zonas      = $instancia->mostrarZonasControl();
$datos_categorias = $instancia->mostrarComponentesControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 67);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Inspeccion de Areas (General)
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reportes/areas_general/listado" class="btn btn-info btn-sm">
							<i class="fa fa-eye"></i>
							&nbsp;
							Listado de inspecciones
						</a>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Quien registra la inspeccion</label>
								<input type="text" class="form-control" value="<?=$nom_log_session?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha de inspeccion <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_inspeccion" required value="<?=date('Y-m-d')?>">
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_inspeccion">
									<i class="fa fa-plus"></i>
									&nbsp;
									Agregar inspecci&oacute;n
								</button>
							</div>
							<div class="col-lg-12 form-group mt-2">
								<div class="table-responsive">
									<table class="table table-hover border table-sm" width="100%" cellspacing="0">
										<thead>
											<tr class="text-center font-weight-bold">
												<th scope="col">Zona</th>
												<th scope="col">Area/Oficina</th>
												<th scope="col">Componente</th>
												<th scope="col">Tipo de inspecci&oacute;n</th>
												<th scope="col">Observaci&oacute;n</th>
											</tr>
										</thead>
										<tbody class="buscar text-center">
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
	require_once VISTA_PATH . 'modulos' . DS . 'reportes' . DS . 'areas_general' . DS . 'agregarInspeccion.php';

	if(isset($_POST['id_log'])){
		$instancia->guardarInspeccionGeneralControl();
	}
	?>
	<script src="<?=PUBLIC_PATH?>js/reportes/funcionesAreasGeneral.js"></script>