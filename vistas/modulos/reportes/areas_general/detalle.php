<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 68);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['id'])) {

	$id_inspeccion = base64_decode($_GET['id']);

	$datos_inspeccion = $instancia->mostrarInfoInspeccionGeneralControl($id_inspeccion);
	$datos_detalle    = $instancia->mostrarDetallesGeneralControl($id_inspeccion);

	$disabled  = ($datos_inspeccion['estado'] == 1) ? 'disabled' : '';
	$ver_boton = ($datos_inspeccion['estado'] == 1) ? 'd-none' : '';

	$estado_bg = ($datos_inspeccion['estado'] == 1) ? 'bg-success' : 'bg-warning';
	$tipo      = ($datos_inspeccion['estado'] == 1) ? 'Solucionado' : 'Pendiente';
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>reportes/areas_general/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Detalle de inspeccion No. <?=$id_inspeccion?>
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<input type="hidden" name="id_inspeccion" value="<?=$id_inspeccion?>">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Usuario reporta</label>
								<input type="text" class="form-control" disabled value="<?=$datos_inspeccion['nom_usuario']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha inspecci&oacute;n</label>
								<input type="date" class="form-control" disabled value="<?=$datos_inspeccion['fecha_inspeccion']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fecha respuesta <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_respuesta" value="<?=date('Y-m-d')?>" <?=$disabled?> required>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado inspecci&oacute;n <span class="text-danger">*</span></label>
								<input type="text" class="form-control text-white <?=$estado_bg?>" value="<?=$tipo?>" disabled>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center">
										<th class="h5 text-success font-weight-bold" colspan="6">Detalles de la inspecci&oacute;n</th>
									</tr>
									<tr class="text-center text-uppercase font-weight-bold">
										<th scope="col">Zona</th>
										<th scope="col">Area</th>
										<th scope="col">Categoria</th>
										<th scope="col">Tipo inspeccion</th>
										<th scope="col">Observacion</th>
										<th scope="col">Observacion Respuesta</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_detalle as $detalle) {
										$id_detalle    = $detalle['id'];
										$nom_zona      = $detalle['nom_zona'];
										$nom_area      = $detalle['nom_area'];
										$nom_categoria = $detalle['nom_categoria'];
										$observacion   = $detalle['observacion'];

										$span_tipo = ($detalle['tipo'] == 1) ? '<span class="badge badge-danger">Correctivo</span>' : '<span class="badge badge-warning">Mantenimiento</span>';
										?>
										<input type="hidden" name="id_detalle[]" value="<?=$id_detalle?>">
										<tr class="text-center">
											<td><?=$nom_zona?></td>
											<td><?=$nom_area?></td>
											<td><?=$nom_categoria?></td>
											<td><?=$span_tipo?></td>
											<td><?=$observacion?></td>
											<td>
												<textarea class="form-control" name="observacion_respuesta[]" <?=$disabled?> rows="5"><?=$detalle['observacion_respuesta']?></textarea>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
						<div class="row p-2">
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Observacion respuesta (General)</label>
								<textarea name="observacion_general" class="form-control" <?=$disabled?> rows="5"><?=$datos_inspeccion['observacion_general']?></textarea>
							</div>
							<div class="col-lg-12 form-group mt-2 text-right <?=$ver_boton?>">
								<button class="btn btn-success btn-sm" type="submit">
									<i class="fa fa-check"></i>
									&nbsp;
									Solucionar inspecci&oacute;n
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->solucionarInspeccionGeneralControl();
}