<div class="modal fade" id="agregar_inspeccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Agregar inspecci&oacute;n</h5>
			</div>
			<div class="modal-body">
				<div class="row p-2">
					<div class="col-lg-6 form-group">
						<label class="font-weight-bold">Zonas <span class="text-danger">*</span></label>
						<select id="zona" class="form-control" required>
							<option value="">Seleccione una opcion...</option>
							<?php
							foreach ($datos_zonas as $zonas) {
								$id_zona  = $zonas['id'];
								$nom_zona = $zonas['nombre'];
								?>
								<option value="<?=$id_zona?>"><?=$nom_zona?></option>
								<?php
							}
							?>
						</select>
					</div>
					<div class="col-lg-6 form-group">
						<label class="font-weight-bold">Area / Oficina <span class="text-danger">*</span></label>
						<select id="area" class="form-control" required>
							<option value="">Selecciona una opcion...</option>
							<?php
							foreach ($datos_areas as $areas) {
								$id_area  = $areas['id'];
								$nom_area = $areas['nombre'];

								if ($areas['activo'] == 1) {
									?>
									<option value="<?=$id_area?>"><?=$nom_area?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="col-lg-6 form-group">
						<label class="font-weight-bold">Componente <span class="text-danger">*</span></label>
						<select class="form-control" id="categoria" required>
							<option value="">Seleccione una opcion...</option>
							<?php
							foreach ($datos_categorias as $categorias) {
								$id_categ  = $categorias['id'];
								$nom_categ = $categorias['nombre'];

								if ($categorias['activo'] == 1) {
									?>
									<option value="<?=$id_categ?>"><?=$nom_categ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="col-lg-6 form-group">
						<label class="font-weight-bold">Tipo de inspecci&oacute;n <span class="text-danger">*</span></label>
						<select id="tipo" class="form-control" required>
							<option value="" selected>Seleccione una opcion...</option>
							<option value="1">Correctiva</option>
							<option value="2">Mantenimiento</option>
						</select>
					</div>
					<div class="col-lg-12 form-group">
						<label class="font-weight-bold">Observaci&oacute;n</label>
						<textarea class="form-control" rows="5" id="observacion"></textarea>
					</div>
					<div class="col-lg-12 form-group text-right mt-2">
						<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
							<i class="fa fa-times"></i>
							&nbsp;
							Cancelar
						</button>
						<button class="btn btn-success btn-sm guardar_inspeccion" type="button">
							<i class="fa fa-save"></i>
							&nbsp;
							Guardar
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>