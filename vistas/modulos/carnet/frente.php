<?php
header('Content-Type: text/html; charset=UTF-8');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
require_once CONTROL_PATH . 'carnet' . DS . 'ControlCarnet.php';

$instancia = ControlCarnet::singleton_carnet();

if (isset($_GET['personal'])) {
	$id_personal = base64_decode($_GET['personal']);

	$datos_personal = $instancia->mostrarDatosPersonalIdControl($id_personal);

	$nom_completo = $datos_personal['nom_prim'] . ' ' . $datos_personal['nom_seg'] . ' ' . $datos_personal['apellido_prim'] . ' ' . $datos_personal['apellido_seg'];

	$nombre    = $nom_completo;
	$documento = $datos_personal['documento'];
	$oficio    = ucfirst($datos_personal['nom_cargo']);

	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>Carnet</title>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;700&display=swap" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	</head>
	<body>
		<center>
			<div class="fondo">
				<img src="<?=PUBLIC_PATH?>img/fondo_carnet.png">
			</div>
			<div class="vertical--portrait">
				<img src="<?=PUBLIC_PATH?>upload/<?=$datos_personal['foto']?>" alt="">
			</div>
			<p class="nombre"><?=$nombre?></p>
			<p class="cargo"><?=$oficio?></p>
		</center>
	</body>
	</html>
	<style type="text/css">

		*{
			font-family: 'Roboto', sans-serif;
		}

		body {
			height: 100%;
			margin: 0px;
			padding: 0px;
		}
		html{
			height: 100%;
		}
		.fondo{
			position: relative;
			width: 100%;
			height: 322.5px;
			overflow: hidden;
		}

		.fondo img{
			width: 100%;
			height: auto;
			margin-top: -9%;
		}
		.nombre{
			width: 100%;
			position: relative;
			margin-top: 6%;
			margin-bottom: 4%;
			font-size: 0.90em;
			color: #52565B;
			font-weight: bold;
			text-transform:uppercase;
		}
		.cargo{
			width: 60%;
			position: relative;
			margin-top: -2%;
			font-size: 0.70em;
			color: #52565B;
			font-weight: 300;
		}

		.vertical--portrait {
			position: relative;
			width: 125px;
			height: 132px;
			overflow: hidden;
			margin-top: -148%;
			z-index: -1000 !important;
		}

		.vertical--portrait img {
			width: 97%;
			height: auto;
		}

		@media print {

		}
	</style>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
	?>
	<script>
		$(".loader").hide();
		window.print();
		window.addEventListener("afterprint", function(event) {
			window.close();
		});
	</script>
	<?php
}?>