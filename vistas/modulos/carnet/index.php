<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'carnet' . DS . 'ControlCarnet.php';

$instancia = ControlCarnet::singleton_carnet();

$datos_categoria = $instancia->mostrarCategoriasCarnetControl();
$datos_cargo     = $instancia->mostrarCargosCarnetControl();

if (isset($_POST['buscar'])) {
	$datos_personal = $instancia->buscarPersonalControl($_POST['buscar']);
} else {
	$datos_personal = $instancia->mostrarPersonalControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 16);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/contabilidad" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Carnets
					</h4>
					<div class="btn-group">
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregarPersonal">
							<i class="fa fa-plus"></i>
							&nbsp;
							Generar Carnet
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Programa / Cargo</th>
									<th scope="col">Codigo Foto</th>
									<th scope="col">Foto</th>
									<th scope="col">Pago</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_personal as $personal) {
									$id_personal      = $personal['id'];
									$nom_completo     = $personal['nom_prim'] . ' ' . $personal['nom_seg'] . ' ' . $personal['apellido_prim'] . ' ' . $personal['apellido_seg'];
									$documento        = $personal['documento'];
									$codigo           = $personal['codigo'];
									$pago             = ($personal['pago'] == 0) ? 'NO' : 'SI';
									$pago             = ($personal['pago'] == 2) ? 'BECADO' : $pago;
									$curso            = $personal['nom_cargo'];
									$id_categoria_sel = $personal['id_categoria'];

									$option_no   = ($personal['pago'] == 0) ? 'selected' : '';
									$option_si   = ($personal['pago'] == 1) ? 'selected' : '';
									$option_beca = ($personal['pago'] == 2) ? 'selected' : '';

									$foto_subida = ($personal['foto'] == '') ? '<span class="badge badge-danger">Falta foto</span>' : '<span class="badge badge-success">Foto Subida</span>';

									$carnet = '';

									if ($personal['foto'] == '' && $pago == 'NO') {
										$carnet = 'd-none';
									}
									if ($personal['foto'] != '' && $pago == 'NO') {
										$carnet = 'd-none';
									}
									if ($personal['foto'] != '' && $pago != 'NO') {
										$carnet = '';
									}
									if ($personal['foto'] == '' && $pago != 'NO') {
										$carnet = 'd-none';
									}

									$fecha_generado = ($personal['fecha_generado'] == '' || $personal['fecha_generado'] == '0000-00-00 00:00:00') ? '' : date('Y-m-d', strtotime($personal['fecha_generado']));
									?>
									<tr class="text-center">
										<td><?=$documento?></td>
										<td class="text-uppercase"><?=$nom_completo?></td>
										<td class="text-uppercase"><?=$curso?></td>
										<td><?=$codigo?></td>
										<td><?=$foto_subida?></td>
										<td><?=$pago?></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-success btn-sm" type="button" data-tooltip="tooltip" title="Subir Foto" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#subir<?=$id_personal?>">
													<i class="fa fa-upload"></i>
												</button>
												<a href="<?=BASE_URL?>carnet/frente?personal=<?=base64_encode($id_personal)?>" class="btn btn-primary btn-sm <?=$carnet?>" target="_blank" data-tooltip="tooltip" title="Imprimir Carnet" data-trigger="hover" data-placement="bottom">
													<i class="fas fa-id-card-alt"></i>
												</a>
											</div>
										</td>
									</tr>

									<div class="modal fade" id="subir<?=$id_personal?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Modal title</h5>
												</div>
												<div class="modal-body">
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id_user" value="<?=$id_personal?>">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="foto_ant" value="<?=$personal['foto']?>">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="documento_edit" value="<?=$personal['documento']?>" readonly>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Primer Nombre <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="nom_prim_edit" value="<?=$personal['nom_prim']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Primer Apellido <span class="text-danger">*</span></label>
																<input type="text" class="form-control" name="apellido_prim_edit" value="<?=$personal['apellido_prim']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Segundo Apellido </label>
																<input type="text" class="form-control" name="apellido_seg_edit" value="<?=$personal['apellido_seg']?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Organizacion <span class="text-danger">*</span></label>
																<select name="categoria" class="form-control categoria" required>
																	<option selected value="">Seleccione una categoria...</option>
																	<?php
																	foreach ($datos_categoria as $categoria) {
																		$id_categoria  = $categoria['id'];
																		$nom_categoria = $categoria['nombre'];

																		$selected = ($id_categoria == $id_categoria_sel) ? 'selected' : '';
																		?>
																		<option value="<?=$id_categoria?>" <?=$selected?>><?=$nom_categoria?></option>
																	<?php }?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Programa / Cargo <span class="text-danger">*</span></label>
																<select name="curso_edit" class="form-control curso" required>
																	<?php
																	foreach ($datos_cargo as $cargo) {
																		$id_cargo  = $cargo['id'];
																		$nom_cargo = $cargo['nombre'];

																		$selected = ($personal['curso'] == $id_cargo) ? 'selected' : '';
																		?>
																		<option value="<?=$id_cargo?>" <?=$selected?>><?=$nom_cargo?></option>
																	<?php }?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Codigo Foto <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$codigo?>" name="codigo_edit" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha de entrega</label>
																<input type="date" class="form-control" value="<?=$fecha_generado?>" name="fecha_entrega">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Pagado</label>
																<select name="pago_edit" class="form-control">
																	<option value="0" <?=$option_no?>>No</option>
																	<option value="1" <?=$option_si?>>SI</option>
																	<option value="2" <?=$option_beca?>>Becado</option>
																</select>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Foto a subir</label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" name="foto" id="<?=$id_personal?>" accept=".png, .jpg, .jpeg">
																	<label class="custom-file-label file_label_<?=$id_personal?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-success btn-sm" type="submit">
																	<i class="fa fa-upload"></i>
																	&nbsp;
																	Aceptar
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'modulos' . DS . 'carnet' . DS . 'agregarPersonal.php';

if (isset($_POST['nom_prim'])) {
	$instancia->registrarPersonalControl();
}

if (isset($_POST['nom_prim_edit'])) {
	$instancia->subirFotoPersonalControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/carnet/funcionesCarnet.js"></script>