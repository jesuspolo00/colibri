<!-- Modal -->
<div class="modal fade" id="agregarPersonal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Generar Carnet</h5>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="documento" id="documento" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Primer Nombre <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nom_prim" required id="nom_prim">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Primer Apellido <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="apellido_prim" id="apellido_prim" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Segundo Apellido </label>
              <input type="text" class="form-control" name="apellido_seg" id="apellido_seg">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Organizacion <span class="text-danger">*</span></label>
              <select name="categoria" class="form-control categoria" required>
                <option selected value="">Seleccione una categoria...</option>
                <?php
                foreach ($datos_categoria as $categoria) {
                  $id_categoria  = $categoria['id'];
                  $nom_categoria = $categoria['nombre'];
                  ?>
                  <option value="<?=$id_categoria?>"><?=$nom_categoria?></option>
                <?php }?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Programa / Cargo <span class="text-danger">*</span></label>
              <select name="curso" class="form-control curso" required></select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Codigo de foto <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="codigo" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Pagado</label>
              <select name="pago" class="form-control">
                <option value="0">No</option>
                <option value="1">Si</option>
                <option value="2">Becado</option>
              </select>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Foto a subir</label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" name="foto" accept=".png, .jpg, .jpeg">
                <label class="custom-file-label file_label" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-12 form-group text-right mt-2">
              <button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-success btn-sm" type="submit">
                <i class="fa fa-check"></i>
                &nbsp;
                Aceptar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
