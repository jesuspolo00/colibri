<div class="modal fade" id="agregar_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Agregar Usuario</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <input type="hidden" name="enlace" value="1">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="documento" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="apellido" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Telefono</label>
              <input type="text" class="form-control numeros" name="telefono">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Correo</label>
              <input type="email" class="form-control" name="correo">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
              <input type="text" class="form-control user" name="usuario" id="usuario" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Contrase&ntilde;a <span class="text-danger">*</span></label>
              <input type="password" class="form-control" name="password" id="password" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Confirmar contrase&ntilde;a <span class="text-danger">*</span></label>
              <input type="password" class="form-control" name="conf_password" id="conf_password" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
              <select name="perfil" class="form-control" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_perfil as $perfil) {
                  $id_perfil  = $perfil['id'];
                  $nom_perfil = $perfil['nombre'];

                  if (!in_array($id_perfil, [1, 5])) {
                    ?>
                    <option value="<?=$id_perfil?>"><?=$nom_perfil?></option>
                    <?php
                  }
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Centro de costo</label>
              <select name="centro_costo" class="form-control">
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_centro_costo as $centro_costo) {
                  $id_centro  = $centro_costo['id'];
                  $nom_centro = $centro_costo['nombre'];
                  ?>
                  <option value="<?=$id_centro?>"><?=$nom_centro?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              &nbsp;
              <button class="btn btn-success btn-sm" id="enviar_form" type="submit">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
