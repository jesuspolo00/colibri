<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil       = $instancia_perfil->mostrarPerfilesControl();
$datos_centro_costo = $instancia->mostrarCentroCostoControl();

if (isset($_POST['buscar'])) {
	$datos_usuarios = $instancia->buscarUsuariosControl($_POST['buscar']);
} else {
	$datos_usuarios = $instancia->mostrarLimiteUsuariosControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 3);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Usuarios
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>imprimir/reporteUsuario" target="_blank" class="btn btn-success btn-sm">
							<i class="fa fa-file-excel"></i>
							&nbsp;
							Descargar Reporte
						</a>
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#agregar_usuario">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar usuario
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Usuario</th>
									<th scope="col">Perfil</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user         = $usuario['id_user'];
									$documento       = $usuario['documento'];
									$nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$correo          = $usuario['correo'];
									$telefono        = $usuario['telefono'];
									$user            = $usuario['user'];
									$perfil          = $usuario['nom_perfil'];
									$activo          = $usuario['estado'];

									$ver_subir_documentos = (empty($usuario['documentos'])) ? '' : 'd-none';
									$ver_boton            = ($usuario['perfil'] == 3) ? '' : 'd-none';

									if ($activo == 1) {
										$icon   = '<i class="fa fa-times"></i>';
										$title  = 'Eliminar';
										$button = 'btn-danger';
										$clase  = 'inactivar_usuario';
									} else {
										$icon   = '<i class="fa fa-check"></i>';
										$title  = 'Activar';
										$button = 'btn-success';
										$clase  = 'activar_usuario';
									}

									$url_hoja = ($usuario['perfil'] == 5) ? BASE_URL . 'proveedor/hojaRegistro?proveedor=' . base64_encode($id_user) : BASE_URL . 'hoja/index?id=' . base64_encode($id_user);

									$ver_activo = ($activo == 0 && $_SESSION['rol'] != 1) ? 'd-none' : '';

									?>
									<tr class="text-center text-dark <?=$ver_activo?>">
										<td><?=$documento?></td>
										<td><?=$nombre_completo?></td>
										<td><?=$correo?></td>
										<td><?=$user?></td>
										<td><?=$perfil?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<button class="btn btn-sm btn-secondary" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#usuario<?=$id_user?>">
													<i class="fa fa-user-edit"></i>
												</button>
												<button class="btn <?=$button?> btn-sm <?=$clase?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" id="<?=$id_user?>" data-log="<?=$id_log?>">
													<?=$icon?>
												</button>
											</div>
										</td>
									</tr>


									<div class="modal fade" id="usuario<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-success font-weight-bold">Agregar usuario</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_user" value="<?=$id_user?>">
													<div class="modal-body border-0">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
																<input type="text" name="documento_edit" class="form-control" value="<?=$documento?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																<input type="text" name="nombre_edit" class="form-control" value="<?=$usuario['nombre']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
																<input type="text" name="apellido_edit" class="form-control" value="<?=$usuario['apellido']?>" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Telefono</label>
																<input type="text" name="telefono_edit" class="form-control" value="<?=$telefono?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Correo</label>
																<input type="text" class="form-control" value="<?=$correo?>" name="correo_edit">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$user?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
																<select name="perfil_edit" class="form-control">
																	<?php
																	foreach ($datos_perfil as $perfiles) {
																		$id_perfil  = $perfiles['id'];
																		$nom_perfil = $perfiles['nombre'];

																		$select = ($id_perfil == $usuario['perfil']) ? 'selected' : '';
																		if (!in_array($id_perfil, [1])) {
																			?>
																			<option value="<?=$id_perfil?>" <?=$select?>><?=$nom_perfil?></option>
																			<?php
																		}
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Centro de costo</label>
																<select name="centro_costo" class="form-control">
																	<option value="" selected>Seleccione una opcion...</option>
																	<?php
																	foreach ($datos_centro_costo as $centro_costo) {
																		$id_centro  = $centro_costo['id'];
																		$nom_centro = $centro_costo['nombre'];

																		$select = ($id_centro == $usuario['centro_costo']) ? 'selected' : '';

																		?>
																		<option value="<?=$id_centro?>" <?=$select?>><?=$nom_centro?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<div class="col-lg-12 form-group text-right">
															<button type="button" class="btn btn-secondary btn-sm float-left restablecer_pass" data-toggle="popover" title="Restablecer contrase&ntilde;a" data-content="La nueva contrase&ntilde;a sera: codetec123456@" data-trigger="hover" data-placement="left" id="<?=$id_user?>">
																<i class="fas fa-sync-alt"></i>
																&nbsp;
																Restablecer contrase&ntilde;a
															</button>
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cerrar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-user-edit"></i>
																&nbsp;
																Aceptar
															</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['documento'])) {
	$instancia->registrarUsuarioControl();
}

if (isset($_POST['documento_edit'])) {
	$instancia->actualizarUsuarioControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/usuarios/funcionesUsuarios.js"></script>