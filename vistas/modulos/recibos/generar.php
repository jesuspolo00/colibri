<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlRecibos::singleton_recibos();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 57);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['proyecto'])) {

	$id_proyecto = base64_decode($_GET['proyecto']);
	$url         = base64_decode($_GET['enlace']);

	$datos_proyecto = $instancia->mostrarProyectosIdControl($id_proyecto);

	if ($url == 1) {
		$enlace = BASE_URL . 'recibos' . DS . 'index';
	}

	if ($url == 2) {
		$enlace = BASE_URL . 'fundepex' . DS . 'proyectos' . DS . 'index';
	}

	if ($url == 3) {
		$enlace = BASE_URL . 'talento' . DS . 'proyectos' . DS . 'index';
	}
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=$enlace?>" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Generar recibo de caja <span class="text-uppercase">(<?=$datos_proyecto['nombre']?>)</span>
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_proyecto" value="<?=$id_proyecto?>">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="enlace" value="<?=$url?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Proyecto <span class="text-danger">*</span></label>
									<input type="text" class="form-control" disabled value="<?=$datos_proyecto['nombre']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Ciudad del proyecto</label>
									<input type="text" class="form-control" disabled value="<?=$datos_proyecto['ciudad']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Pagado a <span class="text-danger">*</span></label>
									<select name="proveedor" class="form-control" required>
										<option value="" selected="">Seleccione una opcion...</option>
										<?php
										foreach ($datos_proveedor as $proveedor) {
											$id_proveedor     = $proveedor['id_proveedor'];
											$nombre_proveedor = $proveedor['nombre'];
											$nit              = $proveedor['num_identificacion'];
											$estado           = $proveedor['estado'];

											$ver = ($estado == 0) ? 'd-none' : '';
											?>
											<option value="<?=$id_proveedor?>" class="<?=$ver?>"><?=$nombre_proveedor . ' (' . $nit . ')'?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Tipo de pago <span class="text-danger">*</span></label>
									<select name="tipo_pago" class="form-control" id="tipo_pago" required>
										<option value="" selected="">Seleccione una opcion...</option>
										<option value="1">Cheque</option>
										<option value="2">Transferencia bancaria</option>
										<option value="3">Efectivo</option>
									</select>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Fecha de pago <span class="text-danger">*</span></label>
									<input type="date" class="form-control" name="fecha_pago" required>
								</div>
								<div class="form-group col-lg-4">
									<label class="font-weight-bold">Valor a pagar <span class="text-danger">*</span></label>
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text" id="basic-addon2">$</span>
										</div>
										<input type="text" class="form-control numeros precio" aria-describedby="basic-addon2" name="valor" required>
									</div>
								</div>
								<div class="form-group col-lg-4" id="banco">
									<label class="font-weight-bold">Banco <span class="text-danger">*</span></label>
									<input type="text" name="banco" class="form-control">
								</div>
								<div class="form-group col-lg-4" id="cheque">
									<label class="font-weight-bold">No. cheque <span class="text-danger">*</span></label>
									<input type="text" name="cheque" class="form-control">
								</div>
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Por concepto</label>
									<textarea cols="10" rows="5" class="form-control" name="concepto"></textarea>
								</div>
								<div class="form-group col-lg-12">
									<button class="btn btn-success btn-sm float-right" type="submit">
										<i class="fa fa-save"></i>
										&nbsp;
										Generar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_log'])) {
		$instancia->generarReciboControl();
	}
}
?>
<script src="<?=PUBLIC_PATH?>js/recibo/funcionesRecibo.js"></script>