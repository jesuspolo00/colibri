<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'recibos' . DS . 'ControlRecibos.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia           = ControlRecibos::singleton_recibos();
$instancia_proveedor = ControlProveedor::singleton_proveedor();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 57);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['proyecto'])) {

	$id_proyecto = base64_decode($_GET['proyecto']);
	$url         = base64_decode($_GET['enlace']);

	$datos_proyecto  = $instancia->mostrarProyectosIdControl($id_proyecto);
	$datos_recibo    = $instancia->mostrarRecibosProyectoControl($id_proyecto);
	$datos_proveedor = $instancia_proveedor->mostrarProveedoresControl();

	if ($url == 1) {
		$enlace = BASE_URL . 'recibos' . DS . 'index';
	}

	if ($url == 2) {
		$enlace = BASE_URL . 'fundepex' . DS . 'proyectos' . DS . 'index';
	}

	if ($url == 3) {
		$enlace = BASE_URL . 'talento' . DS . 'proyectos' . DS . 'index';
	}
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=$enlace?>" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Historial de recibos de caja generados - proyecto <span class="text-uppercase">(<?=$datos_proyecto['nombre']?>)</span>
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Proyecto</label>
								<input type="text" class="form-control" disabled value="<?=$datos_proyecto['nombre']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Ciudad del proyecto</label>
								<input type="text" class="form-control" disabled value="<?=$datos_proyecto['ciudad']?>">
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">No.Recibo</th>
										<th scope="col">Usuario que genero el recibo</th>
										<th scope="col">Pagado a</th>
										<th scope="col">Tipo de pago</th>
										<th scope="col">Banco</th>
										<th scope="col">No.Cheque</th>
										<th scope="col">Fecha pago</th>
										<th scope="col">Valor</th>
										<th scope="col">Concepto</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_recibo as $recibo) {
										$id_recibo     = $recibo['id'];
										$usuario       = $recibo['user_nom'];
										$tipo_pago_num = $recibo['tipo_pago'];
										$fecha_pago    = $recibo['fecha_pago'];
										$valor         = $recibo['valor'];
										$banco         = $recibo['banco'];
										$cheque        = $recibo['numero_cheque'];
										$concepto      = $recibo['concepto'];
										$proveedor     = $recibo['nom_proveedor'];
										$id_proveedor  = $recibo['id_proveedor'];
										$nit_proveedor = $recibo['num_identificacion'];

										if ($tipo_pago_num == 1) {
											$tipo_pago  = 'Cheque';
											$ver_banco  = 'style="display: block;"';
											$ver_cheque = 'style="display: block;"';
										}

										if ($tipo_pago_num == 2) {
											$tipo_pago  = 'Transferencia bancaria';
											$ver_banco  = 'style="display: block;"';
											$ver_cheque = 'style="display: none;"';
										}
										if ($tipo_pago_num == 3) {
											$tipo_pago  = 'Efectivo';
											$ver_banco  = 'style="display: none;"';
											$ver_cheque = 'style="display: none;"';
										}

										if ($recibo['activo'] == 1) {
											?>
											<tr class="text-center">
												<td><?=$id_recibo?></td>
												<td><?=$usuario?></td>
												<td><?=$proveedor?></td>
												<td><?=$tipo_pago?></td>
												<td><?=$banco?></td>
												<td><?=$cheque?></td>
												<td><?=$fecha_pago?></td>
												<td>$<?=number_format($valor)?></td>
												<td><?=$concepto?></td>
												<td>
													<div class="btn-group btn-group-sm" role="group">
														<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Editar recibo" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#editar<?=$id_recibo?>">
															<i class="fa fa-edit"></i>
														</button>
														<a href="<?=BASE_URL?>imprimir/reciboCaja?recibo=<?=base64_encode($id_recibo)?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Descargar recibo generado" data-placement="left" data-trigger="hover">
															<i class="fa fa-download"></i>
														</a>
													</div>
												</td>
											</tr>


											<!-- Modal -->
											<div class="modal fade" id="editar<?=$id_recibo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title font-weight-bold text-success" id="exampleModalLabel">Editar Recibo No. <?=$id_recibo?></h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_recibo" value="<?=$id_recibo?>">
																<input type="hidden" name="enlace" value="<?=$url?>">
																<input type="hidden" name="id_proyecto" value="<?=$datos_proyecto['id']?>">
																<div class="row p-2">
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Proyecto <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" disabled value="<?=$datos_proyecto['nombre']?>">
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Ciudad del proyecto <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" disabled value="<?=$datos_proyecto['ciudad']?>">
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Pagado a <span class="text-danger">*</span></label>
																		<select class="form-control" required name="proveedor_edit">
																			<option value="<?=$id_proveedor?>" selected class="d-none"><?=$proveedor . ' (' . $nit_proveedor . ')'?></option>
																			<?php
																			foreach ($datos_proveedor as $proveedor) {
																				$id_prov          = $proveedor['id_proveedor'];
																				$nombre_proveedor = $proveedor['nombre'];
																				$nit              = $proveedor['num_identificacion'];
																				$estado           = $proveedor['estado'];

																				$ver = ($estado == 0) ? 'd-none' : '';
																				?>
																				<option value="<?=$id_prov?>" class="<?=$ver?>"><?=$nombre_proveedor . ' (' . $nit . ')'?></option>
																				<?php
																			}
																			?>
																		</select>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Tipo de pago <span class="text-danger">*</span></label>
																		<select name="tipo_pago_edit" class="form-control tipo_pago" required data-id="<?=$id_recibo?>">
																			<option value="<?=$tipo_pago_num?>" selected="" class="d-none"><?=$tipo_pago?></option>
																			<option value="1">Cheque</option>
																			<option value="2">Transferencia bancaria</option>
																			<option value="3">Efectivo</option>
																		</select>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Fecha pago <span class="text-danger">*</span></label>
																		<input type="date" class="form-control" name="fecha_pago_edit" value="<?=$fecha_pago?>" required>
																	</div>
																	<div class="form-group col-lg-6">
																		<label class="font-weight-bold">Valor a pagar <span class="text-danger">*</span></label>
																		<div class="input-group">
																			<div class="input-group-append">
																				<span class="input-group-text" id="basic-addon2">$</span>
																			</div>
																			<input type="text" class="form-control numeros precio" aria-describedby="basic-addon2" name="valor_edit" value="<?=number_format($valor)?>" required>
																		</div>
																	</div>
																	<div class="form-group col-lg-6 banco_<?=$id_recibo?>" <?=$ver_banco?>>
																		<label class="font-weight-bold">Banco <span class="text-danger">*</span></label>
																		<input type="text" name="banco_edit" class="form-control" value="<?=$banco?>">
																	</div>
																	<div class="form-group col-lg-6 cheque_<?=$id_recibo?>" <?=$ver_cheque?>>
																		<label class="font-weight-bold">No. cheque <span class="text-danger">*</span></label>
																		<input type="text" name="cheque_edit" class="form-control" value="<?=$cheque?>">
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Por concepto</label>
																		<textarea cols="10" rows="5" class="form-control" name="concepto_edit"><?=$concepto?></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-2 text-right">
																		<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-success btn-sm" type="submit">
																			<i class="fa fa-save"></i>
																			&nbsp;
																			Guardar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
											<?php
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_log'])) {
		$instancia->editarReciboControl();
	}
}
?>
<script src="<?=PUBLIC_PATH?>js/recibo/funcionesRecibo.js"></script>