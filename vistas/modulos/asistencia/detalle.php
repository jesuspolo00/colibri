<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 41);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['asistencia'])) {

	$id_asistencia = base64_decode($_GET['asistencia']);

	$datos_asistencia = $instancia->mostrarDatosAsistenciaControl($id_asistencia);
	$datos_detalle    = $instancia->mostrarDetallesAsistenciaControl($id_asistencia);

	if ($datos_asistencia['finalizado'] == 0 && $datos_asistencia['visto_bueno'] == 0) {
		$span        = 'Pendiente de finalizacion';
		$bg          = 'bg-secondary';
		$ver_botones = 'd-none';
	}

	if ($datos_asistencia['finalizado'] == 1 && $datos_asistencia['visto_bueno'] == 0) {
		$span        = 'Pendiente de visto bueno';
		$bg          = 'bg-warning';
		$ver_botones = '';
	}

	if ($datos_asistencia['finalizado'] == 1 && $datos_asistencia['visto_bueno'] == 1) {
		$span        = 'Denegado';
		$bg          = 'bg-danger';
		$ver_botones = 'd-none';
	}

	if ($datos_asistencia['finalizado'] == 1 && $datos_asistencia['visto_bueno'] == 2) {
		$span        = 'Aprobada';
		$bg          = 'bg-success';
		$ver_botones = 'd-none';
	}

	if ($datos_asistencia['tipo'] == 1) {
		$bg_tipo   = 'bg-danger';
		$text_tipo = 'Distrito';
	} else {
		$bg_tipo   = 'bg-warning';
		$text_tipo = 'Codetec';
	}

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>asistencia/listado" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Asistencia - (<?=$datos_asistencia['usuario']?> - <?=mesesEspanol($datos_asistencia['mes_asistencia']);?> del <?=$datos_asistencia['anio_asistencia']?>)
						</h4>
					</div>
					<div class="card-body">
						<div class="row p-2">
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Programa</label>
								<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_programa']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Competencia</label>
								<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_competencia']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Modulo de formaci&oacute;n</label>
								<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_modulo']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Horas Programadas/Restantes</label>
								<input type="text" class="form-control numeros" value="<?=$datos_asistencia['horas_realizadas']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Docente</label>
								<input type="text" class="form-control" value="<?=$datos_asistencia['usuario']?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo</label>
								<input type="text" class="form-control text-white <?=$bg_tipo?>" value="<?=$text_tipo?>" disabled>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado</label>
								<input type="text" class="form-control text-white <?=$bg?>" value="<?=$span?>" disabled>
							</div>
							<?php
							if (!empty($datos_asistencia['horas_aprobadas_coord'])) {
								?>
								<div class="col-lg-12 form-group">
									<hr>
									<h5 class="text-center text-uppercase text-success font-weight-bold">Proceso de coordinador</h5>
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Coordinador que aprueba</label>
									<input type="text" class="form-control" disabled value="<?=$datos_asistencia['nom_coordinador']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Horas Aprobadas (Coordinador)</label>
									<input type="text" class="form-control" disabled value="<?=$datos_asistencia['horas_aprobadas_coord']?>">
								</div>
								<div class="col-lg-12 form-group">
									<label class="font-weight-bold">Justificacion (Coordinador)</label>
									<textarea class="form-control" disabled rows="5"><?=$datos_asistencia['justificacion_coor']?></textarea>
								</div>
							<?php }?>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold text-uppercase">
										<th scope="col" colspan="5" class="text-success">Detalle de asistencia</th>
									</tr>
									<tr class="text-center font-weight-bold">
										<th scope="col">Fecha</th>
										<th scope="col">Hora Entrada</th>
										<th scope="col">Hora Salida</th>
										<th scope="col">Numero de horas</th>
										<th scope="col">Actividad o tematica desarrollada</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									$sum_horas = 0;
									foreach ($datos_detalle as $detalle) {
										$id_detalle   = $detalle['id'];
										$fecha        = $detalle['fecha'];
										$hora_entrada = $detalle['hora_entrada'];
										$hora_salida  = $detalle['hora_salida'];
										$horas        = $detalle['horas'];
										$tematica     = $detalle['tematica'];

										$sum_horas += $horas;
										?>
										<tr class="text-center">
											<td><?=$fecha?></td>
											<td><?=date("g:i a", strtotime($hora_entrada))?></td>
											<td><?=date("g:i a", strtotime($hora_salida))?></td>
											<td><?=$horas?></td>
											<td><?=$tematica?></td>
										</tr>
										<?php
									}
									?>
									<tr class="text-center font-weight-bold text-uppercase bg-light">
										<td colspan="3">Total Horas</td>
										<td><?=$sum_horas?></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="row <?=$ver_botones?>">
							<div class="col-lg-12 form-group mt-2">
								<h5 class="text-center font-weight-bold text-success text-uppercase">Proceso de aprobacion</h5>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Horas a Aprobar (Coordinador) <span class="text-danger">*</span></label>
								<input type="number" class="form-control numeros" id="horas_aprobadas_coor" value="<?=$sum_horas?>" min="1" required max="<?=$sum_horas?>">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">Justificacion Coordinador</label>
								<textarea class="form-control" id="justificacion_coor" rows="5"></textarea>
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-danger btn-sm denegar" type="button" id="<?=$id_asistencia?>" data-log="<?=$id_log?>" data-value="1">
									<i class="fas fa-times"></i>
									&nbsp;
									Denegar
								</button>
								<button class="btn btn-success btn-sm aceptar" type="button" id="<?=$id_asistencia?>" data-log="<?=$id_log?>" data-value="2">
									<i class="fas fa-check-double"></i>
									&nbsp;
									Aprobar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script src="<?=PUBLIC_PATH?>js/asistencia/funcionesListadoAsistencia.js"></script>