<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_POST['buscar'])) {
	$datos_areas = $instancia->buscarAreasControl($_POST['buscar']);
} else {
	$datos_areas = $instancia->mostrarLimitesAreasControl();
}

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);

if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-success">
						<a href="<?=BASE_URL?>configuracion/panel" class="text-decoration-none">
							<i class="fa fa-arrow-left text-success"></i>
						</a>
						&nbsp;
						Areas
					</h4>
					<div class="btn-group">
						<a href="<?=BASE_URL?>reportes/areas" class="btn btn-info btn-sm">
							<i class="fas fa-search-location"></i>
							&nbsp;
							Inspeccionar Areas
						</a>
						<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#agregar_area">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Area
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar..." aria-describedby="basic-addon2" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-sm border">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Nombre</th>
									<th scope="col">Reservable</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_areas as $area) {
									$id_area    = $area['id'];
									$nombre     = $area['nombre'];
									$activo     = $area['activo'];
									$reservable = ($area['reservable'] == 1) ? '<span class="badge badge-success">Si</span>' : '<span class="badge badge-danger">No</span>';

									if ($activo == 1) {
										$icon   = '<i class="fa fa-times"></i>';
										$title  = 'Eliminar';
										$button = 'btn-danger';
										$clase  = 'inactivar_area';
									} else {
										$icon   = '<i class="fa fa-check"></i>';
										$title  = 'Activar';
										$button = 'btn-success';
										$clase  = 'activar_area';
									}

									$ver_inactivo_area = ($activo == 0 && $_SESSION['rol'] != 1) ? 'd-none' : '';

									?>
									<tr class="text-center text-dark <?=$ver_inactivo_area?>">
										<td>
											<a href="<?=BASE_URL?>areas/detalles?area=<?=base64_encode($id_area)?>"><?=$nombre?></a>
										</td>
										<td><?=$reservable?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>areas/detalles?area=<?=base64_encode($id_area)?>" class="btn btn-success btn-sm" data-tooltip="tooltip" title="Editar" data-placement="bottom">
													<i class="fa fa-edit"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/areas/codigoQR?id=<?=base64_encode($id_area)?>" class="btn btn-primary btn-sm" target="_blank" data-tooltip="tooltip" title="Descargar QR" data-placement="bottom">
													<i class="fas fa-qrcode"></i>
												</a>
												<button class="btn <?=$button?> btn-sm <?=$clase?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" id="<?=$id_area?>" data-log="<?=$id_log?>">
													<?=$icon?>
												</button>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'areas' . DS . 'agregarArea.php';

if (isset($_POST['nombre'])) {
	$instancia->agregarAreasControl();
}
?>
<script src="<?=PUBLIC_PATH?>/js/areas/funcionesAreas.js"></script>