<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$permiso = $instancia_permiso->consultarPermisosPerfilControl($id_perfil_sesion, 4);
if (!$permiso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}

if (isset($_GET['area'])) {

	$id_area = base64_decode($_GET['area']);

	$datos_areas = $instancia->mostrarDatosAreaControl($id_area);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<a href="<?=BASE_URL?>areas/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-success"></i>
							</a>
							&nbsp;
							Editar area - <?=$datos_areas['nombre']?>
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_log" value="<?=$id_log?>">
							<input type="hidden" name="id_area" value="<?=$id_area?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="nombre" required value="<?=$datos_areas['nombre']?>">
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Reservable <span class="text-danger">*</span></label>
									<select name="reserva" class="form-control" required>
										<?php
										$option_si = ($datos_areas['reservable'] == 1) ? 'selected' : '';
										$option_no = ($datos_areas['reservable'] == 0) ? 'selected' : '';
										?>
										<option value="1" <?=$option_si?>>Si</option>
										<option value="0" <?=$option_no?>>No</option>
									</select>
								</div>
								<div class="col-lg-12 form-group text-right mt-2">
									<button class="btn btn-success btn-sm" type="submit">
										<i class="fa fa-edit"></i>
										&nbsp;
										Guardar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_area'])) {
		$instancia->editarAreaControl();
	}
}
