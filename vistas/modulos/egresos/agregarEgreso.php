<!-- Modal -->
<div class="modal fade" id="agregar_ingreso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success font-weight-bold" id="exampleModalLabel">Agregar egreso</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <input type="hidden" name="id_usuario" id="id_usuario">
          <input type="hidden" name="tipo" value="2">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Numero de documento <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="documento" id="documento" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Beneficiario <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" id="nombre" readonly required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="ciudad" id="ciudad" readonly required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Por concepto de <span class="text-danger">*</span></label>
              <select name="concepto" class="form-control" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_concepto as $concepto) {
                  $id_concepto  = $concepto['id'];
                  $nom_concepto = $concepto['nombre'];
                  ?>
                  <option value="<?=$id_concepto?>"><?=$nom_concepto?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Metodo de pago <span class="text-danger">*</span></label>
              <select name="pago" class="form-control" id="tipo_pago" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_metodo as $metodo) {
                  $id_metodo  = $metodo['id'];
                  $nom_metodo = $metodo['nombre'];
                  ?>
                  <option value="<?=$id_metodo?>"><?=$nom_metodo?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group cheque">
              <label class="font-weight-bold">Banco <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="banco">
            </div>
            <div class="col-lg-6 form-group cheque">
              <label class="font-weight-bold">No. Cheque <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="cheque">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Valor <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros precio" name="valor" id="valor" required>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Observacion</label>
              <textarea class="form-control" rows="5" name="observacion"></textarea>
            </div>
            <div class="col-lg-12 form-group text-right mt-4">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-success btn-sm" type="submit">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
