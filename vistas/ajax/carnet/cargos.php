<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'carnet' . DS . 'ControlCarnet.php';

$instancia    = ControlCarnet::singleton_carnet();
$datos_cargos = $instancia->mostrarCargosCategoriaControl($_POST['id']);

echo json_encode(['array' => $datos_cargos]);
