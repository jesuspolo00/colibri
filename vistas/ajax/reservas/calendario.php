<?php
header('Content-Type: application/json');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'reservas' . DS . 'ControlReserva.php';

$instancia = ControlReserva::singleton_reserva();

$datos_reservas = $instancia->mostrarReservasTodasControl();

$fechas = [];

foreach ($datos_reservas as $reservas) {

	$horas = explode(" - ", $reservas['horas']);

	$color = ($reservas['fecha_reserva'] <= date('Y-m-d')) ? '#229954' : '#f1c40f';

	$fechas[] = array(
		'title' => $reservas['nom_area'] . ' - (' . $reservas['usuario_reserva'] . ')',
		'start' => $reservas['fecha_reserva'] . ' ' . $horas[0],
		'end'   => $reservas['fecha_reserva'] . ' ' . $horas[1],
		'color' => $color,
	);

}

echo json_encode($fechas);
