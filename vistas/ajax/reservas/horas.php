<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'reservas' . DS . 'ControlReserva.php';

$instancia   = ControlReserva::singleton_reserva();
$datos_horas = $instancia->mostrarHorasDisponiblesControl($_POST['salon'], $_POST['fecha']);

echo json_encode(['horas' => $datos_horas]);
