<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'carta' . DS . 'ControlCarta.php';

$instancia = ControlCarta::singleton_carta();
$datos     = $instancia->eliminarCartaControl($_POST['id']);

echo json_encode(['mensaje' => $datos]);
