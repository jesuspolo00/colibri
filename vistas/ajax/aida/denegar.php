<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'aprobacion' . DS . 'ControlAprobacion.php';

$instancia = ControlAprobacion::singleton_aprobacion();

$token       = $_POST['id'];
$token_usado = $instancia->verificarAidaBarriosTokenUsoControl($token);

if ($token_usado['token_usado'] == 0) {
    $aprobar_permiso = $instancia->denegarAidaBarriosSolicitudPagoControl($token_usado['id'], $_POST['log']);

    if ($aprobar_permiso == true) {
        echo json_encode(['mensaje' => true]);
    }
}
