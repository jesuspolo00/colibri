<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'hoja' . DS . 'ControlHoja.php';

$instancia = ControlHoja::singleton_hoja();
$dato      = $instancia->eliminarAcademicoControl($_POST['id']);

echo json_encode($dato);