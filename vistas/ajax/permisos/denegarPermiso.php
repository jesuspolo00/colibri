<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';

$instancia        = ControlPermisos::singleton_permisos();
$instancia_perfil = ControlPerfil::singleton_perfil();

$comprobar = $instancia->verificarTokenUsoControl($_POST['id']);

if ($comprobar['token_usado'] == 0) {

	$aprobar = $instancia->denegarPermisoControl($comprobar['id']);

	if ($aprobar == true) {

		$datos_usuario = $instancia_perfil->mostrarDatosPerfilControl($comprobar['id_user']);

		$mensaje_correo = '<html><body>
		<p style="font-size: 1.2em;">
		Tú solicitud de permisos laboral ha sido <b>Denegada</b> para la fecha
		<b>
		' . $comprobar['fecha'] . '
		</b>
		<ul style="font-size: 1.2em;">
		<li><b>Documento:</b> ' . $datos_usuario['documento'] . '</li>
		<li><b>Nombre Completo:</b> ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</li>
		<li><b>Hora Inicio:</b> ' . $comprobar['hora_inicio'] . '</li>
		<li><b>Hora Fin:</b> ' . $comprobar['hora_fin'] . '</li>
		<li><b>Motivo:</b> ' . $comprobar['motivo'] . '</li>
		<li><b>Horario:</b> ' . $comprobar['horario'] . '</li>
		<li><b>Estado de la solicitud:</b> Denegada</li>
		</ul>
		</body>
		</html>
		';

		$datos_correo = array(
			'asunto'  => 'Solictud de permiso laboral',
			'mensaje' => $mensaje_correo,
			'correo'  => array($datos_usuario['correo']),
			'archivo' => array($comprobar['documento_soporte']),
		);

		$envio = Correo::enviarCorreoModel($datos_correo);

		if($envio == true){
			echo 'ok';
		}else{
			echo 'no';
		}

	}

}
