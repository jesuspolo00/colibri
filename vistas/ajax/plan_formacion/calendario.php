<?php
header('Content-Type: application/json');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'asistencia' . DS . 'ControlAsistencia.php';

$instancia = ControlAsistencia::singleton_asistencia();

$datos_plan = $instancia->mostrarPlanesFormacionControl();

$fechas = [];

foreach ($datos_plan as $plan) {

    $color = ($plan['activo'] == 0) ? '#229954' : '#f1c40f';
    $color = ($plan['activo'] == 2) ? '#e74c3c' : $color;

    $fechas[] = array(
        'title' => $plan['nom_tipo'] . ' - ' . $plan['nom_usuario'],
        'start' => $plan['fecha_proyectada'] . ' ' . $plan['hora_proyectada'],
        'end'   => $plan['fecha_proyectada'],
        'color' => $color,
    );
}

echo json_encode($fechas);
