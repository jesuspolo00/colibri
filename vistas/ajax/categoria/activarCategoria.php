<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlCategorias.php';

$instancia = ControlCategorias::singleton_categoria();
$datos     = $instancia->activarCategoriaControl();

$mensaje = ($datos == true) ? 'ok' : 'vacio';

echo json_encode(['mensaje' => $mensaje]);
