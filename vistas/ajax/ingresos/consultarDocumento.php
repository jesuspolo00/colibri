<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'ingresos' . DS . 'ControlIngresos.php';

$instancia       = ControlIngresos::singleton_ingresos();
$datos_documento = $instancia->consultarDocumentoControl($_POST['documento']);

echo json_encode($datos_documento);
