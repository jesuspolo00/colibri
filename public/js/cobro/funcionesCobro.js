$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*--------------------------------------------------------------------*/
    $(".accion").on(tipoEvento, function() {
        var estado = $(this).attr('id');
        var id_cobro = $(this).attr('data-id');
        var id_log = $(this).attr('data-log');
        var horas_coord = $("#horas_coord").val();
        var just_coord = $("#just_coord").val();
        accionEstadoCuentaCobro(estado, id_cobro, id_log, horas_coord, just_coord);
    });
    /*--------------------------------------------------------------------*/
    $(".accion_cont").on(tipoEvento, function() {
        var estado = $(this).attr('id');
        var id_cobro = $(this).attr('data-id');
        var id_log = $(this).attr('data-log');
        var horas_cont = $("#horas_cont").val();
        var just_cont = $("#just_cont").val();
        var horas_pagar = $("#horas_pagar").val();
        var hora_valor = $("#hora_valor").val();
        /*---------------*/
        if (hora_valor == '' || hora_valor == 0 || horas_pagar == '' || horas_pagar == 0 || horas_cont == '' || horas_cont == 0) {
            ohSnap("Campos obligatorios vacios!", {
                color: "red",
                "duration": "1000"
            });
        } else {
            accionEstadoCuentaCobroCont(estado, id_cobro, id_log, horas_cont, just_cont, hora_valor, horas_pagar);
        }
    });
    /*---------------------*/
    function accionEstadoCuentaCobro(estado, id_cobro, id_log, horas_coord, just_coord) {
        try {
            $.ajax({
                url: '../vistas/ajax/cobro/estadoCuentaCobro.php',
                type: 'POST',
                data: {
                    'estado': estado,
                    'id_cobro': id_cobro,
                    'id_log': id_log,
                    'horas_coord': horas_coord,
                    'just_coord': just_coord,
                },
                cache: false,
                success: function(resultado) {
                    if (estado == 2) {
                        if (resultado == 'ok') {
                            swal({
                                title: "Cuenta de cobro aprobada",
                                text: " Se ha aprobado la cuenta de cobro",
                                icon: "success",
                                type: "success",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                    }
                    if (estado == 3) {
                        if (resultado == 'ok') {
                            swal({
                                title: "Cuenta de cobro denegada",
                                text: " Se ha denegado la cuenta de cobro",
                                icon: "error",
                                type: "danger",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------*/
    function accionEstadoCuentaCobroCont(estado, id_cobro, id_log, horas_cont, just_cont, horas_valor, horas_pagar) {
        try {
            $.ajax({
                url: '../vistas/ajax/cobro/estadoCuentaCont.php',
                type: 'POST',
                data: {
                    'estado': estado,
                    'id_cobro': id_cobro,
                    'id_log': id_log,
                    'horas_cont': horas_cont,
                    'just_cont': just_cont,
                    'horas_valor': horas_valor,
                    'horas_pagar': horas_pagar,
                },
                cache: false,
                success: function(resultado) {
                    if (estado == 1) {
                        if (resultado == 'ok') {
                            swal({
                                title: "Cuenta de cobro aprobada",
                                text: " Se ha aprobado la cuenta de cobro",
                                icon: "success",
                                type: "success",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                    }
                    if (estado == 2) {
                        if (resultado == 'ok') {
                            swal({
                                title: "Cuenta de cobro denegada",
                                text: " Se ha denegado la cuenta de cobro",
                                icon: "error",
                                type: "danger",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("listado");
    }
});