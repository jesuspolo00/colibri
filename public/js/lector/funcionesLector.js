$(document).ready(function() {
    var _scannerIsRunning = false;

    function startScanner() {
        Quagga.init({
            inputStream: {
                name: "Live",
                type: "LiveStream",
                target: document.querySelector('#scanner-container'),
                constraints: {
                    width: 480,
                    height: 320,
                    facingMode: "environment"
                },
            },
            area: { // defines rectangle of the detection/localization area
                top: "0%", // top offset
                right: "0%", // right offset
                left: "0%", // left offset
                bottom: "0%" // bottom offset
            },
            singleChannel: false,
            decoder: {
                readers: ["code_128_reader", "ean_reader", "ean_8_reader", "code_39_reader", "code_39_vin_reader", "codabar_reader", "upc_reader", "upc_e_reader", "i2of5_reader"],
                debug: {
                    showCanvas: true,
                    showPatches: true,
                    showFoundPatches: true,
                    showSkeleton: true,
                    showLabels: true,
                    showPatchLabels: true,
                    showRemainingPatchLabels: true,
                    boxFromPatches: {
                        showTransformed: true,
                        showTransformedBox: true,
                        showBB: true
                    }
                }
            },
        }, function(err) {
            if (err) {
                console.log(err);
                return
            }
            Quagga.start();
            // Establecer bandera en se está ejecutando
            _scannerIsRunning = true;
        });
        Quagga.onProcessed(function(result) {
            var drawingCtx = Quagga.canvas.ctx.overlay,
            drawingCanvas = Quagga.canvas.dom.overlay;
            if (result) {
                if (result.boxes) {
                    drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                    result.boxes.filter(function(box) {
                        return box !== result.box;
                    }).forEach(function(box) {
                        Quagga.ImageDebug.drawPath(box, {
                            x: 0,
                            y: 1
                        }, drawingCtx, {
                            color: "green",
                            lineWidth: 2
                        });
                    });
                }
                if (result.box) {
                    Quagga.ImageDebug.drawPath(result.box, {
                        x: 0,
                        y: 1
                    }, drawingCtx, {
                        color: "#00F",
                        lineWidth: 2
                    });
                }
                if (result.codeResult && result.codeResult.code) {
                    Quagga.ImageDebug.drawPath(result.line, {
                        x: 'x',
                        y: 'y'
                    }, drawingCtx, {
                        color: 'red',
                        lineWidth: 3
                    });
                }
            }
        });
        Quagga.onDetected(function(result) {
            Quagga.stop();
            var codigo = result.codeResult.code;
            asistencia(codigo);
        });
    }
    // Iniciar / detener el escáner
    document.getElementById("btn").addEventListener("click", function() {
        startScanner();
    }, false);
    
    startScanner();

    function asistencia(codigo) {
        try {
            $.ajax({
                url: '../vistas/ajax/lector/codigo.php',
                type: 'POST',
                data: {
                    'codigo': codigo,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje != 'error') {
                        window.location.replace(resultado.mensaje);
                    } else {
                        swal("Codigo no existe!", "El codigo escaneado no existe en el sistema", "error");
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});