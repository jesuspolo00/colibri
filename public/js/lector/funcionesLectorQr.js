$(document).ready(function() {
    // Variable de control para detener el escaneo después de la redirección
    let escaneoActivo = true;

    function startScanner() {
        // Obtén el elemento de video
        const videoElement = document.getElementById('video');
        const qrCodeDiv = document.getElementById('qrcode');
        // Accede a la cámara del usuario
        navigator.mediaDevices.getUserMedia({
            video: {
                facingMode: 'environment'
            }
        }).then(function(stream) {
            videoElement.srcObject = stream;
            // Función para realizar la detección de códigos QR
            const scanQRCode = () => {
                if (!escaneoActivo) {
                    // Detener el escaneo después de la redirección
                    return;
                }
                const canvas = document.createElement('canvas');
                const context = canvas.getContext('2d');
                canvas.width = videoElement.videoWidth;
                canvas.height = videoElement.videoHeight;
                context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
                const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
                const code = jsQR(imageData.data, imageData.width, imageData.height);
                if (code) {
                    //qrCodeDiv.innerHTML = `Contenido del código QR: ${code.data}`;
                    window.location.replace(code.data);
                    // Detener el escaneo después de la redirección
                    escaneoActivo = false;
                    return;
                }
                requestAnimationFrame(scanQRCode);
            };
            videoElement.onloadedmetadata = function() {
                requestAnimationFrame(scanQRCode);
            };
        }).catch(function(err) {
            console.error('Error al acceder a la cámara: ', err);
        });
    }
    // Iniciar / detener el escáner
    document.getElementById("btn").addEventListener("click", function() {
        startScanner();
    }, false);
    // Iniciar el escaneo al cargar el documento
    startScanner();
    /*--------------------------------*/
});