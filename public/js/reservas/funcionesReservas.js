$(document).ready(function() {
    /*----------------------*/
    var d = new Date();
    var mes = ((d.getMonth() + 1) > 1 && (d.getMonth() + 1) < 10) ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
    var dia = (d.getDate() > 1 && d.getDate() < 10) ? '0' + d.getDate() : d.getDate();
    var strDate = d.getFullYear() + "-" + mes + "-" + dia;
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------*/
    $("#salon").change(function() {
        var salon = $(this).val();
        var fecha = $("#fecha").val();
        /*---------------------*/
        if (salon == "" && fecha == "") {
            ohSnap("Campos Obligatorios vacios!", {
                color: "red",
                "duration": "1000"
            });
        } else {
            mostrarHorasLibres(salon, fecha)
        }
    });
    /*---------------------*/
    $("#fecha").change(function() {
        var fecha = $(this).val();
        var salon = $("#salon").val();
        /*---------------------*/
        if (salon == "" && fecha == "") {
            ohSnap("Campos Obligatorios vacios!", {
                color: "red",
                "duration": "1000"
            });
        } else {
            if (fecha == strDate) {
                ohSnap("No puedes realizar reservas para el mismo dia!", {
                    color: "red",
                    "duration": "1000"
                });
                $(this).val('');
            } else if (fecha < strDate) {
                ohSnap("No puedes realizar reservas para dias anteriores!", {
                    color: "red",
                    "duration": "1000"
                });
                $(this).val('');
            } else {
                mostrarHorasLibres(salon, fecha)
            }
        }
    });
    /*---------------------*/
    function mostrarHorasLibres(salon, fecha) {
        $("#horas").html('');
        try {
            $.ajax({
                url: '../vistas/ajax/reservas/horas.php',
                type: 'POST',
                data: {
                    'salon': salon,
                    'fecha': fecha,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado) {
                        var array = resultado.horas;
                        array.forEach(function(datos) {
                            $("#horas").append('<div class="custom-control custom-switch ml-2"><input type="checkbox"' + 'class="custom-control-input"' + 'name = "hora[]"' + 'value ="' + datos.id + '"' + 'id="' + datos.id + '"><label class="custom-control-label"' + 'for="' + datos.id + '">' + datos.horas + '</label></div>');
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});