$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*--------------------------------------------------------------------*/
    $(".inactivar_categoria").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarCategoria(id);
    });

    function inactivarCategoria(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/categoria/inactivarCategoria.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Categoria eliminado!", {
                            color: "red",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de eliminacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*--------------------------------------------------------------------*/
    /*--------------------------------------------------------------------*/
    $(".activar_categoria").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarCategoria(id);
    });

    function activarCategoria(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/categoria/activarCategoria.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Categoria Activada!", {
                            color: "green",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de eliminacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*--------------------------------------------------------------------*/
    function recargarPagina() {
        window.location.replace("index");
    }
});