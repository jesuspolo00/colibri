$(document).ready(function() {
    /*----------------------*/
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------*/
    $('.agregar_producto').on(tipoEvento, function() {
        var rand = Math.floor((Math.random() * 9999999) + 1);
        $(".buscar").append('<tr id="fila' + rand + '"><td><input class="form-control" name="producto[]" type="text" required></td>' + '<td><input class="form-control numeros text-center" name="cantidad[]" type="text" required></td><td class="text-center"><button class="btn btn-danger btn-sm remover_input" type="button" id="' + rand + '"><i class="fa fa-minus"></i></button></td></tr>');
    });
    /*---------------------*/
    $(".buscar").on("click", ".remover_input", function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*-------------------*/
    $(".remover_input").on(tipoEvento, function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*-------------------*/
    $(".eliminar_carta").on(tipoEvento, function() {
        var id = $(this).attr('id');
        eliminarCarta(id);
    });
    /*-------------------*/
    function eliminarCarta(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/carta/eliminar.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Carta Eliminada!", {
                            color: "yellow",
                            duration: "1000"
                        });
                        $(".fila_" + id).fadeOut();
                    } else {
                        ohSnap("Error de eliminacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("listado");
    }
});