$(document).ready(function() {
    /*---------------------*/
    $(".enviar_academico").click(function(e) {
        e.preventDefault();
        var form = $("#form_academico").serialize();
        agregarInformacionAcademica(form);
    });
    $(".info_academica").on("click", ".eliminar_di", function() {
        var id = $(this).attr('id');
        eliminarAcademico(id);
    });
    $(".eliminar").click(function() {
        var id = $(this).attr('id');
        eliminarAcademico(id);
    });
    /*---------------------*/
    $(".enviar_laboral").click(function(e) {
        e.preventDefault();
        var form = $("#form_laboral").serialize();
        agregarInformacionLaboral(form);
    });
    $(".info_laboral").on("click", ".eliminar_di_laboral", function() {
        var id = $(this).attr('id');
        eliminarLaboral(id);
    });
    $(".eliminar_laboral").click(function() {
        var id = $(this).attr('id');
        eliminarLaboral(id);
    });
    /*---------------------*/
    function agregarInformacionAcademica(form) {
        try {
            $.ajax({
                url: '../vistas/ajax/hoja/agregarInformacionAcademica.php',
                type: 'POST',
                data: form,
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == true) {
                        $(".info_academica").append('<tr class="text-center academica_' + resultado.id + '">' + '<td>' + resultado.datos.institucion + '</td>' + '<td>' + resultado.datos.ciudad + '</td>' + '<td>' + resultado.datos.pais + '</td>' + '<td>' + resultado.datos.nivel + '</td>' + '<td><span class="font-weight-bold">Graduado</span><br>' + resultado.datos.graduado + '</td>' + '<td><span class="font-weight-bold">FechaGraduaci&oacute;n</span><br>' + resultado.datos.fecha_grado + '</td>' + '<td><span class="font-weight-bold">T&iacute;tuloAlcanzado</span><br>' + resultado.datos.titulo + '</td>' + '<td><button class="btn btn-danger btn-sm eliminar_di" type="button" id="' + resultado.id + '"><i class="fas fa-times"></i>&nbsp; Eliminar</button></td>' + '</tr>');
                        ohSnap("Agregado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                        $('#form_academico').trigger("reset");
                    }
                    if (resultado.mensaje == false) {
                        ohSnap("Error al agregar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------*/
    function agregarInformacionLaboral(form) {
        try {
            $.ajax({
                url: '../vistas/ajax/hoja/agregarInformacionLaboral.php',
                type: 'POST',
                data: form,
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == true) {
                        $(".info_laboral").append('<tr class="text-center laboral_' + resultado.id + '">' 
                            + '<td>' + resultado.datos.empresa + '</td>' 
                            + '<td>' + resultado.datos.ciudad + '</td>' 
                            + '<td>' + resultado.datos.pais + '</td>' 
                            + '<td>' + resultado.datos.cargo + '</td>' 
                            + '<td>' + resultado.datos.telefono + '</td>' 
                            + '<td>' + resultado.datos.direccion + '</td>' 
                            + '<td>' + resultado.datos.fecha_ingreso + '</td>' 
                            + '<td>' + resultado.datos.fecha_salida + '</td>' 
                            + '<td><button class="btn btn-danger btn-sm eliminar_di_laboral" type="button" id="' + resultado.id + '"><i class="fas fa-times"></i>&nbsp; Eliminar</button></td>' + '</tr>');
                        ohSnap("Agregado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                        $('#form_laboral').trigger("reset");
                    }
                    if (resultado.mensaje == false) {
                        ohSnap("Error al agregar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    function eliminarAcademico(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/hoja/eliminarAcademico.php',
                type: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == true) {
                        ohSnap("Eliminado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                        $(".academica_" + id).hide();
                    }
                    if (resultado == false) {
                        ohSnap("Error al eliminar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    function eliminarLaboral(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/hoja/eliminarLaboral.php',
                type: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == true) {
                        ohSnap("Eliminado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                        $(".laboral_" + id).hide();
                    }
                    if (resultado == false) {
                        ohSnap("Error al eliminar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});