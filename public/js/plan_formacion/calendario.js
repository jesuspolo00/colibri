document.addEventListener('DOMContentLoaded', function() {
    /*------------*/
    var d = new Date();
    var dias = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate();
    var mes = ((d.getMonth() + 1) < 10) ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
    var minutos = (d.getMinutes() < 10) ? '0' + d.getMinutes() : d.getMinutes();
    var horas = (d.getHours() < 10) ? '0' + d.getHours() : d.getHours();
    var fecha_actual = d.getFullYear() + "-" + mes + "-" + dias;
    /*---------------------------*/
    /*---------------------------*/
    var calendarEl = document.getElementById('calendarPlan');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        headerToolbar: {
            right: 'prev,next today',
            center: 'title',
            left: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        height: 'auto',
        navLinks: false,
        editable: false,
        selectable: false,
        selectMirror: false,
        nowIndicator: false,
        events: {
            url: '../vistas/ajax/plan_formacion/calendario.php',
        },
        locale: 'es' // Utiliza locale directamente en la configuración
    });
    calendar.render();
});