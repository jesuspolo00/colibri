$(document).ready(function() {
	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*-------------------------------*/
	$(".arte_realizado").on(tipoEvento, function(e) {
		var id = $(this).attr('id');
		var estado = 1;
		planArte(id, estado);
	});
    /*-------------------------------*/
	$(".arte_no_realizado").on(tipoEvento, function(e) {
		var id = $(this).attr('id');
		var estado = 2;
		planArte(id, estado);
	});
    /*-------------------------------*/
	function planArte(id, estado) {
		try {
			$.ajax({
				url: '../vistas/ajax/plan_formacion/arte.php',
				type: 'POST',
				data: {
					'estado': estado,
					'id': id
				},
				cache: false,
				dataType: 'json',
				success: function(resultado) {
					if (resultado.mensaje = 'ok') {
						if (estado == 1) {
							$(".check_" + id).addClass('d-none');
							$(".times_" + id).removeClass('d-none');
                            /*----------------------*/
							$(".span_arte_" + id).html('<span class="badge badge-success">Arte realizado</span>');
						} else {
							$(".check_" + id).removeClass('d-none');
							$(".times_" + id).addClass('d-none');
                            /*----------------------*/
							$(".span_arte_" + id).html('<span class="badge badge-danger">Arte no realizado</span>');
						}
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
});