$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------*/
    $(".aprobar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        aprobarPermiso(id);
        $(".loader").fadeIn("slow");
    });
    /*---------------------*/
    $(".denegar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        denegarPermiso(id);
        $(".loader").fadeIn("slow");
    });
    /*----------------------------*/
    function aprobarPermiso(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/aprobarPermiso.php',
                type: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Aprobado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                        $('.fila_' + id).html('<span class="badge badge-success">Aprobada</span>');
                        $(".loader").fadeOut("slow");
                    } else {
                        ohSnap("Enlace ya vencido!", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".loader").fadeOut("slow");
                        setTimeout(recargarPagina, 1050);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*----------------------------*/
    function denegarPermiso(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/denegarPermiso.php',
                type: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Denegado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                        $('.fila_' + id).html('<span class="badge badge-danger">Denegada</span>');
                        $(".loader").fadeOut("slow");
                    } else {
                        ohSnap("Enlace ya vencido!", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".loader").fadeOut("slow");
                        setTimeout(recargarPagina, 1050);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        location.reload();
    }
});