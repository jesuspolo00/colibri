$(document).ready(function() {
    /*----------------------*/
    /*---------------------*/
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /**-----------------------------------------**/
    $('.agregar_producto').on(tipoEvento, function() {
        var rand = Math.floor((Math.random() * 9999999) + 1);
        $(".buscar").append('<tr id="fila' + rand + '">' + '<td><input type="date" class="form-control" name="fecha[]" required></td>' + '<td><input type="time" class="form-control hora_entrada" name="hora_entrada[]" id="hora_entrada_' + rand + '" data-id="' + rand + '" required></td>' + '<td><input type="time" class="form-control hora_salida" name="hora_salida[]" id="hora_salida_' + rand + '" data-id="' + rand + '" required></td>' + '<td><input type="text" class="form-control numeros text-center hora_diferencia" readonly id="hora_diferencia_' + rand + '" data-id="' + rand + '" name="horas[]" required maxlength="2" minlength="1" value="0"></td>' + '<td><input type="text" class="form-control" name="tematica[]" required></td>' + '<td class="text-center"><button class="btn btn-danger btn-sm remover_input" type="button" id="' + rand + '"><i class="fa fa-minus"></i></button></td>' + '</tr>');
    });
    /*----------------------------*/
    $(".buscar").on("click", ".remover_input", function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*----------------------------*/
    $(".remover_input").on(tipoEvento, function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*----------------------------*/
    $(".finalizar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        finalizarAsistencia(id);
    });
    /*----------------------------*/
    $('#programa').change(function() {
        $('#modulos').html('');
        $('#horas').val('');
        var id_log = $("#log").val();
        var tipo = $("#tipo").val();
        var id = $(this).val();
        competenciasProgramaAsignada(id, id_log, tipo);
    });
    /*----------------------------*/
    $('#competencias').change(function() {
        $('#modulos').html('');
        $('#horas').val('');
        var id_log = $("#log").val();
        var tipo = $("#tipo").val();
        var id = $(this).val();
        modulosCompetenciasAsignada(id, id_log, tipo);
    });
    /*----------------------------*/
    $('#modulos').change(function() {
        $('#horas').val('');
        var id_log = $("#log").val();
        var id = $(this).val();
        var tipo = $("#tipo").val();
        horasModulo(id, id_log, tipo);
    });
    /*----------------------------*/
    $('.inactivar_asig').on(tipoEvento, function() {
        var id = $(this).attr('data-id');
        var estado = $(this).attr('data-estado');
        estadoModulo(estado, id);
    });
    /*----------------------------*/
    $('.activar_asig').on(tipoEvento, function() {
        var id = $(this).attr('data-id');
        var estado = $(this).attr('data-estado');
        estadoModulo(estado, id);
    });
    /*-------------------------*/
    $('.hora_entrada').each(function() {
        var id = $(this).attr('data-id');
        var hora_entrada = $("#hora_entrada" + id).val();
        var hora_salida = $("#hora_salida_" + id).val();
        var hora_diferencia = calculardiferencia(hora_entrada, hora_salida);
        $("#hora_diferencia_" + id).val(hora_diferencia);
    });
    $('.hora_salida').each(function() {
        var id = $(this).attr('data-id');
        var hora_entrada = $("#hora_entrada_" + id).val();
        var hora_salida = $("#hora_salida_" + id).val();
        var hora_diferencia = calculardiferencia(hora_entrada, hora_salida);
        $("#hora_diferencia_" + id).val(hora_diferencia);
    });
    /*----------------------------*/
    $(".buscar").on("change", ".hora_entrada", function() {
        var id = $(this).attr('data-id');
        var hora_entrada = $(this).val();
        var hora_salida = $("#hora_salida_" + id).val();
        var hora_diferencia = calculardiferencia(hora_entrada, hora_salida);
        $("#hora_diferencia_" + id).val(hora_diferencia);
    });
    $(".buscar").on("change", ".hora_salida", function() {
        var id = $(this).attr('data-id');
        var hora_entrada = $("#hora_entrada_" + id).val();
        var hora_salida = $(this).val();
        var hora_diferencia = calculardiferencia(hora_entrada, hora_salida);
        $("#hora_diferencia_" + id).val(hora_diferencia);
    });
    /*-------------------------*/
    function finalizarAsistencia(id) {
        try {
            $.ajax({
                url: '../../vistas/ajax/asistencia/finalizar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        swal({
                            title: "Asistencia Finalizada",
                            text: " Se ha finalizado la asistencia",
                            icon: "success",
                            type: "success",
                            button: false,
                            closeOnClickOutside: false,
                            timer: 2200,
                        }).then((result) => {
                            if (!result) {
                                setTimeout(recargarPagina, 100);
                            }
                        });
                    } else {
                        swal({
                            title: "Error de actualizacion",
                            text: " Lo sentimos ha ocurrido un error, se solucionara lo antes posible.",
                            icon: "error",
                            type: "danger",
                            button: false,
                            closeOnClickOutside: false,
                            timer: 2200,
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------*/
    function competenciasProgramaAsignada(id, id_log, tipo) {
        try {
            $.ajax({
                url: '../../vistas/ajax/asistencia/cargarCompetencias.php',
                method: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log,
                    'tipo': tipo,
                },
                cache: false,
                dataType: "json",
                success: function(resultado) {
                    $('#competencias').append($('<option />', {
                        text: 'Seleccione una opcion...',
                        value: '',
                    }));
                    $.each(resultado, function(key, valor) {
                        $('#competencias').append($('<option />', {
                            text: valor.nom_competencia,
                            value: valor.id_competencia,
                        }));
                    });
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------*/
    function modulosCompetenciasAsignada(id, id_log, tipo) {
        try {
            $.ajax({
                url: '../../vistas/ajax/asistencia/cargarModulos.php',
                method: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log,
                    'tipo': tipo,
                },
                cache: false,
                dataType: "json",
                success: function(resultado) {
                    $('#modulos').append($('<option />', {
                        text: 'Seleccione una opcion...',
                        value: '',
                    }));
                    $.each(resultado, function(key, valor) {
                        $('#modulos').append($('<option />', {
                            text: valor.nom_modulo,
                            value: valor.id_modulo,
                        }));
                    });
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------*/
    function horasModulo(id, id_log, tipo) {
        try {
            $.ajax({
                url: '../../vistas/ajax/asistencia/cargarHoras.php',
                method: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log,
                    'tipo': tipo
                },
                cache: false,
                dataType: "json",
                success: function(resultado) {
                    $("#horas").val(resultado.horas_restantes);
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------*/
    function estadoModulo(estado, id) {
        try {
            $.ajax({
                url: '../../../vistas/ajax/asistencia/eliminarModulo.php',
                method: 'POST',
                data: {
                    'id': id,
                    'estado': estado
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Estado actualizado correctamente!", {
                            color: "green",
                            duration: "1000"
                        });
                        if (estado == 0) {
                            $("#inact_" + id).addClass('d-none');
                            $("#act_" + id).removeClass('d-none');
                            $("#fila_span_" + id).html('<span class="badge badge-danger">Inactivo</span>');
                        }
                        if (estado == 1) {
                            $("#inact_" + id).removeClass('d-none');
                            $("#act_" + id).addClass('d-none');
                            $("#fila_span_" + id).html('<span class="badge badge-success">Activo</span>');
                        }
                    } else {
                        ohSnap("Ha ocurrido un error!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*----------------------------*/
    function calculardiferencia(hora_desde, hora_hasta) {
        var hora_inicio = hora_desde;
        var hora_fin = hora_hasta;
        // Verificar que los valores sean válidos
        if (!hora_inicio || !hora_fin) {
            return '0';
        }
        // Convertir las horas a minutos
        var inicio = hora_inicio.split(':');
        var fin = hora_fin.split(':');
        var minutosInicio = parseInt(inicio[0]) * 60 + parseInt(inicio[1]);
        var minutosFin = parseInt(fin[0]) * 60 + parseInt(fin[1]);
        // Calcular la diferencia en minutos
        var diferenciaMinutos;
        if (minutosInicio <= minutosFin) {
            diferenciaMinutos = minutosFin - minutosInicio;
        } else {
            // Si la hora de inicio es mayor que la hora de finalización, considerar que es el día siguiente
            diferenciaMinutos = (24 * 60 - minutosInicio) + minutosFin;
        }
        // Convertir la diferencia de minutos a horas y minutos
        var horas = Math.floor(diferenciaMinutos / 60);
        var minutos = diferenciaMinutos % 60;
        // Devolver la cantidad de horas pasadas
        return horas /*+ ':' + minutos*/ ;
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});