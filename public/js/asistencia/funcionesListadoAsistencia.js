$(document).ready(function() {
    /*----------------------*/
    /*---------------------*/
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*----------------------------*/
    $(".denegar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        var value = $(this).attr('data-value');
        var horas_coord = $("#horas_aprobadas_coor").val();
        var just_coor = $("#justificacion_coor").val();
        validarAsistencia(id, log, value, horas_coord, just_coor);
    });
    /*----------------------------*/
    $(".aceptar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        var value = $(this).attr('data-value');
        var horas_coord = $("#horas_aprobadas_coor").val();
        var just_coor = $("#justificacion_coor").val();
        validarAsistencia(id, log, value, horas_coord, just_coor);
    });
    /*-------------------------*/
    function validarAsistencia(id, log, value, horas_coord, just_coor) {
        try {
            $.ajax({
                url: '../vistas/ajax/asistencia/validarAsistencia.php',
                method: 'POST',
                data: {
                    'id': id,
                    'log': log,
                    'value': value,
                    'horas': horas_coord,
                    'just': just_coor
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        if (value == 2) {
                            swal({
                                title: "Asistencia aprobada",
                                text: " Se ha aprobado la asistencia",
                                icon: "success",
                                type: "success",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                        if (value == 1) {
                            swal({
                                title: "Asistencia rechazada",
                                text: " Se ha rechazado la asistencia",
                                icon: "error",
                                type: "danger",
                                button: false,
                                closeOnClickOutside: false,
                                timer: 2200,
                            }).then((result) => {
                                if (!result) {
                                    setTimeout(recargarPagina, 100);
                                }
                            });
                        }
                    } else {
                        swal({
                            title: "Error de validacion",
                            text: " Lo sentimos ha ocurrido un error, se solucionara lo antes posible.",
                            icon: "error",
                            type: "danger",
                            button: false,
                            closeOnClickOutside: false,
                            timer: 2200,
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("listado");
    }
});