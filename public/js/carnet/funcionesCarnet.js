$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*--------------------------*/
    $("#documento").keyup(function() {
        var doc = $(this).val();
        if (doc == '') {} else {
            consultarDatos(doc);
        }
    });
    /*--------------------------*/
    $(".categoria").change(function() {
        var id = $(this).val();
        if (id != '') {
            mostrarCargos(id);
        } else {
            $('.curso').html('');
        }
    });
    /*--------------------------*/
    function mostrarCargos(id) {
        $('.curso').html('');
        try {
            $.ajax({
                url: '../vistas/ajax/carnet/cargos.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    $('.curso').append('<option value="" selected>Seleccione una categoria...</option>');
                    var array = resultado.array;
                    array.forEach(function(datos) {
                        $('.curso').append('<option value="' + datos.id + '">' + datos.nombre + '</option>');
                    });
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*--------------------------*/
    function consultarDatos(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/carnet/datos.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado != false) {
                        $("#enviar_personal").attr('disabled', true);
                        $("#nom_prim").val(resultado.nom_prim);
                        $("#apellido_prim").val(resultado.apellido_prim);
                        $("#apellido_seg").val(resultado.apellido_seg);
                    } else {
                        $("#enviar_personal").attr('disabled', false);
                        $("#nom_prim").val('');
                        $("#apellido_prim").val('');
                        $("#apellido_seg").val('');
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});