$(document).ready(function() {
    /*----------------------------*/
	$('#programa').change(function() {
		$('.horas').val('');
		$('#competencias').html('');
		var id = $(this).val();
		competenciasPrograma(id);
	});
    /*----------------------------*/
	$('#competencias').change(function() {
		$('.horas').val('');
		$('#modulos').html('');
		var id = $(this).val();
		modulosCompetencia(id);
	});
    /*----------------------------*/
	$('#modulos').change(function() {
		$('.horas').val('');
		var id = $(this).val();
		infoModulos(id);
	});
    /*----------------------------*/
	$('#horas_teoricas_asig, #horas_practicas_asig').keyup(function() {
		var horas_teoricas_asig = $('#horas_teoricas_asig').val();
		var horas_practicas_asig = $('#horas_practicas_asig').val();
		var horas_teoricas = $('#horas_teoricas').val();
		var horas_practicas = $('#horas_practicas').val();
		sumaHoras(horas_teoricas, horas_practicas, horas_teoricas_asig, horas_practicas_asig);
	});
    /*-------------------------*/
	function competenciasPrograma(id) {
		try {
			$.ajax({
				url: '../../vistas/ajax/programas/cargarCompetencias.php',
				method: 'POST',
				data: {
					'id': id
				},
				cache: false,
				dataType: "json",
				success: function(resultado) {
					$('#competencias').append($('<option />', {
						text: 'Seleccione una competencia...',
						value: '',
					}));
					$.each(resultado, function(key, valor) {
						$('#competencias').append($('<option />', {
							text: valor.nombre,
							value: valor.id,
						}));
					});
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
    /*-------------------------*/
	function modulosCompetencia(id) {
		try {
			$.ajax({
				url: '../../vistas/ajax/programas/cargarModulos.php',
				method: 'POST',
				data: {
					'id': id
				},
				cache: false,
				dataType: "json",
				success: function(resultado) {
					$('#modulos').append($('<option />', {
						text: 'Seleccione un modulo...',
						value: '',
					}));
					$.each(resultado, function(key, valor) {
						$('#modulos').append($('<option />', {
							text: valor.nombre,
							value: valor.id,
						}));
					});
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
    /*-------------------------*/
	function infoModulos(id) {
		try {
			$.ajax({
				url: '../../vistas/ajax/programas/infoModulos.php',
				method: 'POST',
				data: {
					'id': id
				},
				cache: false,
				dataType: "json",
				success: function(resultado) {
					$('#horas_teoricas').val(resultado.horas_teoricas);
					$('#horas_practicas').val(resultado.horas_practicas);
                    /*---------------------------------*/
					horas_teoricas = parseFloat(resultado.horas_teoricas.replace(",", "."));
					horas_practicas = parseFloat(resultado.horas_practicas.replace(",", "."));
					horas_totales = (horas_teoricas + horas_practicas);
					$('#horas_totales').val(horas_totales);
                    /*---------------------------------*/
					$('#horas_teoricas_asig').attr('max', resultado.horas_teoricas);
					$('#horas_practicas_asig').attr('max', resultado.horas_practicas);
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
    /*---------------------------*/
	function sumaHoras(horas_teoricas, horas_practicas, horas_teoricas_asig, horas_practicas_asig) {
		horas_totales_asig = parseFloat(horas_teoricas_asig) + parseFloat(horas_practicas_asig);
		$('#horas_totales_asig').val(horas_totales_asig);
        /*--------------------*/
		horas_teoricas_rest = parseFloat(horas_teoricas) - parseFloat(horas_teoricas_asig);
		horas_practicas_rest = parseFloat(horas_practicas) - parseFloat(horas_practicas_asig);
		$('#horas_teoricas_rest').val(horas_teoricas_rest);
		$('#horas_practicas_rest').val(horas_practicas_rest);
        /*----------------------*/
		horas_totales_rest = horas_teoricas_rest + horas_practicas_rest;
		$('#horas_totales_rest').val(horas_totales_rest);
	}
});