$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------*/
    $("#conf_password").keyup(function() {
        var pass_new = $("#password").val();
        var conf_password = $("#conf_password").val();
        if (conf_password != pass_new) {
            $(".tooltip").show();
            $("#conf_password").focus();
            $("#conf_password").attr('data-toggle', 'tooltip');
            $("#conf_password").addClass('border border-danger');
            $("#conf_password").tooltip({
                title: "La contraseña no coincide",
                trigger: "focus",
                placement: "right"
            });
            $("#conf_password").tooltip('show');
        } else {
            $("#conf_password").removeClass('border border-danger').addClass('border border-success');
            $(".tooltip").hide();
        }
    });
    /*---------------------------------------------------*/
    $("#usuario").blur(function() {
        var usuario = $("#usuario").val();
        if (usuario == '') {
            /*---------------------------------------------------------*/
            $(".tooltip").show();
            $("#usuario").focus();
            $("#usuario").attr('data-toggle', 'tooltip');
            $("#usuario").addClass('border border-danger');
            $("#usuario").tooltip({
                title: "Usuario invalido",
                trigger: "focus",
                placement: "right"
            });
            $("#usuario").tooltip('show');
            $("#enviar_form").attr('disabled', true);
            /*---------------------------------------------------------*/
        } else {
            validarUsuario(usuario);
        }
    });
    /*---------------------------------------------------*/
    $(".activar_usuario").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        activarUsuario(id, log);
    });
    /*---------------------------------------------------*/
    function activarUsuario(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/activarUsuario.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Usuario activado!", {
                            color: "green",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de inactivacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    /*---------------------------------------------------*/
    $(".inactivar_usuario").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var log = $(this).attr('data-log');
        inactivarUsuario(id, log);
    });

    function inactivarUsuario(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/inactivarUsuario.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Usuario eliminado!", {
                            color: "red",
                            duration: "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error de inactivacion!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------*/
    $(".restablecer_pass").on(tipoEvento, function() {
        var id = $(this).attr('id');
        restablecerPass(id);
    });

    function restablecerPass(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/restablecerPass.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Contraseña restblecida!", {
                            color: "green",
                            duration: "1000"
                        });
                    } else {
                        ohSnap("Error de contraseña!", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
    function validarUsuario(usuario) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/validarUsuario.php',
                type: 'POST',
                data: {
                    'usuario': usuario,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'existe') {
                        /*---------------------------------------------------------*/
                        $(".tooltip").show();
                        $("#usuario").focus();
                        $("#usuario").attr('data-toggle', 'tooltip');
                        $("#usuario").addClass('border border-danger');
                        $("#usuario").tooltip({
                            title: "Usuario ya existe",
                            trigger: "focus",
                            placement: "right"
                        });
                        $("#usuario").tooltip('show');
                        $("#enviar_form").attr('disabled', true);
                        /*---------------------------------------------------------*/
                    } else {
                        $("#usuario").removeClass('border border-danger').addClass('border border-success');
                        $("#enviar_form").attr('disabled', false);
                        $(".tooltip").hide();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});