$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------------------------------*/
    $(".aprobar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $('#id_log').val();
        var tipo_pago = $('#tipo_pago_' + id).val();
        aprobarSolicitudAprobacion(id, id_log, tipo_pago);
    });
    /*---------------------------------------------------*/
    $(".denegar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $('#id_log').val();
        denegarSolicitudAprobacion(id, id_log);
    });
    /*---------------------------------------------------*/
    $(".remover").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $('#id_log').val();
        removerEstadoSolicitud(id, id_log);
    });
    /*----------------------------*/
    function removerEstadoSolicitud(id, log) {
        $(".loader").show();
        try {
            $.ajax({
                url: '../vistas/ajax/aprobacion/remover.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == true) {
                        $(".loader").hide();
                        ohSnap("Estado Removido", {
                            color: "orange",
                            duration: "1000"
                        });
                        $('.botones_' + id).css('display','block');
                        $('.btn_remover_' + id).addClass('d-none');
                        $('.archivo_' + id).addClass('d-none');
                        $('.span_' + id).html('<span class="badge badge-warning">Pendiente</span>');
                    } else {
                        ohSnap("Error de Aprobacion", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*----------------------------*/
    function aprobarSolicitudAprobacion(id, log, tipo_pago) {
        $(".loader").show();
        try {
            $.ajax({
                url: '../vistas/ajax/aprobacion/aprobar.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                    'tipo_pago': tipo_pago,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == true) {
                        $(".loader").hide();
                        ohSnap("Solicitud Aprobada", {
                            color: "green",
                            duration: "1000"
                        });
                        $('.botones_' + id).css('display', 'none');
                        $(".btn_remover_" + id).removeClass('d-none');
                        $('.span_' + id).html('<span class="badge badge-success">Aprobado</span>');
                        $('.modal_tipo_pago').modal('hide');
                    } else {
                        ohSnap("Error de Aprobacion", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*----------------------------*/
    function denegarSolicitudAprobacion(id, log) {
        $(".loader").show();
        try {
            $.ajax({
                url: '../vistas/ajax/aprobacion/denegar.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == true) {
                        $(".loader").hide();
                        ohSnap("Solicitud Denegada", {
                            color: "yellow",
                            duration: "1000"
                        });
                        $('.botones_' + id).css('display', 'none');
                        $(".btn_remover_" + id).removeClass('d-none');
                        $('.span_' + id).html('<span class="badge badge-danger">Denegado</span>');
                    } else {
                        ohSnap("Error de Denegacion", {
                            color: "red",
                            duration: "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------------------------------------*/
});