$(document).ready(function() {
    /*------------------------*/
    $("#documento").keyup(function() {
        let documento = $(this).val();
        if (documento != '') {
            consultarDocumento(documento);
        } else {
            $("#nombre").attr('readonly', true);
        }
    });
    /*------------------------*/
    $(".cheque").hide();
    $("#tipo_pago").change(function() {
        var valor = $(this).val();
        if (valor == 5) {
            $(".cheque").show();
        } else {
            $(".cheque").hide();
        }
    });
    /*------------------------*/
    $(".anular").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        anularIngreso(id, log);
    });
    /*------------------------*/
    $(".autorizar").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        autorizarIngreso(id, log);
    });
    /*------------------------*/
    $(".anular_egreso").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        anularEgreso(id, log);
    });
    /*------------------------*/
    $(".autorizar_egreso").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        autorizarEgreso(id, log);
    });
    /*------------------------*/
    $(".anular_egreso_ws").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        anularEgresoWs(id, log);
    });
    /*------------------------*/
    $(".autorizar_egreso_ws").click(function() {
        var id = $(this).attr('data-id');
        var log = $(this).attr('data-log');
        autorizarEgresoWs(id, log);
    });
    /*------------------------*/
    function consultarDocumento(documento) {
        try {
            $.ajax({
                url: '../vistas/ajax/ingresos/consultarDocumento.php',
                type: 'POST',
                data: {
                    'documento': documento,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado != '') {
                        $("#nombre").val(resultado.nombre);
                        $("#ciudad").val(resultado.ciudad);
                    } else {
                        $("#nombre").val('');
                        $("#ciudad").val('');
                    }
                    $("#nombre").attr('readonly', false);
                    $("#ciudad").attr('readonly', false);
                    let id_usuario = (resultado.id == '') ? 0 : resultado.id;
                    $("#id_usuario").val(id_usuario);
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function anularIngreso(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/ingresos/anular.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).addClass('d-none');
                        $("#autorizar_" + id).removeClass('d-none');
                        $(".recibo_" + id).addClass('d-none');
                        $(".span_" + id).html('<span class="badge badge-danger">Anulado</span>');
                        ohSnap("Anulado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Anular!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function autorizarIngreso(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/ingresos/autorizar.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).removeClass('d-none');
                        $("#autorizar_" + id).addClass('d-none');
                        $(".recibo_" + id).removeClass('d-none');
                        $(".span_" + id).html('');
                        ohSnap("Autorizado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Autorizar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function anularEgreso(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/egreso/anular.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).addClass('d-none');
                        $("#autorizar_" + id).removeClass('d-none');
                        $(".recibo_" + id).addClass('d-none');
                        $(".span_" + id).html('<span class="badge badge-danger">Anulado</span>');
                        ohSnap("Anulado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Anular!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function autorizarEgreso(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/egreso/autorizar.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).removeClass('d-none');
                        $("#autorizar_" + id).addClass('d-none');
                        $(".recibo_" + id).removeClass('d-none');
                        $(".span_" + id).html('');
                        ohSnap("Autorizado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Autorizar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function anularEgresoWs(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/egreso_ws/anular.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).addClass('d-none');
                        $("#autorizar_" + id).removeClass('d-none');
                        $(".recibo_" + id).addClass('d-none');
                        $(".span_" + id).html('<span class="badge badge-danger">Anulado</span>');
                        ohSnap("Anulado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Anular!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*------------------------*/
    function autorizarEgresoWs(id, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/egreso_ws/autorizar.php',
                type: 'POST',
                data: {
                    'id': id,
                    'log': log,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("#anular_" + id).removeClass('d-none');
                        $("#autorizar_" + id).addClass('d-none');
                        $(".recibo_" + id).removeClass('d-none');
                        $(".span_" + id).html('');
                        ohSnap("Autorizado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al Autorizar!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});