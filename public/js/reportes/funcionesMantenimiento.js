$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------------*/
    $(".guardar").on(tipoEvento, function() {
        var categoria = $("#categoria").val();
        var categoria_text = $('#categoria option:selected').html();
        var zona = $("#zona").val();
        var zona_text = $('#zona option:selected').html();
        var fecha_mant = $("#fecha_mant").val();
        var observacion = $("#observacion").val();
        if (categoria == '' || zona == '' || fecha_mant == '') {
            swal({
                title: "Campos requeridos",
                text: " No dejar los campos requeridos (*) vacios",
                icon: "error",
                type: "danger",
                button: false,
                closeOnClickOutside: false,
                timer: 2200,
            });
        } else {
            agregarArticuloTabla(categoria, categoria_text, zona, zona_text, fecha_mant, observacion);
        }
    });
     /*---------------------------------*/
    $(".buscar").on("click", ".remover_input", function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*---------------------------------*/
    function agregarArticuloTabla(categoria, categoria_text, zona, zona_text, fecha_mant, observacion) {
        var rand = Math.floor((Math.random() * 9999999) + 1);
        $(".buscar").append('<input name="zona[]" type="hidden" readonly value="'+zona+'">'+
            '<input name="categoria[]" type="hidden" readonly value="'+categoria+'">');
        $(".buscar").append('<tr id="fila'+rand+'">' +
            '<td><input class="form-control" type="text" disabled value="'+categoria_text+'"></td>'+
            '<td><input class="form-control" type="date" readonly value="'+fecha_mant+'" name="fecha_mant[]"></td>'+
            '<td><input class="form-control" type="text" disabled value="'+zona_text+'"></td>'+
            '<td><textarea class="form-control" rows="5" name="observacion[]" readonly>'+observacion+'</textarea></td>'+
            '<td class="text-center"><button class="btn btn-danger btn-sm remover_input" type="button" id="' + rand + '"><i class="fa fa-minus"></i></button></td>' +
        '</tr>');

        $("#zona").val("").trigger("change");
        $("#categoria").val("").trigger("change");
        $("#fecha_mant").val("");
        $("#observacion").val("");
    }
});