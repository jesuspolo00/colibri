$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------------*/
    $(".guardar_inspeccion").click(function() {
        var zona = $("#zona").val();
        var zona_text = $('#zona option:selected').html();
        var area = $("#area").val();
        var area_text = $('#area option:selected').html();
        var categoria = $("#categoria").val();
        var categoria_text = $('#categoria option:selected').html();
        var tipo = $("#tipo").val();
        var tipo_text = $('#tipo option:selected').html();
        var observacion = $("#observacion").val();
        if (zona == '' || area == '' || categoria == '' || tipo == '') {
            swal({
                title: "Campos requeridos",
                text: " No dejar los campos requeridos (*) vacios",
                icon: "error",
                type: "danger",
                button: false,
                closeOnClickOutside: false,
                timer: 2200,
            });
        } else {
            agregarArticuloTabla(zona, zona_text, area, area_text, categoria, categoria_text, tipo, tipo_text, observacion);
        }
    });
    /*---------------------------------*/
    $(".buscar").on("click", ".remover_input", function() {
        var id = $(this).attr('id');
        $("#fila" + id).html("");
        $("#fila" + id).fadeOut();
    });
    /*---------------------------------*/
    function agregarArticuloTabla(zona, zona_text, area, area_text, categoria, categoria_text, tipo, tipo_text, observacion) {
        var rand = Math.floor((Math.random() * 9999999) + 1);
        $(".buscar").append('<input name="zona[]" type="hidden" readonly value="'+zona+'">'+
            '<input name="area[]" type="hidden" readonly value="'+area+'">'+
            '<input name="categoria[]" type="hidden" readonly value="'+categoria+'">'+
            '<input name="tipo[]" type="hidden" readonly value="'+tipo+'">');
        $(".buscar").append('<tr id="fila'+rand+'">' +
            '<td><input class="form-control" type="text" disabled value="'+zona_text+'"></td>'+
            '<td><input class="form-control" type="text" disabled value="'+area_text+'"></td>'+
            '<td><input class="form-control" type="text" disabled value="'+categoria_text+'"></td>'+
            '<td><input class="form-control" type="text" disabled value="'+tipo_text+'"></td>'+
            '<td><textarea class="form-control" rows="5" name="observacion[]" readonly>'+observacion+'</textarea></td>'+
            '<td class="text-center"><button class="btn btn-danger btn-sm remover_input" type="button" id="' + rand + '"><i class="fa fa-minus"></i></button></td>' +
        '</tr>');

        $("#zona").val("").trigger("change");
        $("#area").val("").trigger("change");
        $("#categoria").val("").trigger("change");
        $("#tipo").val("").trigger("change");
        $("#observacion").val("");
    }
});