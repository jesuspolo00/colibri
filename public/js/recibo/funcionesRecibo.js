$(document).ready(function() {
    /*--------------------*/
    $("#banco").hide();
    $("#cheque").hide();
    /*--------------------*/
    $("#tipo_pago").change(function() {
        var valor = $(this).val();
        if (valor == 1) {
            $("#banco").show();
            $("#cheque").show();
        }
        if (valor == 2) {
            $("#banco").show();
            $("#cheque").hide();
        }
        if (valor == 3) {
            $("#banco").hide();
            $("#cheque").hide();
        }
        if (valor == '') {
            $("#banco").hide();
            $("#cheque").hide();
        }
    });
    /*--------------------*/
    $(".tipo_pago").change(function() {
        var valor = $(this).val();
        var id = $(this).attr('data-id');
        if (valor == 1) {
            $(".banco_" + id).show();
            $(".cheque_" + id).show();
        }
        if (valor == 2) {
            $(".banco_" + id).show();
            $(".cheque_" + id).hide();
        }
        if (valor == 3) {
            $(".banco_" + id).hide();
            $(".cheque_" + id).hide();
        }
        if (valor == '') {
            $(".banco_" + id).hide();
            $(".cheque_" + id).hide();
        }
    });
    /*--------------------*/
    $(".tipo_pago").ready(function() {
        var valor = $(this).val();
        var id = $(this).attr('data-id');
        if (valor == 1) {
            $(".banco_" + id).removeClass('d-none');
            $(".cheque_" + id).removeClass('d-none');
        }
        if (valor == 2) {
            $(".banco_" + id).removeClass('d-none');
            $(".cheque_" + id).addClass('d-none');
        }
        if (valor == 3) {
            $(".banco_" + id).removeClass('d-none');
            $(".cheque_" + id).removeClass('d-none');
        }
        if (valor == '') {
            $(".banco_" + id).removeClass('d-none');
            $(".cheque_" + id).removeClass('d-none');
        }
    });
});