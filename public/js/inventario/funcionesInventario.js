$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*$(".hardware").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_hoja = $("#id_hoja").val();
        var id_log = $("#id_log").val();
        asignarComponenteHardware(id, id_log, id_hoja);
    });*/
    $("#desc_codigo").on(tipoEvento, function() {
        var codigo = $("#codigo").val();
        descargarCodigo(codigo);
    });
    $("#btn_asignar").hide();
    $("#harwd").hide();
    /*----------------*/
    $("#btn_agregar").on(tipoEvento, function() {
        $("#harwd").show();
        $("#btn_asignar").show();
        $("#btn_agregar").hide();
        $("#list_hardw").hide();
    });
    /*-----------------------*/
    $("#btn_asignar").on(tipoEvento, function() {
        $("#harwd").hide();
        $("#btn_asignar").hide();
        $("#btn_agregar").show();
        $("#list_hardw").show();
    });
    /*----------------------------*/
    $(".asignar_comp").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var inventario = $(this).attr('data-inventario');
        var log = $(this).attr('data-log');
        asignarHardware(id, inventario, log);
    });
    /*------------------------------*/
    $(".liberar_hardw").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var inventario = $(this).attr('data-inventario');
        var log = $(this).attr('data-log');
        liberarComponente(id, inventario, log);
    });
    /* function asignarComponenteHardware(id, id_log, id_hoja) {
         try {
             $.ajax({
                 url: '../vistas/ajax/hoja_vida/asignarComponenteHardware.php',
                 method: "POST",
                 data: { 'id_inventario': id, 'id_log': id_log, 'id_hoja_vida': id_hoja },
                 success: function(r) {
                     if (r == 'ok') {
                         $("#" + id).css('display', 'none');
                         $(".span-" + id).append('<span class="badge badge-success">Asignado</span>');
                         ohSnap("Asignado correctamente!", { color: "green", 'duration': '1000' });
                     }
                 }
             });
         } catch (evt) {
             alert(evt.message);
         }
     }*/
    function descargarCodigo(codigo) {
        try {
            $.ajax({
                url: '../vistas/ajax/inventario/codigo.php',
                method: "POST",
                data: {
                    'codigo': codigo
                },
                success: function(r) {
                    if (r) {
                        $("#desc_codigo").attr('download', r);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------------------*/
    function asignarHardware(id, inventario, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/inventario/asignarComponenteHardware.php',
                method: "POST",
                data: {
                    'id': id,
                    'id_inventario': inventario,
                    'id_log': log,
                },
                dataType: 'json',
                success: function(r) {
                    if (r.mensaje == 'ok') {
                        $("#" + id).css('display', 'none');
                        $(".span-" + id).append('<span class="badge badge-success">Asignado</span>');
                        ohSnap("Asignado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------------------*/
    function liberarComponente(id, inventario, log) {
        try {
            $.ajax({
                url: '../vistas/ajax/inventario/liberarComponente.php',
                method: "POST",
                data: {
                    'id': id,
                    'id_inventario': inventario,
                    'id_log': log,
                },
                dataType: 'json',
                success: function(r) {
                    if (r.mensaje == 'ok') {
                        $(".remov" + id).fadeOut();
                        ohSnap("Removido correctamente!", {
                            color: "red",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        location.reload();
    }
});