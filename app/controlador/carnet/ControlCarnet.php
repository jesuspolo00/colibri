<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'carnet' . DS . 'ModeloCarnet.php';
require_once CONTROL_PATH . 'numeros.php';

class ControlCarnet
{

    private static $instancia;

    public static function singleton_carnet()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarPersonalControl()
    {
        $mostrar = ModeloCarnet::mostrarPersonalModel();
        return $mostrar;
    }

    public function mostrarCategoriasCarnetControl()
    {
        $mostrar = ModeloCarnet::mostrarCategoriasCarnetModel();
        return $mostrar;
    }

    public function mostrarCargosCarnetControl()
    {
        $mostrar = ModeloCarnet::mostrarCargosCarnetModel();
        return $mostrar;
    }

    public function mostrarCargosCategoriaControl($id)
    {
        $mostrar = ModeloCarnet::mostrarCargosCategoriaModel($id);
        return $mostrar;
    }

    public function mostrarDatosPersonalIdControl($id)
    {
        $mostrar = ModeloCarnet::mostrarDatosPersonalIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosPersonalDocumentoControl($documento)
    {
        $mostrar = ModeloCarnet::mostrarDatosPersonalDocumentoModel($documento);
        return $mostrar;
    }

    public function buscarPersonalControl($buscar)
    {
        $mostrar = ModeloCarnet::buscarPersonalModel($buscar);
        return $mostrar;
    }

    public function registrarPersonalControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['foto']);
            }

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'documento'     => $_POST['documento'],
                'nom_prim'      => $_POST['nom_prim'],
                'nom_seg'       => '',
                'apellido_prim' => $_POST['apellido_prim'],
                'apellido_seg'  => $_POST['apellido_seg'],
                'codigo'        => $_POST['codigo'],
                'pago'          => $_POST['pago'],
                'curso'         => $_POST['curso'],
                'foto'          => $nombre_archivo,
            );

            $guardar = ModeloCarnet::registrarPersonalModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function subirFotoPersonalControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = $_POST['foto_ant'];

            if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {
                $eliminar = eliminarArchivo($nombre_archivo);
                if ($eliminar == true) {
                    $nombre_archivo = guardarArchivo($_FILES['foto']);
                }
            }

            $fecha_entrega = ($_POST['fecha_entrega'] == '') ? '0000-00-00 00:00:00' : $_POST['fecha_entrega'] . ' ' . date('H:i:s');

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_user'       => $_POST['id_user'],
                'documento'     => $_POST['documento_edit'],
                'nom_prim'      => $_POST['nom_prim_edit'],
                'nom_seg'       => '',
                'apellido_prim' => $_POST['apellido_prim_edit'],
                'apellido_seg'  => $_POST['apellido_seg_edit'],
                'curso'         => $_POST['curso_edit'],
                'codigo'        => $_POST['codigo_edit'],
                'pago'          => $_POST['pago_edit'],
                'foto'          => $nombre_archivo,
                'fecha_entrega' => $fecha_entrega,
            );

            $guardar = ModeloCarnet::subirFotoPersonalModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                    window.open("' . BASE_URL . 'carnet/frente?personal=' . base64_encode($_POST['id_user']) . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }
}
