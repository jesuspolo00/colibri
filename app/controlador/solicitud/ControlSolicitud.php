<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'solicitud' . DS . 'ModeloSolicitud.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'messages.php';

class ControlSolicitud
{

    private static $instancia;

    public static function singleton_solicitud()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarSolicitudesControl()
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesModel();
        return $mostrar;
    }

    public function mostratLimiteSolicitudesControl()
    {
        $mostrar = ModeloSolicitud::mostratLimiteSolicitudesModel();
        return $mostrar;
    }

    public function mostrarSolicitudBuscarControl($datos)
    {
        $proveedor = (empty($datos['proveedor'])) ? '' : ' AND s.id_proveedor = ' . $datos['proveedor'];
        $fecha     = (empty($datos['fecha'])) ? '' : ' AND (s.fecha_solicitud = "' . $datos['fecha'] . '" OR s.fecha_aplazado = "' . $datos['fecha'] . '") ';

        $datos = array('proveedor' => $proveedor, 'fecha' => $fecha, 'buscar' => $datos['buscar']);

        $mostrar = ModeloSolicitud::mostrarSolicitudBuscarModel($datos);
        return $mostrar;
    }

    public function mostrarSolicitudesUsuarioControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarDatosSolicitudIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionModel($id);
        return $mostrar;
    }

    public function aprobarSolicitudControl($id, $log)
    {
        $mostrar = ModeloSolicitud::aprobarSolicitudModel($id, $log);
        return $mostrar;
    }

    //------------------------INICIAL------------------------------//

    public function mostrarDatosSolicitudInicialIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudInicialIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudInicialModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionInicialModel($id);
        return $mostrar;
    }

    public function registrarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $ultimo_consecutivo = ModeloSolicitud::ultimoConsecutivoModel();

            $datos_solicitud = array(
                'id_log'          => $_POST['id_log'],
                'id_user'         => $_POST['id_user'],
                'fecha_solicitud' => $_POST['fecha_solicitud'],
                'area'            => $_POST['area'],
                'justificacion'   => $_POST['justificacion'],
                'consecutivo'     => $ultimo_consecutivo['consecutivo'] + 1,
            );

            $guardar = ModeloSolicitud::registrarSolicitudModel($datos_solicitud);

            if ($guardar['guardar'] == true) {

                $array_producto = array();
                $array_producto = $_POST['producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_cantidad));

                foreach ($it as $a) {
                    $datos_producto = array(
                        'id_log'       => $_POST['id_log'],
                        'id_solicitud' => $guardar['id'],
                        'producto'     => $a[0],
                        'cantidad'     => $a[1],
                    );

                    $guardar_producto = ModeloSolicitud::registrarProdcuctosModel($datos_producto);
                }

                if ($guardar_producto['guardar'] == true) {
                    $title   = 'Solicitud registrada';
                    $message = 'Se ha registrado la solicitud';
                    $enlace  = BASE_URL . 'solicitud' . DS . 'index';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }
        }
    }

    public function confirmarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['url'] == 0) ? BASE_URL . 'solicitud/listado' : BASE_URL . 'solicitud/editar?solicitud=' . base64_encode($_POST['id_solicitud']);

            $datos_solicitud = array(
                'id_solicitud'     => $_POST['id_solicitud'],
                'estado'           => $_POST['estado'],
                'fecha_solicitado' => $_POST['fecha_solicitado'],
                'observacion'      => $_POST['observacion'],
                'id_log'           => $_POST['id_log'],
                'iva'              => '',
                'id_proveedor'     => $_POST['proveedor'],
                'generado'         => $_POST['generado'],
                'fecha_aprobacion' => $_POST['fecha_aprobacion'],
                'consecutivo'      => $_POST['consecutivo'],
            );

            $confirmar = ModeloSolicitud::actualizarEstadoModel($datos_solicitud);

            if ($confirmar == true) {

                $array_producto = array();
                $array_producto = $_POST['id_producto'];

                $array_precio = array();
                $array_precio = $_POST['valor'];

                $array_nom = array();
                $array_nom = $_POST['nom_producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $array_iva = array();
                $array_iva = $_POST['iva'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_precio));
                $it->attachIterator(new ArrayIterator($array_nom));
                $it->attachIterator(new ArrayIterator($array_cantidad));
                $it->attachIterator(new ArrayIterator($array_iva));

                foreach ($it as $a) {
                    $id_producto  = $a[0];
                    $precio       = $a[1];
                    $nom_producto = $a[2];
                    $cantidad     = $a[3];

                    $datos_precio = array(
                        'id_producto'  => $id_producto,
                        'nom_producto' => $nom_producto,
                        'cantidad'     => $cantidad,
                        'precio'       => str_replace(',', '', $precio),
                        'iva'          => $a[4],
                    );

                    $actualizar_precio = ModeloSolicitud::preciosProductoControl($datos_precio);
                }

                if ($actualizar_precio == true) {
                    $title   = 'Solicitud actualizada';
                    $message = 'Se ha actualizado la solicitud No. ' . $_POST['consecutivo'];
                    $enlace  = $url;
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de actualizacion';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }
        }
    }

    public function verificarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos_verificacion = array(
                'id_solicitud'        => $_POST['id_solicitud'],
                'id_log'              => $_POST['id_log'],
                'cantidad'            => $_POST['cumple_cant'],
                'calidad'             => $_POST['cumple_calidad'],
                'precios'             => $_POST['cumple_precio'],
                'plazos'              => $_POST['cumple_plazo'],
                'observacion_cant'    => $_POST['observacion_cant'],
                'observacion_calidad' => $_POST['observacion_calidad'],
                'observacion_precio'  => $_POST['observacion_precio'],
                'observacion_plazo'   => $_POST['observacion_plazo'],
                'fecha_verificacion'  => $_POST['fecha_verificacion'],
            );

            $verificacion = ModeloSolicitud::verificarSolicitudModel($datos_verificacion);

            if ($verificacion == true) {
                $title   = 'Solicitud verificada';
                $message = 'Se ha verificado la solicitud No. ' . $_POST['id_solicitud'];
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de verificacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
            echo ' <script>
            window.open("' . BASE_URL . 'imprimir/solicitud?solicitud=' . base64_encode($_POST['id_solicitud']) . '")
            </script> ';
        }
    }

    public function removerProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $eliminar = ModeloSolicitud::removerProductoModel($_POST['id']);
            return $eliminar;
        }
    }

    public function agregarProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $datos = array(
                'id_solicitud' => $_POST['id'],
                'id_log'       => $_POST['id_log'],
                'producto'     => '',
                'cantidad'     => '',
            );

            $guardar = ModeloSolicitud::registrarProdcuctosModel($datos);
            return $guardar;
        }
    }

    public function anularSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'estado'       => 0,
                'motivo'       => $_POST['motivo'],
            );

            $guardar = ModeloSolicitud::anularSolicitudModel($datos);

            if ($guardar == true) {
                $title   = 'Solicitud anulada';
                $message = 'Se ha anulado la solicitud No. ' . $_POST['id_solicitud'];
                $enlace  = BASE_URL . 'solicitud' . DS . 'listado';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
