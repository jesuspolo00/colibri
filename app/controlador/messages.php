<?php

function alertSuccess($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "success",
		type: "success",
		button: false,
		closeOnClickOutside: false,
		timer: 2200,
		})';

    if (!empty($enlace)) {
        $alerta .= '
		.then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			';
    }

    $alerta .= '</script>';
    
    echo $alerta;
}

function alertWarning($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "warning",
		type: "warning",
		button: false,
		closeOnClickOutside: false,
		timer: 2200,
		})';

    if (!empty($enlace)) {
        $alerta .= '
		.then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			';
    }

    $alerta .= '</script>';

    echo $alerta;
}

function alertDanger($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "error",
		type: "danger",
		button: false,
		closeOnClickOutside: false,
		timer: 2200,
	})';

    if (!empty($enlace)) {
        $alerta .= '
		.then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			';
    }

    $alerta .= '</script>';

    echo $alerta;
}
