<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'carta' . DS . 'ModeloCarta.php';

class ControlCarta
{

    private static $instancia;

    public static function singleton_carta()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarLimitesCartasControl()
    {
        $mostrar = ModeloCarta::mostrarLimitesCartasModel();
        return $mostrar;
    }

    public function mostrarInformacionCartaControl($id)
    {
        $mostrar = ModeloCarta::mostrarInformacionCartaModel($id);
        return $mostrar;
    }

    public function mostrarArticulosCartaControl($id)
    {
        $mostrar = ModeloCarta::mostrarArticulosCartaModel($id);
        return $mostrar;
    }

    public function eliminarCartaControl($id)
    {
        $mostrar = ModeloCarta::eliminarCartaModel($id);
        $rs      = ($mostrar == true) ? 'ok' : 'no';
        return $rs;
    }

    public function buscarCartasControl($datos)
    {
        $fecha = (empty($datos['fecha'])) ? '' : ' AND c.fecha_entrega = "' . $datos['fecha'] . '"';

        $datos = array('fecha' => $fecha, 'buscar' => $datos['buscar']);

        $mostrar = ModeloCarta::buscarCartasModel($datos);
        return $mostrar;
    }

    public function generarCartaEntregaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos_carta = array(
                'id_log'      => $_POST['id_log'],
                'fecha'       => $_POST['fecha'],
                'observacion' => $_POST['observacion'],
            );

            $guardar = ModeloCarta::registrarCartaEntregaModel($datos_carta);

            if ($guardar['guardar'] == true) {

                $array_producto = array();
                $array_producto = $_POST['producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_cantidad));

                foreach ($it as $a) {

                    $datos_producto = array(
                        'id_log'   => $_POST['id_log'],
                        'id_carta' => $guardar['id'],
                        'producto' => $a[0],
                        'cantidad' => $a[1],
                    );

                    $guardar_producto = ModeloCarta::guardarArticulosControl($datos_producto);
                }

                if ($guardar_producto == true) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina, 1050);

                    function recargarPagina()
                    {
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error de solicitud!", {color:"red", "duration":"1000"});
                    </script>
                    ';
                }

            }

        }
    }

    public function editarCartaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos_carta = array(
                'id_carta'    => $_POST['id_carta'],
                'id_log'      => $_POST['id_log'],
                'fecha'       => $_POST['fecha_entrega'],
                'observacion' => $_POST['observacion'],
            );

            $guardar = ModeloCarta::editarCartaModel($datos_carta);

            if ($guardar == true) {

                $remover_articulos = ModeloCarta::removerArticuloModel($_POST['id_carta']);

                if ($remover_articulos == true) {

                    $array_producto = array();
                    $array_producto = $_POST['producto'];

                    $array_cantidad = array();
                    $array_cantidad = $_POST['cantidad'];

                    $it = new MultipleIterator();
                    $it->attachIterator(new ArrayIterator($array_producto));
                    $it->attachIterator(new ArrayIterator($array_cantidad));

                    foreach ($it as $a) {

                        $datos_producto = array(
                            'id_log'   => $_POST['id_log'],
                            'id_carta' => $_POST['id_carta'],
                            'producto' => $a[0],
                            'cantidad' => $a[1],
                        );

                        $guardar_producto = ModeloCarta::guardarArticulosControl($datos_producto);
                    }

                    if ($guardar_producto == true) {
                        echo '
                        <script>
                        ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina, 1050);

                        function recargarPagina()
                        {
                            window.location.replace("editar?carta=' . base64_encode($_POST['id_carta']) . '");
                        }
                        </script>
                        ';
                    } else {
                        echo '
                        <script>
                        ohSnap("Error de solicitud!", {color:"red", "duration":"1000"});
                        </script>
                        ';
                    }
                }

            }

        }
    }
}
