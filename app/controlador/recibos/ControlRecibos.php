<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'recibos' . DS . 'ModeloRecibos.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'messages.php';

class ControlRecibos
{

    private static $instancia;

    public static function singleton_recibos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarProyectosControl()
    {
        $mostrar = ModeloRecibos::mostrarProyectosModel();
        return $mostrar;
    }

    public function mostrarProyectosWsControl()
    {
        $mostrar = ModeloRecibos::mostrarProyectosWsModel();
        return $mostrar;
    }

    public function mostrarProyectosFundepexControl()
    {
        $mostrar = ModeloRecibos::mostrarProyectosFundepexModel();
        return $mostrar;
    }

    public function mostrarProyectosTalentoControl()
    {
        $mostrar = ModeloRecibos::mostrarProyectosTalentoModel();
        return $mostrar;
    }

    public function mostrarTodosProyectosControl()
    {
        $mostrar = ModeloRecibos::mostrarTodosProyectosModel();
        return $mostrar;
    }

    public function mostrarTodosProyectosWsControl()
    {
        $mostrar = ModeloRecibos::mostrarTodosProyectosWsModel();
        return $mostrar;
    }

    public function mostrarTodosProyectosFundepexControl()
    {
        $mostrar = ModeloRecibos::mostrarTodosProyectosFundepexModel();
        return $mostrar;
    }

    public function mostrarTodosProyectosTalentoControl()
    {
        $mostrar = ModeloRecibos::mostrarTodosProyectosTalentoModel();
        return $mostrar;
    }

    public function mostrarTipoPagoAprobacionControl()
    {
        $mostrar = ModeloRecibos::mostrarTipoPagoAprobacionModel();
        return $mostrar;
    }

    public function mostrarRecibosProyectoControl($id)
    {
        $mostrar = ModeloRecibos::mostrarRecibosProyectoModel($id);
        return $mostrar;
    }

    public function mostrarProyectosIdControl($id)
    {
        $mostrar = ModeloRecibos::mostrarProyectosIdModel($id);
        return $mostrar;
    }

    public function mostrarDetallesReciboControl($id)
    {
        $mostrar = ModeloRecibos::mostrarDetallesReciboModel($id);
        return $mostrar;
    }

    public function buscarProyectosControl($buscar)
    {
        $mostrar = ModeloRecibos::buscarProyectosModel($buscar);
        return $mostrar;
    }

    public function buscarProyectosWsControl($buscar)
    {
        $mostrar = ModeloRecibos::buscarProyectosWsModel($buscar);
        return $mostrar;
    }

    public function buscarProyectosFundepexControl($buscar)
    {
        $mostrar = ModeloRecibos::buscarProyectosFundepexModel($buscar);
        return $mostrar;
    }

    public function buscarProyectosTalentoControl($buscar)
    {
        $mostrar = ModeloRecibos::buscarProyectosTalentoModel($buscar);
        return $mostrar;
    }

    public function registrarMetodoPagoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'nombre' => $_POST['nombre'],
                'id_log' => $_POST['id_log'],
            );

            $guardar = ModeloRecibos::registrarMetodoPagoModel($datos);

            if ($guardar) {
                $title   = 'Metodo de pago registrado';
                $message = 'Se ha registrado el metodo de pago';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarMetodoPagoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_metodo' => $_POST['id_metodo'],
                'nombre'    => $_POST['nombre_edit'],
                'estado'    => $_POST['estado'],
                'id_log'    => $_POST['id_log'],
            );

            $guardar = ModeloRecibos::editarMetodoPagoModel($datos);

            if ($guardar) {
                $title   = 'Metodo de pago actualzado';
                $message = 'Se ha actualizado el metodo de pago';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registrarProyectosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'  => $_POST['id_log'],
                'nombre'  => $_POST['nombre'],
                'ciudad'  => $_POST['ciudad'],
                'cliente' => $_POST['cliente'],
            );

            $guardar = ModeloRecibos::registrarProyectosModel($datos);

            if ($guardar['guardar'] == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha registrado el proyecto Numero ' . $guardar['id']);
                $log       = ModeloRecibos::historyLogModel($datos_log);

                $title   = 'Proyecto registrado';
                $message = 'Se ha registrado el proyecto';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function editarProyectoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id'      => $_POST['id_proyecto'],
                'id_log'  => $_POST['id_log'],
                'nombre'  => $_POST['nombre_edit'],
                'ciudad'  => $_POST['ciudad_edit'],
                'cliente' => $_POST['cliente_edit'],
            );

            $guardar = ModeloRecibos::editarProyectoModel($datos);

            if ($guardar == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha editado el proyecto Numero ' . $_POST['id_proyecto']);
                $log       = ModeloRecibos::historyLogModel($datos_log);

                $title   = 'Proyecto editado';
                $message = 'Se ha editado el proyecto';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function generarReciboControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor  = str_replace(',', '', $_POST['valor']);
            $banco  = (isset($_POST['banco'])) ? $_POST['banco'] : '';
            $cheque = (isset($_POST['cheque'])) ? $_POST['cheque'] : '';

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_proyecto' => $_POST['id_proyecto'],
                'tipo_pago'   => $_POST['tipo_pago'],
                'fecha_pago'  => $_POST['fecha_pago'],
                'valor'       => $valor,
                'banco'       => $banco,
                'cheque'      => $cheque,
                'concepto'    => $_POST['concepto'],
                'proveedor'   => $_POST['proveedor'],
            );

            $guardar = ModeloRecibos::generarReciboModel($datos);

            if ($guardar['guardar'] == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha generado el recibo de caja Numero ' . $guardar['id']);
                $log       = ModeloRecibos::historyLogModel($datos_log);

                $title   = 'Recibo generado';
                $message = 'Se ha generado el recibo';
                $enlace  = BASE_URL . 'imprimir/reciboCaja?recibo=' . base64_encode($guardar['id']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de generacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function editarReciboControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $valor  = str_replace(',', '', $_POST['valor_edit']);
            $banco  = (isset($_POST['banco_edit'])) ? $_POST['banco_edit'] : '';
            $cheque = (isset($_POST['cheque_edit'])) ? $_POST['cheque_edit'] : '';

            $datos = array(
                'id_log'     => $_POST['id_log'],
                'id_recibo'  => $_POST['id_recibo'],
                'proveedor'  => $_POST['proveedor_edit'],
                'fecha_pago' => $_POST['fecha_pago_edit'],
                'tipo_pago'  => $_POST['tipo_pago_edit'],
                'concepto'   => $_POST['concepto_edit'],
                'valor'      => $valor,
                'banco'      => $banco,
                'cheque'     => $cheque,
            );

            $guardar = ModeloRecibos::editarReciboModel($datos);

            if ($guardar == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha editado el recibo de caja Numero ' . $_POST['id_recibo']);
                $log       = ModeloRecibos::historyLogModel($datos_log);

                $title   = 'Recibo editado';
                $message = 'Se ha editado el recibo';
                $enlace  = BASE_URL . 'listado?proyecto=' . base64_encode($_POST['id_proyecto']) . '&enlace=' . base64_encode($_POST['enlace']);
                alertSuccess($title, $message, $enlace);

                echo '
                <script>
                window.open("' . BASE_URL . 'imprimir/reciboCaja?recibo=' . base64_encode($_POST['id_recibo']) . '");
                </script>
                ';
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
