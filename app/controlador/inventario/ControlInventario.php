<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'inventario' . DS . 'ModeloInventario.php';
require_once MODELO_PATH . 'categorias' . DS . 'ModeloCategorias.php';
require_once CONTROL_PATH . 'messages.php';

class ControlInventario
{

    private static $instancia;

    public static function singleton_inventario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarEstadosControl()
    {
        $mostrar = ModeloInventario::mostrarEstadosModel();
        return $mostrar;
    }

    public function hojaVidaArticuloControl($id)
    {
        $mostrar = ModeloInventario::hojaVidaArticuloModel($id);
        return $mostrar;
    }

    public function mostrarReportesControl($id)
    {
        $mostrar = ModeloInventario::mostrarReportesModel($id);
        return $mostrar;
    }

    public function mostrarTodosReportesControl()
    {
        $mostrar = ModeloInventario::mostrarTodosReportesModel();
        return $mostrar;
    }

    public function mostrarTodosReportesUsuarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarTodosReportesUsuarioModel($id);
        return $mostrar;
    }

    public function reportesMantenimientoControl()
    {
        $mostrar = ModeloInventario::reportesMantenimientoModel();
        return $mostrar;
    }

    public function mostrarFechaReportadoControl($id)
    {
        $mostrar = ModeloInventario::mostrarFechaReportadoModel($id);
        return $mostrar;
    }

    public function listadoArticulosControl($id)
    {
        $mostrar = ModeloInventario::listadoArticulosModel($id);
        return $mostrar;
    }

    public function mostrarHardwareInventarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarHardwareInventarioModel($id);
        return $mostrar;
    }

    public function mostrarComponentesInventarioControl()
    {
        $mostrar = ModeloInventario::mostrarComponentesInventarioModel();
        return $mostrar;
    }

    public function mostrarSoftwareInventarioControl($id)
    {
        $mostrar = ModeloInventario::mostrarSoftwareInventarioModel($id);
        return $mostrar;
    }

    public function mostrarInformacionReporteControl($id)
    {
        $mostrar = ModeloInventario::mostrarInformacionReporteModel($id);
        return $mostrar;
    }

    public function mostrarInformacionReporteSolucionControl($id)
    {
        $mostrar = ModeloInventario::mostrarInformacionReporteSolucionModel($id);
        return $mostrar;
    }

    public function verificarCodigoInventarioControl($codigo)
    {
        $mostrar = ModeloInventario::verificarCodigoInventarioModel($codigo);
        $mostrar = (empty($mostrar['id'])) ? 'no' : BASE_URL . 'inventario/reportar?codigo=' . base64_encode($mostrar['id']);
        return $mostrar;
    }

    public function mostrarArticulosControl($area, $usuario)
    {

        if ($area != "") {
            $consulta = " WHERE i.id_area = " . $area;
        } else if ($usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario;
        } else if ($area != "" && $usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area;
        } else {
            $consulta = "";
        }

        $mostrar = ModeloInventario::mostrarArticulosModel($consulta);
        return $mostrar;
    }

    public function mostrarArticulosPanelControl($area, $usuario, $texto, $zona)
    {
        $area    = (empty($area)) ? '' : ' AND i.id_area = ' . $area;
        $usuario = (empty($usuario)) ? '' : ' AND i.id_user = ' . $usuario;
        $zona    = (empty($zona)) ? '' : ' AND i.id_zona = ' . $zona;

        $datos = array('area' => $area, 'usuario' => $usuario, 'zona' => $zona, 'buscar' => $texto);

        $mostrar = ModeloInventario::mostrarArticulosPanelModel($datos);
        return $mostrar;
    }

    public function mostrarArticulosCantidadPanelControl($area, $usuario, $texto)
    {

        if ($area != "") {
            $consulta = " WHERE i.id_area = " . $area;
        } else if ($usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario;
        } else if ($area != "" && $usuario != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area;
        } else if ($texto != "") {
            $consulta = " WHERE i.descripcion LIKE '%" . $texto . "%'";
        } else if ($area != "" && $usuario != "" && $texto != "") {
            $consulta = " WHERE i.id_user = " . $usuario . " AND i.id_area = " . $area . " AND i.descripcion LIKE '%" . $texto . "%'";
        } else {
            $consulta = "";
        }

        $mostrar = ModeloInventario::mostrarArticulosCantidadPanelModel($consulta);
        return $mostrar;
    }

    public function mostrarTodosArticulosControl()
    {
        $mostrar = ModeloInventario::mostrarTodosArticulosModel();
        return $mostrar;
    }

    public function mostrarTodosArticulosExcelControl()
    {
        $mostrar = ModeloInventario::mostrarTodosArticulosExcelModel();
        return $mostrar;
    }

    public function mostrarCantidadesArticulosControl()
    {
        $mostrar = ModeloInventario::mostrarCantidadesArticulosModel();
        return $mostrar;
    }

    public function cantidadesAreaControl($id)
    {
        $mostrar = ModeloInventario::cantidadesAreaModel($id);
        return $mostrar;
    }

    public function cantidadesGeneralControl()
    {
        $mostrar = ModeloInventario::cantidadesGeneralModel();
        return $mostrar;
    }

    public function numeroAleatorio()
    {
        $numero = rand(10, 99999);

        $codigo = ModeloInventario::verificarCodigoModel($numero);

        if ($codigo['codigo'] != '') {
            $this->numeroAleatorio();
        } else {
            $num_codigo = $numero;
        }

        return $num_codigo;
    }

    public function agregarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_compra = (empty($_POST['fecha_compra'])) ? '0000-00-00' : $_POST['fecha_compra'];

            $cantidad            = $_POST['cantidad'];
            $verificar_hoja_vida = ModeloCategorias::validarHojaVidaCategoriaModel($_POST['categoria']);

            if ($verificar_hoja_vida['hoja_vida'] == 1) {

                for ($i = 0; $i < $cantidad; $i++) {

                    $codigo = $this->numeroAleatorio();

                    $datos = array(
                        'descripcion'  => $_POST['descripcion'],
                        'marca'        => $_POST['marca'],
                        'modelo'       => $_POST['modelo'],
                        'fecha_compra' => $fecha_compra,
                        'codigo'       => $codigo,
                        'id_area'      => $_POST['area'],
                        'id_zona'      => $_POST['zona'],
                        'id_categoria' => $_POST['categoria'],
                        'user_log'     => $_POST['id_log'],
                        'cantidad'     => 1,
                    );

                    $guardar = ModeloInventario::agregarArticuloModel($datos);

                    if ($guardar['guardar'] == true && $verificar_hoja_vida['hoja_vida'] == 1) {

                        $datos_hoja = array(
                            'id_inventario'            => $guardar['id'],
                            'proveedor'                => $_POST['proveedor'],
                            'frecuencia_mantenimiento' => $_POST['frecuencia_mantenimiento'],
                            'fecha_garantia'           => $_POST['fecha_garantia'],
                            'contacto_garantia'        => $_POST['contacto_garantia'],
                            'user_log'                 => $_POST['id_log'],
                        );

                        $guardar_hoja = ModeloInventario::agregarHojaVidaArticuloModel($datos_hoja);

                    }

                }

            } else {

                $codigo = $this->numeroAleatorio();

                $datos = array(
                    'descripcion'  => $_POST['descripcion'],
                    'marca'        => $_POST['marca'],
                    'modelo'       => $_POST['modelo'],
                    'fecha_compra' => $fecha_compra,
                    'codigo'       => $codigo,
                    'id_area'      => $_POST['area'],
                    'id_zona'      => $_POST['zona'],
                    'id_categoria' => $_POST['categoria'],
                    'user_log'     => $_POST['id_log'],
                    'cantidad'     => $cantidad,
                );

                $guardar = ModeloInventario::agregarArticuloModel($datos);

            }

            if ($guardar['guardar'] == true) {
                $title   = 'Articulo registrado';
                $message = 'Se ha registrado la solicitud';
                $enlace  = BASE_URL . 'inventario' . DS . 'panel';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function actualizarHojaVidaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_compra   = ($_POST['fecha_compra'] == '') ? '0000-00-00' : $_POST['fecha_compra'];
            $fecha_garantia = ($_POST['fecha_garantia'] == '') ? '0000-00-00' : $_POST['fecha_garantia'];

            $datos_inventario = array(
                'id_log'                   => $_POST['id_log'],
                'id_inventario'            => $_POST['id_inventario'],
                'descripcion'              => $_POST['descripcion'],
                'marca'                    => $_POST['marca'],
                'modelo'                   => $_POST['modelo'],
                'area'                     => $_POST['area'],
                'zona'                     => $_POST['zona'],
                'fecha_compra'             => $fecha_compra,
                'id_hoja'                  => $_POST['id_hoja'],
                'proveedor'                => $_POST['proveedor'],
                'frecuencia_mantenimiento' => $_POST['frecuencia_mantenimiento'],
                'fecha_garantia'           => $fecha_garantia,
                'contacto_garantia'        => $_POST['contacto_garantia'],
            );

            $actualizar = ModeloInventario::actualizarHojaVidaModel($datos_inventario);

            if ($actualizar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("hojaVida?inventario=' . base64_encode($_POST['id_inventario']) . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function reportarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'] . date(' H:i:s');
            $tipo_reporte  = (isset($_POST['tipo_reporte'])) ? $_POST['tipo_reporte'] : 1;

            $datos_reporte = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_usuario'],
                'id_articulo'   => $_POST['id_articulo'],
                'cantidad'      => $_POST['cantidad'],
                'estado'        => $_POST['estado'],
                'observacion'   => $_POST['observacion'],
                'fecha_reporte' => $fecha_reporte,
                'id_area'       => $_POST['id_area'],
                'id_zona'       => $_POST['id_zona'],
                'tipo_reporte'  => $_POST['tipo_reporte'],
            );

            $datos_articulo = array(
                'id_articulo' => $_POST['id_articulo'],
                'estado'      => $_POST['estado'],
            );

            $actualizar_estado = ModeloInventario::actualizarEstadoArticuloModel($datos_articulo);

            if ($_POST['estado'] == 6) {

                $datos_liberar = array(
                    'id_articulo' => $_POST['id_articulo'],
                    'estado'      => $_POST['estado'],
                    'id_zona'     => 0,
                    'id_area'     => 0,
                    'id_user'     => 0,
                    'id_log'      => $_POST['id_log'],
                );

                $liberar = ModeloInventario::liberarArticuloModel($datos_liberar);
            }

            $guardar = ModeloInventario::reportarArticuloModel($datos_reporte);

            $url = ($_POST['inicio'] == 1) ? BASE_URL . 'inventario/panel' : BASE_URL . 'lector/index';

            if ($guardar == true) {
                $title   = 'Articulo reportado';
                $message = 'Se ha registrado el reporte';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function solucionarReporteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_reporte']) &&
            !empty($_POST['id_reporte'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'] . date(' H:i:s');

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_usuario'],
                'id_articulo'   => $_POST['id_articulo'],
                'estado'        => 4,
                'observacion'   => $_POST['observacion'],
                'id_reporte'    => $_POST['id_reporte'],
                'fecha_reporte' => $fecha_reporte,
                'id_area'       => $_POST['id_area'],
            );

            $actualizar_estado = ModeloInventario::solucionarReporteModel($datos);

            if ($actualizar_estado == true) {
                $title   = 'Reporte solucionado';
                $message = 'Se ha solucionado el reporte';
                $enlace  = BASE_URL . 'reportes/listado';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function reasignarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_articulo_reg']) &&
            !empty($_POST['id_articulo_reg'])
        ) {

            $fecha_reporte = ($_POST['fecha_reporte'] == '') ? date('Y-m-d H:i:s') : $_POST['fecha_reporte'];

            $datos_reporte = array(
                'id_log'        => $_POST['id_log'],
                'id_usuario'    => $_POST['id_log'],
                'id_articulo'   => $_POST['id_articulo_reg'],
                'cantidad'      => $_POST['cantidad'],
                'estado'        => 7,
                'observacion'   => '',
                'fecha_reporte' => $fecha_reporte,
                'id_area'       => $_POST['id_area'],
            );

            $guardar = ModeloInventario::reportarArticuloModel($datos_reporte);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_articulo' => $_POST['id_articulo_reg'],
                'id_user'     => $_POST['usuario_reasignar'],
                'id_area'     => $_POST['area_reasignar'],
                'id_zona'     => $_POST['zona_reasignar'],
                'estado'      => 7,
            );

            $reasignar = ModeloInventario::reasignarArticuloModel($datos);

            if ($reasignar == true) {
                $title   = 'Articulo re-asignado';
                $message = 'Se ha re-asignado el articulo';
                $enlace  = BASE_URL . 'inventario/panel';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de asignacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function agregarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {

            $fecha_compra = ($_POST['fecha_compra'] == '') ? '0000-00-00' : $_POST['fecha_compra'];

            $codigo = $this->numeroAleatorio();

            $datos = array(
                'descripcion'  => $_POST['descripcion'],
                'marca'        => $_POST['marca'],
                'modelo'       => $_POST['modelo'],
                'fecha_compra' => $fecha_compra,
                'codigo'       => $codigo,
                'id_user'      => 0,
                'id_area'      => $_POST['area'],
                'id_categoria' => $_POST['categoria'],
                'estado'       => 1,
                'id_log'       => $_POST['id_user'],
            );

            $guardar = ModeloInventario::agregarArticuloModel($datos);

            if ($guardar['guardar'] == true) {
                $datos_hardware = array(
                    'id_inventario' => $_POST['id_inventario'],
                    'id_componente' => $guardar['id'],
                    'id_log'        => $_POST['id_user'],
                );

                $asignar_hardware = ModeloInventario::agregarHardwareModel($datos_hardware);

                if ($asignar_hardware == true) {
                    $title   = 'Asignado correctamente';
                    $message = 'Se ha asignado el articulo';
                    $enlace  = 'hojaVida?inventario=' . base64_encode($_POST['id_inventario']);
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de actualizacion';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }

        }
    }

    public function agregarSoftwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos = array(
                'id_log'           => $_POST['id_log'],
                'id_inventario'    => $_POST['id_inventario'],
                'descripcion_soft' => $_POST['descripcion_soft'],
                'version_soft'     => $_POST['version_soft'],
                'fabricante_soft'  => $_POST['fabricante_soft'],
                'licencia_soft'    => $_POST['licencia_soft'],
            );

            $guardar_soft = ModeloInventario::agregarSoftwareModel($datos);

            if ($guardar_soft == true) {
                $title   = 'Asignado correctamente';
                $message = 'Se ha asignado el articulo';
                $enlace  = 'hojaVida?inventario=' . base64_encode($_POST['id_inventario']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function asignarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos_hardware = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_componente' => $_POST['id'],
                'id_log'        => $_POST['id_log'],
            );

            $asignar_hardware = ModeloInventario::agregarHardwareModel($datos_hardware);
            return $asignar_hardware;
        }
    }

    public function liberarHardwareControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_inventario']) &&
            !empty($_POST['id_inventario'])
        ) {
            $datos_hardware = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_componente' => $_POST['id'],
                'id_log'        => $_POST['id_log'],
            );

            $asignar_hardware = ModeloInventario::liberarHardwareModel($datos_hardware);
            return $asignar_hardware;
        }
    }

    public function actualizarArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_inventario' => $_POST['id_inventario'],
                'id_log'        => $_POST['id_log'],
                'descripcion'   => $_POST['descripcion'],
                'marca'         => $_POST['marca'],
                'modelo'        => $_POST['modelo'],
                'cantidad'      => $_POST['cantidad'],
                'zona'          => $_POST['zona'],
                'area'          => $_POST['area'],
            );

            $guardar = ModeloInventario::actualizarArticuloModel($datos);

            if ($guardar == true) {
                $title   = 'Articulo actualizado';
                $message = 'Se ha actualizado el articulo';
                $enlace  = BASE_URL . 'inventario' . DS . 'detalles?id=' . base64_encode($_POST['id_inventario']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
