<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'renovacion' . DS . 'ModeloRenovacion.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'numeros.php';

class ControlRenovacion
{

    private static $instancia;

    public static function singleton_renovacion()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarLimitesRenovacionesControl()
    {
        $mostrar = ModeloRenovacion::mostrarLimitesRenovacionesModel();
        return $mostrar;
    }

    public function mostrarAniosRenovacionControl()
    {
        $mostrar = ModeloRenovacion::mostrarAniosRenovacionModel();
        return $mostrar;
    }

    public function buscarRenovacionesControl($datos)
    {
        $anio  = (empty($datos['anio'])) ? '' : ' AND YEAR(r.fecha_inicio) LIKE "%' . $datos['anio'] . '%" OR YEAR(r.fecha_fin) LIKE "%' . $datos['anio'] . '%"';
        $datos = array('anio' => $anio, 'buscar' => $datos['buscar']);

        $mostrar = ModeloRenovacion::buscarRenovacionesModel($datos);
        return $mostrar;
    }

    public function agregarRenovacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_evidencia = '';
            $nombre_archivo   = '';

            if (isset($_FILES['evidencia']['name']) && !empty($_FILES['evidencia']['name'])) {
                $nombre_evidencia = guardarArchivo($_FILES['evidencia']);
            }

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['archivo']);
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'nombre'       => $_POST['nombre'],
                'fecha_inicio' => $_POST['fecha_inicio'],
                'fecha_fin'    => $_POST['fecha_fin'],
                'archivo'      => $nombre_archivo,
                'evidencia'    => $nombre_evidencia,
                'observacion'  => $_POST['observacion'],
                'numero'       => $_POST['numero'],
            );

            $guardar = ModeloRenovacion::agregarRenovacionModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function editarRenovacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_evidencia = $_POST['evidencia_ant'];
            $nombre_archivo   = $_POST['archivo_ant'];

            if (isset($_FILES['evidencia_edit']['name']) && !empty($_FILES['evidencia_edit']['name'])) {
                $eliminar = eliminarArchivo($nombre_evidencia);
                if ($eliminar == true) {
                    $nombre_evidencia = guardarArchivo($_FILES['evidencia_edit']);
                }
            }

            if (isset($_FILES['archivo_edit']['name']) && !empty($_FILES['archivo_edit']['name'])) {
                $eliminar = eliminarArchivo($nombre_archivo);
                if ($eliminar == true) {
                    $nombre_archivo = guardarArchivo($_FILES['archivo_edit']);
                }
            }

            $datos = array(
                'id_renovacion' => $_POST['id_renovacion'],
                'id_log'        => $_POST['id_log'],
                'nombre'        => $_POST['nom_edit'],
                'fecha_inicio'  => $_POST['fecha_inicio_edit'],
                'fecha_fin'     => $_POST['fecha_fin_edit'],
                'archivo'       => $nombre_archivo,
                'evidencia'     => $nombre_evidencia,
                'observacion'   => $_POST['observacion_edit'],
                'numero'        => $_POST['numero_edit'],
            );

            $editar = ModeloRenovacion::editarRenovacionModel($datos);

            if ($editar == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }
}
