<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'cobro' . DS . 'ModeloCobro.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlCobro
{

    private static $instancia;

    public static function singleton_cobro()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarLimiteCuentasCobroControl($id_log)
    {
        $mostrar = ModeloCobro::mostrarLimiteCuentasCobroModel($id_log);
        return $mostrar;
    }

    public function mostrarLimiteListaCuentasCobroControl()
    {
        $mostrar = ModeloCobro::mostrarLimiteListaCuentasCobroModel();
        return $mostrar;
    }

    public function mostrarLimiteLibreCuentasCobroControl()
    {
        $mostrar = ModeloCobro::mostrarLimiteLibreCuentasCobroModel();
        return $mostrar;
    }

    public function mostrarDatosCuentaCobroIdControl($id)
    {
        $mostrar = ModeloCobro::mostrarDatosCuentaCobroIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosCuentaCobroControl($id)
    {
        $mostrar = ModeloCobro::mostrarDatosCuentaCobroModel($id);
        return $mostrar;
    }

    public function mostrarDetallesCuentasCobroControl($id)
    {
        $mostrar = ModeloCobro::mostrarDetallesCuentasCobroModel($id);
        return $mostrar;
    }

    public function mostrarDocumentosCuentasCobroControl($id)
    {
        $mostrar = ModeloCobro::mostrarDocumentosCuentasCobroModel($id);
        return $mostrar;
    }

    public function mostrarDiasSemanaAsistenciaControl($id)
    {
        $mostrar = ModeloCobro::mostrarDiasSemanaAsistenciaModel($id);
        return $mostrar;
    }

    public function mostrarDiasSemanaNumeroControl($id)
    {
        $mostrar = ModeloCobro::mostrarDiasSemanaNumeroModel($id);
        return $mostrar;
    }

    public function mostrarJornadasControl()
    {
        $mostrar = ModeloCobro::mostrarJornadasModel();
        return $mostrar;
    }

    public function validarDocumentoUsuarioControl($id, $tipo)
    {
        $mostrar = ModeloCobro::validarDocumentoUsuarioModel($id, $tipo);
        return $mostrar;
    }

    public function buscarCuentaCobroControl($datos)
    {

        $anio = (empty($datos['anio'])) ? '' : ' AND YEAR(cc.fechareg) = "' . $datos['anio'] . '"';
        $mes  = (empty($datos['mes'])) ? '' : ' AND MONTH(cc.fechareg) = "' . $datos['mes'] . '"';

        $datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $datos['buscar'], 'id' => $datos['id']);

        $mostrar = ModeloCobro::buscarCuentaCobroModel($datos);
        return $mostrar;
    }

    public function buscarCuentaCoordinadorCobroControl($datos, $id_log)
    {

        $anio = (empty($datos['anio'])) ? '' : ' AND YEAR(cc.fechareg) = "' . $datos['anio'] . '"';
        $mes  = (empty($datos['mes'])) ? '' : ' AND MONTH(cc.fechareg) = "' . $datos['mes'] . '"';

        $datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $datos['buscar'], 'id_log' => $id_log);

        $mostrar = ModeloCobro::buscarCuentaCoordinadorCobroModel($datos);
        return $mostrar;
    }

    public function buscarCuentaUsuarioCobroControl($buscar, $fecha, $id_log)
    {
        $fecha   = ($fecha == '') ? '' : ' AND cc.fechareg LIKE "%' . $fecha . '%"';
        $mostrar = ModeloCobro::buscarCuentaUsuarioCobroModel($buscar, $fecha, $id_log);
        return $mostrar;
    }

    public function generarCuentaCobroControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'    => $_POST['id_log'],
                'razon'     => $_POST['razon'],
                'documento' => $_POST['documento'],
                'direccion' => $_POST['direccion'],
                'lugar'     => $_POST['lugar'],
                'telefono'  => $_POST['telefono'],
                'valor'     => $valor,
                'tipo'      => $_POST['tipo'],
            );

            $guardar = ModeloCobro::generarCuentaCobroModel($datos);

            if ($guardar['guardar'] == true) {

                $array_asistencia = array();
                $array_asistencia = $_POST['asistencia'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_asistencia));

                foreach ($it as $dato) {
                    $datos_detalle = array(
                        'id_log'          => $_POST['id_log'],
                        'id_asistencia'   => $dato[0],
                        'id_cuenta_cobro' => $guardar['id'],
                    );

                    $guardar_detalle = ModeloCobro::guardarDetallesCuentaCobroModel($datos_detalle);
                }

                if ($guardar == true) {
                    $title   = 'Cuenta de cobro registrada';
                    $message = 'Se ha registrado la cuenta de cobro No. ' . $guardar['id'];
                    $enlace  = 'detalleCobro?cobro=' . base64_encode($guardar['id']);
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }
        }
    }

    public function guardarDetallesCuentaCobroControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_cuenta_cobro' => $_POST['id_cuenta_cobro'],
                'id_log'          => $_POST['id_log'],
                'razon'           => $_POST['razon'],
                'documento'       => $_POST['documento'],
                'direccion'       => $_POST['direccion'],
                'lugar'           => $_POST['lugar'],
                'telefono'        => $_POST['telefono'],
                'valor'           => $valor,
            );

            $guardar = ModeloCobro::actualizarCuentaCobroModel($datos);

            if ($guardar == true) {

                $array_asistencia = array();
                $array_asistencia = $_POST['asistencia'];

                $array_sede = array();
                $array_sede = $_POST['sede'];

                $array_colegio = array();
                $array_colegio = $_POST['colegio'];

                $array_ciclo = array();
                $array_ciclo = $_POST['ciclo'];

                $array_semestre = array();
                $array_semestre = $_POST['semestre'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_asistencia));
                $it->attachIterator(new ArrayIterator($array_sede));
                $it->attachIterator(new ArrayIterator($array_colegio));
                $it->attachIterator(new ArrayIterator($array_ciclo));
                $it->attachIterator(new ArrayIterator($array_semestre));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_cuenta_cobro' => $_POST['id_cuenta_cobro'],
                        'id_log'          => $_POST['id_log'],
                        'id_asistencia'   => $dato[0],
                        'sede'            => $dato[1],
                        'colegio'         => $dato[2],
                        'jornada'         => $_POST['jornada'],
                        'ciclo'           => $dato[3],
                        'semestre'        => $dato[4],
                    );

                    $guardar_detalle = ModeloCobro::actualizarDetallesCuentaCobroModel($datos_detalle);
                }

                if ($guardar == true) {
                    $title   = 'Cuenta de cobro actualizada';
                    $message = 'Se ha actualizado la cuenta de cobro No. ' . $_POST['id_cuenta_cobro'];
                    $enlace  = 'documentos?cobro=' . base64_encode($_POST['id_cuenta_cobro']);
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }
        }
    }

    public function agregarAsistenciaCobroControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $array_asistencia = array();
            $array_asistencia = $_POST['asistencia'];

            $it = new MultipleIterator();
            $it->attachIterator(new ArrayIterator($array_asistencia));

            foreach ($it as $dato) {
                $datos = array(
                    'id_log'          => $_POST['id_log'],
                    'id_cuenta_cobro' => $_POST['id_cobro'],
                    'id_asistencia'   => $dato[0],
                );

                $guardar = ModeloCobro::guardarDetallesCuentaCobroModel($datos);

            }

            if ($guardar == true) {
                $title   = 'Asistencias agregadas';
                $message = 'Se han agregado las asistencias';
                $enlace  = 'detalleCobro?cobro=' . base64_encode($_POST['id_cobro']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function subirDocumentosCuentaCobroControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            if (isset($_FILES['rut']['name']) && !empty($_FILES['rut']['name'])) {
                $nombre_rut = guardarArchivo($_FILES['rut']);
            }

            if (isset($_FILES['cedula']['name']) && !empty($_FILES['cedula']['name'])) {
                $nombre_cedula = guardarArchivo($_FILES['cedula']);
            }

            if (isset($_FILES['certificacion']['name']) && !empty($_FILES['certificacion']['name'])) {
                $nombre_certificacion = guardarArchivo($_FILES['certificacion']);
            }

            if (isset($_FILES['seguridad']['name']) && !empty($_FILES['seguridad']['name'])) {
                $nombre_seguridad = guardarArchivo($_FILES['seguridad']);
            }

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_cuenta_cobro' => $_POST['id_cuenta_cobro'],
                'rut'             => $nombre_rut,
                'cedula'          => $nombre_cedula,
                'certificacion'   => $nombre_certificacion,
                'seguridad'       => $nombre_seguridad,
            );

            $guardar = ModeloCobro::subirDocumentosCuentaCobroModel($datos);

            if ($guardar == true) {
                $title   = 'Documentos subidos';
                $message = 'Se han subido los documentos';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function estadoCuentaCobroControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_cuenta_cobro' => $_POST['id_cobro'],
                'horas_coord'     => $_POST['horas_coord'],
                'just_coord'      => $_POST['just_coord'],
                'estado'          => $_POST['estado'],
            );

            $guardar = ModeloCobro::estadoCuentaCobroModel($datos);

            $rs = ($guardar == true) ? 'ok' : 'no';
            return $rs;
        }
    }

    public function estadoCuentaCobroContControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_cuenta_cobro' => $_POST['id_cobro'],
                'horas_cont'      => $_POST['horas_cont'],
                'just_cont'       => $_POST['just_cont'],
                'hora_valor'      => str_replace(',', '', $_POST['horas_valor']),
                'horas_pagar'     => $_POST['horas_pagar'],
                'estado'          => $_POST['estado'],
            );

            $guardar = ModeloCobro::estadoContabilidadCuentaCobroModel($datos);

            $rs = ($guardar == true) ? 'ok' : 'no';
            return $rs;
        }
    }
}
