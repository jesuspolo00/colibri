<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';
require_once MODELO_PATH . 'hoja' . DS . 'ModeloHoja.php';
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlHoja
{

    private static $instancia;

    public static function singleton_hoja()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function tipoSangreControl()
    {
        $mostrar = ModeloHoja::tipoSangreModel();
        return $mostrar;
    }

    public function sexoGeneroControl()
    {
        $mostrar = ModeloHoja::sexoGeneroModel();
        return $mostrar;
    }

    public function estadoCivilControl()
    {
        $mostrar = ModeloHoja::estadoCivilModel();
        return $mostrar;
    }

    public function informacionHojaVidaControl($id)
    {
        $mostrar = ModeloHoja::informacionHojaVidaModel($id);
        return $mostrar;
    }

    public function preguntasHojaControl($id)
    {
        $mostrar = ModeloHoja::preguntasHojaModel($id);
        return $mostrar;
    }

    public function eliminarAcademicoControl($id)
    {
        $mostrar = ModeloHoja::eliminarAcademicoModel($id);
        return $mostrar;
    }

    public function eliminarLaboralControl($id)
    {
        $mostrar = ModeloHoja::eliminarLaboralModel($id);
        return $mostrar;
    }

    public function informacionAcademicaControl($id)
    {
        $mostrar = ModeloHoja::informacionAcademicaModel($id);
        return $mostrar;
    }

    public function informacionLaboralControl($id)
    {
        $mostrar = ModeloHoja::informacionLaboralModel($id);
        return $mostrar;
    }

    public function mostrarDocumentosTodosHojaControl($id)
    {
        $mostrar = ModeloHoja::mostrarDocumentosTodosHojaModel($id);
        return $mostrar;
    }

    public function mostrarDocumentosHojaControl($id, $perfil)
    {
        $mostrar = ModeloHoja::mostrarDocumentosHojaModel($id, $perfil);
        return $mostrar;
    }

    public function mostrarDocumentosHojaCidaProyectosControl($id)
    {
        $mostrar = ModeloHoja::mostrarDocumentosHojaCidaProyectosModel($id);
        return $mostrar;
    }

    public function mostrarDocumentosHojaRevisionControl($id, $perfil)
    {
        $mostrar = ModeloHoja::mostrarDocumentosHojaRevisionModel($id, $perfil);
        return $mostrar;
    }

    public function mostrarPreguntasEvaluacionControl($tipo)
    {
        $mostrar = ModeloHoja::mostrarPreguntasEvaluacionModel($tipo);
        return $mostrar;
    }

    public function spanEstadoAutorizacionControl($estado)
    {
        switch ($estado) {
            case 0:
            return '<span class="badge badge-warning">Pendiente de revision</span>';
            break;
            case 1:
            return '<span class="badge badge-success">Aprobado</span>';
            break;
            case 2:
            return '<span class="badge badge-danger">Denegado</span>';
            break;
            default:
            return '<span class="badge badge-secondary">No Aplica</span>';
            break;
        }
    }

    public function actualizarHojaVidaUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nom_archivo = $_POST['foto_ant'];

            if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {
                $eliminar    = eliminarArchivo($nom_archivo);
                $nom_archivo = guardarArchivo($_FILES['foto']);
            }

            $datos_usuario = array(
                'id_usuario' => $_POST['id_usuario'],
                'documento'  => $_POST['documento'],
                'nombre'     => $_POST['nombre'],
                'apellido'   => $_POST['apellido'],
                'correo'     => $_POST['correo'],
                'telefono'   => $_POST['telefono'],
            );

            $actualizar_usuario = ModeloHoja::actualizarUsuarioModel($datos_usuario);

            if ($actualizar_usuario == true) {

                $datos_hoja = array(
                    'id_hoja'            => $_POST['id_hoja'],
                    'sexo'               => $_POST['sexo'],
                    'celular'            => $_POST['celular'],
                    'direccion'          => $_POST['direccion'],
                    'lugar'              => $_POST['lugar'],
                    'barrio'             => $_POST['barrio'],
                    'fecha_nac'          => $_POST['fecha_nac'],
                    'lugar_nac'          => $_POST['lugar_nac'],
                    'tipo_sangre'        => $_POST['tipo_sangre'],
                    'exp_documento'      => $_POST['exp_documento'],
                    'estado_civil'       => $_POST['estado_civil'],
                    'escalafon'          => $_POST['escalafon'],
                    'nivel_academico'    => $_POST['nivel_academico'],
                    'calidad_desempeno'  => $_POST['calidad_desempeno'],
                    'especialidad'       => $_POST['especialidad'],
                    'fecha_ingreso'      => $_POST['fecha_ingreso'],
                    'tiempo_laboral'     => $_POST['tiempo_laboral'],
                    'tipo_vinculacion'   => $_POST['tipo_vinculacion'],
                    'origen_vinculacion' => $_POST['origen_vinculacion'],
                    'fuente_recursos'    => $_POST['fuente_recursos'],
                    'salario'            => str_replace(',', '', $_POST['salario']),
                    'cargo'              => $_POST['cargo'],
                    'foto'               => $nom_archivo,
                    'id_edit'            => $_POST['id_log'],
                );

                $actualizar_hoja = ModeloHoja::actualizarHojaVidaUsuarioModel($datos_hoja);

                if ($actualizar_hoja == true) {

                    $fecha_verificacion = ModeloHoja::mostrarFechaVerificacionModel($_POST['id_hoja']);

                    $mes_verificacion_ant  = date('n', strtotime($fecha_verificacion['fecha_verificacion']));
                    $anio_verificacion_ant = date('Y', strtotime($fecha_verificacion['fecha_verificacion']));

                    if ($mes_verificacion_ant == date('n') && $anio_verificacion_ant < date('Y') /*&& $fecha_verificacion['fecha_verificacion'] != '0000-00-00 00:00:00'*/) {

                        $fecha_verificacion_nueva = date('Y-m-d H:i:s');

                        $actualizar_fecha_verificacion = ModeloHoja::actualizarFechaVerificacionModel($_POST['id_hoja'], $fecha_verificacion_nueva);
                    }

                    $datos_preguntas = array(
                        'id_hoja'             => $_POST['id_hoja'],
                        'id_log'              => $_POST['id_log'],
                        'fecha_diligencia'    => $_POST['fecha_diligencia'],
                        'idiomas'             => $_POST['idiomas'],
                        'idiomas_nivel'       => $_POST['idiomas_nivel'],
                        'referencia_laboral'  => $_POST['referencia_laboral'],
                        'autorizo'            => $_POST['autorizo'],
                        'emergencia'          => $_POST['emergencia'],
                        'referencia_personal' => $_POST['referencia_personal'],
                        'libreta_militar'     => $_POST['libreta_militar'],
                    );

                    $guardar_preguntas = ModeloHoja::guardarPreguntasModel($datos_preguntas);

                    if ($guardar_preguntas == true) {
                        $title   = 'Datos actualizados';
                        $message = 'Se ha realizado la actualizacion de la hoja de vida';
                        $enlace  = BASE_URL . 'hoja' . DS . 'index?id=' . base64_encode($_POST['id_usuario']) . '&enlace=' . base64_encode($_POST['enlace']);
                        alertSuccess($title, $message, $enlace);
                    } else {
                        $title   = 'Error de actualizacion';
                        $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                        $enlace  = '';
                        alertDanger($title, $message, $enlace);
                    }
                }

            }

        }
    }

    public function agregarInformacionAcademicaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_hoja'     => $_POST['id_hoja'],
                'id_log'      => $_POST['id_log'],
                'institucion' => $_POST['institucion'],
                'ciudad'      => $_POST['ciudad'],
                'pais'        => $_POST['pais'],
                'nivel'       => $_POST['nivel'],
                'graduado'    => $_POST['graduado'],
                'fecha_grado' => (empty($_POST['fecha_grado'])) ? '0000-00-00' : $_POST['fecha_grado'],
                'titulo'      => $_POST['titulo'],
            );

            $guardar = ModeloHoja::agregarInformacionAcademicaModel($datos);

            if ($guardar['guardar'] == true) {
                $datos_mostrar = array('mensaje' => true, 'id' => $guardar['id'], 'datos' => $datos);
            } else {
                $datos_mostrar = array('mensaje' => false, 'id' => '', 'datos' => '');
            }

            return $datos_mostrar;

        }
    }

    public function agregarInformacionLaboralControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_hoja'       => $_POST['id_hoja'],
                'id_log'        => $_POST['id_log'],
                'empresa'       => $_POST['empresa'],
                'ciudad'        => $_POST['ciudad'],
                'pais'          => $_POST['pais'],
                'cargo'         => $_POST['cargo'],
                'telefono'      => $_POST['telefono'],
                'direccion'     => $_POST['direccion'],
                'fecha_ingreso' => $_POST['fecha_ingreso'],
                'fecha_salida'  => (empty($_POST['fecha_salida'])) ? '0000-00-00' : $_POST['fecha_salida'],
            );

            $guardar = ModeloHoja::agregarInformacionLaboralModel($datos);

            if ($guardar['guardar'] == true) {
                $datos_mostrar = array('mensaje' => true, 'id' => $guardar['id'], 'datos' => $datos);
            } else {
                $datos_mostrar = array('mensaje' => false, 'id' => '', 'datos' => '');
            }

            return $datos_mostrar;

        }
    }

    public function subirDocumentosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['enlace'] == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
            $url = ($_POST['enlace'] == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

            $array_documento = array();

            if (isset($_FILES['documento']) && !empty($_FILES['documento'])) {
                for ($i = 0; $i <= count($_FILES['documento']['name']) - 1; $i++) {
                    $array_documento[] = guardarVariosArchivos($_FILES['documento'], $i);
                }
            }

            $array_tipo = array();
            $array_tipo = $_POST['tipo_doc'];

            $it = new MultipleIterator();
            $it->attachIterator(new ArrayIterator($array_documento));
            $it->attachIterator(new ArrayIterator($array_tipo));

            foreach ($it as $dato) {

                $datos = array(
                    'id_user'   => $_POST['id_user'],
                    'id_hoja'   => $_POST['id_hoja'],
                    'id_log'    => $_POST['id_log'],
                    'documento' => $dato[0],
                    'tipo_doc'  => $dato[1],
                    'estado'    => 0,
                );

                $guardar = ModeloHoja::subirDocumentosModel($datos);

            }

            if ($guardar == true) {
                $title   = 'Documentos subidos';
                $message = 'Subida de documentos realizada';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function actualizarDocumentosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nom_archivo = '';

            if (isset($_FILES['documento']) && !empty($_FILES['documento'])) {
                $nom_archivo = guardarArchivo($_FILES['documento']);
            }

            if (!empty($_POST['archivo'])) {
                eliminarArchivo($_POST['archivo']);
            }

            $datos_act = array(
                'id_user'  => $_POST['id_user'],
                'id_hoja'  => $_POST['id_hoja_actual'],
                'tipo_doc' => $_POST['tipo_documento'],
            );

            $actualzar_documento = ModeloHoja::actualizarDocumentosModel($datos_act);

            if ($actualzar_documento == true) {

                $datos = array(
                    'id_user'   => $_POST['id_user'],
                    'id_hoja'   => $_POST['id_hoja_actual'],
                    'id_log'    => $_POST['id_log'],
                    'documento' => $nom_archivo,
                    'tipo_doc'  => $_POST['tipo_documento'],
                    'estado'    => 1,
                );

                $guardar = ModeloHoja::subirDocumentosModel($datos);

                if ($guardar == true) {
                    $title   = 'Documentos Actualizados';
                    $message = 'Actualizacion de documentos realizada';
                    $enlace  = BASE_URL . 'hoja' . DS . 'index?id=' . base64_encode($_POST['id_user']) . '&enlace=' . base64_encode($_POST['enlace']);
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de actualizacion';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }
        }
    }

    public function subirNuevoDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['url'] == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
            $url = ($_POST['url'] == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

            if (isset($_FILES['documento_nuevo']) && !empty($_FILES['documento_nuevo'])) {
                $nom_archivo = guardarArchivo($_FILES['documento_nuevo']);
            }

            $datos = array(
                'id_log'         => $_POST['id_log'],
                'id_user'        => $_POST['id_user'],
                'id_hoja'        => $_POST['id_hoja_documento'],
                'nom_archivo'    => $_POST['nom_archivo'],
                'tipo_documento' => $_POST['tipo_documento'],
                'perfil'         => $_POST['id_perfil'],
                'archivo'        => $nom_archivo,
                'estado'         => 1,
            );

            $guardar = ModeloHoja::subirNuevoDocumentoModel($datos);

            if ($guardar == true) {
                $title   = 'Documentos subido';
                $message = 'se ha subido el documento correctamente';
                $enlace  = BASE_URL . 'hoja' . DS . $url . '?id=' . base64_encode($_POST['id_user']) . '&enlace=' . base64_encode($_POST['enlace']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error al subir';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function revisionDocumentosHojaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_user'      => $_POST['id_user'],
                'id_documento' => $_POST['id_documento'],
                'id_perfil'    => $_POST['id_perfil'],
                'estado'       => $_POST['estado'],
                'observacion'  => $_POST['observacion'],
            );

            $guardar = ModeloHoja::revisionDocumentosHojaModel($datos);

            if ($guardar == true) {
                $title   = 'Estado actualizado';
                $message = 'Se ha realizado la revision del documento ' . $_POST['nom_documento'];
                $enlace  = BASE_URL . 'hoja' . DS . 'autorizar?id=' . base64_encode($_POST['id_user']) . '&enlace=' . base64_encode($_POST['enlace']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error al actualizar';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function finalizarRevisionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['enlace'] == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
            $url = ($_POST['enlace'] == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

            $buscar_aut = ModeloUsuario::autorizacionAnioDocumentosModel($_POST['id_user'], date('Y'));

            if (!empty($buscar_aut)) {

                $datos = array(
                    'id_hoja'             => $_POST['id_hoja'],
                    'id_log'              => $_POST['id_log'],
                    'id_user'             => $_POST['id_user'],
                    'fecha_autorizacion'  => date('Y-m-d'),
                    'cantidad_documentos' => $buscar_aut['cantidad_doc'] + $_POST['cantidad_doc'],
                    'id_aut'              => $buscar_aut['id'],
                );

                $guardar = ModeloHoja::actualizarAutorizacionModel($datos);

            } else {

                $datos = array(
                    'id_hoja'             => $_POST['id_hoja'],
                    'id_log'              => $_POST['id_log'],
                    'id_user'             => $_POST['id_user'],
                    'fecha_autorizacion'  => date('Y-m-d'),
                    'cantidad_documentos' => $_POST['cantidad_doc'],
                );

                $guardar = ModeloHoja::finalizarRevisionModel($datos);

            }

            if ($guardar == true) {
                $title   = 'Revision finalizada';
                $message = 'Se ha realizado la verificacion de documentos';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error al actualizar';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function finalizarEvaluacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $array_pregunta = array();
            $array_pregunta = $_POST['pregunta'];

            $array_puntuacion = array();
            $array_puntuacion = $_POST['punt'];

            $it = new MultipleIterator();
            $it->attachIterator(new ArrayIterator($array_pregunta));
            $it->attachIterator(new ArrayIterator($array_puntuacion));

            foreach ($it as $dato) {

                $datos = array(
                    'id_log'   => $_POST['id_log'],
                    'id_user'  => $_POST['id_user'],
                    'id_hoja'  => $_POST['id_hoja'],
                    'pregunta' => $dato[0],
                    'punt'     => $dato[1],
                );

                $guardar = ModeloHoja::finalizarEvaluacionModel($datos);

                if ($guardar == true) {
                    $title   = 'Evaluación finalizada';
                    $message = 'Se ha realizado la evaluacion de desempeño (Anual)';
                    $enlace  = BASE_URL . 'hoja' . DS . 'listado';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error al guardar';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }
            }

        }
    }

    public function verificarHojaVidaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['url'] == 1) ? BASE_URL . 'hoja/listado' : BASE_URL . 'proyectos/incluyeme/index';
            $url = ($_POST['url'] == 3) ? BASE_URL . 'proyectos/cdv/index' : $url;

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_user'     => $_POST['id_user'],
                'id_hoja'     => $_POST['id_hoja'],
                'estado'      => $_POST['estado'],
                'observacion' => $_POST['observacion'],
            );

            $guardar = ModeloHoja::verificarHojaVidaModel($datos);

            if ($guardar == true) {
                $title   = 'Verificación finalizada';
                $message = 'Se ha realizado la verificacion de la hoja de vida';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error al guardar';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }
}
