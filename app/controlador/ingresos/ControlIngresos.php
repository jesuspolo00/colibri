<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'ingresos' . DS . 'ModeloIngresos.php';
require_once CONTROL_PATH . 'messages.php';

class ControlIngresos
{

    private static $instancia;

    public static function singleton_ingresos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarMetodosPagoControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarMetodosPagoModel();
        return $mostrar;
    }

    public function mostrarDatosIngresoIdControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarDatosIngresoIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosEgresoIdControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarDatosEgresoIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosEgresoWsIdControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarDatosEgresoWsIdModel($id);
        return $mostrar;
    }

    public function mostrarDatosUsuariosIngresosControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarDatosUsuariosIngresosModel($id);
        return $mostrar;
    }

    public function mostrarHistorialIngresosUsuarioControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarHistorialIngresosUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarHistorialEgresosUsuarioControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarHistorialEgresosUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarHistorialEgresosWsUsuarioControl($id)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarHistorialEgresosWsUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarConceptoPagoControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::mostrarConceptoPagoModel();
        return $mostrar;
    }

    public function consultarDocumentoControl($documento)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::consultarDocumentoModel($documento);
        return $mostrar;
    }

    public function ultimosIngresosControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::ultimosIngresosModel();
        return $mostrar;
    }

    public function ultimosEgresosControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::ultimosEgresosModel();
        return $mostrar;
    }

    public function ultimosEgresosWsControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::ultimosEgresosWsModel();
        return $mostrar;
    }

    public function todosIngresosControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::todosIngresosModel();
        return $mostrar;
    }

    public function todosEgresosControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::todosEgresosModel();
        return $mostrar;
    }

    public function todosEgresosWsControl()
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::todosEgresosWsModel();
        return $mostrar;
    }

    public function anularIngresoControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::anularIngresoModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function autorizarIngresoControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::autorizarIngresoModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function anularEgresoControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::anularEgresoModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function autorizarEgresoControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::autorizarEgresoModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function anularEgresoWsControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::anularEgresoWsModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function autorizarEgresoWsControl($id, $log)
    {
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::autorizarEgresoWsModel($id, $log);

        $rs = ($mostrar == true) ? 'ok' : 'no';

        return $rs;
    }

    public function buscarIngresosControl($datos)
    {
        $concepto = ($datos['concepto'] == '') ? '' : ' AND ig.concepto = ' . $datos['concepto'];
        $metodo   = ($datos['metodo'] == '') ? '' : ' AND ig.metodo_pago = ' . $datos['metodo'];

        $datos    = array('concepto' => $concepto, 'metodo' => $metodo, 'buscar' => $datos['buscar']);
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::buscarIngresosModel($datos);
        return $mostrar;
    }

    public function buscarEgresosControl($datos)
    {
        $concepto = ($datos['concepto'] == '') ? '' : ' AND ig.concepto = ' . $datos['concepto'];
        $metodo   = ($datos['metodo'] == '') ? '' : ' AND ig.metodo_pago = ' . $datos['metodo'];

        $datos    = array('concepto' => $concepto, 'metodo' => $metodo, 'buscar' => $datos['buscar']);
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::buscarEgresosModel($datos);
        return $mostrar;
    }

    public function buscarEgresosWsControl($datos)
    {
        $concepto = ($datos['concepto'] == '') ? '' : ' AND ig.concepto = ' . $datos['concepto'];
        $metodo   = ($datos['metodo'] == '') ? '' : ' AND ig.metodo_pago = ' . $datos['metodo'];

        $datos    = array('concepto' => $concepto, 'metodo' => $metodo, 'buscar' => $datos['buscar']);
        $consulta = ModeloIngresos::comandoSQL();
        $mostrar  = ModeloIngresos::buscarEgresosWsModel($datos);
        return $mostrar;
    }

    public function registrarIngresoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            if ($_POST['id_usuario'] != 0 || $_POST['id_usuario'] != '') {

                $datos_usuario = array(
                    'id_log'     => $_POST['id_log'],
                    'id_usuario' => $_POST['id_usuario'],
                    'documento'  => $_POST['documento'],
                    'nombre'     => $_POST['nombre'],
                    'ciudad'     => $_POST['ciudad'],
                );

                $actualizar_usuario = ModeloIngresos::actualizarUsuarioIngresosModel($datos_usuario);

                if ($actualizar_usuario == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $_POST['id_usuario'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'observacion' => $_POST['observacion'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                    );

                    $guardar = ModeloIngresos::registrarIngresoModel($datos);
                }

            } else {

                $datos_usuario = array(
                    'id_log'    => $_POST['id_log'],
                    'documento' => $_POST['documento'],
                    'nombre'    => $_POST['nombre'],
                    'ciudad'    => $_POST['ciudad'],
                );

                $guardar_usuario = ModeloIngresos::registrarUsuarioIngresoModel($datos_usuario);

                if ($guardar_usuario['guardar'] == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $guardar_usuario['id'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'observacion' => $_POST['observacion'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                    );

                    $guardar = ModeloIngresos::registrarIngresoModel($datos);
                }
            }

            if ($guardar['guardar'] == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha registrado el ingreso Numero ' . $guardar['id']);
                $log       = ModeloIngresos::historyLogModel($datos_log);

                $title   = 'Ingreso registrado';
                $message = 'Se ha registrado el ingreso';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function registrarEgresosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            if ($_POST['id_usuario'] != 0 || $_POST['id_usuario'] != '') {

                $datos_usuario = array(
                    'id_log'     => $_POST['id_log'],
                    'id_usuario' => $_POST['id_usuario'],
                    'documento'  => $_POST['documento'],
                    'nombre'     => $_POST['nombre'],
                    'ciudad'     => $_POST['ciudad'],
                );

                $actualizar_usuario = ModeloIngresos::actualizarUsuarioIngresosModel($datos_usuario);

                if ($actualizar_usuario == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $_POST['id_usuario'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                        'observacion' => $_POST['observacion'],
                    );

                    $guardar = ModeloIngresos::registrarEgresosModel($datos);
                }

            } else {

                $datos_usuario = array(
                    'id_log'    => $_POST['id_log'],
                    'documento' => $_POST['documento'],
                    'nombre'    => $_POST['nombre'],
                    'ciudad'    => $_POST['ciudad'],
                );

                $guardar_usuario = ModeloIngresos::registrarUsuarioIngresoModel($datos_usuario);

                if ($guardar_usuario['guardar'] == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $guardar_usuario['id'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                        'observacion' => $_POST['observacion'],
                    );

                    $guardar = ModeloIngresos::registrarEgresosModel($datos);
                }
            }

            if ($guardar['guardar'] == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha registrado el egreso Numero ' . $guardar['id']);
                $log       = ModeloIngresos::historyLogModel($datos_log);

                $title   = 'Egreso registrado';
                $message = 'Se ha registrado el egreso';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function registrarEgresosWsControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            if ($_POST['id_usuario'] != 0 || $_POST['id_usuario'] != '') {

                $datos_usuario = array(
                    'id_log'     => $_POST['id_log'],
                    'id_usuario' => $_POST['id_usuario'],
                    'documento'  => $_POST['documento'],
                    'nombre'     => $_POST['nombre'],
                    'ciudad'     => $_POST['ciudad'],
                );

                $actualizar_usuario = ModeloIngresos::actualizarUsuarioIngresosModel($datos_usuario);

                if ($actualizar_usuario == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $_POST['id_usuario'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                        'observacion' => $_POST['observacion'],
                    );

                    $guardar = ModeloIngresos::registrarEgresosWsModel($datos);
                }

            } else {

                $datos_usuario = array(
                    'id_log'    => $_POST['id_log'],
                    'documento' => $_POST['documento'],
                    'nombre'    => $_POST['nombre'],
                    'ciudad'    => $_POST['ciudad'],
                );

                $guardar_usuario = ModeloIngresos::registrarUsuarioIngresoModel($datos_usuario);

                if ($guardar_usuario['guardar'] == true) {

                    $datos = array(
                        'id_log'      => $_POST['id_log'],
                        'id_usuario'  => $guardar_usuario['id'],
                        'fecha'       => $_POST['fecha'],
                        'concepto'    => $_POST['concepto'],
                        'pago'        => $_POST['pago'],
                        'valor'       => $valor,
                        'tipo'        => $_POST['tipo'],
                        'banco'       => $_POST['banco'],
                        'cheque'      => $_POST['cheque'],
                        'observacion' => $_POST['observacion'],
                    );

                    $guardar = ModeloIngresos::registrarEgresosWsModel($datos);
                }
            }

            if ($guardar['guardar'] == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha registrado el egreso Numero ' . $guardar['id']);
                $log       = ModeloIngresos::historyLogModel($datos_log);

                $title   = 'Egreso registrado';
                $message = 'Se ha registrado el egreso';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function agregarConceptoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos   = array('nombre' => $_POST['nom_concepto'], 'id_log' => $_POST['id_log']);
            $guardar = ModeloIngresos::agregarConceptoModel($datos);

            if ($guardar == true) {

                $datos_log = array('id_user' => $_POST['id_log'], 'accion' => 'Ha registrado el concepto en ingresos Numero ' . $guardar['id']);
                $log       = ModeloIngresos::historyLogModel($datos_log);

                $title   = 'Concepto registrado';
                $message = 'Se ha registrado el concepto';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
