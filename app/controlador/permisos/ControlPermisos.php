<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlPermisos
{

    private static $instancia;

    public static function singleton_permisos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consultarPermisosPerfilControl($perfil, $opcion)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::consultarPermisosPerfilModel($perfil, $opcion);
        return $mostrar;
    }

    public function mostrarOpcionesPermisosControl()
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::mostrarOpcionesPermisosModel();
        return $mostrar;
    }

    public function opcionsIdActivosPerfilControl($id_perfil, $id_opcion)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::opcionsIdActivosPerfilModel($id_perfil, $id_opcion);
        return $mostrar;
    }

    public function verificarTokenUsoControl($token)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::verificarTokenUsoModel($token);
        return $mostrar;
    }

    public function aprobarPermisoControl($id)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::aprobarPermisoModel($id);

        if ($mostrar == true) {

            $datos_permiso = ModeloPermisos::mostrarDatosPermisoModel($id);
            $datos_usuario = ModeloPerfil::mostrarDatosPerfilModel($datos_permiso['id_user']);

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de permiso laboral <b>No. ' . $id . '</b> para el dia
            <b>
            ' . $datos_permiso['fecha'] . '
            </b>
            <ul style="font-size: 1.2em;">
            <li><b>Documento:</b> ' . $datos_usuario['documento'] . '</li>
            <li><b>Nombre Completo:</b> ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</li>
            <li><b>Hora Inicio:</b> ' . $datos_permiso['hora_inicio'] . '</li>
            <li><b>Hora Fin:</b> ' . $datos_permiso['hora_fin'] . '</li>
            <li><b>Motivo:</b> ' . $datos_permiso['motivo'] . '</li>
            <li><b>Estado de la solicitud:</b> Aprobada</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de permiso laboral',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => array($datos_permiso['documento_soporte']),
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function denegarPermisoControl($id)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::denegarPermisoModel($id);

        if ($mostrar == true) {

            $datos_permiso = ModeloPermisos::mostrarDatosPermisoModel($id);
            $datos_usuario = ModeloPerfil::mostrarDatosPerfilModel($datos_permiso['id_user']);

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de permiso laboral <b>No. ' . $id . '</b> para el dia
            <b>
            ' . $datos_permiso['fecha'] . '
            </b>
            <ul style="font-size: 1.2em;">
            <li><b>Documento:</b> ' . $datos_usuario['documento'] . '</li>
            <li><b>Nombre Completo:</b> ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</li>
            <li><b>Hora Inicio:</b> ' . $datos_permiso['hora_inicio'] . '</li>
            <li><b>Hora Fin:</b> ' . $datos_permiso['hora_fin'] . '</li>
            <li><b>Motivo:</b> ' . $datos_permiso['motivo'] . '</li>
            <li><b>Estado de la solicitud:</b> Denegada</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de permiso laboral',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => array($datos_permiso['documento_soporte']),
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function mostrarListadoPermisosControl()
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::mostrarListadoPermisosModel();
        return $mostrar;
    }

    public function mostraPermisosUsuariosControl($id)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::mostraPermisosUsuariosModel($id);
        return $mostrar;
    }

    public function mostrarDatosPermisoIdControl($id)
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::mostrarDatosPermisoIdModel($id);
        return $mostrar;
    }

    public function mostrarListadoTodosPermisosControl()
    {
        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::mostrarListadoTodosPermisosModel();
        return $mostrar;
    }

    public function buscarPermisosSolicitadosControl($datos)
    {

        $usuario = ($datos['usuario'] == '') ? '' : ' AND u.id_user = ' . $datos['usuario'];
        $fecha   = ($datos['fecha'] == '') ? '' : ' AND p.fecha = "' . $datos['fecha'] . '"';

        $datos = array('usuario' => $usuario, 'fecha' => $fecha, 'buscar' => $_POST['buscar']);

        $consulta = ModeloPermisos::comandoSQL();
        $mostrar  = ModeloPermisos::buscarPermisosSolicitadosModel($datos);
        return $mostrar;
    }

    public function activarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $eliminar = ModeloPermisos::inactivarPermisoModel($datos);

            if ($eliminar == true) {
                $activar = ModeloPermisos::activarPermisoModel($datos);
            }

            return $activar;
        }
    }

    public function inactivarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $inactivar = ModeloPermisos::inactivarPermisoModel($datos);
            return $inactivar;
        }
    }

    /*----------------------------------*/

    public function registrarPermisoLaboralControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['documento']['name']) && !empty($_FILES['documento']['name'])) {

                $archivo   = $_FILES['documento']['name'];
                $extension = pathinfo($archivo, PATHINFO_EXTENSION);

                $nombre_archivo = strtolower(md5($_POST['id_user'] . '_' . date('YmdHis'))) . '.' . $extension;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($extension == 'png' || $extension == 'jpeg') {
                    $compressed = compressImage($_FILES['documento']['tmp_name'], $ruta_img, 70);
                } else {

                    if (is_uploaded_file($_FILES['documento']['tmp_name'])) {
                        move_uploaded_file($_FILES['documento']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_user'     => $_POST['id_user'],
                'sitio'       => $_POST['sitio'],
                'hora_inicio' => $_POST['hora_inicio'],
                'hora_fin'    => $_POST['hora_fin'],
                'motivo'      => $_POST['motivo'],
                'horario'     => '',
                'documento'   => $nombre_archivo,
                'fecha'       => $_POST['fecha'],
                'token'       => $this->generar_token_seguro(150),
            );

            $guardar = ModeloPermisos::registrarPermisoLaboralModel($datos);

            if ($guardar['guardar'] == true) {

                $datos_usuario = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_user']);
                $datos_permiso = ModeloPermisos::mostrarDatosPermisoModel($guardar['id']);

                $aprobar = BASE_URL . 'permisos/token/aprobar?token=' . $datos_permiso['token'];
                $denegar = BASE_URL . 'permisos/token/denegar?token=' . $datos_permiso['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado un permiso laboral para el dia
                <b>
                ' . $datos_permiso['fecha'] . '
                </b>
                <ul style="font-size: 1.2em;">
                <li><b>Documento:</b> ' . $datos_usuario['documento'] . '</li>
                <li><b>Nombre Completo:</b> ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</li>
                <li><b>Hora Inicio:</b> ' . $datos_permiso['hora_inicio'] . '</li>
                <li><b>Hora Fin:</b> ' . $datos_permiso['hora_fin'] . '</li>
                <li><b>Motivo:</b> ' . $datos_permiso['motivo'] . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Codetec</b> puede <b><a href="' . $aprobar . '" target="_blank" style="color: green;">Aprobar esta solicitud</a></b> o <b><a href="' . $denegar . '" target="_blank" style="color: red;">Denegar esta solicitud</a></b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de permiso laboral',
                    'mensaje' => $mensaje_correo_admin,
                    'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => array($datos_permiso['documento_soporte']),
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado un permiso laboral para el dia
                <b>
                ' . $datos_permiso['fecha'] . '
                </b>
                <ul style="font-size: 1.2em;">
                <li><b>Documento:</b> ' . $datos_usuario['documento'] . '</li>
                <li><b>Nombre Completo:</b> ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</li>
                <li><b>Hora Inicio:</b> ' . $datos_permiso['hora_inicio'] . '</li>
                <li><b>Hora Fin:</b> ' . $datos_permiso['hora_fin'] . '</li>
                <li><b>Motivo:</b> ' . $datos_permiso['motivo'] . '</li>
                <li><b>Estado de la solicitud:</b> Pendiente</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de permiso laboral',
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    'archivo' => array($datos_permiso['documento_soporte']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);

                $title   = 'Permiso solicitado';
                $message = 'Se ha registrado el permiso laboral';
                $enlace  = BASE_URL . 'permisos' . DS . 'solicitud';
                alertSuccess($title, $message, $enlace);

            } else {
                $title   = 'Error de verificacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function generar_token_seguro($longitud)
    {
        if ($longitud < 4) {
            $longitud = 4;
        }

        return bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
    }

}
