<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'aprobacion' . DS . 'ModeloAprobacion.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlAprobacion
{

    private static $instancia;

    public static function singleton_aprobacion()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function verificarTokenUsoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::verificarTokenUsoModel($token);
        return $mostrar;
    }

    public function verificarWsTokenUsoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::verificarWsTokenUsoModel($token);
        return $mostrar;
    }

    public function verificarFundepexTokenUsoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::verificarFundepexTokenUsoModel($token);
        return $mostrar;
    }

    public function verificarTalentoTokenUsoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::verificarTalentoTokenUsoModel($token);
        return $mostrar;
    }

    public function verificarAidaBarriosTokenUsoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::verificarAidaBarriosTokenUsoModel($token);
        return $mostrar;
    }

    public function mostrarSolicitudesAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarSolicitudesAprobacionModel();
        return $mostrar;
    }

    public function mostrarAprobacionUsuarioControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarAprobacionUsuarioModel($id);
        return $mostrar;
    }

    public function buscarSolicitudAprobacionUsuarioControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudAprobacionUsuarioModel($datos);
        return $mostrar;
    }

    public function mostrarAprobacionWsUsuarioControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarAprobacionWsUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarAprobacionFundepexUsuarioControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarAprobacionFundepexUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarAprobacionTalentoUsuarioControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarAprobacionTalentoUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarAprobacionAidaBarriosUsuarioControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarAprobacionAidaBarriosUsuarioModel($id);
        return $mostrar;
    }

    public function buscarSolicitudAprobacionWsUsuarioControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudAprobacionWsUsuarioModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudAprobacionFundepexUsuarioControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudAprobacionFundepexUsuarioModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudAprobacionTalentoUsuarioControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudAprobacionTalentoUsuarioModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudAprobacionAidaBarriosUsuarioControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudAprobacionAidaBarriosUsuarioModel($datos);
        return $mostrar;
    }

    public function mostrarSolicitudesWsAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarSolicitudesWsAprobacionModel();
        return $mostrar;
    }

    public function mostrarSolicitudesFundepexAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarSolicitudesFundepexAprobacionModel();
        return $mostrar;
    }

    public function mostrarSolicitudesTalentoAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarSolicitudesTalentoAprobacionModel();
        return $mostrar;
    }

    public function mostrarSolicitudesAidaBarriosAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarSolicitudesAidaBarriosAprobacionModel();
        return $mostrar;
    }

    public function mostrarTodasSolicitudesAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarTodasSolicitudesAprobacionModel();
        return $mostrar;
    }

    public function mostrarTodasSolicitudesWsAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarTodasSolicitudesWsAprobacionModel();
        return $mostrar;
    }

    public function mostrarTodasSolicitudesFundepexAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarTodasSolicitudesFundepexAprobacionModel();
        return $mostrar;
    }

    public function mostrarTodasSolicitudesTalentoAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarTodasSolicitudesTalentoAprobacionModel();
        return $mostrar;
    }

    public function mostrarTodasSolicitudesAidaBarriosAprobacionControl()
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarTodasSolicitudesAidaBarriosAprobacionModel();
        return $mostrar;
    }

    public function removerEstadoSolicitudControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::removerEstadoSolicitudModel($token);
        return $mostrar;
    }

    public function removerEstadoSolicitudWsControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::removerEstadoSolicitudWsModel($token);
        return $mostrar;
    }

    public function removerEstadoSolicitudFundepexControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::removerEstadoSolicitudFundepexModel($token);
        return $mostrar;
    }

    public function removerEstadoSolicitudTalentoControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::removerEstadoSolicitudTalentoModel($token);
        return $mostrar;
    }

    public function removerEstadoSolicitudAidaBarriosControl($token)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::removerEstadoSolicitudAidaBarriosModel($token);
        return $mostrar;
    }

    public function buscarSolicitudesAprobacionControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudesAprobacionModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudesWsAprobacionControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudesWsAprobacionModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudesFundepexAprobacionControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudesFundepexAprobacionModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudesTalentoAprobacionControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudesTalentoAprobacionModel($datos);
        return $mostrar;
    }

    public function buscarSolicitudesAidaBarriosAprobacionControl($datos)
    {

        $proyecto = (empty($datos['proyecto'])) ? '' : ' AND ap.id_proyecto = ' . $datos['proyecto'];
        $fecha    = (empty($datos['fecha'])) ? '' : ' AND ap.fechareg LIKE "%' . $datos['fecha'] . '%"';

        $datos = array('fecha' => $fecha, 'proyecto' => $proyecto, 'buscar' => $datos['buscar']);

        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::buscarSolicitudesAidaBarriosAprobacionModel($datos);
        return $mostrar;
    }

    public function mostrarInformacionAprobacionPagoControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarInformacionAprobacionPagoModel($id);
        return $mostrar;
    }

    public function mostrarInformacionWsAprobacionPagoControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarInformacionWsAprobacionPagoModel($id);
        return $mostrar;
    }

    public function mostrarInformacionFundepexAprobacionPagoControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarInformacionFundepexAprobacionPagoModel($id);
        return $mostrar;
    }

    public function mostrarInformacionTalentoAprobacionPagoControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarInformacionTalentoAprobacionPagoModel($id);
        return $mostrar;
    }

    public function mostrarInformacionAidaBarriosAprobacionPagoControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarInformacionAidaBarriosAprobacionPagoModel($id);
        return $mostrar;
    }

    public function mostrarArchivosAprobacionControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarArchivosAprobacionModel($id);
        return $mostrar;
    }

    public function mostrarArchivosWsAprobacionControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarArchivosWsAprobacionModel($id);
        return $mostrar;
    }

    public function mostrarArchivosFundepexAprobacionControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarArchivosFundepexAprobacionModel($id);
        return $mostrar;
    }

    public function mostrarArchivosTalentoAprobacionControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarArchivosTalentoAprobacionModel($id);
        return $mostrar;
    }

    public function mostrarArchivosAidaBarriosAprobacionControl($id)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::mostrarArchivosAidaBarriosAprobacionModel($id);
        return $mostrar;
    }

    public function aprobarSolicitudPagoControl($id, $log, $tipo_pago)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::aprobarSolicitudPagoModel($id, $log, $tipo_pago);
        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }
        return $mostrar;
    }

    public function aprobarWsSolicitudPagoControl($id, $log, $tipo_pago)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::aprobarWsSolicitudPagoModel($id, $log, $tipo_pago);
        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosWsAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosWsAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }
        return $mostrar;
    }

    public function aprobarFundepexSolicitudPagoControl($id, $log, $tipo_pago)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::aprobarFundepexSolicitudPagoModel($id, $log, $tipo_pago);
        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosFundepexAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosFundepexAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }
        return $mostrar;
    }

    public function aprobarTalentoSolicitudPagoControl($id, $log, $tipo_pago)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::aprobarTalentoSolicitudPagoModel($id, $log, $tipo_pago);
        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosTalentoAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosTalentoAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }
        return $mostrar;
    }

    public function aprobarAidaBarriosSolicitudPagoControl($id, $log, $tipo_pago)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::aprobarAidaBarriosSolicitudPagoModel($id, $log, $tipo_pago);
        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosAidaBarriosAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosAidaBarriosAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>APROBADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }
        return $mostrar;
    }

    public function denegarSolicitudPagoControl($id, $log)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::denegarSolicitudPagoModel($id, $log);

        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function denegarWsSolicitudPagoControl($id, $log)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::denegarWsSolicitudPagoModel($id, $log);

        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosWsAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosWsAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function denegarFundepexSolicitudPagoControl($id, $log)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::denegarFundepexSolicitudPagoModel($id, $log);

        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosFundepexAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosFundepexAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function denegarTalentoSolicitudPagoControl($id, $log)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::denegarTalentoSolicitudPagoModel($id, $log);

        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosTalentoAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosTalentoAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function denegarAidaBarriosSolicitudPagoControl($id, $log)
    {
        $consulta = ModeloAprobacion::comandoSQL();
        $mostrar  = ModeloAprobacion::denegarAidaBarriosSolicitudPagoModel($id, $log);

        if ($mostrar == true) {

            $datos_aprobacion = ModeloAprobacion::mostrarDatosAidaBarriosAprobacionIdModel($id);
            $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($datos_aprobacion['id_user']);
            $datos_archivos   = ModeloAprobacion::mostrarArchivosAidaBarriosAprobacionModel($datos_aprobacion['id']);

            $archivos = [];

            foreach ($datos_archivos as $archivo) {
                $archivos[] = $archivo['archivo'];
            }

            $mensaje_correo = '<html><body>
            <p style="font-size: 1.2em;">
            Se ha <b>DENEGADO</b> tu solicitud de pago <b>No. ' . $id . '</b> con la siguiente informacion.
            <ul style="font-size: 1.2em;">
            <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
            <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
            <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
            <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
            </ul>
            </body>
            </html>
            ';

            $datos_correo = array(
                'asunto'  => 'Solictud de aprobacion de pago',
                'mensaje' => $mensaje_correo,
                //'correo'  => array('jesuspolo00@gmail.com'),
                'correo'  => array($datos_usuario['correo']),
                'archivo' => $archivos,
            );

            $envio = Correo::enviarCorreoModel($datos_correo);
            /*---------------------------------------*/
        }

        return $mostrar;
    }

    public function registrarSolicitudAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'nom_archivo' => $_POST['nom_archivo'],
                'archivo'     => '',
                'concepto'    => $_POST['concepto'],
                'token'       => $this->generar_token_seguro(150),
                'id_proyecto' => $_POST['proyecto'],
                'valor'       => $valor,
            );

            $guardar = ModeloAprobacion::registrarSolicitudAprobacionModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_FILES) && !empty($_FILES)) {

                    for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                        $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                        $datos_archivo = array('id_aprobacion' => $guardar['id'], 'archivo' => $nombre_archivo);

                        $guardar_archivo = ModeloAprobacion::guardarArchivoAprobacionModel($datos_archivo);

                    }
                }

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosAprobacionIdModel($guardar['id']);
                $datos_archivos   = ModeloAprobacion::mostrarArchivosAprobacionModel($guardar['id']);

                $archivos = [];

                foreach ($datos_archivos as $archivo) {
                    $archivos[] = $archivo['archivo'];
                }

                $aprobar = BASE_URL . 'aprobacion/token/aprobar?token=' . $datos_aprobacion['token'];
                $denegar = BASE_URL . 'aprobacion/token/denegar?token=' . $datos_aprobacion['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Colibri.</b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de aprobacion de pago',
                    'mensaje' => $mensaje_correo_admin,
                    'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => $archivos,
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de aprobacion de pago',
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => $archivos,
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Solicitud registrada';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_aprobacion' => $_POST['id_aprobacion'],
                'nom_archivo'   => $_POST['nom_archivo'],
                'archivo'       => '',
                'concepto'      => $_POST['concepto'],
                'id_proyecto'   => $_POST['proyecto'],
                'valor'         => $valor,
                'fecha_sol'     => $_POST['fecha_sol'] . date(' H:i:s'),
            );

            $editar = ModeloAprobacion::editarAprobacionModel($datos);

            if (isset($_FILES) && !empty($_FILES)) {

                $archivos_aprobacion = ModeloAprobacion::mostrarArchivosAprobacionModel($_POST['id_aprobacion']);

                for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                    $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                    if ($nombre_archivo != '') {

                        foreach ($archivos_aprobacion as $archivo) {
                            $eliminar         = eliminarArchivo($archivo['archivo']);
                            $eliminar_archivo = ModeloAprobacion::eliminarArchivoAprobacionModel($archivo['id']);
                        }

                        $datos_archivo   = array('id_aprobacion' => $_POST['id_aprobacion'], 'archivo' => $nombre_archivo);
                        $guardar_archivo = ModeloAprobacion::guardarArchivoAprobacionModel($datos_archivo);
                    }

                }
            }

            if ($editar == true) {
                $title   = 'Solicitud editada';
                $message = 'Se ha editado la solicitud';
                $enlace  = 'editar?aprobacion=' . base64_encode($_POST['id_aprobacion']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAprobacionWsControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_aprobacion' => $_POST['id_aprobacion'],
                'nom_archivo'   => $_POST['nom_archivo'],
                'archivo'       => '',
                'concepto'      => $_POST['concepto'],
                'id_proyecto'   => $_POST['proyecto'],
                'valor'         => $valor,
                'fecha_sol'     => $_POST['fecha_sol'] . date(' H:i:s'),
            );

            $editar = ModeloAprobacion::editarAprobacionWsModel($datos);

            if (isset($_FILES) && !empty($_FILES)) {

                $archivos_aprobacion = ModeloAprobacion::mostrarArchivosWsAprobacionModel($_POST['id_aprobacion']);

                for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                    $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                    if ($nombre_archivo != '') {

                        foreach ($archivos_aprobacion as $archivo) {
                            $eliminar         = eliminarArchivo($archivo['archivo']);
                            $eliminar_archivo = ModeloAprobacion::eliminarArchivoAprobacionWsModel($archivo['id']);
                        }

                        $datos_archivo   = array('id_aprobacion' => $_POST['id_aprobacion'], 'archivo' => $nombre_archivo);
                        $guardar_archivo = ModeloAprobacion::guardarArchivoWsAprobacionModel($datos_archivo);
                    }

                }
            }

            if ($editar == true) {
                $title   = 'Solicitud editada';
                $message = 'Se ha editado la solicitud';
                $enlace  = 'editar?aprobacion=' . base64_encode($_POST['id_aprobacion']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAprobacionFundepexControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_aprobacion' => $_POST['id_aprobacion'],
                'nom_archivo'   => $_POST['nom_archivo'],
                'archivo'       => '',
                'concepto'      => $_POST['concepto'],
                'id_proyecto'   => $_POST['proyecto'],
                'valor'         => $valor,
                'fecha_sol'     => $_POST['fecha_sol'] . date(' H:i:s'),
            );

            $editar = ModeloAprobacion::editarAprobacionFundepexModel($datos);

            if (isset($_FILES) && !empty($_FILES)) {

                $archivos_aprobacion = ModeloAprobacion::mostrarArchivosFundepexAprobacionModel($_POST['id_aprobacion']);

                for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                    $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                    if ($nombre_archivo != '') {

                        foreach ($archivos_aprobacion as $archivo) {
                            $eliminar         = eliminarArchivo($archivo['archivo']);
                            $eliminar_archivo = ModeloAprobacion::eliminarArchivoAprobacionFundepexModel($archivo['id']);
                        }

                        $datos_archivo   = array('id_aprobacion' => $_POST['id_aprobacion'], 'archivo' => $nombre_archivo);
                        $guardar_archivo = ModeloAprobacion::guardarArchivoFundepexAprobacionModel($datos_archivo);
                    }

                }
            }

            if ($editar == true) {
                $title   = 'Solicitud editada';
                $message = 'Se ha editado la solicitud';
                $enlace  = 'editar?aprobacion=' . base64_encode($_POST['id_aprobacion']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAprobacionTalentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_aprobacion' => $_POST['id_aprobacion'],
                'nom_archivo'   => $_POST['nom_archivo'],
                'archivo'       => '',
                'concepto'      => $_POST['concepto'],
                'id_proyecto'   => $_POST['proyecto'],
                'valor'         => $valor,
                'fecha_sol'     => $_POST['fecha_sol'] . date(' H:i:s'),
            );

            $editar = ModeloAprobacion::editarAprobacionTalentoModel($datos);

            if (isset($_FILES) && !empty($_FILES)) {

                $archivos_aprobacion = ModeloAprobacion::mostrarArchivosTalentoAprobacionModel($_POST['id_aprobacion']);

                for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                    $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                    if ($nombre_archivo != '') {

                        foreach ($archivos_aprobacion as $archivo) {
                            $eliminar         = eliminarArchivo($archivo['archivo']);
                            $eliminar_archivo = ModeloAprobacion::eliminarArchivoAprobacionTalentoModel($archivo['id']);
                        }

                        $datos_archivo   = array('id_aprobacion' => $_POST['id_aprobacion'], 'archivo' => $nombre_archivo);
                        $guardar_archivo = ModeloAprobacion::guardarArchivoTalentoAprobacionModel($datos_archivo);
                    }

                }
            }

            if ($editar == true) {
                $title   = 'Solicitud editada';
                $message = 'Se ha editado la solicitud';
                $enlace  = 'editar?aprobacion=' . base64_encode($_POST['id_aprobacion']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAprobacionAidaBarriosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_aprobacion' => $_POST['id_aprobacion'],
                'nom_archivo'   => $_POST['nom_archivo'],
                'archivo'       => '',
                'concepto'      => $_POST['concepto'],
                'id_proyecto'   => $_POST['proyecto'],
                'valor'         => $valor,
                'fecha_sol'     => $_POST['fecha_sol'] . date(' H:i:s'),
            );

            $editar = ModeloAprobacion::editarAprobacionAidaBarriosModel($datos);

            if (isset($_FILES) && !empty($_FILES)) {

                $archivos_aprobacion = ModeloAprobacion::mostrarArchivosAidaBarriosAprobacionModel($_POST['id_aprobacion']);

                for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                    $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                    if ($nombre_archivo != '') {

                        foreach ($archivos_aprobacion as $archivo) {
                            $eliminar         = eliminarArchivo($archivo['archivo']);
                            $eliminar_archivo = ModeloAprobacion::eliminarArchivoAprobacionAidaBarriosModel($archivo['id']);
                        }

                        $datos_archivo   = array('id_aprobacion' => $_POST['id_aprobacion'], 'archivo' => $nombre_archivo);
                        $guardar_archivo = ModeloAprobacion::guardarArchivoAidaBarriosAprobacionModel($datos_archivo);
                    }

                }
            }

            if ($editar == true) {
                $title   = 'Solicitud editada';
                $message = 'Se ha editado la solicitud';
                $enlace  = 'editar?aprobacion=' . base64_encode($_POST['id_aprobacion']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registrarSolicitudWsAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'nom_archivo' => $_POST['nom_archivo'],
                'archivo'     => '',
                'concepto'    => $_POST['concepto'],
                'token'       => $this->generar_token_seguro(150),
                'id_proyecto' => $_POST['proyecto'],
                'valor'       => $valor,
            );

            $guardar = ModeloAprobacion::registrarSolicitudWsAprobacionModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_FILES) && !empty($_FILES)) {

                    for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                        $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                        $datos_archivo = array('id_aprobacion' => $guardar['id'], 'archivo' => $nombre_archivo);

                        $guardar_archivo = ModeloAprobacion::guardarArchivoWsAprobacionModel($datos_archivo);

                    }
                }

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosWsAprobacionIdModel($guardar['id']);
                $datos_archivos   = ModeloAprobacion::mostrarArchivosWsAprobacionModel($guardar['id']);

                $archivos = [];

                foreach ($datos_archivos as $archivo) {
                    $archivos[] = $archivo['archivo'];
                }

                $aprobar = BASE_URL . 'aprobacion_ws/token/aprobar?token=' . $datos_aprobacion['token'];
                $denegar = BASE_URL . 'aprobacion_ws/token/denegar?token=' . $datos_aprobacion['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Colibri.</b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de WS aprobacion de pago',
                    'mensaje' => $mensaje_correo_admin,
                    'correo'  => array('orlando@codetec.edu.co'),
                    //'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => $archivos,
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de WS aprobacion de pago',
                    'mensaje' => $mensaje_correo,
//                    'correo'  => array('orlando@codetec.edu.co'),
                    'correo'  => array($datos_usuario['correo']),
                    'archivo' => $archivos,
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Solicitud registrada';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registrarSolicitudFundepexAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'nom_archivo' => $_POST['nom_archivo'],
                'archivo'     => '',
                'concepto'    => $_POST['concepto'],
                'token'       => $this->generar_token_seguro(150),
                'id_proyecto' => $_POST['proyecto'],
                'valor'       => $valor,
            );

            $guardar = ModeloAprobacion::registrarSolicitudFundepexAprobacionModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_FILES) && !empty($_FILES)) {

                    for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                        $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                        $datos_archivo = array('id_aprobacion' => $guardar['id'], 'archivo' => $nombre_archivo);

                        $guardar_archivo = ModeloAprobacion::guardarArchivoFundepexAprobacionModel($datos_archivo);

                    }
                }

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosFundepexAprobacionIdModel($guardar['id']);
                $datos_archivos   = ModeloAprobacion::mostrarArchivosFundepexAprobacionModel($guardar['id']);

                $archivos = [];

                foreach ($datos_archivos as $archivo) {
                    $archivos[] = $archivo['archivo'];
                }

                $aprobar = BASE_URL . 'fundepex/aprobacion/token/aprobar?token=' . $datos_aprobacion['token'];
                $denegar = BASE_URL . 'fundepex/aprobacion/token/denegar?token=' . $datos_aprobacion['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Colibri.</b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de Fundepex aprobacion de pago',
                    'mensaje' => $mensaje_correo_admin,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => $archivos,
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de Fundepex aprobacion de pago',
                    'mensaje' => $mensaje_correo,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array($datos_usuario['correo']),
                    'archivo' => $archivos,
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Solicitud registrada';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registrarSolicitudTalentoAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'nom_archivo' => $_POST['nom_archivo'],
                'archivo'     => '',
                'concepto'    => $_POST['concepto'],
                'token'       => $this->generar_token_seguro(150),
                'id_proyecto' => $_POST['proyecto'],
                'valor'       => $valor,
            );

            $guardar = ModeloAprobacion::registrarSolicitudTalentoAprobacionModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_FILES) && !empty($_FILES)) {

                    for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                        $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                        $datos_archivo = array('id_aprobacion' => $guardar['id'], 'archivo' => $nombre_archivo);

                        $guardar_archivo = ModeloAprobacion::guardarArchivoTalentoAprobacionModel($datos_archivo);

                    }
                }

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosTalentoAprobacionIdModel($guardar['id']);
                $datos_archivos   = ModeloAprobacion::mostrarArchivosTalentoAprobacionModel($guardar['id']);

                $archivos = [];

                foreach ($datos_archivos as $archivo) {
                    $archivos[] = $archivo['archivo'];
                }

                $aprobar = BASE_URL . 'talento/aprobacion/token/aprobar?token=' . $datos_aprobacion['token'];
                $denegar = BASE_URL . 'talento/aprobacion/token/denegar?token=' . $datos_aprobacion['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Colibri.</b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de Fundepex aprobacion de pago',
                    'mensaje' => $mensaje_correo_admin,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => $archivos,
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de Talento Caribe aprobacion de pago',
                    'mensaje' => $mensaje_correo,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array($datos_usuario['correo']),
                    'archivo' => $archivos,
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Solicitud registrada';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registrarSolicitudAidaBarriosAprobacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $valor = str_replace(',', '', $_POST['valor']);

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'nom_archivo' => $_POST['nom_archivo'],
                'archivo'     => '',
                'concepto'    => $_POST['concepto'],
                'token'       => $this->generar_token_seguro(150),
                'id_proyecto' => 32,
                'valor'       => $valor,
            );

            $guardar = ModeloAprobacion::registrarSolicitudAidaBarriosAprobacionModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_FILES) && !empty($_FILES)) {

                    for ($i = 0; $i < count($_FILES['archivo']['name']); $i++) {

                        $nombre_archivo = guardarVariosArchivos($_FILES['archivo'], $i);

                        $datos_archivo = array('id_aprobacion' => $guardar['id'], 'archivo' => $nombre_archivo);

                        $guardar_archivo = ModeloAprobacion::guardarArchivoAidaBarriosAprobacionModel($datos_archivo);

                    }
                }

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosAidaBarriosAprobacionIdModel($guardar['id']);
                $datos_archivos   = ModeloAprobacion::mostrarArchivosAidaBarriosAprobacionModel($guardar['id']);

                $archivos = [];

                foreach ($datos_archivos as $archivo) {
                    $archivos[] = $archivo['archivo'];
                }

                $aprobar = BASE_URL . 'aida/aprobacion/token/aprobar?token=' . $datos_aprobacion['token'];
                $denegar = BASE_URL . 'aida/aprobacion/token/denegar?token=' . $datos_aprobacion['token'];

                $mensaje_correo_admin = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                <p style="font-size: 1.2em;">Esta solicitud fue enviada desde el <b>Sistema de Colibri.</b></p>
                </p>
                </body>
                </html>
                ';

                $datos_correo_admin = array(
                    'asunto'  => 'Solictud de Aida Barrios aprobacion de pago',
                    'mensaje' => $mensaje_correo_admin,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array('orlando@codetec.edu.co'),
                    'archivo' => $archivos,
                );

                $envio_admin = Correo::enviarCorreoModel($datos_correo_admin);
                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha solicitado una <b>APROBACION DE PAGO</b> para el siguiente archivo
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Confirmacion - Solictud de Aida Barrios aprobacion de pago',
                    'mensaje' => $mensaje_correo,
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'correo'  => array($datos_usuario['correo']),
                    'archivo' => $archivos,
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Solicitud registrada';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function subirComprobanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {

                $archivo   = $_FILES['archivo']['name'];
                $extension = pathinfo($archivo, PATHINFO_EXTENSION);

                $nombre_archivo = strtolower(md5($_POST['id_log'] . '_' . $archivo . '_' . date('YmdHis'))) . '.' . $extension;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($extension == 'png' || $extension == 'jpeg') {
                    $compressed = compressImage($_FILES['archivo']['tmp_name'], $ruta_img, 70);
                } else {

                    if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
                        move_uploaded_file($_FILES['archivo']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'comprobante'  => $nombre_archivo,
            );

            $guardar = ModeloAprobacion::subirComprobanteModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosAprobacionIdModel($_POST['id_solicitud']);

                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha subido el <b>COMPROBANTE DE PAGO</b> para la siguiente de solicitud de aprobacion de pago:
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Comprobante de pago - Solictud de aprobacion de pago No. ' . $_POST['id_solicitud'],
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => array($datos_aprobacion['comprobante']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Documento subido';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function subirWsComprobanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {

                $archivo   = $_FILES['archivo']['name'];
                $extension = pathinfo($archivo, PATHINFO_EXTENSION);

                $nombre_archivo = strtolower(md5($_POST['id_log'] . '_' . $archivo . '_' . date('YmdHis'))) . '.' . $extension;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($extension == 'png' || $extension == 'jpeg') {
                    $compressed = compressImage($_FILES['archivo']['tmp_name'], $ruta_img, 70);
                } else {

                    if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
                        move_uploaded_file($_FILES['archivo']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'comprobante'  => $nombre_archivo,
            );

            $guardar = ModeloAprobacion::subirWsComprobanteModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosWsAprobacionIdModel($_POST['id_solicitud']);

                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha subido el <b>COMPROBANTE DE PAGO</b> para la siguiente de solicitud de aprobacion de pago:
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Comprobante de pago - Solictud de WS aprobacion de pago No. ' . $_POST['id_solicitud'],
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => array($datos_aprobacion['comprobante']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Documento subido';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function subirFundepexComprobanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['archivo']);
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'comprobante'  => $nombre_archivo,
            );

            $guardar = ModeloAprobacion::subirFundepexComprobanteModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosFundepexAprobacionIdModel($_POST['id_solicitud']);

                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha subido el <b>COMPROBANTE DE PAGO</b> para la siguiente de solicitud de aprobacion de pago:
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Comprobante de pago - Solictud de Fundepex aprobacion de pago No. ' . $_POST['id_solicitud'],
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => array($datos_aprobacion['comprobante']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Documento subido';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function subirTalentoComprobanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['archivo']);
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'comprobante'  => $nombre_archivo,
            );

            $guardar = ModeloAprobacion::subirTalentoComprobanteModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosTalentoAprobacionIdModel($_POST['id_solicitud']);

                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha subido el <b>COMPROBANTE DE PAGO</b> para la siguiente de solicitud de aprobacion de pago:
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Comprobante de pago - Solictud de Fundepex aprobacion de pago No. ' . $_POST['id_solicitud'],
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => array($datos_aprobacion['comprobante']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Documento subido';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function subirAidaBarriosComprobanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['archivo']);
            }

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'comprobante'  => $nombre_archivo,
            );

            $guardar = ModeloAprobacion::subirAidaBarriosComprobanteModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_aprobacion = ModeloAprobacion::mostrarDatosAidaBarriosAprobacionIdModel($_POST['id_solicitud']);

                /*---------------------------------------*/

                $mensaje_correo = '<html><body>
                <p style="font-size: 1.2em;">
                El usuario
                <b>
                ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '
                </b>
                ha subido el <b>COMPROBANTE DE PAGO</b> para la siguiente de solicitud de aprobacion de pago:
                <ul style="font-size: 1.2em;">
                <li><b>Proyecto Asociado:</b> ' . $datos_aprobacion['nom_proyecto'] . '</li>
                <li><b>Nombre de archivo:</b> ' . $datos_aprobacion['nom_archivo'] . '</li>
                <li><b>Concepto de solicitud:</b> ' . $datos_aprobacion['concepto'] . '</li>
                <li><b>Valor:</b> $' . number_format($datos_aprobacion['valor']) . '</li>
                </ul>
                </body>
                </html>
                ';

                $datos_correo = array(
                    'asunto'  => 'Comprobante de pago - Solictud de Aida Barrios aprobacion de pago No. ' . $_POST['id_solicitud'],
                    'mensaje' => $mensaje_correo,
                    'correo'  => array($datos_usuario['correo']),
                    //'correo'  => array('jesuspolo00@gmail.com'),
                    'archivo' => array($datos_aprobacion['comprobante']),
                );

                $envio = Correo::enviarCorreoModel($datos_correo);
                /*---------------------------------------*/

                $title   = 'Documento subido';
                $message = 'Se ha registrado la solicitud';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function generar_token_seguro($longitud)
    {
        if ($longitud < 4) {
            $longitud = 4;
        }

        return bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
    }
}
