<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'areas' . DS . 'ModeloAreas.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlAreas
{

    private static $instancia;

    public static function singleton_areas()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarAreasControl()
    {
        $mostrar = ModeloAreas::mostrarAreasModel();
        return $mostrar;
    }

    public function mostrarZonasControl()
    {
        $mostrar = ModeloAreas::mostrarZonasModel();
        return $mostrar;
    }

    public function mostrarLimitesAreasControl()
    {
        $mostrar = ModeloAreas::mostrarLimitesAreasModel();
        return $mostrar;
    }

    public function mostrarAreasReservablesControl()
    {
        $mostrar = ModeloAreas::mostrarAreasReservablesModel();
        return $mostrar;
    }

    public function buscarAreasControl($nombre)
    {
        $mostrar = ModeloAreas::buscarAreasModel($nombre);
        return $mostrar;
    }

    public function mostrarDatosAreaControl($id)
    {
        $mostrar = ModeloAreas::mostrarDatosAreaModel($id);
        return $mostrar;
    }

    public function mostrarDatosZonaControl($id)
    {
        $mostrar = ModeloAreas::mostrarDatosZonaModel($id);
        return $mostrar;
    }

    public function mostrarComponentesControl()
    {
        $mostrar = ModeloAreas::mostrarComponentesModel();
        return $mostrar;
    }

    public function mostrarInspeccionesLimiteControl()
    {
        $mostrar = ModeloAreas::mostrarInspeccionesLimiteModel();
        return $mostrar;
    }

    public function mostrarInspeccionesTodasControl()
    {
        $mostrar = ModeloAreas::mostrarInspeccionesTodasModel();
        return $mostrar;
    }

    public function mostrarInspeccionesTodasMantControl()
    {
        $mostrar = ModeloAreas::mostrarInspeccionesTodasMantModel();
        return $mostrar;
    }

    public function mostrarInspeccionesMantLimiteControl()
    {
        $mostrar = ModeloAreas::mostrarInspeccionesMantLimiteModel();
        return $mostrar;
    }

    public function mostrarHistorialInspeccionesControl($id)
    {
        $mostrar = ModeloAreas::mostrarHistorialInspeccionesModel($id);
        return $mostrar;
    }

    public function mostrarInformacionInspeccionControl($id)
    {
        $mostrar = ModeloAreas::mostrarInformacionInspeccionModel($id);
        return $mostrar;
    }

    public function mostrarInspeccionGeneralLimiteControl()
    {
        $mostrar = ModeloAreas::mostrarInspeccionGeneralLimiteModel();
        return $mostrar;
    }

    public function buscarInspeccionGeneralControl($datos)
    {

        $estado = (empty($datos['estado'])) ? '' : ' AND rp.estado = "' . $datos['estado'] . '"';
        $fecha  = (empty($datos['fecha'])) ? '' : ' AND rp.fecha_inspeccion = "' . $datos['fecha'] . '"';

        $datos = array('fecha' => $fecha, 'buscar' => $datos['buscar'], 'estado' => $estado);

        $mostrar = ModeloAreas::buscarInspeccionGeneralModel($datos);
        return $mostrar;
    }

    public function buscarInspeccionesLimiteControl($datos)
    {

        $fecha = (empty($datos['fecha'])) ? '' : ' AND ai.fecha = "' . $datos['fecha'] . '"';
        $zona  = (empty($datos['zona'])) ? '' : ' AND ai.id_zona = ' . $datos['zona'];

        $datos = array('fecha' => $fecha, 'zona' => $zona, 'buscar' => $datos['buscar']);

        $mostrar = ModeloAreas::buscarInspeccionesLimiteModel($datos);
        return $mostrar;
    }

    public function buscarInspeccionesMantLimiteControl($datos)
    {

        $fecha = (empty($datos['fecha'])) ? '' : ' AND ai.fecha = "' . $datos['fecha'] . '"';
        $zona  = (empty($datos['zona'])) ? '' : ' AND ai.id_zona = ' . $datos['zona'];

        $datos = array('fecha' => $fecha, 'zona' => $zona, 'buscar' => $datos['buscar']);

        $mostrar = ModeloAreas::buscarInspeccionesMantLimiteModel($datos);
        return $mostrar;
    }

    public function mostrarInfoInspeccionGeneralControl($id)
    {
        $mostrar = ModeloAreas::mostrarInfoInspeccionGeneralModel($id);
        return $mostrar;
    }

    public function mostrarDetallesGeneralControl($id)
    {
        $mostrar = ModeloAreas::mostrarDetallesGeneralModel($id);
        return $mostrar;
    }

    public function mostrarListadoLimitePlanMantenimientoControl()
    {
        $mostrar = ModeloAreas::mostrarListadoLimitePlanMantenimientoModel();
        return $mostrar;
    }

    public function mostrarInformacionPlanControl($id)
    {
        $mostrar = ModeloAreas::mostrarInformacionPlanModel($id);
        return $mostrar;
    }

    public function mostrarDetallesPlanControl($id)
    {
        $mostrar = ModeloAreas::mostrarDetallesPlanModel($id);
        return $mostrar;
    }

    public function agregarAreasControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log' => $_POST['id_log'],
                'nombre' => $_POST['nombre'],
            );

            $guardar = ModeloAreas::agregarAreasModel($datos);

            if ($guardar == true) {
                $title   = 'Registro completo';
                $message = 'Se ha registrado correctamente';
                $enlace  = BASE_URL . 'areas/index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function registrarInspeccionAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['enlace'] == 0) ? BASE_URL . 'reportes/areas' : BASE_URL . 'lector/qr';

            $nombre_archivo = '';

            if (isset($_FILES['evidencia']['name']) && !empty($_FILES['evidencia']['name'])) {
                $nombre_archivo = guardarArchivo($_FILES['evidencia']);
            }

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'zona'        => $_POST['zona'],
                'area'        => $_POST['area'],
                'categoria'   => $_POST['categoria'],
                'fecha'       => $_POST['fecha'],
                'observacion' => $_POST['observacion'],
                'area'        => $_POST['area'],
                'tipo'        => $_POST['tipo'],
                'evidencia'   => $nombre_archivo,
            );

            $guardar = ModeloAreas::registrarInspeccionAreaModel($datos);

            if ($guardar == true) {
                $title   = 'Inspeccion registrada';
                $message = 'Se ha registrado correctamente';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function activarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloAreas::activarAreaModel($_POST['id']);
            return $activar;
        }
    }

    public function inactivarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloAreas::inactivarAreaModel($_POST['id']);
            return $activar;
        }
    }

    public function solucionarInspeccionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['url'] == 1) ? 'listadoAreas' : 'listadoAreasMant';

            $datos = array(
                'id_log'                => $_POST['id_log'],
                'id_area'               => $_POST['id_area'],
                'id_zona'               => $_POST['id_zona'],
                'id_componente'         => $_POST['id_componente'],
                'fecha_respuesta'       => $_POST['fecha_solucion'] . date(' H:i:s'),
                'observacion_respuesta' => $_POST['observacion_respuesta'],
            );

            $guardar = ModeloAreas::solucionarInspeccionModel($datos);

            if ($guardar == true) {
                $title   = 'Reporte actualizado';
                $message = 'Se ha actualizado el reporte';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function guardarInspeccionGeneralControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'           => $_POST['id_log'],
                'fecha_inspeccion' => $_POST['fecha_inspeccion'],
            );

            $generar = ModeloAreas::guardarInspeccionGeneralModel($datos);

            if ($generar['guardar'] == true) {

                $array_zona = array();
                $array_zona = $_POST['zona'];

                $array_area = array();
                $array_area = $_POST['area'];

                $array_categoria = array();
                $array_categoria = $_POST['categoria'];

                $array_tipo = array();
                $array_tipo = $_POST['tipo'];

                $array_observacion = array();
                $array_observacion = $_POST['observacion'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_zona));
                $it->attachIterator(new ArrayIterator($array_area));
                $it->attachIterator(new ArrayIterator($array_categoria));
                $it->attachIterator(new ArrayIterator($array_tipo));
                $it->attachIterator(new ArrayIterator($array_observacion));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'zona'        => $dato[0],
                        'area'        => $dato[1],
                        'categoria'   => $dato[2],
                        'tipo'        => $dato[3],
                        'observacion' => $dato[4],
                        'id_reporte'  => $generar['id'],
                    );

                    $guardar = ModeloAreas::guardaDetalleInspeccionGeneralModel($datos_detalle);

                }

                if ($guardar == true) {
                    $title   = 'Inspeccion realizada';
                    $message = 'Se ha registrado la inspeccion';
                    $enlace  = BASE_URL . 'reportes' . DS . 'areas_general' . DS . 'index';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            }
        }
    }

    public function solucionarInspeccionGeneralControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_respuesta = $_POST['fecha_respuesta'] . date(' H:i:s');

            $datos = array(
                'id_log'              => $_POST['id_log'],
                'id_inspeccion'       => $_POST['id_inspeccion'],
                'observacion_general' => $_POST['observacion_general'],
                'fecha_respuesta'     => $fecha_respuesta,
                'estado'              => 1,
            );

            $solucion = ModeloAreas::solucionarInspeccionGeneralModel($datos);

            if ($solucion == true) {

                $array_id_detalle = array();
                $array_id_detalle = $_POST['id_detalle'];

                $array_observacion_respuesta = array();
                $array_observacion_respuesta = $_POST['observacion_respuesta'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_id_detalle));
                $it->attachIterator(new ArrayIterator($array_observacion_respuesta));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_log'                => $_POST['id_log'],
                        'id_detalle'            => $dato[0],
                        'observacion_respuesta' => $dato[1],
                        'fecha_respuesta'       => $fecha_respuesta,
                    );

                    $guardar = ModeloAreas::solucionarDetallesInspeccionModel($datos_detalle);

                }

                if ($guardar == true) {
                    $title   = 'Inspeccion solucionada';
                    $message = 'Se ha solucionado la inspeccion';
                    $enlace  = BASE_URL . 'reportes' . DS . 'areas_general' . DS . 'listado';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error al solucionar';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            }

        }
    }

    public function guardarPlanMantenimientoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'              => $_POST['id_log'],
                'fecha_inicio'        => $_POST['fecha_inicio'],
                'fecha_fin'           => $_POST['fecha_fin'],
                'observacion_general' => $_POST['observacion_general'],
            );

            $plan = ModeloAreas::guardarPlanMantenimientoModel($datos);

            if ($plan['guardar'] == true) {

                $array_categoria = array();
                $array_categoria = $_POST['categoria'];

                $array_zona = array();
                $array_zona = $_POST['zona'];

                $array_fecha_mant = array();
                $array_fecha_mant = $_POST['fecha_mant'];

                $array_observacion = array();
                $array_observacion = $_POST['observacion'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_categoria));
                $it->attachIterator(new ArrayIterator($array_zona));
                $it->attachIterator(new ArrayIterator($array_fecha_mant));
                $it->attachIterator(new ArrayIterator($array_observacion));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_plan'     => $plan['id'],
                        'categoria'   => $dato[0],
                        'zona'        => $dato[1],
                        'fecha_mant'  => $dato[2],
                        'observacion' => $dato[3],
                    );

                    $guardar = ModeloAreas::guardarDetallePlanMantenimientoModel($datos_detalle);

                }

                if ($guardar == true) {
                    $title   = 'Plan registrado';
                    $message = 'Se ha registrado el plan';
                    $enlace  = BASE_URL . 'reportes' . DS . 'plan_mantenimiento' . DS . 'index';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error al registrado';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            }

        }
    }

    public function finalizarPlanMantenimientoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'                => $_POST['id_log'],
                'id_plan'               => $_POST['id_plan'],
                'estado'                => 1,
                'observacion_respuesta' => $_POST['observacion_respuesta_general'],
            );

            $respuesta = ModeloAreas::finalizarPlanMantenimientoModel($datos);

            if ($respuesta == true) {

                $array_id_detalle = array();
                $array_id_detalle = $_POST['id_detalle'];

                $array_estado = array();
                $array_estado = $_POST['estado'];

                $array_observacion_respuesta = array();
                $array_observacion_respuesta = $_POST['observacion_respuesta'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_id_detalle));
                $it->attachIterator(new ArrayIterator($array_estado));
                $it->attachIterator(new ArrayIterator($array_observacion_respuesta));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_log'                => $_POST['id_log'],
                        'id_detalle'            => $dato[0],
                        'estado'                => $dato[1],
                        'observacion_respuesta' => $dato[2],
                    );

                    $guardar = ModeloAreas::finalizarDetallesPlanModel($datos_detalle);

                }

                if ($guardar == true) {
                    $title   = 'Plan finalizado';
                    $message = 'Se ha finalizado el plan ' . $_POST['id_plan'];
                    $enlace  = BASE_URL . 'reportes' . DS . 'plan_mantenimiento' . DS . 'listado';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error al actualizar';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            }

        }
    }
}
