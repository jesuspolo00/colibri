<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'messages.php';

class ControlUsuario
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarUsuariosControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarUsuariosModel();
        return $mostrar;
    }

    public function mostrarProveedoresControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarProveedoresModel();
        return $mostrar;
    }

    public function mostrarCentroCostoControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarCentroCostoModel();
        return $mostrar;
    }

    public function mostrarLimiteUsuariosControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarLimiteUsuariosModel();
        return $mostrar;
    }

    public function mostrarLimiteRolUsuariosControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarLimiteRolUsuariosModel();
        return $mostrar;
    }

    public function mostrarLimiteUsuariosHojaVidaControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarLimiteUsuariosHojaVidaModel();
        return $mostrar;
    }

    public function mostrarLimiteUsuariosHojaVidaIncluyemeControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarLimiteUsuariosHojaVidaIncluyemeModel();
        return $mostrar;
    }

    public function mostrarLimiteUsuariosHojaVidaCdvControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarLimiteUsuariosHojaVidaCdvModel();
        return $mostrar;
    }

    public function mostrarProfesoresControl()
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarProfesoresModel();
        return $mostrar;
    }

    public function buscarUsuariosControl($buscar)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::buscarUsuariosModel($buscar);
        return $mostrar;
    }

    public function buscarUsuariosHojaVidaControl($buscar)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::buscarUsuariosHojaVidaModel($buscar);
        return $mostrar;
    }

    public function buscarUsuariosHojaVidaIncluyemeControl($buscar)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::buscarUsuariosHojaVidaIncluyemeModel($buscar);
        return $mostrar;
    }

    public function buscarUsuariosHojaVidaCdvControl($buscar)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::buscarUsuariosHojaVidaCdvModel($buscar);
        return $mostrar;
    }

    public function autorizacionAnioDocumentosControl($id, $anio)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::autorizacionAnioDocumentosModel($id, $anio);
        return $mostrar;
    }

    public function validarAutorizacionUsuarioControl($id_user, $id, $anio)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::validarAutorizacionUsuarioModel($id_user, $id, $anio);
        return $mostrar;
    }

    public function validarEvaluacionAnualUsuarioControl($id, $anio, $tipo)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::validarEvaluacionAnualUsuarioModel($id, $anio, $tipo);
        return $mostrar;
    }

    public function mostrarDatosEvaluacionControl($id, $tipo)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarDatosEvaluacionModel($id, $tipo);
        return $mostrar;
    }

    public function mostrarAutorizacionPerfilControl($id, $anio, $perfil)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarAutorizacionPerfilModel($id, $anio, $perfil);
        return $mostrar;
    }

    public function mostrarDetalleEvaluacionControl($id, $anio, $tipo)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarDetalleEvaluacionModel($id, $anio, $tipo);
        return $mostrar;
    }

    public function mostrarPuntajeEvaluacionAnioControl($id, $anio, $tipo)
    {
        $consulta = ModeloUsuario::comandoSQL();
        $mostrar  = ModeloUsuario::mostrarPuntajeEvaluacionAnioModel($id, $anio, $tipo);
        return $mostrar;
    }

    public function registrarUsuarioControl()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $url = ($_POST['enlace'] == 1) ? BASE_URL . 'usuarios/index' : BASE_URL . 'hoja/listado';
            $url = ($_POST['enlace'] == 3) ? BASE_URL . 'proyectos/incluyeme/index' : $url;
            $url = ($_POST['enlace'] == 4) ? BASE_URL . 'proyectos/cdv/index' : $url;

            if ($_POST['conf_password'] != $_POST['password']) {

                $title   = 'Contraseñas no validas';
                $message = 'Las contraseñas no coinciden.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
                die();

            }

            $pass         = Hash::hashpass($_POST['conf_password']);
            $centro_costo = (empty($_POST['centro_costo'])) ? 0 : $_POST['centro_costo'];

            $datos = array(
                'documento'    => $_POST['documento'],
                'nombre'       => $_POST['nombre'],
                'apellido'     => $_POST['apellido'],
                'telefono'     => $_POST['telefono'],
                'correo'       => $_POST['correo'],
                'usuario'      => $_POST['usuario'],
                'perfil'       => $_POST['perfil'],
                'pass'         => $pass,
                'user_log'     => $_POST['id_log'],
                'centro_costo' => $centro_costo,
            );

            $guardar = ModeloUsuario::registrarUsuarioModel($datos);

            if ($guardar == true) {

                if ($_POST['perfil'] != 1 || $_POST['perfil'] != 5) {
                    $hoja_vida = ModeloUsuario::guardarHojaVidaModel($guardar['id'], $_POST['id_log']);
                }

                $title   = 'Usuario registrado';
                $message = 'Se ha realizado el registro correctamente';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function actualizarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_user'      => $_POST['id_user'],
                'documento'    => $_POST['documento_edit'],
                'nombre'       => $_POST['nombre_edit'],
                'apellido'     => $_POST['apellido_edit'],
                'telefono'     => $_POST['telefono_edit'],
                'correo'       => $_POST['correo_edit'],
                'perfil'       => $_POST['perfil_edit'],
                'centro_costo' => $_POST['centro_costo'],
            );

            $actualizar = ModeloUsuario::actualizarUsuarioModel($datos);

            if ($actualizar == true) {
                $title   = 'Usuario actualizado';
                $message = 'Se ha realizado la actualizacion correctamente';
                $enlace  = $url;
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function validarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $validar = ModeloUsuario::validarUsuarioModel($_POST['usuario']);
            return $validar;
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['log']) &&
            !empty($_POST['log'])
        ) {
            $validar = ModeloUsuario::inactivarUsuarioModel($_POST['id'], $_POST['log']);
            return $validar;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['log']) &&
            !empty($_POST['log'])
        ) {
            $validar = ModeloUsuario::activarUsuarioModel($_POST['id'], $_POST['log']);
            return $validar;
        }
    }

    public function restablecerPassControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $pass = Hash::hashpass('codetec123456@');

            $validar = ModeloUsuario::restablecerPassModel($_POST['id'], $pass);
            return $validar;
        }
    }
}
