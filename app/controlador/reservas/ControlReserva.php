<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'reservas' . DS . 'ModeloReserva.php';

class ControlReserva
{

    private static $instancia;

    public static function singleton_reserva()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTodasHorasControl()
    {
        $mostrar = ModeloReserva::mostrarTodasHorasModel();
        return $mostrar;
    }

    public function mostrarReservasTodasControl()
    {
        $mostrar = ModeloReserva::mostrarReservasTodasModel();
        return $mostrar;
    }

    public function mostrarHorasDisponiblesControl($salon, $fecha)
    {
        $mostrar = ModeloReserva::mostrarHorasDisponiblesModel($salon, $fecha);
        return $mostrar;
    }

    public function mostrarAreasReservaHoyControl()
    {
        $mostrar = ModeloReserva::mostrarAreasReservaHoyModel();
        return $mostrar;
    }

    public function detalleReservaSalonControl($salon, $fecha)
    {
        $fecha = (empty($fecha)) ? '' : ' AND r.fecha_reserva = "' . $fecha . '"';

        $mostrar = ModeloReserva::detalleReservaSalonModel($salon, $fecha);
        return $mostrar;
    }

    public function buscarAreasReservaControl($datos)
    {

        $area  = (empty($datos['area'])) ? '' : ' AND a.id = ' . $datos['area'];
        $fecha = (empty($datos['fecha'])) ? '' : ' AND r.fecha_reserva = "' . $datos['fecha'] . '"';

        $datos = array('area' => $area, 'fecha' => $fecha, 'buscar' => $datos['buscar']);

        $mostrar = ModeloReserva::buscarAreasReservaModel($datos);
        return $mostrar;
    }

    public function reservarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $array_hora = array();
            $array_hora = $_POST['hora'];

            foreach ($array_hora as $hora) {
                $datos = array(
                    'id_log'  => $_POST['id_log'],
                    'area'    => $_POST['salon'],
                    'fecha'   => $_POST['fecha_reserva'],
                    'hora'    => $hora,
                    'detalle' => $_POST['detalle'],
                );

                $guardar = ModeloReserva::reservarAreaModel($datos);
            }

            if ($guardar == true) {
                $title   = 'Reserva registrada';
                $message = 'Se ha reservado el salon';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
