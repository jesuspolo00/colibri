<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'asistencia' . DS . 'ModeloAsistencia.php';
require_once CONTROL_PATH . 'messages.php';

class ControlAsistencia
{

    private static $instancia;

    public static function singleton_asistencia()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarProgramasAsistenciaControl()
    {
        $mostrar = ModeloAsistencia::mostrarProgramasAsistenciaModel();
        return $mostrar;
    }

    public function mostrarCargaAcademicaAsignadaControl($id, $tipo)
    {
        $mostrar = ModeloAsistencia::mostrarCargaAcademicaAsignadaModel($id, $tipo);
        return $mostrar;
    }

    public function mostrarCompetenciasProgramaAsignadaControl($id, $id_log, $tipo)
    {
        $mostrar = ModeloAsistencia::mostrarCompetenciasProgramaAsignadaModel($id, $id_log, $tipo);
        return $mostrar;
    }

    public function mostrarModulosCompetenciaAsignadaControl($id, $id_log, $tipo)
    {
        $mostrar = ModeloAsistencia::mostrarModulosCompetenciaAsignadaModel($id, $id_log, $tipo);
        return $mostrar;
    }

    public function mostrarLimiteProgramasControl()
    {
        $mostrar = ModeloAsistencia::mostrarLimiteProgramasModel();
        return $mostrar;
    }

    public function buscarProgramaControl($buscar)
    {
        $mostrar = ModeloAsistencia::buscarProgramaModel($buscar);
        return $mostrar;
    }

    public function mostrarLimiteAsistenciaControl($id_log)
    {
        $mostrar = ModeloAsistencia::mostrarLimiteAsistenciaModel($id_log);
        return $mostrar;
    }

    public function buscarAsistenciaControl($id_log, $buscar)
    {
        $mostrar = ModeloAsistencia::buscarAsistenciaModel($id_log, $buscar);
        return $mostrar;
    }

    public function mostrarLimiteListadoAsistenciaControl()
    {
        $mostrar = ModeloAsistencia::mostrarLimiteListadoAsistenciaModel();
        return $mostrar;
    }

    public function mostrarLimiteListadoCoordinadorAsistenciaControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarLimiteListadoCoordinadorAsistenciaModel($id);
        return $mostrar;
    }

    public function mostrarDetallesPlanFormacionControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarDetallesPlanFormacionModel($id);
        return $mostrar;
    }

    public function mostrarAsistenciaPlanFormacionControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarAsistenciaPlanFormacionModel($id);
        return $mostrar;
    }

    public function mostrarTipoPlanControl()
    {
        $mostrar = ModeloAsistencia::mostrarTipoPlanModel();
        return $mostrar;
    }

    public function buscarAsistenciasListadoControl($datos)
    {
        $anio = (empty($datos['anio'])) ? '' : ' AND (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) = ' . $datos['anio'];
        $mes  = (empty($datos['mes'])) ? '' : ' AND (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) = ' . $datos['mes'];

        $datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $datos['buscar']);

        $mostrar = ModeloAsistencia::buscarAsistenciasListadoModel($datos);
        return $mostrar;
    }

    public function buscarListadoCoordinadorAsistenciaControl($datos, $id_log)
    {
        $anio = (empty($datos['anio'])) ? '' : ' AND (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) = ' . $datos['anio'];
        $mes  = (empty($datos['mes'])) ? '' : ' AND (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) = ' . $datos['mes'];

        $datos = array('anio' => $anio, 'mes' => $mes, 'buscar' => $datos['buscar'], 'id_log' => $id_log);

        $mostrar = ModeloAsistencia::buscarListadoCoordinadorAsistenciaModel($datos);
        return $mostrar;
    }

    public function mostrarDatosAsistenciaControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarDatosAsistenciaModel($id);
        return $mostrar;
    }

    public function mostrarDetallesAsistenciaControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarDetallesAsistenciaModel($id);
        return $mostrar;
    }

    public function mostrarModulosCompetenciaControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarModulosCompetenciaModel($id);
        return $mostrar;
    }

    public function mostrarCompetenciasControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarCompetenciasModel($id);
        return $mostrar;
    }

    public function competenciasProgramaControl($id)
    {
        $mostrar = ModeloAsistencia::competenciasProgramaModel($id);
        return $mostrar;
    }

    public function modulosCompetenciaControl($id)
    {
        $mostrar = ModeloAsistencia::modulosCompetenciaModel($id);
        return $mostrar;
    }

    public function mostrarInfoModulosControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarInfoModulosModel($id);
        return $mostrar;
    }

    public function mostrarHorasModuloControl($id, $id_log, $tipo)
    {
        $mostrar = ModeloAsistencia::mostrarHorasModuloModel($id, $id_log, $tipo);
        return $mostrar;
    }

    public function mostrarDatosProgramaIdControl($id)
    {
        $mostrar = ModeloAsistencia::mostrarDatosProgramaIdModel($id);
        return $mostrar;
    }

    public function mostrarLimiteCargaAcademicaControl()
    {
        $mostrar = ModeloAsistencia::mostrarLimiteCargaAcademicaModel();
        return $mostrar;
    }

    public function finalizarAsistenciaControl($id)
    {
        $mostrar = ModeloAsistencia::finalizarAsistenciaModel($id);
        $rs      = ($mostrar == true) ? 'ok' : 'no';
        return $rs;
    }

    public function estadoModuloControl($estado, $id)
    {
        $mostrar = ModeloAsistencia::estadoModuloModel($estado, $id);
        $rs      = ($mostrar == true) ? 'ok' : 'no';
        return $rs;
    }

    public function validarAsistenciaControl($id, $log, $value, $hora_coor, $just_coor)
    {
        $mostrar = ModeloAsistencia::validarAsistenciaModel($id, $log, $value, $hora_coor, $just_coor);
        $rs      = ($mostrar == true) ? 'ok' : 'no';
        return $rs;
    }

    public function mostrarPlanFormacionControl()
    {
        $mostrar = ModeloAsistencia::mostrarPlanFormacionModel();
        return $mostrar;
    }

    public function mostrarPlanesFormacionControl()
    {
        $mostrar = ModeloAsistencia::mostrarPlanesFormacionModel();
        return $mostrar;
    }

    public function mostrarPlanFormacionDatosTokenControl($token)
    {
        $mostrar = ModeloAsistencia::mostrarPlanFormacionDatosTokenModel($token);
        return $mostrar;
    }

    public function planFormacionArteControl($estado, $id)
    {
        $mostrar = ModeloAsistencia::planFormacionArteModel($estado, $id);
        return $mostrar;
    }

    public function buscarPlanFormacionControl($datos)
    {
        $usuario = (empty($datos['usuario'])) ? '' : ' AND p.id_user = ' . $datos['usuario'];
        $fecha   = (empty($datos['fecha'])) ? '' : ' AND p.fecha_proyectada = ' . $datos['fecha'];
        $arte    = (empty($datos['arte'])) ? '' : ' AND p.arte = ' . $datos['arte'];

        $datos = array('usuario' => $usuario, 'fecha' => $fecha, 'buscar' => $datos['buscar'], 'arte' => $arte);

        $mostrar = ModeloAsistencia::buscarPlanFormacionModel($datos);
        return $mostrar;
    }

    public function tomarAsistenciaPlanFormacionControl($id, $usuario)
    {
        $validar_usuario = ModeloAsistencia::validarUsuarioAsistenciaPlanFormacionModel($id, $usuario);

        if (empty($validar_usuario)) {

            $asistencia = ModeloAsistencia::tomarAsistenciaPlanFormacionModel($id, $usuario);

            if ($asistencia == true) {
                $title   = 'Asistencia tomada';
                $message = 'Se ha tomado la asistencia del plan de formacion No. ' . $id;
                $enlace  = BASE_URL . 'inicio';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error al tomar la asistencia';
                $message = 'Ha ocurrido un error al tomar la asistencia, por favor intente nuevamente.';
                $enlace  = BASE_URL . 'lector' . DS . 'qr';
                alertDanger($title, $message, $enlace);
            }

        } else {
            $title   = 'Asistencia ya tomada';
            $message = 'Ya se ha tomado la asistencia de este plan de formacion';
            $enlace  = BASE_URL . 'inicio';
            alertDanger($title, $message, $enlace);
        }

    }

    public function mostrarAsistenciasFiltroControl($datos)
    {

        $programa = '';
        $id_log   = (!empty($datos['id_log'])) ? ' AND sa.id_log = ' . $datos['id_log'] : '';
        $mes      = (!empty($datos['mes'])) ? ' AND (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) =  ' . $datos['mes'] : '';
        $anio     = (!empty($datos['anio'])) ? ' AND (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) = ' . $datos['anio'] : '';

        $datos = array('programa' => $programa, 'id_log' => $id_log, 'mes' => $mes, 'anio' => $anio, 'tipo' => $datos['tipo']);

        $mostrar = ModeloAsistencia::mostrarAsistenciasFiltroModel($datos);
        return $mostrar;
    }

    public function buscarCargasAcademicasControl($datos)
    {

        $anio    = (empty($datos['anio'])) ? '' : ' AND c.anio = "' . $datos['anio'] . '"';
        $docente = (empty($datos['docente'])) ? '' : ' AND c.id_profesor = ' . $datos['docente'];

        $datos = array('anio' => $anio, 'docente' => $docente, 'buscar' => $datos['buscar'], 'id' => $datos['id']);

        $mostrar = ModeloAsistencia::buscarCargasAcademicasModel($datos);
        return $mostrar;
    }

    public function guardarProgramaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log' => $_POST['id_log'],
                'nombre' => $_POST['nom_programa'],
            );

            $guardar = ModeloAsistencia::guardarProgramaModel($datos);

            if ($guardar == true) {
                $title   = 'Programa registrado';
                $message = 'Se ha registrado el programa';
                $enlace  = 'listado';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function editarCompetenciaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_competencia'  => $_POST['id_competencia'],
                'programa'        => $_POST['programa_edit'],
                'nom_competencia' => $_POST['nom_competencia_edit'],
                'id_log'          => $_POST['id_log'],
            );

            $guardar = ModeloAsistencia::editarCompetenciaModel($datos);

            if ($guardar == true) {
                $title   = 'Competencia editada';
                $message = 'Se ha editado la competencia';
                $enlace  = BASE_URL . 'programa/detallePrograma?id=' . base64_encode($_POST['programa_old']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function editarProgramaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_programa'  => $_POST['id_programa'],
                'nom_programa' => $_POST['nom_programa'],
            );

            $guardar = ModeloAsistencia::editarProgramaModel($datos);

            if ($guardar == true) {
                $title   = 'Programa editado';
                $message = 'Se ha editado el programa';
                $enlace  = BASE_URL . 'programa/detallePrograma?id=' . base64_encode($_POST['id_programa']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function guardarCompetenciaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'   => $_POST['id_log'],
                'programa' => $_POST['programa'],
                'nombre'   => $_POST['nom_competencia'],
            );

            $guardar = ModeloAsistencia::guardarCompetenciaModel($datos);

            if ($guardar == true) {
                $title   = 'Competencia registrada';
                $message = 'Se ha registrado la competencia';
                $enlace  = 'detallePrograma?id=' . base64_encode($_POST['programa']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function guardarModuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'programa'        => $_POST['programa'],
                'competencia'     => $_POST['competencia'],
                'nombre'          => $_POST['nom_modulo'],
                'horas_teoricas'  => $_POST['horas_teoricas'],
                'horas_practicas' => $_POST['horas_practicas'],
                'estado'          => $_POST['estado'],
            );

            $guardar = ModeloAsistencia::guardarModuloModel($datos);

            if ($guardar == true) {
                $title   = 'Asignatura registrado';
                $message = 'Se ha registrado la asignatura';
                $enlace  = BASE_URL . 'programa/detallePrograma?id=' . base64_encode($_POST['programa']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function editarModuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_modulo']) &&
            !empty($_POST['id_modulo'])
        ) {

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'id_modulo'       => $_POST['id_modulo'],
                'competencia'     => $_POST['competencia'],
                'nombre'          => $_POST['nom_modulo_edit'],
                'horas_teoricas'  => $_POST['horas_teoricas'],
                'horas_practicas' => $_POST['horas_practicas'],
                'estado'          => $_POST['estado'],
            );

            $guardar = ModeloAsistencia::editarModuloModel($datos);

            if ($guardar == true) {
                $title   = 'Asignatura editado';
                $message = 'Se ha editado la asignatura';
                $enlace  = BASE_URL . 'programa/detallePrograma?id=' . base64_encode($_POST['programa']);
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de edicion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function guardarAsistenciaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'programa'    => $_POST['programa'],
                'competencia' => $_POST['competencia'],
                'modulo'      => $_POST['modulo'],
                'tipo'        => $_POST['tipo'],
            );

            $guardar = ModeloAsistencia::guardarAsistenciaModel($datos);

            if ($guardar['guardar'] == true) {

                $array_fecha = array();
                $array_fecha = $_POST['fecha'];

                $array_hora_entrada = array();
                $array_hora_entrada = $_POST['hora_entrada'];

                $array_hora_salida = array();
                $array_hora_salida = $_POST['hora_salida'];

                $array_horas = array();
                $array_horas = $_POST['horas'];

                $array_tematica = array();
                $array_tematica = $_POST['tematica'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_fecha));
                $it->attachIterator(new ArrayIterator($array_hora_entrada));
                $it->attachIterator(new ArrayIterator($array_hora_salida));
                $it->attachIterator(new ArrayIterator($array_horas));
                $it->attachIterator(new ArrayIterator($array_tematica));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_asistencia' => $guardar['id'],
                        'fecha'         => $dato[0],
                        'hora_entrada'  => $dato[1],
                        'hora_salida'   => $dato[2],
                        'horas'         => $dato[3],
                        'tematica'      => $dato[4],
                        'id_log'        => $_POST['id_log'],
                    );

                    $guardar_detalle = ModeloAsistencia::guardarDetalleAsistenciaModel($datos_detalle);
                }

                if ($guardar_detalle == true) {
                    $title   = 'Asistencia guardada';
                    $message = 'Se ha guardado la asistencia';
                    $enlace  = 'index';
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function editarAsistenciaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'            => $_POST['id_log'],
                'id_asistencia'     => $_POST['id_asistencia'],
                'horas_programadas' => $_POST['horas_programadas'],
            );

            $guardar = ModeloAsistencia::editarAsistenciaModel($datos);

            if ($guardar == true) {

                $vaciar_detalle = ModeloAsistencia::vaciarDetalleAsistenciaModel($_POST['id_asistencia']);

                $array_fecha = array();
                $array_fecha = $_POST['fecha'];

                $array_hora_entrada = array();
                $array_hora_entrada = $_POST['hora_entrada'];

                $array_hora_salida = array();
                $array_hora_salida = $_POST['hora_salida'];

                $array_horas = array();
                $array_horas = $_POST['horas'];

                $array_tematica = array();
                $array_tematica = $_POST['tematica'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_fecha));
                $it->attachIterator(new ArrayIterator($array_hora_entrada));
                $it->attachIterator(new ArrayIterator($array_hora_salida));
                $it->attachIterator(new ArrayIterator($array_horas));
                $it->attachIterator(new ArrayIterator($array_tematica));

                foreach ($it as $dato) {

                    $datos_detalle = array(
                        'id_asistencia' => $_POST['id_asistencia'],
                        'fecha'         => $dato[0],
                        'hora_entrada'  => $dato[1],
                        'hora_salida'   => $dato[2],
                        'horas'         => $dato[3],
                        'tematica'      => $dato[4],
                        'id_log'        => $_POST['id_log'],
                    );

                    $guardar_detalle = ModeloAsistencia::guardarDetalleAsistenciaModel($datos_detalle);
                }

                if ($guardar_detalle == true) {
                    $title   = 'Asistencia editada';
                    $message = 'Se ha editado la asistencia No. ' . $_POST['id_asistencia'];
                    $enlace  = 'detalles?asistencia=' . base64_encode($_POST['id_asistencia']);
                    alertSuccess($title, $message, $enlace);
                } else {
                    $title   = 'Error de registro';
                    $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                    $enlace  = '';
                    alertDanger($title, $message, $enlace);
                }

            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function asignarCargaAcedmicaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'programa'    => $_POST['programa'],
                'competencia' => $_POST['competencia'],
                'modulo'      => $_POST['modulo'],
                'profesor'    => $_POST['profesor'],
                'horas'       => $_POST['horas'],
                'jornada'     => $_POST['jornada'],
                'tipo'        => $_POST['tipo'],
                'anio'        => $_POST['anio'],
                'mes_inicio'  => $_POST['mes_inicio'],
                'mes_fin'     => $_POST['mes_fin'],
                'sede'        => $_POST['sede'],
                'colegio'     => $_POST['colegio'],
                'ciclo'       => $_POST['ciclo'],
                'semestre'    => $_POST['semestre'],
            );

            $guardar = ModeloAsistencia::asignarCargaAcedmicaModel($datos);

            if ($guardar == true) {
                $title   = 'Carga academica Asignada';
                $message = 'Se ha asignado la carga academica';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

    public function guardarPlanFormacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'         => $_POST['id_log'],
                'linea'          => $_POST['linea'],
                'temas'          => $_POST['temas'],
                'objetivo'       => $_POST['objetivo'],
                'recursos'       => $_POST['recursos'],
                'fecha_ejecutar' => $_POST['fecha_ejecutar'],
                'token'          => $this->tokenAleatorio(),
                'hora'           => $_POST['hora'],
                'tipo'           => $_POST['tipo'],
            );

            $guardar = ModeloAsistencia::guardarPlanFormacionModel($datos);

            if ($guardar == true) {
                $title   = 'Plan de formacion agendado';
                $message = 'Se ha agendado el plan de formacion';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function actualizarEstadoPlanFormacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'id_plan'     => $_POST['id_plan'],
                'fecha'       => $_POST['fecha'],
                'observacion' => $_POST['observacion'],
                'estado'      => $_POST['estado'],
            );

            $guardar = ModeloAsistencia::actualizarEstadoPlanFormacionModel($datos);

            if ($guardar == true) {
                $title   = 'Plan de formacion actualizado';
                $message = 'Se ha actualizado el plan de formacion';
                $enlace  = 'listado';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function tokenAleatorio()
    {
        $token = bin2hex(random_bytes(32));

        $codigo = ModeloAsistencia::verificarCodigoPlanFormacionModel($token);

        if ($codigo['token'] != '') {
            $this->tokenAleatorio();
        } else {
            $num_codigo = $token;
        }

        return $num_codigo;
    }
}
