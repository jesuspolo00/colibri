<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloAsistencia extends conexion
{

    public static function guardarProgramaModel($datos)
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_log) VALUES (:n, :idl);";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarCompetenciaModel($datos)
    {
        $tabla  = 'asistencia_competencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_programa, id_log) VALUES (:n, :idp, :idl);";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idp', $datos['programa']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTipoPlanModel()
    {
        $tabla  = 'tipo_plan';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE estado = 1;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInfoModulosModel($id)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarModuloModel($datos)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_competencia, horas_teoricas, horas_practicas, activo, id_log) VALUES (:n, :idp, :ht, :hp, :e, :idl);";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idp', $datos['competencia']);
            $preparado->bindParam(':ht', $datos['horas_teoricas']);
            $preparado->bindParam(':hp', $datos['horas_practicas']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarCompetenciaModel($datos)
    {
        $tabla  = 'asistencia_competencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, id_programa = :idp, id_log = :idl WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nom_competencia']);
            $preparado->bindParam(':idp', $datos['programa']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_competencia']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarModuloModel($datos)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, id_competencia = :idp, horas_teoricas = :ht, horas_practicas = :hp, id_log = :idl, activo = :est WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idp', $datos['competencia']);
            $preparado->bindParam(':ht', $datos['horas_teoricas']);
            $preparado->bindParam(':hp', $datos['horas_practicas']);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_modulo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProgramasAsistenciaModel()
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCompetenciasProgramaAsignadaModel($id, $id_log, $tipo)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.id_competencia,
        asp.nombre AS nom_competencia
        FROM
        carga_academica c
        LEFT JOIN asistencia_competencia asp
        ON asp.id = c.id_competencia
        WHERE c.id_profesor = " . $id_log . "
        AND c.activo = 1
        AND c.tipo = " . $tipo . "
        AND c.anio = '" . date('Y') . "'
        AND asp.id_programa = " . $id . "
        GROUP BY c.id_competencia
        ORDER BY asp.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarModulosCompetenciaAsignadaModel($id, $id_log, $tipo)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.id_modulo,
        asp.nombre AS nom_modulo
        FROM
        carga_academica c
        LEFT JOIN asistencia_modulos asp
        ON asp.id = c.id_modulo
        WHERE c.id_profesor = " . $id_log . "
        AND c.activo = 1
        AND c.tipo = " . $tipo . "
        AND c.anio = '" . date('Y') . "'
        AND asp.id_competencia = " . $id . "
        GROUP BY c.id_modulo
        ORDER BY asp.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCargaAcademicaAsignadaModel($id, $tipo)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.id_programa,
        asp.nombre AS nom_programa
        FROM
        carga_academica c
        LEFT JOIN asistencia_programa asp
        ON asp.id = c.id_programa
        WHERE c.id_profesor = " . $id . "
        AND c.activo = 1
        AND c.tipo = " . $tipo . "
        AND c.anio = " . date('Y') . "
        GROUP BY c.id_programa
        ORDER BY asp.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteProgramasModel()
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        sp.*,
        (SELECT COUNT(sc.id) FROM asistencia_competencia sc WHERE sc.id_programa = sp.id) AS cantidad_competencia,
        (SELECT COUNT(sm.id) FROM asistencia_modulos sm WHERE sm.id_competencia IN (SELECT sc.id FROM asistencia_competencia sc WHERE sc.id_programa = sp.id)) AS cantidad_modulos
        FROM asistencia_programa sp
        GROUP BY sp.id
        ORDER BY sp.nombre ASC LIMIT 20;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarProgramaModel($buscar)
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        sp.*,
        (SELECT COUNT(sc.id) FROM asistencia_competencia sc WHERE sc.id_programa = sp.id) AS cantidad_competencia,
        (SELECT COUNT(sm.id) FROM asistencia_modulos sm WHERE sm.id_competencia IN (SELECT sc.id FROM asistencia_competencia sc WHERE sc.id_programa = sp.id)) AS cantidad_modulos
        FROM asistencia_programa sp
        WHERE sp.nombre LIKE '%" . $buscar . "%'
        GROUP BY sp.id
        ORDER BY sp.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCompetenciasModel($id)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ac.*
        FROM asistencia_competencia ac
        WHERE ac.id_programa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarModulosCompetenciaModel($id)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        am.*
        FROM asistencia_modulos am
        WHERE am.id_competencia = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function competenciasProgramaModel($id)
    {
        $tabla  = 'asistencia_competencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_programa = :id AND activo = 1 ORDER BY nombre ASC;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function modulosCompetenciaModel($id)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_competencia = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasModuloModel($id, $id_log, $tipo)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        COALESCE(asm.horas - (SELECT IFNULL(SUM(asd.horas), 0) FROM asistencia_detalle asd WHERE asd.id_log = :idl AND asd.fecha LIKE '%" . date('Y') . "%' AND asd.id_asistencia IN(
            SELECT ass.id FROM asistencia ass WHERE ass.tipo = :tp)), 0) AS horas_restantes,
            asm.*
            FROM asistencia_modulos asm
            WHERE asm.id = :id
            AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':tp', $tipo);
            $preparado->bindParam(':idl', $id_log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarAsistenciaModel($datos)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_programa, id_competencia, id_modulo, id_log, tipo) VALUES (:idp, :idc, :idm, :idl, :tp);";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $datos['programa']);
            $preparado->bindParam(':idc', $datos['competencia']);
            $preparado->bindParam(':idm', $datos['modulo']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tp', $datos['tipo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id     = $cnx->ultimoIngreso($tabla);
                $result = array('guardar' => true, 'id' => $id);
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarDetalleAsistenciaModel($datos)
    {
        $tabla  = 'asistencia_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
                id_asistencia,
                fecha,
                hora_entrada,
                hora_salida,
                horas,
                tematica,
                id_log
                )
            VALUES
            (
                '" . $datos['id_asistencia'] . "',
                '" . $datos['fecha'] . "',
                '" . $datos['hora_entrada'] . "',
                '" . $datos['hora_salida'] . "',
                '" . $datos['horas'] . "',
                '" . $datos['tematica'] . "',
                '" . $datos['id_log'] . "');";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteAsistenciaModel($id)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                sa.*,
                sp.nombre AS nom_programa,
                sm.nombre AS nom_modulo,
                COALESCE(sm.horas - (SELECT IFNULL(SUM(asd.horas), 0) FROM asistencia_detalle asd WHERE asd.id_asistencia = sa.id AND asd.fecha LIKE '%" . date('Y') . "%'), 0) AS horas_realizadas,
                    (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                    (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                    sc.nombre AS nom_competencia,
                    sm.horas
                    FROM asistencia sa
                    LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                    LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                    LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                    WHERE sa.id_log = :id
                    ORDER BY sa.id DESC LIMIT 30;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteListadoAsistenciaModel()
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    sa.*,
                    sp.nombre AS nom_programa,
                    sm.nombre AS nom_modulo,
                    (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                    (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                    CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                    sm.horas,
                    sc.nombre as nom_competencia
                    FROM asistencia sa
                    LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                    LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                    LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                    LEFT JOIN usuarios u ON u.id_user = sa.id_log
                    ORDER BY sa.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteListadoCoordinadorAsistenciaModel($id)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    sa.*,
                    sp.nombre AS nom_programa,
                    sm.nombre AS nom_modulo,
                    (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                    (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                    CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                    sm.horas,
                    sc.nombre AS nom_competencia
                    FROM asistencia sa
                    LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                    LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                    LEFT JOIN asistencia_competencia sc ON sc.id = sc.id_competencia
                    LEFT JOIN usuarios u ON u.id_user = sa.id_log
                    WHERE sp.id_log = " . $id . "
                    ORDER BY sa.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarAsistenciasListadoModel($datos)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    sa.*,
                    sp.nombre AS nom_programa,
                    sm.nombre AS nom_modulo,
                    (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                    (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                    CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                    sm.horas,
                    sc.nombre as nom_competencia
                    FROM asistencia sa
                    LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                    LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                    LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                    LEFT JOIN usuarios u ON u.id_user = sa.id_log
                    WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', sp.nombre, ' ', sm.nombre, ' ', sa.id) LIKE '%" . $datos['buscar'] . "%'
                    " . $datos['anio'] . "
                    " . $datos['mes'] . "
                    AND sa.activo = 1
                    ORDER BY sa.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarListadoCoordinadorAsistenciaModel($datos)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    sa.*,
                    sp.nombre AS nom_programa,
                    sm.nombre AS nom_modulo,
                    (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                    (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                    CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                    sm.horas,
                    sc.nombre as nom_competencia
                    FROM asistencia sa
                    LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                    LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                    LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                    LEFT JOIN usuarios u ON u.id_user = sa.id_log
                    WHERE sp.id_log = " . $datos['id_log'] . "
                    AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', sp.nombre, ' ', sm.nombre, ' ', sa.id) LIKE '%" . $datos['buscar'] . "%'
                    " . $datos['anio'] . "
                    " . $datos['mes'] . "
                    AND sa.activo = 1
                    ORDER BY sa.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAsistenciaModel($id)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    sa.*,
                    sp.nombre AS nom_programa,
                    sm.nombre AS nom_modulo,
                    COALESCE(sm.horas - (SELECT IFNULL(SUM(asd.horas), 0) FROM asistencia_detalle asd WHERE asd.id_asistencia = sa.id AND asd.fecha LIKE '%" . date('Y') . "%'), 0) AS horas_realizadas,
                        (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                        (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                        CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                        (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia = sa.id) AS cantidad_horas,
                        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = sa.id_visto_bueno) AS nom_coordinador,
                        sc.nombre as nom_competencia,
                        sm.horas
                        FROM asistencia sa
                        LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                        LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                        LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                        LEFT JOIN usuarios u ON u.id_user = sa.id_log
                        WHERE sa.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesAsistenciaModel($id)
    {
        $tabla  = 'asistencia_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        *
                        FROM asistencia_detalle
                        WHERE id_asistencia = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarAsistenciaModel($datos)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET horas_programadas = :h, id_edit = :ide, fecha_edit = NOW() WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':h', $datos['horas_programadas']);
            $preparado->bindParam(':ide', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_asistencia']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function vaciarDetalleAsistenciaModel($id)
    {
        $tabla  = 'asistencia_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_asistencia = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosProgramaIdModel($id)
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function estadoModuloModel($estado, $id)
    {
        $tabla  = 'asistencia_modulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = :est WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':est', $estado);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarProgramaModel($datos)
    {
        $tabla  = 'asistencia_programa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = '" . $datos['nom_programa'] . "', id_edit = :idl, fecha_edit = NOW() WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_programa']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarAsistenciaModel($id, $log, $value, $hora_coor, $just_coor)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET visto_bueno = " . $value . ", id_visto_bueno = " . $log . ", fecha_visto_bueno = NOW(), horas_aprobadas_coord = '" . $hora_coor . "', justificacion_coor = '" . $just_coor . "' WHERE id = " . $id . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarAsistenciaModel($id)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET finalizado = 1 WHERE id = " . $id . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAsistenciasFiltroModel($datos)
    {
        $tabla  = 'asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        sa.*,
                        sp.nombre AS nom_programa,
                        sm.nombre AS nom_modulo,
                        (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                        (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia,
                        CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                        sm.horas,
                        sc.nombre as nom_competencia
                        FROM asistencia sa
                        LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                        LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                        LEFT JOIN asistencia_competencia sc ON sc.id = sa.id_competencia
                        LEFT JOIN usuarios u ON u.id_user = sa.id_log
                        WHERE sa.visto_bueno = 2
                        AND sa.cuenta_cobro = 0
                        AND sa.tipo = '" . $datos['tipo'] . "'
                        " . $datos['programa'] . "
                        " . $datos['id_log'] . "
                        " . $datos['mes'] . "
                        " . $datos['anio'] . "
                        ORDER BY sa.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function asignarCargaAcedmicaModel($datos)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO carga_academica (id_programa, id_competencia, id_modulo, id_profesor, tipo, horas, jornada, id_log, anio, mes_inicio, mes_fin, sede, colegio, ciclo, semestre)
                        VALUES (
                            '" . $datos['programa'] . "',
                            '" . $datos['competencia'] . "',
                            '" . $datos['modulo'] . "',
                            '" . $datos['profesor'] . "',
                            '" . $datos['tipo'] . "',
                            '" . $datos['horas'] . "',
                            '" . $datos['jornada'] . "',
                            '" . $datos['id_log'] . "',
                            '" . $datos['anio'] . "',
                            '" . $datos['mes_inicio'] . "',
                            '" . $datos['mes_fin'] . "',
                            '" . $datos['sede'] . "',
                            '" . $datos['colegio'] . "',
                            '" . $datos['ciclo'] . "',
                            '" . $datos['semestre'] . "'
                        );";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarCargasAcademicasModel($datos)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        c.*,
                        asp.nombre AS nom_programa,
                        asm.nombre AS nom_modulo,
                        j.nombre AS nom_jornada,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario
                        FROM carga_academica c
                        LEFT JOIN asistencia_modulos asm ON asm.id = c.id_modulo
                        LEFT JOIN asistencia_programa asp ON asp.id = c.id_programa
                        LEFT JOIN usuarios u ON u.id_user = c.id_profesor
                        LEFT JOIN jornada j ON j.id = c.jornada
                        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', asm.nombre, ' ', asp.nombre) LIKE '%" . $datos['buscar'] . "%'
                        " . $datos['docente'] . "
                        " . $datos['anio'] . "
                        ORDER BY c.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteCargaAcademicaModel()
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        c.*,
                        asp.nombre AS nom_programa,
                        asm.nombre AS nom_modulo,
                        j.nombre AS nom_jornada,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario
                        FROM carga_academica c
                        LEFT JOIN asistencia_modulos asm ON asm.id = c.id_modulo
                        LEFT JOIN asistencia_programa asp ON asp.id = c.id_programa
                        LEFT JOIN usuarios u ON u.id_user = c.id_profesor
                        LEFT JOIN jornada j ON j.id = c.jornada
                        ORDER BY c.id DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarAsistenciaModel($id, $buscar)
    {
        $tabla  = 'carga_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        sa.*,
                        sp.nombre AS nom_programa,
                        sm.nombre AS nom_modulo,
                        (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                        (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = sa.id GROUP BY sd.fecha LIMIT 1) AS anio_asistencia
                        FROM asistencia sa
                        LEFT JOIN asistencia_programa sp ON sp.id = sa.id_programa
                        LEFT JOIN asistencia_modulos sm ON sm.id = sa.id_modulo
                        LEFT JOIN usuarios u ON u.id_user = sa.id_log
                        WHERE sa.id_log = :id
                        AND CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', sp.nombre, ' ', sm.nombre) LIKE '%" . $buscar . "%'
                        ORDER BY sa.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarPlanFormacionModel($datos)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_user, linea, temas, objetivo, recursos, fecha_proyectada, id_log, token, hora_proyectada, tipo) VALUES (:idu, :ln, :tm, :ob, :rc, :fp, :idl, :tk, :h, :tp);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_log']);
            $preparado->bindParam(':ln', $datos['linea']);
            $preparado->bindParam(':tm', $datos['temas']);
            $preparado->bindParam(':ob', $datos['objetivo']);
            $preparado->bindParam(':rc', $datos['recursos']);
            $preparado->bindParam(':fp', $datos['fecha_ejecutar']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tk', $datos['token']);
            $preparado->bindParam(':h', $datos['hora']);
            $preparado->bindParam(':tp', $datos['tipo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarCodigoPlanFormacionModel($token)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = :tk;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':tk', $token);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPlanFormacionModel()
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        p.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
                        tp.nombre as nom_tipo
                        FROM plan_formacion p
                        LEFT JOIN usuarios u ON u.id_user = p.id_user
                        LEFT JOIN tipo_plan tp ON tp.id = p.tipo
                        ORDER BY p.fecha_proyectada DESC LIMIT 25;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPlanesFormacionModel()
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        p.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
                        tp.nombre as nom_tipo
                        FROM plan_formacion p
                        LEFT JOIN usuarios u ON u.id_user = p.id_user
                        LEFT JOIN tipo_plan tp ON tp.id = p.tipo
                        ORDER BY p.fecha_proyectada DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarPlanFormacionModel($datos)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        p.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
                        tp.nombre as nom_tipo
                        FROM plan_formacion p
                        LEFT JOIN usuarios u ON u.id_user = p.id_user
                        LEFT JOIN tipo_plan tp ON tp.id = p.tipo
                        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', p.id, ' ', p.linea, ' ', p.temas) LIKE '%" . $datos['buscar'] . "%'
                        " . $datos['fecha'] . "
                        " . $datos['usuario'] . "
                        " . $datos['arte'] . "
                        ORDER BY p.fecha_proyectada DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPlanFormacionDatosTokenModel($token)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        p.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario
                        FROM plan_formacion p
                        LEFT JOIN usuarios u ON u.id_user = p.id_user
                        WHERE p.token  = :t;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':t', $token);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesPlanFormacionModel($id)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        p.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
                        tp.nombre as nom_tipo
                        FROM plan_formacion p
                        LEFT JOIN usuarios u ON u.id_user = p.id_user
                        LEFT JOIN tipo_plan tp ON tp.id = p.tipo
                        WHERE p.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarUsuarioAsistenciaPlanFormacionModel($id, $usuario)
    {
        $tabla  = 'plan_formacion_asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_plan = :idp AND id_user = :idu AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $id);
            $preparado->bindParam(':idu', $usuario);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function tomarAsistenciaPlanFormacionModel($id, $usuario)
    {
        $tabla  = 'plan_formacion_asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_plan, id_user, fechareg) VALUES (:idp, :idu, NOW());";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $id);
            $preparado->bindParam(':idu', $usuario);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAsistenciaPlanFormacionModel($id)
    {
        $tabla  = 'plan_formacion_asistencia';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        pa.*,
                        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
                        u.documento,
                        u.telefono,
                        u.correo
                        FROM plan_formacion_asistencia pa
                        LEFT JOIN usuarios u ON u.id_user = pa.id_user
                        WHERE pa.id_plan = :id AND pa.activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoPlanFormacionModel($datos)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = :est, fecha_ejecutada = :fe, hora_ejecutada = NOW(), id_log = :idl, observacion = :ob WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':fe', $datos['fecha']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':id', $datos['id_plan']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function planFormacionArteModel($estado, $id)
    {
        $tabla  = 'plan_formacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET arte = :est WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':est', $estado);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
