<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloIngresos extends conexion
{

    public static function mostrarMetodosPagoModel()
    {
        $tabla  = 'metodo_pago';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarConceptoPagoModel()
    {
        $tabla  = 'ingreso_concepto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarDocumentoModel($documento)
    {
        $tabla  = 'ingreso_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE documento = '" . $documento . "' ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarIngresoModel($datos)
    {
        $tabla  = 'ingresos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_usuario,
            fecha,
            concepto,
            metodo_pago,
            valor,
            id_log,
            tipo_registro,
            observacion,
            banco,
            cheque
            )
        VALUES
        (
            '" . $datos['id_usuario'] . "',
            '" . $datos['fecha'] . "',
            '" . $datos['concepto'] . "',
            '" . $datos['pago'] . "',
            '" . $datos['valor'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['tipo'] . "',
            '" . $datos['observacion'] . "',
            '" . $datos['banco'] . "',
            '" . $datos['cheque'] . "');";
            try {
                $preparado = $cnx->preparar($cmdsql);
                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                if ($preparado->execute()) {
                    $id        = $cnx->ultimoIngreso($tabla);
                    $resultado = array('id' => $id, 'guardar' => true);
                    return $resultado;
                } else {
                    return false;
                }
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage();
            }
            $cnx->closed();
            $cnx = null;
        }

        public static function registrarEgresosModel($datos)
        {
            $tabla  = 'egresos';
            $cnx    = conexion::singleton_conexion();
            $cmdsql = "INSERT INTO " . $tabla . " (
                id_usuario,
                fecha,
                concepto,
                metodo_pago,
                valor,
                id_log,
                tipo_registro,
                banco,
                cheque,
                observacion
                )
            VALUES
            (
                '" . $datos['id_usuario'] . "',
                '" . $datos['fecha'] . "',
                '" . $datos['concepto'] . "',
                '" . $datos['pago'] . "',
                '" . $datos['valor'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['tipo'] . "',
                '" . $datos['banco'] . "',
                '" . $datos['cheque'] . "',
                '" . $datos['observacion'] . "');";
                try {
                    $preparado = $cnx->preparar($cmdsql);
                    $preparado->setFetchMode(PDO::FETCH_ASSOC);
                    if ($preparado->execute()) {
                        $id        = $cnx->ultimoIngreso($tabla);
                        $resultado = array('id' => $id, 'guardar' => true);
                        return $resultado;
                    } else {
                        return false;
                    }
                } catch (PDOException $e) {
                    print "Error!: " . $e->getMessage();
                }
                $cnx->closed();
                $cnx = null;
            }

            public static function registrarEgresosWsModel($datos)
            {
                $tabla  = 'egresos_ws';
                $cnx    = conexion::singleton_conexion();
                $cmdsql = "INSERT INTO " . $tabla . " (
                    id_usuario,
                    fecha,
                    concepto,
                    metodo_pago,
                    valor,
                    id_log,
                    tipo_registro,
                    banco,
                    cheque,
                    observacion
                    )
                VALUES
                (
                    '" . $datos['id_usuario'] . "',
                    '" . $datos['fecha'] . "',
                    '" . $datos['concepto'] . "',
                    '" . $datos['pago'] . "',
                    '" . $datos['valor'] . "',
                    '" . $datos['id_log'] . "',
                    '" . $datos['tipo'] . "',
                    '" . $datos['banco'] . "',
                    '" . $datos['cheque'] . "',
                    '" . $datos['observacion'] . "');";
                    try {
                        $preparado = $cnx->preparar($cmdsql);
                        $preparado->setFetchMode(PDO::FETCH_ASSOC);
                        if ($preparado->execute()) {
                            $id        = $cnx->ultimoIngreso($tabla);
                            $resultado = array('id' => $id, 'guardar' => true);
                            return $resultado;
                        } else {
                            return false;
                        }
                    } catch (PDOException $e) {
                        print "Error!: " . $e->getMessage();
                    }
                    $cnx->closed();
                    $cnx = null;
                }

                public static function agregarConceptoModel($datos)
                {
                    $tabla  = 'ingreso_concepto';
                    $cnx    = conexion::singleton_conexion();
                    $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_log) VALUES (:n, :idl);";
                    try {
                        $preparado = $cnx->preparar($cmdsql);
                        $preparado->bindParam(':n', $datos['nombre']);
                        $preparado->bindParam(':idl', $datos['id_log']);
                        $preparado->setFetchMode(PDO::FETCH_ASSOC);
                        if ($preparado->execute()) {
                            $id        = $cnx->ultimoIngreso($tabla);
                            $resultado = array('id' => $id, 'guardar' => true);
                            return $resultado;
                        } else {
                            return false;
                        }
                    } catch (PDOException $e) {
                        print "Error!: " . $e->getMessage();
                    }
                    $cnx->closed();
                    $cnx = null;
                }

                public static function registrarUsuarioIngresoModel($datos)
                {
                    $tabla  = 'ingreso_usuario';
                    $cnx    = conexion::singleton_conexion();
                    $cmdsql = "INSERT INTO " . $tabla . " (documento, nombre, ciudad, id_log) VALUES ('" . $datos['documento'] . "','" . $datos['nombre'] . "','" . $datos['ciudad'] . "','" . $datos['id_log'] . "');";
                        try {
                            $preparado = $cnx->preparar($cmdsql);
                            $preparado->setFetchMode(PDO::FETCH_ASSOC);
                            if ($preparado->execute()) {
                                $id        = $cnx->ultimoIngreso($tabla);
                                $resultado = array('guardar' => true, 'id' => $id);
                                return $resultado;
                            } else {
                                return false;
                            }
                        } catch (PDOException $e) {
                            print "Error!: " . $e->getMessage();
                        }
                        $cnx->closed();
                        $cnx = null;
                    }

                    public static function actualizarUsuarioIngresosModel($datos)
                    {
                        $tabla  = 'ingreso_usuario';
                        $cnx    = conexion::singleton_conexion();
                        $cmdsql = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, ciudad = :c, id_log = :idl, fecha_edit = NOW() WHERE id = :id;
                        INSERT INTO history_log (id_user, accion) VALUES (:idl, 'Ha editado al usuario de ingresos - egresos Numero " . $datos['id_usuario'] . "');";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':d', $datos['documento']);
                                $preparado->bindParam(':n', $datos['nombre']);
                                $preparado->bindParam(':c', $datos['ciudad']);
                                $preparado->bindParam(':idl', $datos['id_log']);
                                $preparado->bindParam(':id', $datos['id_usuario']);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function ultimosIngresosModel()
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM ingresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC LIMIT 20;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function ultimosEgresosModel()
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM egresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC LIMIT 20;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function ultimosEgresosWsModel()
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC LIMIT 20;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function todosIngresosModel()
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM ingresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function todosEgresosModel()
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM egresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function todosEgresosWsModel()
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarDatosIngresoIdModel($id)
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM ingresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetch();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarDatosEgresoIdModel($id)
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM egresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetch();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarDatosEgresoWsIdModel($id)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetch();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarHistorialIngresosUsuarioModel($id)
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM ingresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id_usuario = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarHistorialEgresosUsuarioModel($id)
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM egresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id_usuario = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarHistorialEgresosWsUsuarioControl($id)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id_usuario = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarHistorialEgresosWsUsuarioModel($id)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE ig.id_usuario = :id
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function mostrarDatosUsuariosIngresosModel($id)
                        {
                            $tabla  = 'ingreso_usuario';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT * FROM ingreso_usuario ig WHERE ig.id = :id;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $id);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetch();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function buscarIngresosModel($datos)
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM ingresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE CONCAT(igu.documento, ' ', igu.nombre,' ', igu.ciudad, ' ', igc.nombre, ' ', mt.nombre, ' ', ig.fecha,' ' ,ig.valor) LIKE '%" . $datos['buscar'] . "%' " . $datos['concepto'] . " " . $datos['metodo'] . "
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function buscarEgresosModel($datos)
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM egresos ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE CONCAT(igu.documento, ' ', igu.nombre,' ', igu.ciudad, ' ', igc.nombre, ' ', mt.nombre, ' ', ig.fecha,' ' ,ig.valor) LIKE '%" . $datos['buscar'] . "%' " . $datos['concepto'] . " " . $datos['metodo'] . "
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function buscarEgresosWsModel($datos)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SELECT
                            ig.*,
                            mt.nombre AS nom_pago,
                            igc.nombre AS nom_concepto,
                            igu.documento AS doc_user,
                            igu.nombre AS nom_user,
                            igu.ciudad AS nom_ciudad,
                            CONCAT(u.nombre, ' ', u.apellido) AS usuario_realiza
                            FROM " . $tabla . " ig
                            LEFT JOIN metodo_pago mt ON mt.id = ig.metodo_pago
                            LEFT JOIN ingreso_concepto igc ON igc.id = ig.concepto
                            LEFT JOIN ingreso_usuario igu ON igu.id = ig.id_usuario
                            LEFT JOIN usuarios u ON u.id_user = ig.id_log
                            WHERE CONCAT(igu.documento, ' ', igu.nombre,' ', igu.ciudad, ' ', igc.nombre, ' ', mt.nombre, ' ', ig.fecha,' ' ,ig.valor) LIKE '%" . $datos['buscar'] . "%' " . $datos['concepto'] . " " . $datos['metodo'] . "
                            ORDER BY ig.id DESC;";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->setFetchMode(PDO::FETCH_ASSOC);
                                if ($preparado->execute()) {
                                    return $preparado->fetchAll();
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function comandoSQL()
                        {
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "SET SQL_BIG_SELECTS=1";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function historyLogModel($datos)
                        {
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "INSERT INTO history_log (id_user, accion) VALUES (:id, :c);";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                $preparado->bindParam(':id', $datos['id_user']);
                                $preparado->bindParam(':c', $datos['accion']);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function anularIngresoModel($id, $log)
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 0, id_anulado = " . $log . ", fecha_anulado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function autorizarIngresoModel($id, $log)
                        {
                            $tabla  = 'ingresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 1, id_autorizado = " . $log . ", fecha_autorizado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function anularEgresoModel($id, $log)
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 0, id_anulado = " . $log . ", fecha_anulado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function autorizarEgresoModel($id, $log)
                        {
                            $tabla  = 'egresos';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 1, id_autorizado = " . $log . ", fecha_autorizado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function anularEgresoWsModel($id, $log)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 0, id_anulado = " . $log . ", fecha_anulado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }

                        public static function autorizarEgresoWsModel($id, $log)
                        {
                            $tabla  = 'egresos_ws';
                            $cnx    = conexion::singleton_conexion();
                            $cmdsql = "UPDATE " . $tabla . " SET activo = 1, id_autorizado = " . $log . ", fecha_autorizado = NOW() WHERE id = " . $id . ";";
                            try {
                                $preparado = $cnx->preparar($cmdsql);
                                if ($preparado->execute()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (PDOException $e) {
                                print "Error!: " . $e->getMessage();
                            }
                            $cnx->closed();
                            $cnx = null;
                        }
                    }
