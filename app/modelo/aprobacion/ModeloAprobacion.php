<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloAprobacion extends conexion
{

    public static function registrarSolicitudAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            nom_archivo,
            archivo,
            token,
            id_log,
            id_user,
            concepto,
            id_proyecto,
            valor
            )
        VALUES
        (
            '" . $datos['nom_archivo'] . "',
            '" . $datos['archivo'] . "',
            '" . $datos['token'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['concepto'] . "',
            '" . $datos['id_proyecto'] . "',
            '" . $datos['valor'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarSolicitudWsAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
                nom_archivo,
                archivo,
                token,
                id_log,
                id_user,
                concepto,
                id_proyecto,
                valor
                )
            VALUES
            (
                '" . $datos['nom_archivo'] . "',
                '" . $datos['archivo'] . "',
                '" . $datos['token'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['concepto'] . "',
                '" . $datos['id_proyecto'] . "',
                '" . $datos['valor'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarSolicitudFundepexAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
                nom_archivo,
                archivo,
                token,
                id_log,
                id_user,
                concepto,
                id_proyecto,
                valor
                )
            VALUES
            (
                '" . $datos['nom_archivo'] . "',
                '" . $datos['archivo'] . "',
                '" . $datos['token'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['concepto'] . "',
                '" . $datos['id_proyecto'] . "',
                '" . $datos['valor'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarSolicitudTalentoAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
                nom_archivo,
                archivo,
                token,
                id_log,
                id_user,
                concepto,
                id_proyecto,
                valor
                )
            VALUES
            (
                '" . $datos['nom_archivo'] . "',
                '" . $datos['archivo'] . "',
                '" . $datos['token'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['concepto'] . "',
                '" . $datos['id_proyecto'] . "',
                '" . $datos['valor'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarSolicitudAidaBarriosAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
                nom_archivo,
                archivo,
                token,
                id_log,
                id_user,
                concepto,
                id_proyecto,
                valor
                )
            VALUES
            (
                '" . $datos['nom_archivo'] . "',
                '" . $datos['archivo'] . "',
                '" . $datos['token'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['concepto'] . "',
                '" . $datos['id_proyecto'] . "',
                '" . $datos['valor'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAprobacionIdModel($id)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto
                FROM " . $tabla . " ap WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosWsAprobacionIdModel($id)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto
                FROM " . $tabla . " ap WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosFundepexAprobacionIdModel($id)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto
                FROM " . $tabla . " ap WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosTalentoAprobacionIdModel($id)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto
                FROM " . $tabla . " ap WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAidaBarriosAprobacionIdModel($id)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto
                FROM " . $tabla . " ap WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarTokenUsoModel($token)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarWsTokenUsoModel($token)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarFundepexTokenUsoModel($token)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarTalentoTokenUsoModel($token)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarAidaBarriosTokenUsoModel($token)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarSolicitudPagoModel($id, $log, $tipo_pago)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, id_aprobado = :idl, fecha_aprobado = NOW(), token_usado = 1, tipo_pago = :tp WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->bindParam(':tp', $tipo_pago);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarWsSolicitudPagoModel($id, $log, $tipo_pago)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, id_aprobado = :idl, fecha_aprobado = NOW(), token_usado = 1, tipo_pago = :tp WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->bindParam(':tp', $tipo_pago);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarFundepexSolicitudPagoModel($id, $log, $tipo_pago)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, id_aprobado = :idl, fecha_aprobado = NOW(), token_usado = 1, tipo_pago = :tp WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->bindParam(':tp', $tipo_pago);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarTalentoSolicitudPagoModel($id, $log, $tipo_pago)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, id_aprobado = :idl, fecha_aprobado = NOW(), token_usado = 1, tipo_pago = :tp WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->bindParam(':tp', $tipo_pago);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarAidaBarriosSolicitudPagoModel($id, $log, $tipo_pago)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, id_aprobado = :idl, fecha_aprobado = NOW(), token_usado = 1, tipo_pago = :tp WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->bindParam(':tp', $tipo_pago);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarSolicitudPagoModel($id, $log)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, id_denegado = :idl, fecha_denegado = NOW(), token_usado = 1 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarWsSolicitudPagoModel($id, $log)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, id_denegado = :idl, fecha_denegado = NOW(), token_usado = 1 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarFundepexSolicitudPagoModel($id, $log)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, id_denegado = :idl, fecha_denegado = NOW(), token_usado = 1 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarTalentoSolicitudPagoModel($id, $log)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, id_denegado = :idl, fecha_denegado = NOW(), token_usado = 1 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarAidaBarriosSolicitudPagoModel($id, $log)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, id_denegado = :idl, fecha_denegado = NOW(), token_usado = 1 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAprobacionUsuarioModel($id)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        WHERE ap.id_user = :id
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudAprobacionUsuarioModel($datos)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        LEFT JOIN proyecto p ON p.id = ap.id_proyecto
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proyecto'] . "
        " . $datos['fecha'] . "
        ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAprobacionWsUsuarioModel($id)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        WHERE ap.id_user = :id
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAprobacionFundepexUsuarioModel($id)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        WHERE ap.id_user = :id
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAprobacionTalentoUsuarioModel($id)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        WHERE ap.id_user = :id
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAprobacionAidaBarriosUsuarioModel($id)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        WHERE ap.id_user = :id
        ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudAprobacionWsUsuarioModel($datos)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        LEFT JOIN proyecto p ON p.id = ap.id_proyecto
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proyecto'] . "
        " . $datos['fecha'] . "
        ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudAprobacionFundepexUsuarioModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        LEFT JOIN proyecto p ON p.id = ap.id_proyecto
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proyecto'] . "
        " . $datos['fecha'] . "
        ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudAprobacionTalentoUsuarioModel($datos)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        LEFT JOIN proyecto p ON p.id = ap.id_proyecto
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proyecto'] . "
        " . $datos['fecha'] . "
        ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudAprobacionAidaBarriosUsuarioModel($datos)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.documento,
        u.correo,
        (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
        CASE
        WHEN ap.estado = 1 THEN 'Pendiente'
        WHEN ap.estado = 2 THEN 'Aprobado'
        WHEN ap.estado = 3 THEN 'Denegado'
        END AS estado_sol
        FROM " . $tabla . " ap
        LEFT JOIN usuarios u ON u.id_user = ap.id_user
        LEFT JOIN proyecto p ON p.id = ap.id_proyecto
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proyecto'] . "
        " . $datos['fecha'] . "
        ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesWsAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesFundepexAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesTalentoAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesAidaBarriosAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasSolicitudesAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasSolicitudesWsAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasSolicitudesFundepexAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasSolicitudesTalentoAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasSolicitudesAidaBarriosAprobacionModel()
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudesAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN proyecto p ON p.id = ap.id_proyecto
                WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['proyecto'] . "
                " . $datos['fecha'] . "
                ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudesWsAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN proyecto p ON p.id = ap.id_proyecto
                WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['proyecto'] . "
                " . $datos['fecha'] . "
                ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudesFundepexAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN proyecto p ON p.id = ap.id_proyecto
                WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['proyecto'] . "
                " . $datos['fecha'] . "
                ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudesTalentoAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN proyecto p ON p.id = ap.id_proyecto
                WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['proyecto'] . "
                " . $datos['fecha'] . "
                ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarSolicitudesAidaBarriosAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN proyecto p ON p.id = ap.id_proyecto
                WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido, ' ', ap.id, ' ', ap.nom_archivo, ' ', ap.concepto, ' ', p.nombre, ' ', ap.valor) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['proyecto'] . "
                " . $datos['fecha'] . "
                ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArchivoAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_archivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_aprobacion) VALUEs (:n, :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aprobacion']);
            $preparado->bindParam(':n', $datos['archivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArchivoWsAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_archivo_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_aprobacion) VALUEs (:n, :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aprobacion']);
            $preparado->bindParam(':n', $datos['archivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArchivoFundepexAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_archivo_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_aprobacion) VALUEs (:n, :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aprobacion']);
            $preparado->bindParam(':n', $datos['archivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArchivoTalentoAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_archivo_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_aprobacion) VALUEs (:n, :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aprobacion']);
            $preparado->bindParam(':n', $datos['archivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArchivoAidaBarriosAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_archivo_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_aprobacion) VALUEs (:n, :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aprobacion']);
            $preparado->bindParam(':n', $datos['archivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArchivosAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, nombre as archivo FROM " . $tabla . " WHERE id_aprobacion = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArchivosWsAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, nombre as archivo FROM " . $tabla . " WHERE id_aprobacion = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArchivosFundepexAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, nombre as archivo FROM " . $tabla . " WHERE id_aprobacion = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArchivosTalentoAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, nombre as archivo FROM " . $tabla . " WHERE id_aprobacion = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArchivosAidaBarriosAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, nombre as archivo FROM " . $tabla . " WHERE id_aprobacion = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionAprobacionPagoModel($id)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol,
                apt.nombre as nom_tipo_pago
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN aprobacion_tipo_pago apt ON apt.id = ap.tipo_pago
                WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionWsAprobacionPagoModel($id)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol,
                apt.nombre as nom_tipo_pago
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN aprobacion_tipo_pago apt ON apt.id = ap.tipo_pago
                WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionFundepexAprobacionPagoModel($id)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol,
                apt.nombre as nom_tipo_pago
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN aprobacion_tipo_pago apt ON apt.id = ap.tipo_pago
                WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionTalentoAprobacionPagoModel($id)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol,
                apt.nombre as nom_tipo_pago
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN aprobacion_tipo_pago apt ON apt.id = ap.tipo_pago
                WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionAidaBarriosAprobacionPagoModel($id)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                ap.*,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
                u.documento,
                u.correo,
                (SELECT p.nombre FROM proyecto p WHERE p.id = ap.id_proyecto) AS nom_proyecto,
                CASE
                WHEN ap.estado = 1 THEN 'Pendiente'
                WHEN ap.estado = 2 THEN 'Aprobado'
                WHEN ap.estado = 3 THEN 'Denegado'
                END AS estado_sol,
                apt.nombre as nom_tipo_pago
                FROM " . $tabla . " ap
                LEFT JOIN usuarios u ON u.id_user = ap.id_user
                LEFT JOIN aprobacion_tipo_pago apt ON apt.id = ap.tipo_pago
                WHERE ap.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirComprobanteModel($datos)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
                comprobante = '" . $datos['comprobante'] . "',
                fecha_comprobante = NOW(),
                id_log_comprobante = " . $datos['id_log'] . "
                WHERE id = " . $datos['id_solicitud'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirWsComprobanteModel($datos)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
                comprobante = '" . $datos['comprobante'] . "',
                fecha_comprobante = NOW(),
                id_log_comprobante = " . $datos['id_log'] . "
                WHERE id = " . $datos['id_solicitud'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirFundepexComprobanteModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
                comprobante = '" . $datos['comprobante'] . "',
                fecha_comprobante = NOW(),
                id_log_comprobante = " . $datos['id_log'] . "
                WHERE id = " . $datos['id_solicitud'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirTalentoComprobanteModel($datos)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
                comprobante = '" . $datos['comprobante'] . "',
                fecha_comprobante = NOW(),
                id_log_comprobante = " . $datos['id_log'] . "
                WHERE id = " . $datos['id_solicitud'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirAidaBarriosComprobanteModel($datos)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
                comprobante = '" . $datos['comprobante'] . "',
                fecha_comprobante = NOW(),
                id_log_comprobante = " . $datos['id_log'] . "
                WHERE id = " . $datos['id_solicitud'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerEstadoSolicitudModel($token)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, token_usado = 0
                WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerEstadoSolicitudWsModel($token)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, token_usado = 0
                WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerEstadoSolicitudFundepexModel($token)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, token_usado = 0
                WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerEstadoSolicitudTalentoModel($token)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, token_usado = 0
                WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerEstadoSolicitudAidaBarriosModel($token)
    {
        $tabla  = 'aprobacion_pagos_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, token_usado = 0
                WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarAprobacionModel($datos)
    {
        $tabla  = 'aprobacion_pagos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proyecto = '" . $datos['id_proyecto'] . "', nom_archivo = '" . $datos['nom_archivo'] . "', concepto = '" . $datos['concepto'] . "', valor = '" . $datos['valor'] . "', fechareg = '" . $datos['fecha_sol'] . "', id_edit = '" . $datos['id_log'] . "', fecha_edit = NOW() WHERE id = '" . $datos['id_aprobacion'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarAprobacionWsModel($datos)
    {
        $tabla  = 'aprobacion_pagos_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proyecto = '" . $datos['id_proyecto'] . "', nom_archivo = '" . $datos['nom_archivo'] . "', concepto = '" . $datos['concepto'] . "', valor = '" . $datos['valor'] . "', fechareg = '" . $datos['fecha_sol'] . "', id_edit = '" . $datos['id_log'] . "', fecha_edit = NOW() WHERE id = '" . $datos['id_aprobacion'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarAprobacionFundepexModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proyecto = '" . $datos['id_proyecto'] . "', nom_archivo = '" . $datos['nom_archivo'] . "', concepto = '" . $datos['concepto'] . "', valor = '" . $datos['valor'] . "', fechareg = '" . $datos['fecha_sol'] . "', id_edit = '" . $datos['id_log'] . "', fecha_edit = NOW() WHERE id = '" . $datos['id_aprobacion'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

     public static function editarAprobacionTalentoModel($datos)
    {
        $tabla  = 'aprobacion_pagos_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proyecto = '" . $datos['id_proyecto'] . "', nom_archivo = '" . $datos['nom_archivo'] . "', concepto = '" . $datos['concepto'] . "', valor = '" . $datos['valor'] . "', fechareg = '" . $datos['fecha_sol'] . "', id_edit = '" . $datos['id_log'] . "', fecha_edit = NOW() WHERE id = '" . $datos['id_aprobacion'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarAprobacionAidaBarriosModel($datos)
    {
        $tabla  = 'aprobacion_pagos_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proyecto = '" . $datos['id_proyecto'] . "', nom_archivo = '" . $datos['nom_archivo'] . "', concepto = '" . $datos['concepto'] . "', valor = '" . $datos['valor'] . "', fechareg = '" . $datos['fecha_sol'] . "', id_edit = '" . $datos['id_log'] . "', fecha_edit = NOW() WHERE id = '" . $datos['id_aprobacion'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarArchivoAprobacionModel($id)
    {
        $tabla  = 'aprobacion_archivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarArchivoAprobacionWsModel($id)
    {
        $tabla  = 'aprobacion_archivo_ws';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarArchivoAprobacionFundepexModel($id)
    {
        $tabla  = 'aprobacion_archivo_fundepex';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

     public static function eliminarArchivoAprobacionTalentoModel($id)
    {
        $tabla  = 'aprobacion_archivo_talento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarArchivoAprobacionAidaBarriosModel($id)
    {
        $tabla  = 'aprobacion_archivo_aida_barrios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
