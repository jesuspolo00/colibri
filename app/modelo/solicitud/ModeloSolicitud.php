<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloSolicitud extends conexion
{

    public static function registrarSolicitudModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_user,
            id_area,
            fecha_solicitud,
            id_log,
            justificacion,
            consecutivo
            )
        VALUES (:idu, :ida, :fs, :idl, :j, :c);

        INSERT INTO solicitudes_inicial (
            id_user,
            id_area,
            fecha_solicitud,
            id_log,
            justificacion
            )
        VALUES (:idu, :ida, :fs, :idl, :j);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['area']);
            $preparado->bindParam(':fs', $datos['fecha_solicitud']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':j', $datos['justificacion']);
            $preparado->bindParam(':c', $datos['consecutivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarProdcuctosModel($datos)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_solicitud,
            producto,
            cantidad,
            id_log,
            iva
            )
        VALUES (:ids,:p,:c,:idl,0);

        INSERT INTO solicitud_productos_inicial (
            id_solicitud,
            producto,
            cantidad,
            id_log,
            iva
            )
        VALUES (:ids,:p,:c,:idl,0);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['id_solicitud']);
            $preparado->bindParam(':p', $datos['producto']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesModel()
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_proveedor) AS nom_proveedor
        FROM solicitudes s ORDER BY id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostratLimiteSolicitudesModel()
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_proveedor) AS nom_proveedor
        FROM solicitudes s ORDER BY consecutivo DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudBuscarModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_proveedor) AS nom_proveedor
        FROM solicitudes s
        LEFT JOIN usuarios u ON u.id_user = s.id_user
        LEFT JOIN areas a ON a.id = s.id_area
        WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', s.consecutivo, ' ', s.justificacion, ' ', s.fecha_solicitud, ' ', a.nombre) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['proveedor'] . "
        " . $datos['fecha'] . "
        ORDER BY consecutivo DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSolicitudesUsuarioModel($id)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario
        FROM solicitudes s WHERE s.id_user = :id ORDER BY fechareg DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSolicitudIdModel($id)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_log) AS nom_aprobado,
        (SELECT u.documento FROM usuarios u WHERE u.id_user = s.id_user) AS documento,
        (SELECT u.telefono FROM usuarios u WHERE u.id_user = s.id_user) AS telefono,
        (SELECT p.nombre FROM proveedor_detalle p WHERE p.id = s.id_proveedor) AS nom_proveedor
        FROM solicitudes s WHERE s.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSolicitudInicialIdModel($id)
    {
        $tabla  = 'solicitudes_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*,
        (SELECT a.nombre FROM areas a WHERE a.id = s.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_user) AS nom_usuario,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = s.id_log) AS nom_aprobado,
        (SELECT u.documento FROM usuarios u WHERE u.id_user = s.id_user) AS documento,
        (SELECT u.telefono FROM usuarios u WHERE u.id_user = s.id_user) AS telefono,
        (SELECT ss.consecutivo FROM solicitudes ss WHERE ss.id = s.id) AS consecutivo
        FROM solicitudes s WHERE s.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProdcutosSolicitudModel($id)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " sp WHERE sp.id_solicitud = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProdcutosSolicitudInicialModel($id)
    {
        $tabla  = 'solicitud_productos_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " sp WHERE sp.id_solicitud = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET
        estado = '" . $datos['estado'] . "',
        observacion = '" . $datos['observacion'] . "',
        id_log = '" . $datos['id_log'] . "',
        iva = '" . $datos['iva'] . "',
        id_proveedor = '" . $datos['id_proveedor'] . "',
        fecha_solicitud = '" . $datos['fecha_solicitado'] . "',
        generado = '" . $datos['generado'] . "',
        fecha_editado = NOW(),
        id_editado = '" . $datos['id_log'] . "',
        fecha_estado = '" . $datos['fecha_aprobacion'] . "',
        consecutivo = '" . $datos['consecutivo'] . "'
        WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_solicitud']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function preciosProductoControl($datos)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET precio = :p, producto = :n, cantidad = :c, iva = :iv WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_producto']);
            $preparado->bindParam(':p', $datos['precio']);
            $preparado->bindParam(':n', $datos['nom_producto']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':iv', $datos['iva']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarSolicitudModel($datos)
    {
        $tabla  = 'solicitud_verificacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_solicitud,
            cantidad,
            observacion_cant,
            calidad,
            observacion_calidad,
            precios,
            observacion_precios,
            plazos,
            observacion_plazo,
            id_log,
            fecha_verificacion
            )
        VALUES
        (
            '" . $datos['id_solicitud'] . "',
            '" . $datos['cantidad'] . "',
            '" . $datos['observacion_cant'] . "',
            '" . $datos['calidad'] . "',
            '" . $datos['observacion_calidad'] . "',
            '" . $datos['precios'] . "',
            '" . $datos['observacion_precio'] . "',
            '" . $datos['plazos'] . "',
            '" . $datos['observacion_plazo'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['fecha_verificacion'] . "'
            );

            INSERT INTO solicitud_verificacion_inicial (
            id_solicitud,
            cantidad,
            observacion_cant,
            calidad,
            observacion_calidad,
            precios,
            observacion_precios,
            plazos,
            observacion_plazo,
            id_log,
            fecha_verificacion
            )
            VALUES
            (
            '" . $datos['id_solicitud'] . "',
            '" . $datos['cantidad'] . "',
            '" . $datos['observacion_cant'] . "',
            '" . $datos['calidad'] . "',
            '" . $datos['observacion_calidad'] . "',
            '" . $datos['precios'] . "',
            '" . $datos['observacion_precio'] . "',
            '" . $datos['plazos'] . "',
            '" . $datos['observacion_plazo'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['fecha_verificacion'] . "'
            );
            ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVerificacionModel($id)
    {
        $tabla  = 'solicitud_verificacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = id_log) AS nom_usuario FROM " . $tabla . " WHERE id_solicitud = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarSolicitudModel($id, $log)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, id_estado_user = :idl, fecha_estado = NOW() WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idl', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosVerificacionInicialModel($id)
    {
        $tabla  = 'solicitud_verificacion_inicial';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = id_log) AS nom_usuario FROM " . $tabla . " WHERE id_solicitud = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerProductoModel($id)
    {
        $tabla  = 'solicitud_productos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function anularSolicitudModel($datos)
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET motivo = :m, activo = :e, id_log = :idl, id_estado_user = :idl, fecha_estado = NOW() WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_solicitud']);
            $preparado->bindParam(':m', $datos['motivo']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function ultimoConsecutivoModel()
    {
        $tabla  = 'solicitudes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT consecutivo FROM " . $tabla . " ORDER BY consecutivo DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
