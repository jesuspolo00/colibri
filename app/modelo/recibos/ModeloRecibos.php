<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloRecibos extends conexion
{
    public static function registrarProyectosModel($datos)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, ciudad, id_proveedor, id_log) VALUES (:n, :c, :idp, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':idp', $datos['cliente']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarMetodoPagoModel($datos)
    {
        $tabla  = 'aprobacion_tipo_pago';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_log) VALUEs (:n, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarMetodoPagoModel($datos)
    {
        $tabla  = 'aprobacion_tipo_pago';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, activo = :e, id_log = :idl WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_metodo']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarProyectoModel($datos)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, ciudad = :c, id_proveedor = :idp, id_log = :idl WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':idp', $datos['cliente']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProyectosModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE id_proveedor NOT IN(6, 209, 210) ORDER BY p.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProyectosWsModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE id_proveedor = 6 ORDER BY p.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProyectosFundepexModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE id_proveedor = 209 ORDER BY p.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProyectosTalentoModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE id_proveedor = 210 ORDER BY p.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosProyectosModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE p.id_proveedor NOT IN(6, 209, 210) AND p.id NOT IN(32) ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosProyectosWsModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE p.id_proveedor = 6 ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosProyectosFundepexModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE p.id_proveedor = 209 ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosProyectosTalentoModel()
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE p.id_proveedor = 210 ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTipoPagoAprobacionModel()
    {
        $tabla  = 'aprobacion_tipo_pago';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProyectosIdModel($id)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        p.ciudad as ciudad_proyecto,
        (SELECT pr.nombre FROM proveedor_detalle pr WHERE pr.id_proveedor = p.id_proveedor) AS proveedor,
        (SELECT pr.num_identificacion FROM proveedor_detalle pr WHERE pr.id_proveedor = p.`id_proveedor`) AS nit_proveedor,
        (SELECT r.id FROM recibos r WHERE r.id_proyecto = p.id AND r.activo = 1  ORDER BY r.id DESC LIMIT 1) AS recibo
        FROM proyecto p WHERE p.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarProyectosModel($buscar)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        pr.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        pr.nombre as proveedor,
        p.ciudad as ciudad_proyecto,
        pr.num_identificacion as nit_proveedor
        FROM " . $tabla . " p
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        WHERE CONCAT(p.nombre, ' ', p.ciudad, ' ', pr.nombre) LIKE '%" . $buscar . "%' AND id_proveedor NOT IN (6,209,210);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarProyectosWsModel($buscar)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        pr.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        pr.nombre as proveedor,
        p.ciudad as ciudad_proyecto,
        pr.num_identificacion as nit_proveedor
        FROM " . $tabla . " p
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        WHERE CONCAT(p.nombre, ' ', p.ciudad, ' ', pr.nombre) LIKE '%" . $buscar . "%' AND id_proveedor = 6;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarProyectosFundepexModel($buscar)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        pr.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        pr.nombre as proveedor,
        p.ciudad as ciudad_proyecto,
        pr.num_identificacion as nit_proveedor
        FROM " . $tabla . " p
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        WHERE CONCAT(p.nombre, ' ', p.ciudad, ' ', pr.nombre) LIKE '%" . $buscar . "%' AND id_proveedor = 209;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarProyectosTalentoModel($buscar)
    {
        $tabla  = 'proyecto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        p.*,
        pr.*,
        p.id as id_proyecto,
        p.nombre as nom_proyecto,
        pr.nombre as proveedor,
        p.ciudad as ciudad_proyecto,
        pr.num_identificacion as nit_proveedor
        FROM " . $tabla . " p
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        WHERE CONCAT(p.nombre, ' ', p.ciudad, ' ', pr.nombre) LIKE '%" . $buscar . "%' AND id_proveedor = 210;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function generarReciboModel($datos)
    {
        $tabla  = 'recibos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_proyecto, tipo_pago, fecha_pago, valor, banco, numero_cheque, id_user, concepto, id_proveedor) VALUES (:idp, :tp, :fp, :v, :b, :nc, :idl, :cp, :idpr)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $datos['id_proyecto']);
            $preparado->bindParam(':tp', $datos['tipo_pago']);
            $preparado->bindParam(':fp', $datos['fecha_pago']);
            $preparado->bindParam(':v', $datos['valor']);
            $preparado->bindParam(':b', $datos['banco']);
            $preparado->bindParam(':nc', $datos['cheque']);
            $preparado->bindParam(':cp', $datos['concepto']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idpr', $datos['proveedor']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesReciboModel($id)
    {
        $tabla  = 'recibos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        r.*,
        CONCAT(u.nombre, ' ', u.apellido) AS user_nom,
        p.nombre AS nom_proyecto,
        pr.nombre AS nom_cliente,
        prr.nombre AS nom_proveedor,
        p.ciudad
        FROM recibos r
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        LEFT JOIN proyecto p ON p.id = r.id_proyecto
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        LEFT JOIN proveedor_detalle prr ON prr.id_proveedor = r.id_proveedor
        WHERE  r.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarRecibosProyectoModel($id)
    {
        $tabla  = 'recibos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        r.*,
        CONCAT(u.nombre, ' ', u.apellido) AS user_nom,
        p.nombre AS nom_proyecto,
        pr.nombre AS nom_cliente,
        prr.nombre AS nom_proveedor,
        p.ciudad,
        prr.id_proveedor,
        prr.num_identificacion
        FROM recibos r
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        LEFT JOIN proyecto p ON p.id = r.id_proyecto
        LEFT JOIN proveedor_detalle pr ON pr.id_proveedor = p.id_proveedor
        LEFT JOIN proveedor_detalle prr ON prr.id_proveedor = r.id_proveedor
        WHERE  r.id_proyecto = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarReciboModel($datos)
    {
        $tabla  = 'recibos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_proveedor = :idp, tipo_pago = :tp, fecha_pago = :fp, valor = :v, banco = :b, numero_cheque = :ch, concepto = :cp, id_user = :idl WHERE id = :idr;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            $preparado->bindParam(':idp', $datos['proveedor']);
            $preparado->bindParam(':tp', $datos['tipo_pago']);
            $preparado->bindParam(':fp', $datos['fecha_pago']);
            $preparado->bindParam(':v', $datos['valor']);
            $preparado->bindParam(':b', $datos['banco']);
            $preparado->bindParam(':ch', $datos['cheque']);
            $preparado->bindParam(':cp', $datos['concepto']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idr', $datos['id_recibo']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function historyLogModel($datos)
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO history_log (id_user, accion) VALUES (:id, :c);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':c', $datos['accion']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
