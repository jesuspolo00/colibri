<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloReserva extends conexion
{

    public static function mostrarTodasHorasModel()
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reservarAreaModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_area, fecha_reserva, id_hora, detalle, id_log) VALUES ('" . $datos['area'] . "', '" . $datos['fecha'] . "', '" . $datos['hora'] . "', '" . $datos['detalle'] . "', '" . $datos['id_log'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasDisponiblesModel($salon, $fecha)
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            h.*
            FROM " . $tabla . " h
            WHERE h.id NOT IN(SELECT r.id_hora FROM reservas r WHERE r.id_area = '" . $salon . "' AND r.fecha_reserva = '" . $fecha . "' AND r.activo = 1);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAreasReservaHoyModel()
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                a.*
                FROM areas a
                WHERE a.id IN(SELECT r.id_area FROM reservas r) AND reservable = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function detalleReservaSalonModel($salon, $fecha)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                r.*,
                a.nombre AS nom_area,
                h.horas,
                CONCAT(u.nombre, ' ', u.apellido) AS nom_user
                FROM " . $tabla . " r
                LEFT JOIN areas a ON a.id = r.id_area
                LEFT JOIN horas h ON h.id = r.id_hora
                LEFT JOIN usuarios u ON u.id_user = r.id_log
                WHERE r.id_area = '" . $salon . "' AND r.activo = 1
                " . $fecha . " ORDER BY r.fecha_reserva DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarAreasReservaModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                a.*,
                r.fecha_reserva
                FROM areas a
                LEFT JOIN reservas r ON r.id_area = a.id
                LEFT JOIN usuarios u ON u.id_user = r.id_log
                WHERE a.reservable = 1
                AND CONCAT(u.nombre, ' ', u.apellido, ' ', a.nombre, ' ', r.fecha_reserva) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['fecha'] . "
                " . $datos['area'] . "
                GROUP BY a.id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarReservasTodasModel()
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                r.*,
                a.nombre AS nom_area,
                h.horas,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario_reserva
                FROM reservas r
                LEFT JOIN horas h ON h.id = r.id_hora
                LEFT JOIN areas a ON a.id = r.id_area
                LEFT JOIN usuarios u ON u.id_user = r.id_log;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
