<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloAreas extends conexion
{

    public static function agregarAreasModel($datos)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, user_log) VALUES (:n, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAreasModel()
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAreasReservablesModel()
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE reservable = 1 AND activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimitesAreasModel()
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " ORDER BY nombre ASC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarAreasModel($nombre)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE nombre LIKE '%" . $nombre . "%';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAreaModel($id)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarZonasModel()
    {
        $tabla  = 'zonas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarComponentesModel()
    {
        $tabla  = 'componentes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarAreaModel($id)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarAreaModel($id)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarInspeccionAreaModel($datos)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_area, id_categoria, fecha, observacion, id_log, id_zona, estado, evidencia) VALUES (:ida, :idc, :f, :ob, :idl, :idz, :est, :ev);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ida', $datos['area']);
            $preparado->bindParam(':idc', $datos['categoria']);
            $preparado->bindParam(':f', $datos['fecha']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idz', $datos['zona']);
            $preparado->bindParam(':est', $datos['tipo']);
            $preparado->bindParam(':ev', $datos['evidencia']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInspeccionesLimiteModel()
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        COUNT(ai.id) AS cantidad_reportado
        FROM areas_inspeccion AS ai
        LEFT JOIN areas AS a ON a.id = ai.id_area
        LEFT JOIN componentes AS c ON c.id = ai.id_categoria
        LEFT JOIN zonas AS z ON z.id = ai.id_zona
        LEFT JOIN usuarios AS u ON u.id_user = ai.id_log
        WHERE ai.estado = 1 AND ai.id_respuesta IS NULL
        GROUP BY ai.id, z.nombre, a.id, c.id, u.nombre, u.apellido ORDER BY ai.id DESC, ai.fecha DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInspeccionesTodasModel()
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE ai.estado = 1 AND ai.id_respuesta IS NULL
        ORDER BY ai.id DESC, ai.fecha DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInspeccionesTodasMantModel()
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE ai.estado = 2 AND ai.id_respuesta IS NULL
        ORDER BY ai.id DESC, ai.fecha DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInspeccionesMantLimiteModel()
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        COUNT(ai.id) AS cantidad_reportado
        FROM areas_inspeccion AS ai
        LEFT JOIN areas AS a ON a.id = ai.id_area
        LEFT JOIN componentes AS c ON c.id = ai.id_categoria
        LEFT JOIN zonas AS z ON z.id = ai.id_zona
        LEFT JOIN usuarios AS u ON u.id_user = ai.id_log
        WHERE ai.estado = 2 AND ai.id_respuesta IS NULL
        GROUP BY ai.id, z.nombre, a.id, c.id, u.nombre, u.apellido ORDER BY ai.id DESC, ai.fecha DESC LIMIT 20;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionInspeccionModel($id)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = ai.id_respuesta) as nom_respuesta
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE ai.id = :id
        ORDER BY ai.id DESC, ai.fecha DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInspeccionGeneralLimiteModel()
    {
        $tabla  = 'reporte_areas_general';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        (SELECT COUNT(rpd.id) FROM reporte_areas_general_detalle rpd WHERE rpd.id_reporte = rp.id GROUP BY rpd.id_reporte) AS cantidad,
        rp.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario
        FROM reporte_areas_general rp
        LEFT JOIN usuarios u ON u.id_user = rp.id_user ORDER BY rp.fecha_inspeccion DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInspeccionGeneralModel($datos)
    {
        $tabla  = 'reporte_areas_general';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        (SELECT COUNT(rpd.id) FROM reporte_areas_general_detalle rpd WHERE rpd.id_reporte = rp.id GROUP BY rpd.id_reporte) AS cantidad,
        rp.*,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario
        FROM reporte_areas_general rp
        LEFT JOIN usuarios u ON u.id_user = rp.id_user
        WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', rp.id) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['estado'] . "
        " . $datos['fecha'] . "
        ORDER BY rp.fecha_inspeccion DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInspeccionesLimiteModel($datos)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        COUNT(ai.id) AS cantidad_reportado
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE ai.estado = 1 AND
        CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', z.nombre, ' ', a.nombre, ' ', c.nombre) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['fecha'] . "
        " . $datos['zona'] . "
        AND ai.id_respuesta IS NULL
        GROUP BY ai.id, z.nombre, a.id, c.id, u.nombre, u.apellido ORDER BY ai.id DESC, ai.fecha DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInspeccionesMantLimiteModel($datos)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        COUNT(ai.id) AS cantidad_reportado
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE ai.estado = 2 AND
        CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', z.nombre, ' ', a.nombre, ' ', c.nombre) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['fecha'] . "
        " . $datos['zona'] . "
        AND ai.id_respuesta IS NULL
        GROUP BY ai.id, z.nombre, a.id, c.id, u.nombre, u.apellido ORDER BY ai.id DESC, ai.fecha DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function solucionarInspeccionModel($datos)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET fecha_respuesta = :fr, observacion_respuesta = :obr, id_respuesta = :idr WHERE id_area = :ida AND id_zona = :idz AND id_categoria = :idc;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':fr', $datos['fecha_respuesta']);
            $preparado->bindParam(':obr', $datos['observacion_respuesta']);
            $preparado->bindParam(':idr', $datos['id_log']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idz', $datos['id_zona']);
            $preparado->bindParam(':idc', $datos['id_componente']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosZonaModel($id)
    {
        $tabla  = 'zonas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM zonas WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHistorialInspeccionesModel($id)
    {
        $tabla  = 'areas_inspeccion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ai.*,
        z.nombre AS nom_zona,
        a.nombre AS nom_area,
        c.nombre AS nom_categoria,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user
        FROM areas_inspeccion ai
        LEFT JOIN areas a ON a.id = ai.id_area
        LEFT JOIN componentes c ON c.id = ai.id_categoria
        LEFT JOIN zonas z ON z.id = ai.id_zona
        LEFT JOIN usuarios u ON u.id_user = ai.id_log
        WHERE z.id = :id
        ORDER BY ai.id DESC, ai.fecha ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarInspeccionGeneralModel($datos)
    {
        $tabla  = 'reporte_areas_general';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_user, fecha_inspeccion) VALUES (:id, :fi);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_log']);
            $preparado->bindParam(':fi', $datos['fecha_inspeccion']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id  = $cnx->ultimoIngreso($tabla);
                $rsl = array('guardar' => true, 'id' => $id);
                return $rsl;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardaDetalleInspeccionGeneralModel($datos)
    {
        $tabla  = 'reporte_areas_general_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_reporte, id_zona, id_area, id_categoria, tipo, observacion) VALUES ('" . $datos['id_reporte'] . "','" . $datos['zona'] . "','" . $datos['area'] . "','" . $datos['categoria'] . "','" . $datos['tipo'] . "','" . $datos['observacion'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInfoInspeccionGeneralModel($id)
    {
        $tabla  = 'reporte_areas_general';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            rp.*,
            CONCAT(u.nombre, ' ', u.apellido) AS nom_usuario,
            CONCAT(us.nombre, ' ', us.apellido) AS nom_respuesta
            FROM reporte_areas_general rp
            LEFT JOIN usuarios u ON u.id_user = rp.id_user
            LEFT JOIN usuarios us ON us.id_user = rp.id_respuesta
            WHERE rp.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesGeneralModel($id)
    {
        $tabla  = 'reporte_areas_general_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            rpd.*,
            z.nombre AS nom_zona,
            a.nombre AS nom_area,
            c.nombre AS nom_categoria
            FROM reporte_areas_general_detalle rpd
            LEFT JOIN zonas z ON z.id = rpd.id_zona
            LEFT JOIN areas a ON a.id = rpd.id_area
            LEFT JOIN componentes c ON c.id = rpd.id_categoria
            WHERE rpd.id_reporte = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function solucionarInspeccionGeneralModel($datos)
    {
        $tabla  = 'reporte_areas_general';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :est, observacion_general = :ob, id_respuesta = :idl, fecha_respuesta = :fr WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion_general']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':fr', $datos['fecha_respuesta']);
            $preparado->bindParam(':id', $datos['id_inspeccion']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function solucionarDetallesInspeccionModel($datos)
    {
        $tabla  = 'reporte_areas_general_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET observacion_respuesta = :ob, fecha_respuesta = :fr, id_respuesta = :idl WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ob', $datos['observacion_respuesta']);
            $preparado->bindParam(':fr', $datos['fecha_respuesta']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_detalle']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarPlanMantenimientoModel($datos)
    {
        $tabla  = 'plan_mantenimiento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_user, fecha_inicio, fecha_fin, observacion_general) VALUES (:idu, :fi, :ff, :ob);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_log']);
            $preparado->bindParam(':fi', $datos['fecha_inicio']);
            $preparado->bindParam(':ff', $datos['fecha_fin']);
            $preparado->bindParam(':ob', $datos['observacion_general']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id  = $cnx->ultimoIngreso($tabla);
                $rsl = array('guardar' => true, 'id' => $id);
                return $rsl;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarDetallePlanMantenimientoModel($datos)
    {
        $tabla  = 'plan_mantenimiento_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_plan, id_categoria, id_zona, fecha_mant, observacion) VALUES (:idp, :idc, :idz, :fm, :ob);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $datos['id_plan']);
            $preparado->bindParam(':idc', $datos['categoria']);
            $preparado->bindParam(':idz', $datos['zona']);
            $preparado->bindParam(':fm', $datos['fecha_mant']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarListadoLimitePlanMantenimientoModel()
    {
        $tabla  = 'plan_mantenimiento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            pl.*,
            CONCAT(u.nombre, ' ', u.apellido) AS nom_user
            FROM plan_mantenimiento pl
            LEFT JOIN usuarios u ON u.id_user  = pl.id_user
            ORDER BY pl.fecha_fin DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionPlanModel($id)
    {
        $tabla  = 'plan_mantenimiento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            pl.*,
            CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
            hu.cargo
            FROM plan_mantenimiento pl
            LEFT JOIN usuarios u ON u.id_user  = 3
            LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = 3
            WHERE pl.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesPlanModel($id)
    {
        $tabla  = 'plan_mantenimiento_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            pd.*,
            z.nombre AS nom_zona,
            c.nombre AS nom_categoria
            FROM plan_mantenimiento_detalle pd
            LEFT JOIN zonas z ON z.id = pd.id_zona
            LEFT JOIN categoria c ON c.id = pd.id_categoria
            WHERE pd.id_plan = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarPlanMantenimientoModel($datos)
    {
        $tabla  = 'plan_mantenimiento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :est, id_respuesta = :idr, observacion_respuesta = :obr, fecha_respuesta = NOW() WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':obr', $datos['observacion_respuesta']);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':idr', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_plan']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarDetallesPlanModel($datos)
    {
        $tabla  = 'plan_mantenimiento_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :est, id_respuesta = :idr, observacion_respuesta = :obr, fecha_respuesta = NOW() WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':obr', $datos['observacion_respuesta']);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':idr', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_detalle']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
