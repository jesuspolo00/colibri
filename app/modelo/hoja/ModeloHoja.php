<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloHoja extends conexion
{

    public static function tipoSangreModel()
    {
        $tabla  = 'tipo_sangre';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function sexoGeneroModel()
    {
        $tabla  = 'genero';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function estadoCivilModel()
    {
        $tabla  = 'estado_civil';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionHojaVidaModel($id)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        h.*,
        g.nombre AS nom_genero,
        ts.nombre AS nom_sangre,
        ec.nombre AS nom_estado,
        (SELECT hd.documento FROM hoja_vida_documentos hd WHERE hd.id_user = h.id_usuario AND hd.tipo_doc = 13) AS foto_hoja_vida,
        g.id as sexo,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_verifica
        FROM " . $tabla . " h
        LEFT JOIN genero g ON g.id = h.sexo
        LEFT JOIN tipo_sangre ts ON ts.id = h.tipo_sangre
        LEFT JOIN estado_civil ec ON ec.id = h.estado_civil
        LEFT JOIN usuarios u ON u.id_user = h.id_verificacion
        WHERE h.activo = 1 AND h.id_usuario = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET documento = '" . $datos['documento'] . "', nombre = '" . $datos['nombre'] . "', apellido = '" . $datos['apellido'] . "', correo = '" . $datos['correo'] . "', telefono = '" . $datos['telefono'] . "' WHERE id_user = '" . $datos['id_usuario'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarHojaVidaUsuarioModel($datos)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "update
        " . $tabla . "
        set
        sexo = '" . $datos['sexo'] . "',
        celular = '" . $datos['celular'] . "',
        direccion = '" . $datos['direccion'] . "',
        lugar = '" . $datos['lugar'] . "',
        barrio = '" . $datos['barrio'] . "',
        fecha_nac = '" . $datos['fecha_nac'] . "',
        lugar_nac = '" . $datos['lugar_nac'] . "',
        tipo_sangre = '" . $datos['tipo_sangre'] . "',
        exp_documento = '" . $datos['exp_documento'] . "',
        estado_civil = '" . $datos['estado_civil'] . "',
        escalafon = '" . $datos['escalafon'] . "',
        nivel_academico = '" . $datos['nivel_academico'] . "',
        calidad_desempeno = '" . $datos['calidad_desempeno'] . "',
        especialidad = '" . $datos['especialidad'] . "',
        fecha_ingreso = '" . $datos['fecha_ingreso'] . "',
        tiempo_laboral = '" . $datos['tiempo_laboral'] . "',
        tipo_vinculacion = '" . $datos['tipo_vinculacion'] . "',
        origen_vinculacion = '" . $datos['origen_vinculacion'] . "',
        fuente_recursos = '" . $datos['fuente_recursos'] . "',
        salario = '" . $datos['salario'] . "',
        cargo = '" . $datos['cargo'] . "',
        foto = '" . $datos['foto'] . "',
        id_edit = '" . $datos['id_edit'] . "',
        fecha_edit = NOW()
        where id = '" . $datos['id_hoja'] . "';
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarPreguntasModel($datos)
    {
        $tabla  = 'hoja_preguntas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id_hoja = '" . $datos['id_hoja'] . "' ORDER BY id DESC LIMIT 1;
        INSERT INTO " . $tabla . "
        (id_hoja,fecha_diligencia, idiomas, idiomas_nivel, referencia_laboral, autorizo, emergencia, referencia_personal, libreta_militar, id_log) VALUES ('" . $datos['id_hoja'] . "','" . $datos['fecha_diligencia'] . "','" . $datos['idiomas'] . "','" . $datos['idiomas_nivel'] . "','" . $datos['referencia_laboral'] . "','" . $datos['autorizo'] . "','" . $datos['emergencia'] . "','" . $datos['referencia_personal'] . "','" . $datos['libreta_militar'] . "','" . $datos['id_log'] . "'
    );";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function preguntasHojaModel($id)
{
    $tabla  = 'hoja_preguntas';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 AND id_hoja = :id ORDER BY id DESC LIMIT 1;";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindParam(':id', $id);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return $preparado->fetch();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function agregarInformacionAcademicaModel($datos)
{
    $tabla  = 'informacion_academica';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "INSERT INTO " . $tabla . " (id_hoja, institucion, ciudad, pais, nivel, graduado, fecha_grado, titulo, id_log) VALUES ('" . $datos['id_hoja'] . "', '" . $datos['institucion'] . "','" . $datos['ciudad'] . "','" . $datos['pais'] . "','" . $datos['nivel'] . "','" . $datos['graduado'] . "','" . $datos['fecha_grado'] . "','" . $datos['titulo'] . "','" . $datos['id_log'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id = $cnx->ultimoIngreso($tabla);
                $rs = array('guardar' => true, 'id' => $id);
                return $rs;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarAcademicoModel($id)
    {
        $tabla  = 'informacion_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = " . $id . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionAcademicaModel($id)
    {
        $tabla  = 'informacion_academica';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_hoja = " . $id . " AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionLaboralModel($id)
    {
        $tabla  = 'informacion_laboral';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_hoja = " . $id . " AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarInformacionLaboralModel($datos)
    {
        $tabla  = 'informacion_laboral';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_hoja, empresa, ciudad, pais, cargo, telefono, direccion, fecha_ingreso, fecha_salida, id_log) VALUES ('" . $datos['id_hoja'] . "','" . $datos['empresa'] . "','" . $datos['ciudad'] . "','" . $datos['pais'] . "','" . $datos['cargo'] . "','" . $datos['telefono'] . "','" . $datos['direccion'] . "','" . $datos['fecha_ingreso'] . "','" . $datos['fecha_salida'] . "','" . $datos['id_log'] . "'
    );";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            $id = $cnx->ultimoIngreso($tabla);
            $rs = array('guardar' => true, 'id' => $id);
            return $rs;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function eliminarLaboralModel($id)
{
    $tabla  = 'informacion_laboral';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = " . $id . ";";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function subirDocumentosModel($datos)
{
    $tabla  = 'hoja_vida_documentos';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "INSERT INTO hoja_vida_documentos (id_user,
        id_hoja,
        documento,
        tipo_doc,
        id_log,
        autorizacion_uno)
    VALUES (
        '" . $datos['id_user'] . "',
        '" . $datos['id_hoja'] . "',
        '" . $datos['documento'] . "',
        '" . $datos['tipo_doc'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['estado'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarDocumentosModel($datos)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_user = '" . $datos['id_user'] . "' and id_hoja = '" . $datos['id_hoja'] . "' AND tipo_doc = '" . $datos['tipo_doc'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosTodosHojaModel($id)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hd.*,
        ht.perfil_autoriza,
        ht.nombre AS nom_tipo
        FROM " . $tabla . " hd
        LEFT JOIN hoja_vida_tipo_doc ht ON ht.id = hd.tipo_doc
        WHERE hd.id_user = :id
        ORDER BY
        CASE
        WHEN hd.perfil_aut = 16 THEN 1
        WHEN hd.perfil_aut = 17 THEN 2
        WHEN hd.perfil_aut = 11 THEN 3
        END;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosHojaModel($id, $perfil)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hd.*,
        ht.perfil_autoriza,
        ht.nombre AS nom_tipo,
        ht.id as id_tipo
        FROM " . $tabla . " hd
        LEFT JOIN hoja_vida_tipo_doc ht ON ht.id = hd.tipo_doc
        WHERE ht.perfil_autoriza = :p AND hd.id_user = :id
        ORDER BY ht.perfil_autoriza DESC, nom_tipo ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':p', $perfil);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosHojaCidaProyectosModel($id)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hd.*,
        ht.perfil_autoriza,
        ht.nombre AS nom_tipo,
        ht.id as id_tipo
        FROM " . $tabla . " hd
        LEFT JOIN hoja_vida_tipo_doc ht ON ht.id = hd.tipo_doc
        WHERE hd.id_user = :id
        ORDER BY ht.perfil_autoriza DESC, nom_tipo ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosHojaRevisionModel($id, $perfil)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hd.*,
        ht.perfil_autoriza,
        ht.nombre AS nom_tipo
        FROM hoja_vida_documentos hd
        LEFT JOIN hoja_vida_tipo_doc ht ON ht.id = hd.tipo_doc
        WHERE hd.perfil_aut = 17 AND hd.id_user = 203
        ORDER BY hd.perfil_aut DESC, nom_tipo ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':p', $perfil);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPreguntasEvaluacionModel($tipo)
    {
        $tabla  = ' hoja_vida_evaluacion_preguntas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 AND tipo_pregunta = :tipo;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':tipo', $tipo);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirNuevoDocumentoModel($datos)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_user, id_hoja, nombre_archivo, documento, tipo_doc, perfil_aut, id_log, autorizacion_uno)
        VALUES (:id, :idh, :n, :d, :td, :pa, :idl, :aut);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':idh', $datos['id_hoja']);
            $preparado->bindParam(':n', $datos['nom_archivo']);
            $preparado->bindParam(':d', $datos['archivo']);
            $preparado->bindParam(':td', $datos['tipo_documento']);
            $preparado->bindParam(':pa', $datos['perfil']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':aut', $datos['estado']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function revisionDocumentosHojaModel($datos)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET autorizacion_uno = :aut, observacion_uno = :ob, id_autoriza_uno = :ida, perfil_aut = :paut, fecha_aut_uno = NOW() WHERE id = :id AND id_user = :idu;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':aut', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':ida', $datos['id_log']);
            $preparado->bindParam(':paut', $datos['id_perfil']);
            $preparado->bindParam(':id', $datos['id_documento']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarRevisionModel($datos)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_hoja, id_user, id_autoriza, fecha_autorizacion, cantidad_doc) VALUES (:idh, :idu, :ida, :fa, :cd);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idh', $datos['id_hoja']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_log']);
            $preparado->bindParam(':fa', $datos['fecha_autorizacion']);
            $preparado->bindParam(':cd', $datos['cantidad_documentos']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarAutorizacionModel($datos)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_autoriza = :idl, cantidad_doc = :cd WHERE id = :id;'";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_aut']);
            $preparado->bindParam(':cd', $datos['cantidad_documentos']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarEvaluacionModel($datos)
    {
        $tabla  = 'hoja_vida_evaluacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_hoja, id_user, pregunta, puntuacion, id_log) VALUES (:idh, :idu, :pr, :pt, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idh', $datos['id_hoja']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':pr', $datos['pregunta']);
            $preparado->bindParam(':pt', $datos['punt']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarHojaVidaModel($datos)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET verificacion = '" . $datos['estado'] . "', id_verificacion = '" . $datos['id_log'] . "', fecha_verificacion = NOW(), observacion_verifica = '" . $datos['observacion'] . "' WHERE id_usuario = :idu AND id = :idh;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':idh', $datos['id_hoja']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFechaVerificacionModel($id)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT fecha_verificacion FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarFechaVerificacionModel($id, $fecha_nueva)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET fecha_verificacion = :f WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':f', $fecha_nueva);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
