<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloCarnet extends conexion
{

    public static function registrarPersonalModel($datos)
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            documento,
            nom_prim,
            nom_seg,
            apellido_prim,
            apellido_seg,
            pago,
            codigo,
            id_log,
            curso,
            foto
            )
        VALUES
        (
            '" . $datos['documento'] . "',
            '" . $datos['nom_prim'] . "',
            '" . $datos['nom_seg'] . "',
            '" . $datos['apellido_prim'] . "',
            '" . $datos['apellido_seg'] . "',
            '" . $datos['pago'] . "',
            '" . $datos['codigo'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['curso'] . "',
            '" . $datos['foto'] . "'
        );";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPersonalModel()
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = 'SELECT *,
        (SELECT c.nombre FROM carnet_cargos c WHERE c.id = curso) AS nom_cargo,
        (SELECT c.id_categoria FROM carnet_cargos c WHERE c.id = curso) AS id_categoria
        FROM ' . $tabla . ' ORDER BY id DESC LIMIT 20;';
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCategoriasCarnetModel()
    {
        $tabla  = 'carnet_categoria';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCargosCarnetModel()
    {
        $tabla  = 'carnet_cargos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCargosCategoriaModel($id)
    {
        $tabla  = 'carnet_cargos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_categoria = :id AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarPersonalModel($buscar)
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *,
        (SELECT c.nombre FROM carnet_cargos c WHERE c.id = curso) AS nom_cargo,
        (SELECT c.id_categoria FROM carnet_cargos c WHERE c.id = curso) AS id_categoria
        FROM carnet_usuario WHERE CONCAT(nom_prim, ' ', nom_seg, ' ', apellido_prim, ' ', apellido_seg, ' ', curso, ' ', codigo, ' ', documento) LIKE '%" . $buscar . "%';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirFotoPersonalModel($datos)
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        " . $tabla . "
        SET
        documento = '" . $datos['documento'] . "',
        nom_prim = '" . $datos['nom_prim'] . "',
        nom_seg = '" . $datos['nom_seg'] . "',
        apellido_prim = '" . $datos['apellido_prim'] . "',
        apellido_seg = '" . $datos['apellido_seg'] . "',
        curso = '" . $datos['curso'] . "',
        pago = '" . $datos['pago'] . "',
        codigo = '" . $datos['codigo'] . "',
        foto = '" . $datos['foto'] . "',
        id_log = '" . $datos['id_log'] . "',
        fecha_generado = '" . $datos['fecha_entrega'] . "',
        fecha_editado = NOW(),
        id_edit = '" . $datos['id_log'] . "'
        WHERE id = :id;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosPersonalIdModel($id)
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT c.nombre FROM carnet_cargos c WHERE c.id = curso) AS nom_cargo,
        (SELECT c.id_categoria FROM carnet_cargos c WHERE c.id = curso) AS id_categoria FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosPersonalDocumentoModel($documento)
    {
        $tabla  = 'carnet_usuario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, (SELECT c.nombre FROM carnet_cargos c WHERE c.id = curso) AS nom_cargo,
        (SELECT c.id_categoria FROM carnet_cargos c WHERE c.id = curso) AS id_categoria FROM " . $tabla . " WHERE documento = '" . $documento . "'";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
