<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloCobro extends conexion
{

    public static function generarCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_log, documento, lugar, direccion, telefono, valor, razon, tipo) VALUES (
            '" . $datos['id_log'] . "','" . $datos['documento'] . "','" . $datos['lugar'] . "','" . $datos['direccion'] . "','" . $datos['telefono'] . "','" . $datos['valor'] . "', '" . $datos['razon'] . "', '" . $datos['tipo'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id     = $cnx->ultimoIngreso($tabla);
                $result = array('guardar' => true, 'id' => $id);
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarDetallesCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_cuenta_cobro, id_asistencia, id_log) VALUES ('" . $datos['id_cuenta_cobro'] . "', '" . $datos['id_asistencia'] . "', '" . $datos['id_log'] . "');
                UPDATE asistencia SET cuenta_cobro = 1 WHERE id = '" . $datos['id_asistencia'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarDetallesCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
                " . $tabla . "
                SET
                sede = '" . $datos['sede'] . "',
                colegio = '" . $datos['colegio'] . "',
                jornada = '" . $datos['jornada'] . "',
                semestre = '" . $datos['semestre'] . "',
                ciclo = '" . $datos['ciclo'] . "',
                id_log = '" . $datos['id_log'] . "'
                WHERE id_asistencia = '" . $datos['id_asistencia'] . "' AND id_cuenta_cobro = '" . $datos['id_cuenta_cobro'] . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteCuentasCobroModel($id)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE cc.id_log = :id ORDER BY cc.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteListaCuentasCobroModel()
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                ORDER BY cc.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteLibreCuentasCobroModel()
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE cc.`id_aprobado` IS NOT NULL
                ORDER BY cc.id DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosCuentaCobroIdModel($id)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_log = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE CONCAT(cc.id, ' ', cc.documento, ' ', u.nombre, ' ', u.apellido, ' ', cc.direccion) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['anio'] . "
                " . $datos['mes'] . "
                ORDER BY cc.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarCuentaCoordinadorCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE cc.id IN(SELECT ccd.id_cuenta_cobro FROM cuenta_cobro_detalle ccd
                    WHERE ccd.id_asistencia IN(SELECT ass.id FROM asistencia ass WHERE ass.id_programa IN(SELECT p.id FROM asistencia_programa p WHERE p.id_log = " . $datos['id_log'] . ")))
                AND CONCAT(cc.id, ' ', cc.documento, ' ', u.nombre, ' ', u.apellido, ' ', cc.direccion) LIKE '%" . $datos['buscar'] . "%'
                " . $datos['anio'] . "
                " . $datos['mes'] . "
                ORDER BY cc.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarCuentaUsuarioCobroModel($buscar, $fecha, $id_log)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE CONCAT(cc.id, ' ', cc.documento, ' ', u.nombre, ' ', u.apellido, ' ', cc.direccion) LIKE '%" . $buscar . "%'
                AND cc.id_log = :id
                " . $fecha . "
                ORDER BY cc.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id_log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosCuentaCobroModel($id)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cc.*,
                (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS mes_cuenta_asistencia,
                (SELECT YEAR(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia
                    IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id) GROUP BY sd.fecha LIMIT 1) AS anio_cuenta_asistencia,
                CONCAT(u.nombre, ' ', u.apellido) AS usuario,
                (SELECT SUM(asd.horas) FROM asistencia_detalle asd WHERE asd.id_asistencia IN(SELECT cd.id_asistencia FROM cuenta_cobro_detalle cd WHERE cd.id_cuenta_cobro = cc.id)) AS horas_aprobadas_asistencia,
                (SELECT CONCAT(u.nombre , ' ', u.apellido) FROM usuarios u WHERE u.id_user = cc.id_aprobado) AS nom_coordinador,
                (SELECT CONCAT(u.nombre , ' ', u.apellido) FROM usuarios u WHERE u.id_user = cc.id_contabilidad) AS nom_contabilidad
                FROM cuenta_cobro cc
                LEFT JOIN usuarios u ON u.id_user = cc.id_log
                WHERE cc.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetallesCuentasCobroModel($id)
    {
        $tabla  = 'cuenta_cobro_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                cd.*,
                (SELECT asm.nombre FROM asistencia_modulos asm WHERE asm.id = ass.id_modulo) AS modulo,
                (SELECT sc.nombre FROM asistencia_competencia sc WHERE sc.id = ass.id_competencia) AS competencia,
                 (SELECT MONTH(sd.fecha) FROM asistencia_detalle sd WHERE sd.id_asistencia = ass.id GROUP BY sd.fecha LIMIT 1) AS mes_asistencia,
                (SELECT asm.nombre FROM asistencia_programa asm WHERE asm.id = ass.id_programa) AS programa,
                (SELECT SUM(sa.horas) FROM asistencia_detalle sa WHERE sa.id_asistencia = cd.id_asistencia) AS horas,
                (SELECT j.nombre FROM jornada j WHERE j.id IN(SELECT cg.jornada FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo) ORDER BY j.id DESC LIMIT 1) AS nom_jornada,
                (SELECT cg.sede FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo ORDER BY cg.id DESC LIMIT 1) AS sede_coord,
                (SELECT cg.colegio FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo ORDER BY cg.id DESC LIMIT 1) AS colegio_coord,
                (SELECT cg.ciclo FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo ORDER BY cg.id DESC LIMIT 1) AS ciclo_coord,
                (SELECT cg.semestre FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo ORDER BY cg.id DESC LIMIT 1) AS semestre_coord,
                (SELECT j.id FROM jornada j WHERE j.id IN(SELECT cg.jornada FROM carga_academica cg WHERE cg.id_programa = ass.id_programa AND cg.id_modulo = ass.id_modulo
                    AND cg.id_profesor = ass.id_log AND cg.activo) ORDER BY j.id DESC LIMIT 1) AS id_jornada
                FROM cuenta_cobro_detalle cd
                LEFT JOIN asistencia ass ON ass.id = cd.id_asistencia
                WHERE cd.id_cuenta_cobro = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosCuentasCobroModel($id)
    {
        $tabla  = 'cuenta_cobro_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_cuenta_cobro = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDiasSemanaAsistenciaModel($id)
    {
        $tabla  = 'asistencia_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, WEEKDAY(fecha) AS dia FROM asistencia_detalle
                WHERE (id_asistencia, WEEKDAY(fecha)) IN (
                  SELECT id_asistencia, WEEKDAY(fecha)
                  FROM asistencia_detalle
                  WHERE id_asistencia = :id
                  GROUP BY id_asistencia, WEEKDAY(fecha)
              );";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDiasSemanaNumeroModel($id)
    {
        $tabla  = 'asistencia_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id, DAY(fecha) AS dia FROM asistencia_detalle
                WHERE (id_asistencia, DAY(fecha)) IN (
                  SELECT id_asistencia, DAY(fecha)
                  FROM asistencia_detalle
                  WHERE id_asistencia = :id
                  GROUP BY id_asistencia, DAY(fecha)
              );";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarJornadasModel()
    {
        $tabla  = 'jornada';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarDocumentoUsuarioModel($id, $tipo)
    {
        $tabla  = 'hoja_vida_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE tipo_doc = :tp AND id_user = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':tp', $tipo);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
                " . $tabla . "
                SET
                razon = '" . $datos['razon'] . "',
                documento = '" . $datos['documento'] . "',
                lugar = '" . $datos['lugar'] . "',
                direccion = '" . $datos['direccion'] . "',
                telefono = '" . $datos['telefono'] . "',
                valor = '" . $datos['valor'] . "',
                cuenta_detalle = 1,
                fecha_detalle = NOW()
                WHERE id = '" . $datos['id_cuenta_cobro'] . "';
                ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarDetallesCuentaCobroModel($id)
    {
        $tabla  = 'cuenta_cobro_detalle';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM cuenta_cobro_detalle WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirDocumentosCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_cuenta_cobro, rut, cedula, certificacion, seguridad, id_log) VALUES (:idc, :rut, :ced, :cert, :segu, :idl);
                UPDATE cuenta_cobro SET cuenta_documentos = 1, estado = 1, fecha_documentos = NOW() WHERE id = :idc;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idc', $datos['id_cuenta_cobro']);
            $preparado->bindParam(':rut', $datos['rut']);
            $preparado->bindParam(':ced', $datos['cedula']);
            $preparado->bindParam(':cert', $datos['certificacion']);
            $preparado->bindParam(':segu', $datos['seguridad']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualzarDocumentosCuentaModel($datos)
    {
        $tabla  = 'cuenta_cobro_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET rut = :rut, cedula = :ced, certificacion = :cert, seguridad = :segu, id_log = :idl WHERE id_cuenta_cobro = :idc;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idc', $datos['id_cuenta_cobro']);
            $preparado->bindParam(':rut', $datos['rut']);
            $preparado->bindParam(':ced', $datos['cedula']);
            $preparado->bindParam(':cert', $datos['certificacion']);
            $preparado->bindParam(':segu', $datos['seguridad']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarDocumentosCuentaModel($id)
    {
        $tabla  = 'cuenta_cobro_documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_cuenta_cobro = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function estadoCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET horas_coord = :hc, justificacion_coord = :jc, estado = :est, id_aprobado = :idp, fecha_aprobado = NOW() WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_cuenta_cobro']);
            $preparado->bindParam(':est', $datos['estado']);
            $preparado->bindParam(':hc', $datos['horas_coord']);
            $preparado->bindParam(':jc', $datos['just_coord']);
            $preparado->bindParam(':idp', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function estadoContabilidadCuentaCobroModel($datos)
    {
        $tabla  = 'cuenta_cobro';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado_contabilidad = :estc, horas_cont = :hc, justificacion_cont = :jc, id_contabilidad = :idc, fecha_contabilidad = NOW(), horas_pagar = :hp, hora_valor = :hv WHERE id = :id;;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':estc', $datos['estado']);
            $preparado->bindParam(':hc', $datos['horas_cont']);
            $preparado->bindParam(':jc', $datos['just_cont']);
            $preparado->bindParam(':idc', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_cuenta_cobro']);
            $preparado->bindParam(':hp', $datos['horas_pagar']);
            $preparado->bindParam(':hv', $datos['hora_valor']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
