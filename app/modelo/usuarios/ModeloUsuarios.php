<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuario extends conexion
{

    public static function mostrarUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil NOT IN(1,5) AND u.estado = 1
        ORDER BY u.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProveedoresModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil IN(5) AND u.estado = 1
        ORDER BY u.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCentroCostoModel()
    {
        $tabla  = 'centro_costo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProfesoresModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil IN(3) AND u.estado = 1
        ORDER BY u.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT hd.id FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user ORDER BY hd.id DESC LIMIT 1) AS documentos
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil NOT IN(1)
        ORDER BY u.nombre LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteRolUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT hd.id FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user ORDER BY hd.id DESC LIMIT 1) AS documentos
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil NOT IN(1,5)
        ORDER BY u.nombre LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteUsuariosHojaVidaModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil NOT IN(1,5,10, 19, 20) AND u.estado = 1
        ORDER BY u.nombre ASC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteUsuariosHojaVidaIncluyemeModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil IN(19) AND u.estado = 1
        ORDER BY u.nombre ASC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimiteUsuariosHojaVidaCdvModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil IN(20) AND u.estado = 1
        ORDER BY u.nombre ASC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarUsuariosModel($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT hd.id FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user ORDER BY hd.id DESC LIMIT 1) AS documentos
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE u.perfil NOT IN(1) AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', p.nombre, ' ', u.user) LIKE '%" . $buscar . "%'
        ORDER BY u.nombre;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarUsuariosHojaVidaModel($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil NOT IN(1,5,10,19,20) AND u.estado = 1 AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', p.nombre, ' ', u.user) LIKE '%" . $buscar . "%'
        ORDER BY u.nombre;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarUsuariosHojaVidaIncluyemeModel($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil IN(19) AND u.estado = 1 AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', p.nombre, ' ', u.user) LIKE '%" . $buscar . "%'
        ORDER BY u.nombre;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarUsuariosHojaVidaCdvModel($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil,
        (SELECT COUNT(id) FROM hoja_vida_documentos hd WHERE hd.id_user = u.id_user AND hd.tipo_doc NOT IN(0) ORDER BY hd.id) AS documentos,
        hu.fechareg AS fecha_registro,
        hu.verificacion
        FROM usuarios u
        LEFT JOIN perfiles p ON p.id = u.perfil
        LEFT JOIN hoja_vida_usuarios hu ON hu.id_usuario = u.id_user
        WHERE u.perfil IN(20) AND u.estado = 1 AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', p.nombre, ' ', u.user) LIKE '%" . $buscar . "%'
        ORDER BY u.nombre;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
          documento,
          nombre,
          apellido,
          correo,
          telefono,
          user,
          pass,
          perfil,
          centro_costo,
          user_log
          )
        VALUES
        (
            '" . $datos['documento'] . "',
            '" . $datos['nombre'] . "',
            '" . $datos['apellido'] . "',
            '" . $datos['correo'] . "',
            '" . $datos['telefono'] . "',
            '" . $datos['usuario'] . "',
            '" . $datos['pass'] . "',
            '" . $datos['perfil'] . "',
            '" . $datos['centro_costo'] . "',
            '" . $datos['user_log'] . "'
        );";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id  = $cnx->ultimoIngreso($tabla);
                $rsl = array('guardar' => true, 'id' => $id);
                return $rsl;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarUsuarioModel($usuario)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE user = '" . $usuario . "' ORDER BY id_user DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModel($id, $log)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 0, user_log = :l, fecha_inactivo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':l', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModel($id, $log)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 1, user_log = :l, fecha_activo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':l', $log);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, apellido = :ap, telefono = :t, correo = :c, perfil = :p, user_log = :idl, centro_costo = :ct WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ap', $datos['apellido']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->bindParam(':ct', $datos['centro_costo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function restablecerPassModel($id, $pass)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET pass = :p WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':p', $pass);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarHojaVidaModel($id, $id_log)
    {
        $tabla  = 'hoja_vida_usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_usuario, id_log) VALUEs ('" . $id . "', '" . $id_log . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function autorizacionAnioDocumentosModel($id, $anio)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM hoja_vida_autorizacion WHERE id_user = :id AND fecha_autorizacion LIKE '%" . $anio . "%' ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarAutorizacionUsuarioModel($id_user, $id, $anio)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM hoja_vida_autorizacion WHERE id_user = :idu AND id_autoriza = :id AND fecha_autorizacion LIKE '%" . $anio . "%' ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idu', $id_user);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarEvaluacionAnualUsuarioModel($id, $anio, $tipo)
    {
        $tabla  = 'hoja_vida_evaluacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            ev.*
            FROM " . $tabla . " ev
            LEFT JOIN hoja_vida_evaluacion_preguntas hp ON hp.id = ev.pregunta
            WHERE ev.id_user = '" . $id . "' AND hp.tipo_pregunta = '" . $tipo . "' AND YEAR(ev.fechareg) = '" . $anio . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosEvaluacionModel($id, $tipo)
    {
        $tabla  = 'hoja_vida_evaluacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            he.id_hoja,
            he.id_user,
            COUNT(he.pregunta) AS cant_preguntas,
            SUM(he.puntuacion) AS suma_puntuacion,
            YEAR(he.fechareg) AS anio,
            CONCAT(u.nombre, ' ', u.apellido) AS evaluador,
            hp.tipo_pregunta
            FROM hoja_vida_evaluacion he
            LEFT JOIN usuarios u ON u.id_user = he.id_log
            LEFT JOIN hoja_vida_evaluacion_preguntas hp ON hp.id = he.pregunta
            WHERE he.id_user = '" . $id . "' AND hp.tipo_pregunta = '" . $tipo . "'
            GROUP BY he.id_user, he.id_hoja, he.id_log, hp.tipo_pregunta, YEAR(he.fechareg);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPuntajeEvaluacionAnioModel($id, $anio, $tipo)
    {
        $tabla  = 'hoja_vida_evaluacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            he.id_hoja,
            he.id_user,
            COUNT(he.pregunta) AS cant_preguntas,
            SUM(he.puntuacion) AS suma_puntuacion,
            YEAR(he.fechareg) AS anio,
            CONCAT(u.nombre, ' ', u.apellido) AS evaluador,
            hp.tipo_pregunta
            FROM hoja_vida_evaluacion he
            LEFT JOIN usuarios u ON u.id_user = he.id_log
            LEFT JOIN hoja_vida_evaluacion_preguntas hp ON hp.id = he.pregunta
            WHERE he.id_user = '" . $id . "' AND hp.tipo_pregunta = '" . $tipo . "' AND YEAR(he.fechareg) = '" . $anio . "'
            GROUP BY he.id_user, he.id_hoja, he.id_log, hp.tipo_pregunta, YEAR(he.fechareg);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAutorizacionPerfilModel($id, $anio, $perfil)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            COUNT(ht.id) AS cantidad_total_documentos,
            COUNT(CASE WHEN hd.autorizacion_uno = 1 THEN hd.id ELSE NULL END) AS cantidad_documentos_autorizados
            FROM hoja_vida_documentos hd
            LEFT JOIN hoja_vida_tipo_doc ht ON ht.id = hd.tipo_doc
            WHERE hd.id_user = '" . $id . "' AND ht.perfil_autoriza = '" . $perfil . "'
            GROUP BY ht.perfil_autoriza;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDetalleEvaluacionModel($id, $anio, $tipo)
    {
        $tabla  = 'hoja_vida_autorizacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            he.*,
            hp.pregunta AS nom_pregunta
            FROM hoja_vida_evaluacion he
            LEFT JOIN hoja_vida_evaluacion_preguntas hp ON hp.id = he.pregunta
            WHERE he.id_user = '" . $id . "' AND YEAR(he.fechareg) = '" . $anio . "' AND hp.tipo_pregunta = '" . $tipo . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
