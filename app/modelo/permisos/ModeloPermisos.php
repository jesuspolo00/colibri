<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermisos extends conexion
{

    public static function consultarPermisosPerfilModel($perfil, $opcion)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_perfil = :p AND activo = 1 AND id_opcion = :o ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':p', $perfil);
            $preparado->bindParam(':o', $opcion);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarOpcionesPermisosModel()
    {
        $tabla  = 'inv_opcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        op.*,
        IF(m.id = 2 , op.nombre, CONCAT(m.nombre,  ' - ', op.nombre)) AS opcion
        FROM inv_opcion op
        LEFT JOIN inv_modulo m ON m.id = op.id_modulo
        WHERE op.activo = 1 ORDER BY opcion ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function opcionsIdActivosPerfilModel($perfil, $id_opcion)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_perfil = :id AND id_opcion = :io AND activo = 1 ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $perfil);
            $preparado->bindParam(':io', $id_opcion);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarPermisoModel($datos)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_opcion, id_perfil, user_log) VALUES (:o, :p, :ul);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':o', $datos['opcion']);
            $preparado->bindValue(':p', $datos['perfil']);
            $preparado->bindParam(':ul', $datos['user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarPermisoModel($datos)
    {
        $tabla  = 'inv_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_opcion = :o AND id_perfil = :p;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':o', $datos['opcion']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    /*--------------------------*/

    public static function registrarPermisoLaboralModel($datos)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_user,
        sitio_trabajo,
        hora_inicio,
        hora_fin,
        motivo,
        horario,
        documento_soporte,
        id_log,
        fecha,
        token
        )
        VALUES
        (
        '" . $datos['id_user'] . "',
        '" . $datos['sitio'] . "',
        '" . $datos['hora_inicio'] . "',
        '" . $datos['hora_fin'] . "',
        '" . $datos['motivo'] . "',
        '" . $datos['horario'] . "',
        '" . $datos['documento'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['fecha'] . "',
        '" . $datos['token'] . "'
        );
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id     = $cnx->ultimoIngreso($tabla);
                $result = array('id' => $id, 'guardar' => true);
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosPermisoModel($id)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarTokenUsoModel($token)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE token = '" . $token . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarPermisoModel($id)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 2, token_usado = 1, id_usuario_estado = 3 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function denegarPermisoModel($id)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 3, token_usado = 1, id_usuario_estado = 3 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarListadoPermisosModel()
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        u.documento,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.correo,
        u.telefono
        FROM " . $tabla . " p
        LEFT JOIN usuarios u ON u.id_user = p.id_user
        ORDER BY p.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostraPermisosUsuariosModel($id)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        u.documento,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.correo,
        u.telefono
        FROM " . $tabla . " p
        LEFT JOIN usuarios u ON u.id_user = p.id_user
        WHERE p.id_user = :id
        ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosPermisoIdModel($id)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        u.documento,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.correo,
        u.telefono
        FROM " . $tabla . " p
        LEFT JOIN usuarios u ON u.id_user = p.id_user
        WHERE p.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarListadoTodosPermisosModel()
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        u.documento,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.correo,
        u.telefono
        FROM " . $tabla . " p
        LEFT JOIN usuarios u ON u.id_user = p.id_user
        ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarPermisosSolicitadosModel($datos)
    {
        $tabla  = 'permisos_laborales';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        u.documento,
        CONCAT(u.nombre, ' ', u.apellido) AS nom_user,
        u.correo,
        u.telefono
        FROM permisos_laborales p
        LEFT JOIN usuarios u ON u.id_user = p.id_user
        WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', p.hora_inicio, ' ', p.fecha, ' ', p.hora_fin) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['usuario'] . "
        " . $datos['fecha'] . "
        ORDER BY p.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
