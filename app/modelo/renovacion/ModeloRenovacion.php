<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloRenovacion extends conexion
{

    public static function agregarRenovacionModel($datos)
    {
        $tabla  = 'renovacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, fecha_inicio, fecha_fin, evidencia, id_log, observacion, archivo, numero) VALUES (:n, :fi, :ff, :ev, :idl, :ob, :arch, :nm)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':fi', $datos['fecha_inicio']);
            $preparado->bindParam(':ff', $datos['fecha_fin']);
            $preparado->bindParam(':ev', $datos['evidencia']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':arch', $datos['archivo']);
            $preparado->bindParam(':nm', $datos['numero']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimitesRenovacionesModel()
    {
        $tabla  = 'renovacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " ORDER BY fecha_fin DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarRenovacionesModel($datos)
    {
        $tabla  = 'renovacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT r.* FROM renovacion r WHERE CONCAT(r.nombre) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['anio'] . "
        ORDER BY r.fecha_fin DESC
        ;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAniosRenovacionModel()
    {
        $tabla  = 'renovacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        YEAR(fecha_inicio) AS anio
        FROM renovacion
        GROUP BY YEAR(fecha_inicio)
        UNION
        SELECT
        YEAR(fecha_fin) AS anio
        FROM renovacion
        GROUP BY YEAR(fecha_fin);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarRenovacionModel($datos)
    {
        $tabla  = 'renovacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        " . $tabla . "
        SET
        nombre = '" . $datos['nombre'] . "',
        fecha_inicio = '" . $datos['fecha_inicio'] . "',
        fecha_fin = '" . $datos['fecha_fin'] . "',
        numero = '" . $datos['numero'] . "',
        evidencia = '" . $datos['evidencia'] . "',
        archivo = '" . $datos['archivo'] . "',
        observacion = '" . $datos['observacion'] . "',
        id_edit = '" . $datos['id_log'] . "',
        fecha_edit = NOW()
        WHERE id = :id;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_renovacion']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
