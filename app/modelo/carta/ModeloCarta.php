<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloCarta extends conexion
{

    public static function registrarCartaEntregaModel($datos)
    {
        $tabla  = 'carta';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_user,
        fecha_entrega,
        observacion,
        id_log
        )
        VALUES
        (
        '" . $datos['id_log'] . "',
        '" . $datos['fecha'] . "',
        '" . $datos['observacion'] . "',
        '" . $datos['id_log'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id     = $cnx->ultimoIngreso($tabla);
                $result = array('guardar' => true, 'id' => $id);
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarCartaModel($datos)
    {
        $tabla  = 'carta';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET fecha_entrega = '" . $datos['fecha'] . "', observacion = '" . $datos['observacion'] . "', id_edit = " . $datos['id_log'] . ", fecha_edit = NOW() WHERE id = " . $datos['id_carta'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarArticulosControl($datos)
    {
        $tabla  = 'carta_articulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_carta,
        articulo,
        cantidad,
        id_log
        )
        VALUES
        (
        '" . $datos['id_carta'] . "',
        '" . $datos['producto'] . "',
        '" . $datos['cantidad'] . "',
        '" . $datos['id_log'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLimitesCartasModel()
    {
        $tabla  = 'carta';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        CONCAT(u.nombre,' ', u.apellido) AS nom_user
        FROM carta c
        LEFT JOIN usuarios u ON u.id_user = c.id_user
        WHERE activo = 1
        ORDER BY c.fecha_entrega DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInformacionCartaModel($id)
    {
        $tabla  = 'carta';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        CONCAT(u.nombre,' ', u.apellido) AS nom_user
        FROM carta c
        LEFT JOIN usuarios u ON u.id_user = c.id_user
        WHERE activo = 1 AND c.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosCartaModel($id)
    {
        $tabla  = 'carta_articulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*
        FROM " . $tabla . " c
        WHERE c.id_carta = :id ORDER BY c.id ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarCartasModel($datos)
    {
        $tabla  = 'carta_articulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        CONCAT(u.nombre,' ', u.apellido) AS nom_user
        FROM carta c
        LEFT JOIN usuarios u ON u.id_user = c.id_user
        WHERE activo = 1 AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', c.observacion, ' ', .c.id) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['fecha'] . "
        ORDER BY c.`fecha_entrega` DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerArticuloModel($id)
    {
        $tabla  = 'carta_articulos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_carta = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public static function eliminarCartaModel($id)
    {
        $tabla  = 'carta';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
